#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""Core functions and classes.
"""


__all__ = [
    "Pin",
    "Mass",
    "Resonator",
    "ConstantStrength",
    "Beam",
    "ElasticPlate",
    "DiffractionSimulation",
    "ScatteringSimulation",
    "BandsSimulation",
]


from abc import ABC, abstractmethod
from dataclasses import dataclass

from . import backend as bk
from .eig import gram_schmidt, nonlinear_eigensolver, null
from .special import (
    dgreens_function_dk,
    g1,
    grad_greens_function_cartesian,
    greens_function,
    greens_function_cartesian,
    radial_coordinates,
)

# from joblib import Memory
# memory = Memory("/tmp", verbose=0)
# self.build_matrix = memory.cache(self.build_matrix)

_BIG = 1e12


class Scatterer(ABC):
    """Base class representing a scatterer."""

    @abstractmethod
    def strength(self):
        pass

    @abstractmethod
    def dstrength_domega(self):
        pass


@dataclass
class Pin(Scatterer):
    """Class representing a pin.

    Parameters
    ----------
    position : tuple
        The position of the pin on the plate.

    """

    position: tuple  #: The position

    def strength(self, omega) -> float:
        """Strength function of frequency `omega`"""
        # t_alpha * D in [Torrent2013]
        return _BIG

    def dstrength_domega(self, omega) -> float:
        """Derivative of strength function w.r.t. `omega`"""
        return 0


@dataclass
class Mass(Scatterer):
    """Class representing a mass.

    Parameters
    ----------
    mass : float
        Value of the mass.
    position : tuple
        The position of the mass on the plate.

    """

    mass: float
    position: tuple

    def strength(self, omega) -> float:
        """Strength function of frequency `omega`"""
        # t_alpha * D in [Torrent2013]
        return self.mass * omega**2

    def dstrength_domega(self, omega) -> float:
        """Derivative of strength function w.r.t. `omega`"""
        return self.mass * omega * 2


@dataclass
class Resonator(Scatterer):
    """Class representing a resonator.

    Parameters
    ----------
    mass : float
        Value of the mass.
    stiffness : float
        Stiffness constant of the spring.
    position : tuple
        The position of the resonator on the plate.
    damping : float
        Damping coefficient (default is 0)

    """

    mass: float
    stiffness: float
    position: tuple
    damping: tuple = 0

    @property
    def omega_r(self) -> float:
        """Resonant frequency."""
        return (self.stiffness / self.mass) ** 0.5

    # def strength(self, omega) -> float:
    #     """Strength function of frequency `omega`"""
    #     # t_alpha * D in [Torrent2013]
    #     return self.mass * self.omega_r**2 * omega**2 / (self.omega_r**2 - omega**2)

    # def dstrength_domega(self, omega) -> float:
    #     """Derivative of strength function w.r.t. `omega`"""
    #     return (
    #         2 * self.mass * self.omega_r**4 * omega / (self.omega_r**2 - omega**2) ** 2
    #     )
    def strength(self, omega) -> float:
        """Strength function of frequency `omega`"""
        # t_alpha * D in [Torrent2013]
        return 1 / (
            1 / (self.mass * omega**2)
            - 1 / (self.stiffness - 1j * self.damping * omega)
        )

    def dstrength_domega(self, omega) -> float:
        """Derivative of strength function w.r.t. `omega`"""
        t = self.strength(omega)
        return t**2 * (
            2 / (self.mass * omega**3)
            + 1j * self.damping / (self.stiffness - 1j * self.damping) ** 2
        )


@dataclass
class ConstantStrength:
    """Class representing a scatterer with constant strength.

    Parameters
    ----------
    strength : float
        Value of the strength.
    position : tuple
        The position of the resonator on the plate.

    """

    impedance: float
    position: tuple

    # @property
    def strength(self, omega=0) -> float:
        # t_alpha * D in [Torrent2013]
        return self.impedance

    def dstrength_domega(self, omega=0) -> float:
        """Derivative of strength function w.r.t. `omega`"""
        return 0


@dataclass
class Beam:
    """Class representing a beam.

    Parameters
    ----------
    length : float
        Length of the beam.
    area : float
        Area of the cross section.
    rho : float
        Mass density.
    E : float
        Young's modulus
    position : tuple
        The position of the beam on the plate.

    """

    length: float
    area: float
    rho: float
    E: float
    position: tuple

    def strength(self, omega) -> float:
        """Strength function of frequency `omega`"""
        c = (self.E / self.rho) ** 0.5
        k = omega / c

        return self.rho * self.area * k * c**2 * bk.tan(bk.array(k * self.length))

    def dstrength_domega(self, omega) -> float:
        """Strength function of frequency `omega`"""
        c = (self.E / self.rho) ** 0.5
        k = omega / c
        kl = bk.array(k * self.length)

        return (
            self.rho * self.area * c**2 * (bk.tan(kl) + k * self.length / cos(kl) ** 2)
        )


@dataclass
class ElasticPlate:
    """Class representing a thin elastic plate.

    Parameters
    ----------
    h : float
        Thickness
    rho : float
        Mass density
    E : float
        Young's modulus
    nu: float
        Poisson ratio
    """

    h: float  #: thickness
    rho: float  #: mass density
    E: float  #: Young's modulus
    nu: float  #: Poisson ratio
    incoming: bool = False  #: Incoming wave?

    @property
    def bending_stiffness(self) -> float:
        r"""The bending stiffness of the plate defined
        as :math:`D = E h^3 /(12 (1-\nu^2))`
        """
        return self.E * self.h**3 / (12 * (1 - self.nu**2))

    @property
    def D(self) -> float:
        """Alias for :class:`ElasticPlate.bending_stiffness`"""
        return self.bending_stiffness

    def omega0(self, a) -> float:
        """Gives the resonant frequency of a plate with refrence length `a`."""

        return (self.bending_stiffness / (self.rho * a**2 * self.h)) ** 0.5

    def wavenumber(self, omega):
        """Gives the wavenumber of the bare plate as a function of frequency `omega`

        Parameters
        ----------
        omega : float
            Frequency

        """
        return omega**0.5 * (self.rho * self.h / self.bending_stiffness) ** 0.25

    def dwavenumber_domega(self, omega):
        """Gives the derivative of the wavenumber with respect to the frequency `omega`

        Parameters
        ----------
        omega : float
            Frequency

        """
        k = self.wavenumber(omega)
        return omega / (2 * k**3) * (self.rho * self.h / self.bending_stiffness)

    def frequency(self, k):
        """Gives the frequency of the bare plate as a function of wavenumber `k`

        Parameters
        ----------
        k : float
            Wavenumber

        """
        return k**2 / (self.rho * self.h / self.bending_stiffness) ** 0.5

    def greens_function_cartesian(self, omega, x, y):
        """Green's function of the bare plate in cartesian coordinates.

        Parameters
        ----------
        omega : array
            frequency
        x : array
            x coordinates
        y : array
            y coordinates


        Returns
        -------
        array
            Green's function
        """
        # if self.incoming:
        #     omega = omega.conj()
        return greens_function_cartesian(self.wavenumber(omega), x, y)

    def greens_function(self, omega, r):
        """Green's function of the bare plate in radial coordinates.

        Parameters
        ----------
        omega : array
            frequency
        r : array
            radial coordinate


        Returns
        -------
        array
            Green's function
        """
        # if self.incoming:
        #     omega = omega.conj()
        return greens_function(self.wavenumber(omega), r)

    def dgreens_function_domega(self, omega, r):
        """Green's function derivative wrt omega.

        Parameters
        ----------
        omega : array
            frequency
        r : array
            radial coordinate


        Returns
        -------
        array
            Green's function
        """
        # if self.incoming:
        #     omega = omega.conj()
        return dgreens_function_dk(self.wavenumber(omega), r) * self.dwavenumber_domega(
            omega
        )


class _Simulation:
    def __init__(self, plate, res_array):
        self.plate = plate
        self.res_array = res_array

    @property
    def n_res(self):
        """Number of resonators.

        Returns
        -------
        int
            Number of resonators.
        """
        return len(self.res_array)

    def wavenumber(self, omega):
        """Wavenumber of the plate as a function of frequency omega.

        Parameters
        ----------
        omega : array
            frequency

        Returns
        -------
        array
            Wavenumber
        """
        return self.plate.wavenumber(omega)

    def dwavenumber_domega(self, omega):
        return self.plate.dwavenumber_domega(omega)

    def _strength(self, omega, resonator):
        # T_alpha in [Torrent2013]
        """
        Strength function of a resonator.

        Parameters
        ----------
        omega : array
            Frequency
        resonator : Resonator
            The resonator

        Returns
        -------
        array
            The strength function
        """
        k = self.plate.wavenumber(omega)
        # if isinstance(resonator, Pin):
        #     return 1j * 8 * k**2
        t_alpha = resonator.strength(omega) / self.plate.bending_stiffness
        return t_alpha / (1 - 1j * t_alpha / (8 * k**2))

    def _dstrength_domega(self, omega, resonator):
        k = self.plate.wavenumber(omega)
        dk = self.plate.dwavenumber_domega(omega)
        t_alpha = resonator.strength(omega) / self.plate.bending_stiffness
        dt_alpha_domega = (
            resonator.dstrength_domega(omega) / self.plate.bending_stiffness
        )
        Q = t_alpha / (8 * k**2)
        dQ = dt_alpha_domega / (8 * k**2)
        return 16 * k * (k / 2 * dQ - 1j * dk * Q**2) / (1 - 1j * Q) ** 2

    def plane_wave(self, x, y, omega, angle):
        """A plane wave

        Parameters
        ----------
        x : array
            x coordinates
        y : array
            y coordinates
        omega : float
            Frequency
        angle : float
            Angle in radians

        Returns
        -------
        array
            The plane wave field
        """
        k = self.plate.wavenumber(omega)
        angle = bk.array(angle)
        return bk.exp(1j * k * (bk.cos(angle) * x + bk.sin(angle) * y))

    def point_source(self, x, y, omega, position):
        """A point source.

        Parameters
        ----------
        x : array
            x coordinates
        y : array
            y coordinates
        omega : float
            Frequency
        position : tuple
            Position of the source.


        Returns
        -------
        array
            The field from a line source
        """
        xs, ys = position
        return self.plate.greens_function_cartesian(omega, x - xs, y - ys)


class ScatteringSimulation(_Simulation):
    """Class to run a scattering simulation.

    Parameters
    ----------
    plate : :class:`ElasticPlate`
        The plate
    res_array : array of :class:`Pin`, :class:`Mass` or :class:`Resonator`
        An array containing the scatterers, it can be a mixture of pins, masses or resonators.
    alternative : bool
        Use an alternative formulation to solve for the scattering coefficients.

    """

    def __init__(self, plate, res_array, alternative=False):
        super().__init__(plate, res_array)

        self.alternative = alternative

        dists = bk.array(bk.zeros((self.n_res, self.n_res), dtype=bk.float64))
        for alpha, res_alpha in enumerate(self.res_array):
            xalpha, yalpha = res_alpha.position
            for beta in range(alpha, self.n_res):
                res_beta = res_array[beta]
                xbeta, ybeta = res_beta.position
                dists[alpha, beta] = dists[beta, alpha] = radial_coordinates(
                    xalpha - xbeta, yalpha - ybeta
                )

        self._dr = dists

    def build_rhs(self, phi0, omega, *args, **kwargs):
        """
        Build the right-hand side vector.

        Parameters
        ----------
        phi0 : callable
            The incident field. Signature should be ``phi0(x, y, omega, *args, **kwargs)``.
        omega : float
            Frequency
        *args : tuple
            Additional arguments for `phi0`
        **kwargs : dict
            Additional keyword arguments for `phi0`

        Returns
        -------
        array
            The right-hand side vector
        """
        omega = bk.array(omega)
        psi_inc = bk.array(bk.zeros((self.n_res, *omega.shape), dtype=bk.complex128))
        for alpha, res_alpha in enumerate(self.res_array):
            xalpha, yalpha = res_alpha.position
            psi_inc[alpha] = phi0(xalpha, yalpha, omega, *args, **kwargs)

        return psi_inc

    def build_matrix(self, omega):
        """
        Build the matrix M.

        Parameters
        ----------
        omega : float
            Frequency

        Returns
        -------
        array
            The matrix M
        """
        omega = bk.array(omega)
        matrix = bk.array(
            bk.zeros((self.n_res, self.n_res, *omega.shape), dtype=bk.complex128)
        )
        for alpha, res_alpha in enumerate(self.res_array):
            for beta in range(alpha, self.n_res):
                res_beta = self.res_array[beta]
                delta = 1 if alpha == beta else 0
                dr = self._dr[alpha, beta]
                G0 = self.plate.greens_function(omega, dr)
                if self.alternative:
                    t_beta = res_beta.strength(omega) / self.plate.D
                    mat = delta / t_beta - G0
                else:
                    T_beta = self._strength(omega, res_beta)
                    mat = delta - (1 - delta) * T_beta * G0
                matrix[alpha, beta] = matrix[beta, alpha] = mat
        return matrix

    def build_matrix_derivative(self, omega):
        """
        Build the derivative of the matrix M with respect to omega.

        Parameters
        ----------
        omega : float
            Frequency

        Returns
        -------
        array
            The derivative of the matrix M with respect to omega
        """
        omega = bk.array(omega)
        matrix = bk.array(
            bk.zeros((self.n_res, self.n_res, *omega.shape), dtype=bk.complex128)
        )
        for alpha, res_alpha in enumerate(self.res_array):
            for beta in range(alpha, self.n_res):
                res_beta = self.res_array[beta]
                delta = 1 if alpha == beta else 0
                dr = self._dr[alpha, beta]
                dG0 = self.plate.dgreens_function_domega(omega, dr)
                if self.alternative:
                    t_beta = res_beta.strength(omega) / self.plate.D
                    dt_beta = res_beta.dstrength_domega(omega) / self.plate.D
                    mat = -dt_beta / t_beta**2 * delta - dG0
                else:
                    dT_beta = self._dstrength_domega(omega, res_beta)
                    G0 = self.plate.greens_function(omega, dr)
                    T_beta = self._strength(omega, res_beta)
                    mat = -(1 - delta) * (dT_beta * G0 + T_beta * dG0)
                matrix[alpha, beta] = matrix[beta, alpha] = mat
        return matrix

    # def get_eigenvector(self, omega):
    #     M = self.build_matrix(omega)
    #     return null(M)

    def solve(self, incident, omega, *args, **kwargs):
        """Solve the multiple scattering problem

        Parameters
        ----------
        incident : callable
            The incident field. Signature should be ``incident(x, y, omega, *args, **kwargs)``.
        omega: float
            Frequency
        Returns
        -------
        array
            The solution vector
        """
        omega = bk.array(omega)
        matrix = self.build_matrix(omega)
        rhs = self.build_rhs(incident, omega, *args, **kwargs)
        if omega.shape != ():
            matrix = matrix.T
            rhs = bk.stack([rhs]).T
            sol = bk.linalg.solve(matrix, rhs)[..., 0]
        else:
            sol = bk.linalg.solve(matrix, rhs)
        return sol

    def _get_field(
        self, x, y, incident, kernel, solution_vector, omega, *args, **kwargs
    ):
        if incident is None:
            W = 0
        else:
            W = incident(x, y, omega, *args, **kwargs)
        for alpha, res_alpha in enumerate(self.res_array):
            xalpha, yalpha = res_alpha.position
            G0 = kernel(omega, x - xalpha, y - yalpha)
            coeff = 1 if self.alternative else self._strength(omega, res_alpha)
            W += coeff * solution_vector[alpha] * G0
        return W

    def get_field(self, x, y, incident, solution_vector, omega, *args, **kwargs):
        """Get the total field.

        Parameters
        ----------
        x : array
            x coordinates
        y : array
            y coordinates
        incident : callable
            The incident field. Signature should be ``incident(x, y, omega, *args, **kwargs)``.
        solution_vector: array
            The solution vector as returned by :class:`ScatteringSimulation.solve`
        omega: float or complex
            Frequency

        Returns
        -------
        array
            The total field
        """
        return self._get_field(
            x,
            y,
            incident,
            self.plate.greens_function_cartesian,
            solution_vector,
            omega,
            *args,
            **kwargs
        )

    def get_scattered_field(self, x, y, solution_vector, omega):
        """Get the scattered field.

        Parameters
        ----------
        x : array
            x coordinates
        y : array
            y coordinates
        solution_vector: array
            The solution vector as returned by :class:`ScatteringSimulation.solve`
        omega: float or complex
            Frequency


        Returns
        -------
        array
            The scattered field
        """
        return self._get_field(
            x, y, None, self.plate.greens_function_cartesian, solution_vector, omega
        )

    def eigensolve(self, *args, **kwargs):
        """
        Solve the eigenvalue problem using a non-linear eigensolver.

        Parameters
        ----------
        omega0 : float
            Lower bound of the frequency range
        omega1 : float
            Upper bound of the frequency range
        *args :
            Additional arguments for :func:`nonlinear_eigensolver`
        **kwargs :
            Additional keyword arguments for :func:`nonlinear_eigensolver`

        Returns
        -------
        array
            Eigenvalues
        array
            Eigenvectors
        array
            Left eigenvectors (if `return_left` is `True`)
        """
        return nonlinear_eigensolver(self, *args, **kwargs)

    def get_mode(self, x, y, eigenvector, eigenvalue):
        """
        Get the scattered field corresponding to a given eigenmode.

        Parameters
        ----------
        x : array
            x coordinates
        y : array
            y coordinates
        eigenvector : array
            The eigenvector as returned by :func:`eigensolve`
        eigenvalue : float or complex
            The eigenvalue as returned by :func:`eigensolve`

        Returns
        -------
        array
            The scattered field
        """
        return self.get_scattered_field(x, y, eigenvector, eigenvalue)

    def get_strength_poles(self):
        """
        Compute the poles of the strength function of scatterers.

        Returns
        -------
        array
            The strength poles
        """
        singularity_Talpha = []
        plate = self.plate
        for res in self.res_array:
            q = (
                1
                / 16
                * (res.mass * res.stiffness / (plate.rho * plate.h * plate.D)) ** 0.5
                + 0j
            )
            pole = res.omega_r * (-1j * q + (1 - q**2 + 0j) ** 0.5)
            singularity_Talpha.append(pole)
            pole = res.omega_r * (-1j * q - (1 - q**2) ** 0.5)
            singularity_Talpha.append(pole)
        return bk.array(singularity_Talpha)

    def _convert_phi_alternative(self, phi_n, omega):
        """
        Convert scattering coefficients from the second definition of the M matrix
        to the first definition.

        Parameters
        ----------
        phi_n : array
            The scattering coefficients at the position of the scatterers by using
            the second definition of the M matrix
        omega : float or array
            The frequency

        Returns
        -------
        array
            The scattering coefficients at the position of the scatterers by using
            the first definition of the M matrix
        """
        if self.alternative:
            return phi_n
        return (
            phi_n * bk.stack([self._strength(omega, res) for res in self.res_array]).T
        )

    def get_far_field_radiation(self, phi_n, omegas, angles):
        """
        Compute the far field radiation pattern.

        Equation (12) in :cite:p:`marti2023bound`

        Parameters
        ----------
        phi_n : array
            The scattering coefficients at the position of the scatterers by using
            the second definition of the M matrix
        omegas : array
            The frequencies
        angles: array
            Far field angles in radian

        Returns
        -------
        F : array
             The far field radiation pattern
        """
        phi_n = bk.array(phi_n)
        omegas = bk.array(omegas)
        angles = bk.array(angles)
        Nomega = len(omegas)
        Nangles = len(angles)
        phi_n = bk.broadcast_to(phi_n, (Nomega, self.n_res))
        phi_n = self._convert_phi_alternative(phi_n, omegas)
        omegas = bk.broadcast_to(omegas, (Nangles, Nomega))
        angles = bk.broadcast_to(angles, (Nomega, Nangles)).T

        k = self.plate.wavenumber(omegas)

        F = 0
        for alpha, res_alpha in enumerate(self.res_array):
            xalpha, yalpha = res_alpha.position
            angle_alpha = bk.arctan2(yalpha, xalpha)
            ralpha = radial_coordinates(xalpha, yalpha)
            F += phi_n[:, alpha] * bk.exp(
                -1j * k * ralpha * bk.cos(angles - angle_alpha)
            )
        return F

    def get_scattering_cross_section(self, phi_n, omegas, Nangles=360):
        """Compute the scattering cross section of the cluster in the farfield.

        The scattering cross section is computed by integrating the
        squared absolute value of the far-field radiation pattern over all
        angles in the far field.

        Parameters
        ----------
        phi_n : array
            The scattering coefficients at the position of the scatterers by using
            the second definition of the M matrix
        omegas : array-like
            The frequencies
        Nangles : int
            Number of far field angles (from `0` to `2\\pi`). Default is 360.

        Returns
        -------
        sigma_sc : float
             Scattering cross section

        Notes
        -----
        This function uses the formula given in equation (A6b) of
        :cite:p:`packo2021metaclusters`.
        """
        k = self.plate.wavenumber(omegas)
        angles = bk.linspace(0, 2 * bk.pi, Nangles)
        F = self.get_far_field_radiation(phi_n, omegas, angles)
        integral = bk.trapezoid(bk.abs(F.T) ** 2, angles)
        return integral / (16 * bk.pi * k**2)

    def get_extinction_cross_section(self, phi_n, omegas, incident_angle):
        """Compute the extinction cross section of the cluster in the farfield

        Equation (A6a) in :cite:p:`packo2021metaclusters`

        Parameters
        ----------
        phi_n : array
            The scattering coefficients at the position of the scatterers by using
            the second definition of the M matrix
        omegas : array
            The frequencies
        incident_angle : float
            The incident angle

        Returns
        -------
        sigma_ext : float
             Extinction cross section
        """
        F = self.get_far_field_radiation(phi_n, omegas, [incident_angle])
        return bk.imag(F[0])

    def get_absorption_cross_section(self, phi_n, omegas, sum=True):
        """Compute the absorption cross section of the cluster in the farfield.

        Equation (A6c) in :cite:p:`packo2021metaclusters`

        Parameters
        ----------
        phi_n : array
            The scattering coefficients at the position of the scatterers by using
            the second definition of the M matrix
        omegas : float or array
            The frequency
        sum: bool
            Wether to return the sum of all resonator contributions

        Returns
        -------
        sigma_abs : float or array
             Absorption cross section (total if sum=True, else array with value for each resonator)
        """
        phi_n = self._convert_phi_alternative(phi_n, omegas)
        impedances = bk.stack([res.strength(omegas) for res in self.res_array])
        sigma_abs = -bk.imag(1 / impedances.T) * bk.abs(phi_n) ** 2
        if sum:
            return bk.sum(sigma_abs, axis=1)
        return sigma_abs

    # def get_energy_efficiency(self, phi_n, omega):
    #     """
    #     Equation (17) in :cite:p:`packo2021metaclusters`
    #     """
    #     sigma_sc = self.get_scattering_cross_section(self, phi_n, omega)
    #     sigma_abs = self.get_extintion_cross_section(self, phi_n, omega)
    #     return sigma_sc / sigma_abs

    def get_multiplicities(self, eigenvalues, M=None, tol=1e-6):
        """
        Compute the multiplicity of each eigenvalue.

        Parameters
        ----------
        eigenvalues : array
            The eigenvalues
        M : array, optional
            The matrix at each eigenvalue. If None, it is computed.
        tol : float
            The tolerance for the rank computation

        Returns
        -------
        multiplicities : array
            The multiplicity of each eigenvalue
        """
        if M is None:
            M = [self.build_matrix(omega) for omega in eigenvalues]
        multiplicities = [self.n_res - bk.linalg.matrix_rank(m, tol=tol) for m in M]
        return multiplicities

    def normalize_eigenvectors(self, eigenvalues, eigenvectors, dM=None):
        """
        Normalize the eigenvectors such that they satisfy the orthonormality
        condition with respect to the inner product defined by the derivative
        of the matrix M.

        Parameters
        ----------
        eigenvalues : array
            The eigenvalues
        eigenvectors : array
            The eigenvectors
        dM : array, optional
            The derivative of the matrix M at each eigenvalue. If None, it is
            computed.

        Returns
        -------
        eigenvectors_norm : array
            The normalized eigenvectors
        """
        if dM is None:
            dM = [self.build_matrix_derivative(omega) for omega in eigenvalues]
        eigenvectors_norm = bk.zeros_like(eigenvectors)
        for n, _dM in enumerate(dM):
            phin = eigenvectors[:, n]
            eigenvectors_norm[:, n] = phin / (phin @ (_dM @ phin)) ** 0.5

        return eigenvectors_norm

    def update_eig(self, evs, eigenvectors, multiplicities, M, dM):
        """
        Update the eigenvectors and eigenvalues, given their multiplicities and
        the matrices M and dM.

        Parameters
        ----------
        evs : array
            The eigenvalues
        eigenvectors : array
            The eigenvectors
        multiplicities : array
            The multiplicity of each eigenvalue
        M : array
            The matrix M at each eigenvalue
        dM : array
            The derivative of the matrix M at each eigenvalue

        Returns
        -------
        eigvals : array
            The updated eigenvalues
        eigenvectors : array
            The updated eigenvectors
        """
        Nmodes = len(evs)
        eigvals = []
        eigmodes = []
        for i in range(Nmodes):
            mult = multiplicities[i]
            if mult > 1:
                vs = null(M[i], mult=mult)
                dMi = bk.stack([dM[i]] * mult)
                vs = gram_schmidt(vs, dMi)
                for jm in range(mult):
                    eigvals.append(evs[i])
                    eigmodes.append(vs[:, jm])
            else:
                eigvals.append(evs[i])
                eigmodes.append(eigenvectors[:, i])

        eigenvectors = bk.stack(eigmodes).T
        evs = bk.array(eigvals)
        return evs, eigenvectors

    def get_expansion_coefficients(
        self, eigenvalues, eigenvectors, omegas, incident, param_inc, arbitrary=None
    ):
        """
        Compute the expansion coefficients for a given incident field.

        Parameters
        ----------
        eigenvalues : array
            The eigenvalues
        eigenvectors : array
            The eigenvectors
        omegas : array
            The frequencies
        incident : callable
            The incident field
        param_inc : dict
            The parameters for the incident field
        arbitrary : callable, optional
            A function to scale the expansion coefficients. If None, it is set to
            a function that returns 1.

        Returns
        -------
        array
            The expansion coefficients
        """
        if arbitrary == None:

            def arbitrary(omega):
                return 1

        Nmodes = len(eigenvalues)
        nfreq = len(omegas)
        bs = bk.zeros((nfreq, Nmodes), dtype=bk.complex128)
        for i, omega in enumerate(omegas):
            psi_inc = self.build_rhs(incident, omega, param_inc)
            for n in range(Nmodes):
                omegan = eigenvalues[n]
                num = psi_inc @ eigenvectors[:, n]
                bs[i, n] = num / (omega - omegan) * arbitrary(omegan) / arbitrary(omega)
        return bs

    def get_expansion_coefficients_adjoint(
        self,
        eigenvalues,
        eigenvectors,
        omegas,
        incident,
        param_inc,
        phi,
        param,
        ires,
        ipos=0,
        arbitrary=None,
    ):
        """
        Compute the expansion coefficients of the adjoint for a given incident field.

        Parameters
        ----------
        eigenvalues : array
            The eigenvalues
        eigenvectors : array
            The eigenvectors
        omegas : array
            The frequencies
        phi : array
            The solution vector
        param : str
            The parameter to derivate ("mass", "stiffness" or "pos")
        ires : int
            The index of the resonator
        ipos : int
            The index of position (0 for x and 1 for y)
        arbitrary : callable, optional
            A function to scale the expansion coefficients. If None, it is set to
            a function that returns 1.

        Returns
        -------
        array
            The adjoint expansion coefficients
        """
        if arbitrary == None:

            def arbitrary(omega):
                return 1

        Nmodes = len(eigenvalues)
        nfreq = len(omegas)
        cs = bk.zeros((nfreq, Nmodes), dtype=bk.complex128)
        for i, omega in enumerate(omegas):
            dM = self.build_dmatrix_dparam(omega, ires, param, ipos)
            psi_inc = (
                self.build_dincident_dposition_rhs(
                    omega, incident, param_inc, ires, param, ipos
                )
                - dM @ phi[i]
            )
            for n in range(Nmodes):
                omegan = eigenvalues[n]
                num = psi_inc @ eigenvectors[:, n]
                cs[i, n] = num / (omega - omegan) * arbitrary(omegan) / arbitrary(omega)
        return cs

    def sensitivity_mass(self, omega_n, phi_n, ires):
        """
        Compute the sensitivity of an eigenvalue with respect to the mass of
        resonator number `ires`.

        Parameters
        ----------
        omega_n : float
            Eigenfrequency
        phi_n : array
            Eigenmode
        ires : int
            The index of the resonator

        Returns
        -------
        float
            The sensitivity of the eigenfrequency with respect to the mass of
            resonator number `ires`
        """
        return phi_n[ires] * (
            self.plate.D / (self.res_array[ires].mass ** 2 * omega_n**2) * phi_n[ires]
        )

    def sensitivity_stiffness(self, omega_n, phi_n, ires):
        """
        Compute the sensitivity of an eigenvalue with respect to the
        stiffness of resonator number `ires`.

        Parameters
        ----------
        omega_n : float
            Eigenfrequency
        phi_n : array
            Eigenmode
        ires : int
            The index of the resonator

        Returns
        -------
        float
            The sensitivity of the eigenfrequency with respect to the
            stiffness of resonator number `ires`
        """
        return -phi_n[ires] * (
            self.plate.D / self.res_array[ires].stiffness ** 2 * phi_n[ires]
        )

    def sensitivity_position(self, omega_n, phi_n, ires, ipos):
        """
        Compute the sensitivity of of an eigenvalue with respect to the
        position of resonator number `ires`.

        Parameters
        ----------
        omega_n : float
            Eigenfrequency
        phi_n : array
            Eigenmode
        ires : int
            The index of the resonator
        ipos : int
            The component of the position to vary (0 for x, 1 for y)

        Returns
        -------
        float
            The sensitivity of the eigenfrequency with respect to the
            position of resonator number `ires`
        """
        omega_n = bk.array(omega_n)
        dMdp = bk.array(
            bk.zeros((2, self.n_res, self.n_res, *omega_n.shape), dtype=bk.complex128)
        )
        k_n = self.wavenumber(omega_n)
        for alpha in range(self.n_res):
            for beta in range(alpha, self.n_res):
                if (alpha == ires or beta == ires) and alpha != beta:
                    xa, ya = self.res_array[alpha].position
                    xb, yb = self.res_array[beta].position
                    rab = radial_coordinates(xa - xb, ya - yb)
                    G1 = g1(k_n, rab)
                    sgn = 1 if alpha == ires else -1
                    dMdp[0, alpha, beta] = dMdp[0, beta, alpha] = (
                        sgn * (xa - xb) * k_n * G1 / rab
                    )
                    dMdp[1, alpha, beta] = dMdp[1, beta, alpha] = (
                        sgn * (ya - yb) * k_n * G1 / rab
                    )

        return -phi_n @ (dMdp[ipos] @ phi_n)

    def dmatrix_dmass(self, omega, ires):
        dMdp = bk.zeros((self.n_res, self.n_res), dtype=bk.complex128)
        dMdp[ires, ires] = -self.plate.D / (self.res_array[ires].mass ** 2 * omega**2)
        return dMdp

    def dmatrix_dstiffness(self, omega, ires):
        dMdp = bk.zeros((self.n_res, self.n_res), dtype=bk.complex128)
        dMdp[ires, ires] = self.plate.D / self.res_array[ires].stiffness ** 2
        return dMdp

    def dmatrix_dpos(self, omega, ires, ipos):
        dMdp = bk.array(bk.zeros((2, self.n_res, self.n_res), dtype=bk.complex128))
        k = self.wavenumber(omega)
        for alpha in range(self.n_res):
            for beta in range(alpha, self.n_res):
                if (alpha == ires or beta == ires) and alpha != beta:
                    xa, ya = self.res_array[alpha].position
                    xb, yb = self.res_array[beta].position
                    rab = radial_coordinates(xa - xb, ya - yb)
                    G1 = g1(k, rab)
                    sgn = 1 if alpha == ires else -1
                    dMdp[0, alpha, beta] = dMdp[0, beta, alpha] = (
                        sgn * (xa - xb) * k * G1 / rab
                    )
                    dMdp[1, alpha, beta] = dMdp[1, beta, alpha] = (
                        sgn * (ya - yb) * k * G1 / rab
                    )
        return dMdp[ipos]

    def build_dmatrix_dparam(self, omega, ires, param, ipos):
        if param == "mass":
            return self.dmatrix_dmass(omega, ires)
        elif param == "stiffness":
            return self.dmatrix_dstiffness(omega, ires)
        else:
            return self.dmatrix_dpos(omega, ires, ipos)

    # def sensitivity_eigenvector_mass(self, omega_n, phi_n, ires, M=None, dM=None):
    #     """
    #     Compute the sensitivity of an eigenvector with respect to the mass of
    #     resonator number `ires`.

    #     Parameters
    #     ----------
    #     omega_n : float
    #         Eigenfrequency
    #     phi_n : array
    #         Eigenmode
    #     ires : int
    #         The index of the resonator
    #     M : array
    #         The matrix at omega_n
    #     dM : array
    #         The matrix derivative wrt omega at omega_n

    #     Returns
    #     -------
    #     float
    #         The sensitivity of the eigenvector with respect to the mass of
    #         resonator number `ires`
    #     """
    #     if M is None:
    #         M = self.build_matrix(omega_n)
    #     if dM is None:
    #         dM = self.build_matrix_derivative(omega_n)
    #     sens = self.sensitivity_mass(omega_n, phi_n, ires)
    #     dMdp = bk.zeros((self.n_res, self.n_res), dtype=bk.complex128)
    #     dMdp[ires, ires] = self.plate.D / (self.res_array[ires].mass ** 2 * omega_n**2)
    #     # F = bk.zeros((self.n_res, self.n_res), dtype=bk.complex128)
    #     # for i1 in range(self.n_res):
    #     #     for j1 in range(self.n_res):
    #     #         F[i1, j1] = (
    #     #             omega_n
    #     #         )
    #     rhs = -(dMdp + sens * dM) @ phi_n
    #     return bk.linalg.solve(M, rhs)

    # def sensitivity_eigenvector_position(
    #     self, omega_n, phi_n, ires, ipos, M=None, dM=None
    # ):
    #     omega_n = bk.array(omega_n)
    #     if M is None:
    #         M = self.build_matrix(omega_n)
    #     if dM is None:
    #         dM = self.build_matrix_derivative(omega_n)
    #     dMdp = bk.array(
    #         bk.zeros((2, self.n_res, self.n_res, *omega_n.shape), dtype=bk.complex128)
    #     )
    #     k_n = self.wavenumber(omega_n)
    #     for alpha in range(self.n_res):
    #         for beta in range(alpha, self.n_res):
    #             if (alpha == ires or beta == ires) and alpha != beta:
    #                 xa, ya = self.res_array[alpha].position
    #                 xb, yb = self.res_array[beta].position
    #                 rab = radial_coordinates(xa - xb, ya - yb)
    #                 G1 = g1(
    #                     k_n,
    #                     rab,
    #                 )
    #                 sgn = 1 if alpha == ires else -1
    #                 dMdp[0, alpha, beta] = dMdp[0, beta, alpha] = (
    #                     sgn * (xa - xb) * k_n * G1 / rab
    #                 )
    #                 dMdp[1, alpha, beta] = dMdp[1, beta, alpha] = (
    #                     sgn * (ya - yb) * k_n * G1 / rab
    #                 )
    #     devdp = self.sensitivity_position(omega_n, phi_n, ires, ipos)

    #     return -bk.multiply(bk.linalg.inv(M), (dMdp[ipos] + dM * devdp)) @ phi_n

    def sensitivity_bn_position(
        self,
        omega_n,
        phi_n,
        ires,
        ipos,
        omegas,
        incident,
        param_inc,
        M=None,
        dM=None,
        arbitrary=None,
        darbitrarydomega=None,
    ):

        omega_n = bk.array(omega_n)
        if M is None:
            M = [self.build_matrix(omega) for omega in omega_n]
        if dM is None:
            dM = [self.build_matrix_derivative(omega) for omega in omega_n]

        if arbitrary == None:

            def arbitrary(omega):
                return 1

        if darbitrarydomega == None:

            def darbitrarydomega(omega):
                return 0

        omegas = [omegas]
        Nmodes = len(omega_n)
        nfreq = len(omegas)
        Nres = self.n_res
        dbsdp = bk.zeros((nfreq, Nmodes), dtype=bk.complex128)
        for i, omega in enumerate(omegas):
            psi_inc = self.build_rhs(incident, omega, param_inc)
            for n in range(Nmodes):
                omegan = omega_n[n]
                num = psi_inc.T @ phi_n[:, n]
                dnum = psi_inc.T @ self.sensitivity_eigenvector_position(
                    omegan, phi_n[:, n], ires, ipos, M[n], dM[n]
                )
                first_term = (
                    num
                    / (omega - omegan)
                    * darbitrarydomega(omegan)
                    * self.sensitivity_position(omegan, phi_n[:, n], ires, ipos)
                    / arbitrary(omega)
                )
                second_term = (
                    -num
                    / (
                        (omega - omegan) ** 2
                        * self.sensitivity_position(omegan, phi_n[:, n], ires, ipos)
                    )
                    * arbitrary(omegan)
                    / arbitrary(omega)
                )
                third_term = (
                    dnum / (omega - omegan) * arbitrary(omegan) / arbitrary(omega)
                )
                dbsdp[i, n] = first_term + second_term + third_term
        return dbsdp

    def dpoint_source_dposition_rhs(self, omega, position, ires):
        k = self.wavenumber(omega)
        xs, ys = position
        xr, yr = self.res_array[ires].position
        return grad_greens_function_cartesian(k, xr - xs, yr - ys)

    def dplane_wave_dposition_rhs(self, omega, angle, ires):
        angle = bk.array(angle)
        k = self.wavenumber(omega)
        xr, yr = self.res_array[ires].position
        pw = self.plane_wave(xr, yr, omega, angle)
        kx, ky = k * bk.cos(angle), k * bk.sin(angle)
        return 1j * bk.vstack([kx * pw, ky * pw])

    def build_dincident_dposition_rhs(
        self, omega, incident, param_inc, ires, param, ipos
    ):
        out = bk.zeros(self.n_res, dtype=bk.complex128)
        if param == "pos":
            name = getattr(incident, "__name__", str(incident))
            if name == "plane_wave":
                func = self.dplane_wave_dposition_rhs
            elif name == "point_source":
                func = self.dpoint_source_dposition_rhs
            else:
                raise ValueError("incident must be plane_wave or point_source")
            out[ires] = func(omega, param_inc, ires)[ipos]
        return out


class BandsSimulation(_Simulation):
    """Class to compute phononic bands.

    Parameters
    ----------
    plate : :class:`ElasticPlate`
        The plate
    res_array : list
        Array of scatterers. This can be :class:`Pin`, :class:`Mass` or :class:`Resonator` or a mix of those.
    lattice_vectors : tuple of tuples
        The two vectors `(v1x, v1y), (v2x, v2y)` defining the lattice.

    """

    def __init__(self, plate, res_array, lattice_vectors):
        super().__init__(plate, res_array)

        self.lattice_vectors = bk.array(
            lattice_vectors, dtype=bk.float64
        )  #: The lattice vectors
        self.reciprocal = 2 * bk.pi * bk.linalg.inv(self.lattice_vectors).T

        self.a = min([bk.linalg.norm(v) for v in self.lattice_vectors])
        self.omega0 = plate.omega0(self.a)
        v0, v1 = self.lattice_vectors
        self.area = bk.abs(v0[0] * v1[1] - v0[1] * v1[0])

    def _reciprocal_sum(self, Omega, K, alpha, beta, M):
        out = 0
        for m in range(-M, M + 1):
            for n in range(-M, M + 1):
                G = m * self.reciprocal[0] + n * self.reciprocal[1]
                Rab = bk.array(self.res_array[beta].position) - bk.array(
                    self.res_array[alpha].position
                )
                out += bk.exp(-1j * G @ Rab) / (
                    bk.linalg.norm(K + G) ** 4 * self.a**4 - Omega**2 * self.a**2
                )
        return out

    def _gamma(self, beta):
        return self.res_array[beta].mass / (self.plate.rho * self.plate.h * self.area)

    def _matrix_element(self, Omega, K, alpha, beta, M):
        S = self._reciprocal_sum(Omega, K, alpha, beta, M)
        Omega_beta = self.res_array[beta].omega_r / self.omega0
        return (
            self._gamma(beta) * Omega**2 * self.a**2 / (1 - Omega**2 / Omega_beta**2)
        ) * S

    def _build_matrix(self, Omega, K, M):
        matrix = bk.array(bk.zeros((self.n_res, self.n_res), dtype=bk.complex128))
        for alpha in range(self.n_res):
            for beta in range(self.n_res):
                matrix[alpha, beta] = self._matrix_element(Omega, K, alpha, beta, M)
        return bk.eye(self.n_res) - matrix

    def _getP(self, alpha, M):
        P = []
        for m in range(-M, M + 1):
            for n in range(-M, M + 1):
                G = bk.array(
                    m * self.reciprocal[0] + n * self.reciprocal[1], dtype=bk.float64
                )
                Ra = bk.array(self.res_array[alpha].position, dtype=bk.float64)
                ph = bk.exp(1j * (Ra @ G))
                P.append(ph)
        return bk.array(P) if P == [] else bk.stack(P)

    def _getK(self, K, M):
        K = bk.array(K)
        P = []
        for m in range(-M, M + 1):
            for n in range(-M, M + 1):
                G = bk.array(
                    m * self.reciprocal[0] + n * self.reciprocal[1], dtype=bk.float64
                )
                P.append(bk.linalg.norm(K + G) ** 4)
        return bk.diag(bk.array(P, dtype=bk.float64))

    def eigensolve(self, k, Npw, return_modes=True, hermitian=False):
        """Solve the eigenvalue problem.

        Parameters
        ----------
        k : tuple
            Wavevector
        Npw : int
            Number of plane waves in each direction (from `-Npw` to `Npw`)
        return_modes : bool, optional
            Compute eigenmodes?
        hermitian : bool, optional
            Is the problem Hermitian?

        Returns
        -------
        array or tuple of arrays
            Eigenvalues (and eigenmodes if `return_modes` is `True`)
        """
        N = (2 * Npw + 1) ** 2

        Ps = []
        for alpha in range(self.n_res):
            P = self._getP(alpha, Npw)
            Ps.append(P)
        Ps = bk.array(Ps) if Ps == [] else bk.stack(Ps)

        Kmat0 = self._getK(k, Npw)

        Nmat = self.n_res + N
        Kmat = bk.zeros((Nmat, Nmat), dtype=bk.complex128)
        Q = 0
        stiffnesses = []
        masses = []
        gamma_inf = _BIG
        for alpha, res in enumerate(self.res_array):
            Omega_inf_square = _BIG if isinstance(res, Mass) else 1
            mass = (
                self.plate.rho * self.plate.h * self.area * gamma_inf
                if isinstance(res, Pin)
                else res.mass
            )
            stiffness = (
                res.stiffness
                if isinstance(res, Resonator)
                else Omega_inf_square
                / self.plate.D
                * mass
                / (self.plate.rho * self.plate.h)
            )

            stiffnesses.append(stiffness)
            masses.append(mass)
            P = bk.stack([Ps[alpha]]).T
            Q += P @ bk.conj(P).T * stiffness
            Kmat[N + alpha, :N] = -stiffness * bk.conj(Ps[alpha])
            Kmat[:N, N + alpha] = -stiffness * (Ps[alpha])

        Kmat[:N, :N] = self.plate.bending_stiffness * self.area * Kmat0 + Q
        Kmat[N:, N:] = bk.diag(bk.array(stiffnesses, dtype=bk.float64))

        Mmat = bk.zeros((Nmat), dtype=bk.complex128)
        Mmat[:N] = self.plate.rho * self.plate.h * self.area
        Mmat[N:] = bk.array(masses)

        self.Kmat = Kmat
        self.Mmat = Mmat
        Amat = Kmat / Mmat

        if return_modes:
            eigensolve = bk.linalg.eigh if hermitian else bk.linalg.eig
            eigvals, modes = eigensolve(Amat)
        else:
            eigensolve = bk.linalg.eigvalsh if hermitian else bk.linalg.eigvals
            eigvals = eigensolve(Amat)
        omegans = eigvals**0.5
        isort = bk.argsort(omegans.real)
        if return_modes:
            return omegans[isort], modes[:, isort]
        return omegans[isort]

    def get_field(self, mode_coeffs, x, y):
        """Get the mode field

        Parameters
        ----------
        mode_coeffs : array
            Fourrier coefficients of the mode
        x : array
            x coordinates
        y : array
            y coordinates

        Returns
        -------
        array
            Modal field in real space
        """
        M = int(((len(mode_coeffs) - self.n_res) ** 0.5 - 1) / 2)
        field = 0
        i = 0
        for m in range(-M, M + 1):
            for n in range(-M, M + 1):
                G = m * self.reciprocal[0] + n * self.reciprocal[1]
                kr = G[0] * x + G[1] * y
                field += mode_coeffs[i] * bk.exp(-1j * kr)
                i += 1
        return field


class DiffractionSimulation(_Simulation):
    """Class to run a diffraction simulation.

    Parameters
    ----------
    plate : :class:`ElasticPlate`
        The plate
    res_array : array of :class:`Pin`, :class:`Mass` or :class:`Resonator`
        An array containing the scatterers, it can be a mixture of pins, masses or resonators.
    period : float
        Period of the grating
    nh : int
        Number of harmonics such that the calculations use orders [-nh,...0, ...,nh]

    """

    def __init__(self, plate, res_array, period, nh=0, force_propa=False):
        super().__init__(plate, res_array)

        for res in self.res_array:
            if not (0 <= res.position[0] < period):
                raise ValueError("Resonator x position must be in [0,period[")

        self.period = period
        self.nh = nh
        self.force_propa = force_propa

    @property
    def harmonics(self):
        """Property returning the array of harmonic orders [-nh,...0, ...,nh]
        that are used in the calculations.

        Returns
        -------
        array
            The array of harmonics
        """
        return bk.array(range(-self.nh, self.nh + 1))

    def _G0(self, omega):
        k = self.plate.wavenumber(omega)
        return 1 / (4 * self.period * k**2)

    def _dG0_domega(self, omega):
        k = self.plate.wavenumber(omega)
        dk_domega = self.plate.dwavenumber_domega(omega)
        return -dk_domega / (2 * self.period * k**3)

    def _g(self, n):
        return 2 * n * bk.pi / self.period

    def _get_xis(self, k, kx, n):
        qn = kx + self._g(n)
        xip = bk.sqrt((qn**2 + k**2 + 0j)).conj()
        xim = bk.sqrt((qn**2 - k**2 + 0j)).conj()
        # if self.plate.incoming:
        #     return xip.conj(), xim.conj()
        return xip, xim

    def is_propagative(self, k, kx, n):
        """Check if the nth harmonic is propagative.

        Parameters
        ----------
        k : float
            Wavenumber
        kx : float
            Wavenumber in the x direction
        n : int
            Order of the harmonic

        Returns
        -------
        bool
            True if the nth harmonic is propagative, False otherwise
        """
        if self.force_propa:
            return True
        return bk.abs(kx + self._g(n)) < k

    def _lattice_sum(self, k, kx, xa, ya, xb, yb, harmonics=None):
        """
        Compute the lattice sum for the diffraction simulation.

        Parameters
        ----------
        k : float
            Wavenumber
        kx : float
            Wavenumber in the x direction
        xa, ya : float
            Position of the first scatterer
        xb, yb : float
            Position of the second scatterer
        harmonics : array, optional
            Orders of the harmonics to use. If None, uses all harmonics.

        Returns
        -------
        complex
            The lattice sum
        """
        if harmonics is None:
            harmonics = self.harmonics

        dx = xa - xb
        abs_dy = bk.abs(ya - yb)
        lat_sum = 0
        for n in harmonics:
            xip, xim = self._get_xis(k, kx, n)
            t = bk.exp(-xim * abs_dy) / xim
            t -= bk.exp(-xip * abs_dy) / xip
            t *= bk.exp(1j * (kx + self._g(n)) * dx)
            lat_sum += t
        return lat_sum

    def _d_lattice_sum_dk(self, k, kx, xa, ya, xb, yb, harmonics=None):
        """
        Compute the derivative of the lattice sum with respect to k.

        Parameters
        ----------
        k : float
            Wavenumber
        kx : float
            Wavenumber in the x direction
        xa, ya : float
            Position of the first scatterer
        xb, yb : float
            Position of the second scatterer
        harmonics : array, optional
            Orders of the harmonics to use. If None, uses all harmonics.

        Returns
        -------
        complex
            The derivative of the lattice sum with respect to k
        """
        if harmonics is None:
            harmonics = self.harmonics

        dx = xa - xb
        abs_dy = bk.abs(ya - yb)
        lat_sum = 0
        for n in harmonics:
            xip, xim = self._get_xis(k, kx, n)
            t = bk.exp(-xim * abs_dy) * (1 + xim * abs_dy) / (xim**3)
            t += bk.exp(-xip * abs_dy) * (1 + xip * abs_dy) / (xip**3)
            t *= bk.exp(1j * (kx + self._g(n)) * dx)
            lat_sum += t
        # lat_sum *= bk.conjugate(k**0.5) ** 2
        lat_sum *= k
        return lat_sum

    def _d_lattice_sum_domega(self, omega, k, kx, xa, ya, xb, yb, harmonics=None):
        """
        Compute the derivative of the lattice sum with respect to omega.

        Parameters
        ----------
        omega : float
            Frequency
        k : float
            Wavenumber
        kx : float
            Wavenumber in the x direction
        xa, ya : float
            Position of the first scatterer
        xb, yb : float
            Position of the second scatterer
        harmonics : array, optional
            Orders of the harmonics to use. If None, uses all harmonics.

        Returns
        -------
        complex
            The derivative of the lattice sum with respect to omega
        """
        return self._d_lattice_sum_dk(
            k, kx, xa, ya, xb, yb, harmonics
        ) * self.plate.dwavenumber_domega(omega)

    def build_matrix(self, omega, kx):
        """
        Build the matrix M.

        Parameters
        ----------
        omega : float
            Frequency
        kx : float
            Wavenumber in the x direction

        Returns
        -------
        array
            The matrix M
        """
        omega = bk.array(omega)
        # if self.plate.incoming:
        #     omega = omega.conj()
        k = self.plate.wavenumber(omega)
        # if self.plate.incoming:
        #     k = k.conj()
        G0 = self._G0(omega)
        # if self.plate.incoming:
        #     G0 = G0.conj()
        matrix = bk.array(
            bk.zeros((self.n_res, self.n_res, *omega.shape), dtype=bk.complex128)
        )
        for alpha, res_alpha in enumerate(self.res_array):
            for beta, res_beta in enumerate(self.res_array):
                res_beta = self.res_array[beta]
                delta = 1 if alpha == beta else 0
                t_beta = res_beta.strength(omega) / self.plate.D
                chi = G0 * self._lattice_sum(
                    k, kx, *res_alpha.position, *res_beta.position
                )
                matrix[alpha, beta] = delta / t_beta - chi
        # if self.plate.incoming:
        #     axes = (1,0) + tuple(i+2 for i in range(len(omega.shape)))
        #     return bk.transpose(matrix.conj(), axes)
        return matrix

    def build_matrix_derivative(self, omega, kx):
        """
        Build the derivative of the matrix M with respect to omega.

        Parameters
        ----------
        omega : float
            Frequency
        kx : float
            Wavenumber in the x direction

        Returns
        -------
        array
            The derivative of the matrix M with respect to omega
        """
        omega = bk.array(omega)
        # if self.plate.incoming:
        #     omega = omega.conj()
        k = self.plate.wavenumber(omega)
        # if self.plate.incoming:
        #     k = k.conj()
        G0 = self._G0(omega)
        dG0 = self._dG0_domega(omega)
        # if self.plate.incoming:
        #     G0 = G0.conj()
        matrix = bk.array(
            bk.zeros((self.n_res, self.n_res, *omega.shape), dtype=bk.complex128)
        )
        for alpha, res_alpha in enumerate(self.res_array):
            for beta, res_beta in enumerate(self.res_array):
                res_beta = self.res_array[beta]
                delta = 1 if alpha == beta else 0
                t_beta = res_beta.strength(omega) / self.plate.D
                S = self._lattice_sum(k, kx, *res_alpha.position, *res_beta.position)
                dS = self._d_lattice_sum_domega(
                    omega, k, kx, *res_alpha.position, *res_beta.position
                )
                dchi = dG0 * S + G0 * dS
                dt_beta = res_beta.dstrength_domega(omega) / self.plate.D
                matrix[alpha, beta] = -delta * dt_beta / t_beta**2 - dchi
        # if self.plate.incoming:
        #     axes = (1,0) + tuple(i+2 for i in range(len(omega.shape)))
        #     return bk.transpose(matrix.conj(), axes)
        return matrix

    def build_rhs(self, omega, angle):
        """
        Build the right-hand side vector

        Parameters
        ----------
        omega : float
            Frequency
        angle : float
            Incident angle (in radians)

        Returns
        -------
        array
            The right-hand side vector
        """
        psi_inc = bk.array(bk.zeros((self.n_res), dtype=bk.complex128))
        for alpha, res_alpha in enumerate(self.res_array):
            psi_inc[alpha] = self.plane_wave(*res_alpha.position, omega, angle)

        return psi_inc

    def solve(self, omega, angle):
        """Solve the diffraction problem

        Parameters
        ----------
        omega: float
            Frequency
        angle: float
            Incident angle (in radians)
        Returns
        -------
        array
            The solution vector
        """
        k = self.plate.wavenumber(omega)
        angle = bk.array(angle)
        kx = k * bk.cos(angle)
        matrix = self.build_matrix(omega, kx)
        rhs = self.build_rhs(omega, angle)
        return bk.linalg.solve(matrix, rhs)

    def _get_field(self, x, y, incident, solution_vector, omega, angle):
        k = self.plate.wavenumber(omega)
        angle = bk.array(angle)
        kx = k * bk.cos(angle)
        G0 = self._G0(omega)
        if incident is None:
            W = 0
        else:
            W = incident(x, y, omega, angle)
        for alpha, res_alpha in enumerate(self.res_array):
            chi = G0 * self._lattice_sum(k, kx, x, y, *res_alpha.position)
            W += solution_vector[alpha] * chi
        return W

    def get_field(self, x, y, solution_vector, omega, angle):
        """Get the total field.

        Parameters
        ----------
        x : array
            x coordinates
        y : array
            y coordinates
        solution_vector: array
            The solution vector as returned by :class:`ScatteringSimulation.solve`
        omega: float or complex
            Frequency
        angle: float
            Incident angle (in radians)

        Returns
        -------
        array
            The total field
        """
        return self._get_field(
            x,
            y,
            self.plane_wave,
            solution_vector,
            omega,
            angle,
        )

    def get_scattered_field(self, x, y, solution_vector, omega, angle):
        """Get the scattered field.

        Parameters
        ----------
        x : array
            x coordinates
        y : array
            y coordinates
        solution_vector: array
            The solution vector as returned by :class:`ScatteringSimulation.solve`
        omega: float or complex
            Frequency
        angle: float
            Incident angle (in radians)


        Returns
        -------
        array
            The scattered field
        """
        return self._get_field(
            x,
            y,
            None,
            solution_vector,
            omega,
            angle,
        )

    def orders_angle(self, k, angle, n):
        """
        Compute the angle of the nth order diffracted beam.

        Parameters
        ----------
        k : float
            Wavenumber
        angle : float
            Incident angle (in radians)
        n : int
            Order of the diffracted beam

        Returns
        -------
        float
            Angle of the nth order diffracted beam
        """
        angle = bk.array(angle)
        return bk.arccos(bk.cos(angle) + 2 * n * bk.pi / (self.period * k))

    @staticmethod
    def _wavevector_orders(k, thetan):
        thetan = bk.array(thetan)
        kx, ky = k * bk.cos(thetan), k * bk.sin(thetan)
        return bk.array([kx, ky]), bk.array([kx, -ky])

    def get_propagative_harmonics(self):
        """
        Return the propagative harmonics.

        The propagative harmonics are the ones for which the nth order diffracted beam is propagative.
        The nth order diffracted beam is propagative if the modulus of the x wavenumber is less than the modulus of the wavenumber.
        The x wavenumber is the projection of the wavenumber on the x axis.

        Returns
        -------
        array
            The propagative harmonics
        """
        propa = [self.is_propagative(k, kx, n) for n in self.harmonics]
        propagative = []
        for i, prop in enumerate(propa):
            if prop:
                propagative.append(self.harmonics[i])
        return propagative

    def get_efficiencies(self, sol, omega, angle):
        """
        Compute the efficiencies of the diffraction orders.

        Parameters
        ----------
        sol : array
            The solution vector
        omega : float
            Frequency
        angle : float
            Incident angle (in radians)

        Returns
        -------
        Rdict, Tdict : dict
            Dictionaries containing the reflected and transmitted
            amplitudes, energies and total efficiencies
        """
        angle = bk.array(angle)
        k = self.plate.wavenumber(omega)
        kx = k * bk.cos(angle)
        G0 = self._G0(omega)

        propa = [self.is_propagative(k, kx, n) for n in self.harmonics]
        t, r, T, R = {}, {}, {}, {}
        sumT = sumR = 0

        for n in self.harmonics:
            if self.is_propagative(k, kx, n):
                thetan = self.orders_angle(k, angle, n)
                cst = 1j * G0 / (k * bk.sin(thetan))
                knp, knm = self._wavevector_orders(k, thetan)
                rn = tn = 0
                for beta, res in enumerate(self.res_array):
                    pos = bk.array(res.position)
                    tn += sol[beta] * bk.exp(-1j * (knp @ pos))
                    rn += sol[beta] * bk.exp(-1j * (knm @ pos))
                rn *= cst
                tn *= cst
                if n == 0:
                    tn += 1

                nrm_ = bk.sin(thetan) / bk.sin(angle)
                Rn = bk.abs(rn) ** 2 * nrm_
                Tn = bk.abs(tn) ** 2 * nrm_

                sumR += Rn
                sumT += Tn

            else:
                rn = tn = Rn = Tn = 0

            key = str(int(n))
            r[key] = rn
            t[key] = tn
            R[key] = Rn
            T[key] = Tn
        Rdict = dict(amplitude=r, energy=R, total=sumR)
        Tdict = dict(amplitude=t, energy=T, total=sumT)
        return Rdict, Tdict

    def get_min_harmonics(self, omega_max, angle, nhmax=100):
        """
        Compute the minimum number of harmonics to consider for a given frequency
        and incident angle.

        Parameters
        ----------
        omega_max : float
            Maximum frequency
        angle : float
            Incident angle in radians
        nhmax : int, optional
            Maximum number of harmonics to consider. Defaults to 100.

        Returns
        -------
        int
            The minimum number of harmonics to consider
        """
        k = self.plate.wavenumber(omega_max)
        angle = bk.array(angle)
        kx = k * bk.cos(angle)
        nh_ = 0
        propa = True

        while propa:
            nh_ += 1
            propa1 = self.is_propagative(k, kx, -nh_)
            propa2 = self.is_propagative(k, kx, nh_)
            propa = propa1 or propa2
            if nh_ > nhmax:
                break
        nh_ -= 1

        return nh_

    def eigensolve(self, *args, **kwargs):
        """
        Solve the eigenvalue problem using a non-linear eigensolver.

        Parameters
        ----------
        omega0 : float
            Lower bound of the frequency range
        omega1 : float
            Upper bound of the frequency range
        *args :
            Additional arguments for :func:`nonlinear_eigensolver`
        **kwargs :
            Additional keyword arguments for :func:`nonlinear_eigensolver`

        Returns
        -------
        array
            Eigenvalues
        array
            Eigenvectors
        array
            Left eigenvectors (if `return_left` is `True`)
        """
        kwargs["return_left"] = True
        ev, vr, vl = nonlinear_eigensolver(self, *args, **kwargs)
        return bk.array(ev), bk.array(vr), bk.array(vl).conj()

    def normalize_eigenvectors(
        self, kx, eigenvalues, eigenvectors_right, eigenvectors_left, dM=None
    ):
        """
        Normalize the eigenvectors such that they satisfy the orthonormality
        condition with respect to the inner product defined by the derivative
        of the matrix M.

        Parameters
        ----------
        eigenvalues : array
            The eigenvalues
        eigenvectors_right : array
            The right eigenvectors
        eigenvectors_left : array
            The left eigenvectors
        dM : array, optional
            The derivative of the matrix M at each eigenvalue. If None, it is
            computed.

        Returns
        -------
        eigenvectors_right_norm : array
            The normalized right eigenvectors
        eigenvectors_left_norm : array
            The normalized left eigenvectors
        """
        if dM is None:
            dM = [self.build_matrix_derivative(omega, kx) for omega in eigenvalues]
        eigenvectors_right_norm = bk.zeros_like(eigenvectors_right)
        eigenvectors_left_norm = bk.zeros_like(eigenvectors_left)
        for n, _dM in enumerate(dM):
            phin_r = eigenvectors_right[:, n]
            phin_l = eigenvectors_left[:, n]
            nrm = (phin_l.conj() @ (_dM @ phin_r)) ** 0.5
            eigenvectors_right_norm[:, n] = phin_r / nrm
            eigenvectors_left_norm[:, n] = phin_l / nrm.conj()

        return eigenvectors_right_norm, eigenvectors_left_norm

    def get_multiplicities(self, kx, eigenvalues, M=None, tol=1e-6):
        """
        Compute the multiplicity of each eigenvalue.

        Parameters
        ----------
        kx : float
            The x-component of the wavevector
        eigenvalues : array
            The eigenvalues
        M : array, optional
            The matrix at each eigenvalue. If None, it is computed.
        tol : float
            The tolerance for the rank computation

        Returns
        -------
        array
            The multiplicity of each eigenvalue
        """
        if M is None:
            M = [self.build_matrix(omega, kx) for omega in eigenvalues]
        multiplicities = [self.n_res - bk.linalg.matrix_rank(m, tol=tol) for m in M]
        return multiplicities

    def update_eig(
        self, kx, evs, eigenvectors_right, eigenvectors_left, multiplicities, M, dM
    ):
        """
        Update the eigenvectors and eigenvalues, given their multiplicities and
        the matrices M and dM.

        Parameters
        ----------
        kx : float
            The x-component of the wavevector
        evs : array
            The eigenvalues
        eigenvectors_right : array
            The right eigenvectors
        eigenvectors_left : array
            The left eigenvectors
        multiplicities : array
            The multiplicity of each eigenvalue
        M : array
            The matrix M at each eigenvalue
        dM : array
            The derivative of the matrix M at each eigenvalue

        Returns
        -------
        eigvals : array
            The updated eigenvalues
        eigenvectors_right : array
            The updated right eigenvectors
        eigenvectors_left : array
            The updated left eigenvectors
        """
        Nmodes = len(evs)
        eigvals = []
        eigmodes_right = []
        eigmodes_left = []
        for i in range(Nmodes):
            mult = multiplicities[i]
            if mult > 1:
                vs_r = null(M[i], mult=mult)
                dMi = bk.stack([dM[i]] * mult)
                vs_r = gram_schmidt(vs_r, dMi)
                vs_l = null(M[i].T.conj(), mult=mult)
                dMi = bk.stack([dM[i].T.conj()] * mult)
                vs_l = gram_schmidt(vs_l, dMi)
                for jm in range(mult):
                    eigvals.append(evs[i])
                    eigmodes_right.append(vs_r[:, jm])
                    eigmodes_left.append(vs_l[:, jm])
            else:
                eigvals.append(evs[i])
                eigmodes_right.append(eigenvectors_right[:, i])
                eigmodes_left.append(eigenvectors_left[:, i])

        eigenvectors_right = bk.stack(eigmodes_right).T
        eigenvectors_left = bk.stack(eigmodes_left).T
        evs = bk.array(eigvals)
        return evs, eigenvectors_right, eigenvectors_left

    def get_expansion_coefficients(
        self, eigenvalues, eigenvectors_left, kx, omegas, arbitrary=None
    ):
        """
        Compute the expansion coefficients for a given incident field.

        Parameters
        ----------
        eigenvalues : array
            The eigenvalues
        eigenvectors_left : array
            The left eigenvectors
        kx : float
            The x-component of the wavevector
        omegas : array
            The frequencies
        arbitrary : callable, optional
            A function to scale the expansion coefficients. If None, it is set to
            a function that returns 1.

        Returns
        -------
        array
            The expansion coefficients
        """
        if arbitrary == None:

            def arbitrary(omega):
                return 1

        Nmodes = len(eigenvalues)
        nfreq = len(omegas)
        bs = bk.zeros((nfreq, Nmodes), dtype=bk.complex128)
        for i, omega in enumerate(omegas):
            k0 = self.plate.wavenumber(omega)
            angle = bk.arccos(kx / k0)
            psi_inc = self.build_rhs(omega, angle)
            for n in range(Nmodes):
                omegan = eigenvalues[n]
                num = eigenvectors_left[:, n].conj() @ psi_inc
                bs[i, n] = num / (omega - omegan) * arbitrary(omegan) / arbitrary(omega)
        return bs
