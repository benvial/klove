#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


import scipy.special as sp

from . import backend as bk
from . import get_backend

BACKEND = get_backend()

EPS = 1e-14


def j0(kr):
    return sp.jn(0, kr)


def j1(kr):
    return sp.jn(1, kr)


def y0(kr):
    return sp.yv(0, kr)


def y1(kr):
    return sp.yv(1, kr)


def k0(kr):
    return sp.kv(0, kr)


def k1(kr):
    return sp.kv(1, kr)


def j2(kr):
    return sp.jn(2, kr)


def y2(kr):
    return sp.yv(2, kr)


def k2(kr):
    return sp.kv(2, kr)


def j3(kr):
    return sp.jn(3, kr)


def y3(kr):
    return sp.yv(3, kr)


def k3(kr):
    return sp.kv(3, kr)


def j4(kr):
    return sp.jn(4, kr)


def y4(kr):
    return sp.yv(4, kr)


def k4(kr):
    return sp.kv(4, kr)


def torch_j2(kr):
    return 2 / kr * bk.special.bessel_j1(kr) - bk.special.bessel_j0(kr)


def torch_y2(kr):
    return 2 / kr * bk.special.bessel_y1(kr) - bk.special.bessel_y0(kr)


def torch_k2(kr):
    return 2 / kr * bk.special.modified_bessel_k1(kr) + bk.special.modified_bessel_k0(
        kr
    )


def torch_j3(kr):
    return 4 / kr * torch_j2(kr) - bk.special.bessel_j1(kr)


def torch_y3(kr):
    return 4 / kr * torch_y2(kr) - bk.special.bessel_y1(kr)


def torch_k3(kr):
    return 4 / kr * torch_k2(kr) + bk.special.modified_bessel_k1(kr)


def torch_j4(kr):
    return 6 / kr * torch_j3(kr) - torch_j2(kr)


def torch_y4(kr):
    return 6 / kr * torch_y3(kr) - torch_y2(kr)


def torch_k4(kr):
    return 6 / kr * torch_k3(kr) + torch_k2(kr)


bessel_j0 = bk.special.bessel_j0 if BACKEND == "torch" else sp.j0
bessel_y0 = bk.special.bessel_y0 if BACKEND == "torch" else sp.y0
bessel_k0 = bk.special.modified_bessel_k0 if BACKEND == "torch" else sp.k0
bessel_j1 = bk.special.bessel_j1 if BACKEND == "torch" else sp.j1
bessel_y1 = bk.special.bessel_y1 if BACKEND == "torch" else sp.y1
bessel_k1 = bk.special.modified_bessel_k1 if BACKEND == "torch" else sp.k1
bessel_j2 = torch_j2 if BACKEND == "torch" else j2
bessel_y2 = torch_y2 if BACKEND == "torch" else y2
bessel_k2 = torch_k2 if BACKEND == "torch" else k2
bessel_j3 = torch_j3 if BACKEND == "torch" else j3
bessel_y3 = torch_y3 if BACKEND == "torch" else y3
bessel_k3 = torch_k3 if BACKEND == "torch" else k3
bessel_j4 = torch_j4 if BACKEND == "torch" else j4
bessel_y4 = torch_y4 if BACKEND == "torch" else y4
bessel_k4 = torch_k4 if BACKEND == "torch" else k4


def switch_complex(f1, f2):
    def wrapper(z):
        z = bk.array(z)
        cond = bk.is_complex(z) if BACKEND == "torch" else bk.any(bk.iscomplex(z))
        if cond:
            return f2(z)
        return f1(z.real)

    return wrapper


bessel_j0 = switch_complex(bessel_j0, j0)
bessel_y0 = switch_complex(bessel_y0, y0)
bessel_k0 = switch_complex(bessel_k0, k0)
bessel_j1 = switch_complex(bessel_j1, j1)
bessel_y1 = switch_complex(bessel_y1, y1)
bessel_k1 = switch_complex(bessel_k1, k1)
bessel_j2 = switch_complex(bessel_j2, j2)
bessel_y2 = switch_complex(bessel_y2, y2)
bessel_k2 = switch_complex(bessel_k2, k2)
bessel_j3 = switch_complex(bessel_j3, j3)
bessel_y3 = switch_complex(bessel_y3, y3)
bessel_k3 = switch_complex(bessel_k3, k3)
bessel_j4 = switch_complex(bessel_j4, j4)
bessel_y4 = switch_complex(bessel_y4, y4)
bessel_k4 = switch_complex(bessel_k4, k4)


def _gfreal(kr):
    return bk.where(bk.abs(bk.array(kr)) < EPS, 1.0, bessel_j0(kr))


def _gfimag(kr):
    return bk.where(
        bk.abs(bk.array(kr)) < EPS,
        0.0,
        bessel_y0(kr) + 2 / bk.pi * bessel_k0(kr),
    )


def _dgfreal(kr):
    return bk.where(bk.abs(bk.array(kr)) < EPS, 0.0, -bessel_j1(kr))


def _dgfimag(kr):
    return bk.where(
        bk.abs(bk.array(kr)) < EPS,
        0.0,
        -bessel_y1(kr) - 2 / bk.pi * bessel_k1(kr),
    )


if BACKEND == "torch":

    class _GFreal(bk.autograd.Function):
        @staticmethod
        def forward(ctx, kr):
            ctx.save_for_backward(kr)
            return _gfreal(kr)

        @staticmethod
        def backward(ctx, grad_output):
            (kr,) = ctx.saved_tensors
            return grad_output * _dgfreal(kr)

    class _GFimag(bk.autograd.Function):
        @staticmethod
        def forward(ctx, kr):
            ctx.save_for_backward(kr)
            return _gfimag(kr)

        @staticmethod
        def backward(ctx, grad_output):
            (kr,) = ctx.saved_tensors
            return grad_output * _dgfimag(kr)

    gfreal = _GFreal.apply
    gfimag = _GFimag.apply


else:
    gfreal = _gfreal
    gfimag = _gfimag


def _norma_gf(k):
    return 1j / (8 * k**2)


def greens_function(k, r):
    """
    Green's function of the Kirchoff Love equation in 2D.

    Parameters
    ----------
    k : array
        wavenumber
    r : array
        radial coordinate

    Returns
    -------
    array
        Green's function
    """
    # r = bk.array(r)
    # k = bk.array(k)
    # r_ = bk.broadcast_to(r, (k.shape + r.shape))
    # k_ = bk.broadcast_to(k, (r.shape + k.shape)).T
    # kr = k_ * r_
    # norm = _norma_gf(k_)
    kr = k * r
    return _norma_gf(k) * (gfreal(kr) + 1j * gfimag(kr))


def dgreens_function_dk(k, r):
    """
    Derivative of the Green's function with respect to k.

    Parameters
    ----------
    k : array
        wavenumber
    r : array
        radial coordinate

    Returns
    -------
    array
        Derivative of the Green's function
    """

    G0 = greens_function(k, r)
    G1 = _gf_fun(k, r, bessel_j1, bessel_y1, bessel_k1)
    return -2 / k * G0 - G1 * r


def greens_function_cartesian(k, x, y):
    """
    Green's function of the Kirchoff Love equation in 2D in cartesian coordinates.

    Parameters
    ----------
    k : array
        wavenumber
    x : array
        x coordinates
    y : array
        y coordinates

    Returns
    -------
    array
        Green's function
    """

    r = radial_coordinates(x, y)
    return greens_function(k, r)


def dgreens_function_cartesian_dk(k, x, y):
    """
    Derivative of the Green's function with respect to k in cartesian coordinates.

    Parameters
    ----------
    k : array
        wavenumber
    x : array
        x coordinates
    y : array
        y coordinates

    Returns
    -------
    array
        Derivative of the Green's function
    """
    r = radial_coordinates(x, y)
    return dgreens_function_dk(k, r)


def radial_coordinates(x, y):
    """
    Return the radial coordinates from x and y coordinates.

    Parameters
    ----------
    x : array
        x coordinates
    y : array
        y coordinates

    Returns
    -------
    array
        radial coordinates
    """
    return EPS + (x**2 + y**2) ** 0.5


def grad_radial_coordinates(x, y):
    """
    Return the gradient (dr/dx,dr/dy) coordinates from x and y coordinates.

    Parameters
    ----------
    x : array
        x coordinates
    y : array
        y coordinates

    Returns
    -------
    array
        gradient
    """
    r = radial_coordinates(x, y)
    return bk.vstack([x / r, y / r])


def _gf_fun(k, r, jfun, yfun, kfun):
    kr = k * r
    re = jfun(kr)
    im = yfun(kr) + 2 / bk.pi * kfun(kr)
    norm = _norma_gf(k)
    return norm * (re + 1j * im)


def grad_greens_function_cartesian(k, x, y):
    r = radial_coordinates(x, y)
    out = -k / r * _gf_fun(k, r, bessel_j1, bessel_y1, bessel_k1)
    out = bk.where(bk.abs(k * r) < EPS, 0.0 + 0.0 * 1j, out)
    return bk.vstack([x * out, y * out])


def laplacian_greens_function_cartesian(k, x, y):
    r = radial_coordinates(x, y)
    G1 = _gf_fun(k, r, bessel_j1, bessel_y1, bessel_k1)
    G2 = _gf_fun(k, r, bessel_j2, bessel_y2, bessel_k2)
    out = -2 * k / r * G1 + k**2 * G2
    out = bk.where(bk.abs(k * r) < EPS, 0.0 + 0.0 * 1j, out)
    return out


def grad_laplacian_greens_function_cartesian(k, x, y):
    r = radial_coordinates(x, y)
    G2 = _gf_fun(k, r, bessel_j2, bessel_y2, bessel_k2)
    G3 = _gf_fun(k, r, bessel_j3, bessel_y3, bessel_k3)

    def tmp(x):
        out = 4 * k**2 * x / r**2 * G2 - k**3 * x / r * G3
        out = bk.where(bk.abs(k * r) < EPS, 0.0 + 0.0 * 1j, out)
        return out

    return bk.vstack([tmp(x), tmp(y)])


def h0(k, r):
    return bessel_j0(k * r) + 1j * bessel_y0(k * r)


def g0(k, r):
    return (h0(k, r) - h0(1j * k, r)) * 1j / (8 * k**2)


def h1(k, r):
    return bessel_j1(k * r) + 1j * bessel_y1(k * r)


def g1(k, r):
    # return (h1(k, r) - 1j * h1(1j * k, r)) * 1j / (8 * k**2)
    return (h1(k, r) + 2 * 1j / bk.pi * k1(k * r)) * 1j / (8 * k**2)


def h2(k, r):
    return bessel_j2(k * r) + 1j * bessel_y2(k * r)


def g2(k, r):
    return (h2(k, r) + h2(1j * k, r)) * 1j / (8 * k**2)


def h3(k, r):
    return bessel_j3(k * r) + 1j * bessel_y3(k * r)


def g3(k, r):
    return (h3(k, r) + 1j * h3(1j * k, r)) * 1j / (8 * k**2)


def h4(k, r):
    return bessel_j4(k * r) + 1j * bessel_y4(k * r)


def g4(k, r):
    return (h4(k, r) - h4(1j * k, r)) * 1j / (8 * k**2)


def dg0_dk(k, r):
    return -2 / k * g0(k, r) - r * g1(k, r)


def dg1_dk(k, r):
    return -1 / k * g1(k, r) - r * g2(k, r)


def dg2_dk(k, r):
    return -r * g3(k, r)


def dg3_dk(k, r):
    return 1 / k * g3(k, r) - r * g4(k, r)


def gradg(k, x, y):
    tmp = grad_greens_function_cartesian(k, x, y)
    return bk.vstack([tmp[0], tmp[1]])


def dgradg_dk(k, x, y):
    r = radial_coordinates(x, y)
    f = k * g2(k, r)
    return bk.vstack([f * x, f * y])


def lapg(k, x, y):
    return laplacian_greens_function_cartesian(k, x, y)


def dlapg_dk(k, x, y):
    r = radial_coordinates(x, y)
    return 4 * k * g2(k, r) - k**2 * r * g3(k, r)


def gradlapg(k, x, y):
    tmp = grad_laplacian_greens_function_cartesian(k, x, y)
    return bk.vstack([tmp[0], tmp[1]])


def dgradlapg_dk(k, x, y):
    r = radial_coordinates(x, y)
    f = k / r * (8 / r * g2(k, r) - 8 * k * g3(k, r) + k**2 * r * g4(k, r))
    return bk.vstack([f * x, f * y])
