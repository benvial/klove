#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

__all__ = ["init_bands", "init_bands_plot"]


from . import backend as bk


def init_bands(sym_points, nband):
    """
    Initialize the k points for a band structure calculation.

    Parameters
    ----------
    sym_points : tuple of tuples
        A tuple of three tuples, each containing the x and y coordinates of
        a symmetry point in the Brillouin zone. The first tuple is the
        Gamma point, the second tuple is the X point, and the third tuple is
        the M point.
    nband : int
        The number of k points to use between each symmetry point.

    Returns
    -------
    ks : array
        A 2D array with shape (nk, 2), where each row is a k point in the
        Brillouin zone, and the columns are the x and y coordinates of each k
        point. The k points are ordered as follows: first, the k points from
        the Gamma point to the X point, then the k points from the X point to
        the M point, and finally the k points from the M point to the Gamma
        point.
    """
    Gamma_point, X_point, M_point = sym_points
    _kx = bk.linspace(Gamma_point[0], X_point[0], nband)
    _ky = bk.linspace(Gamma_point[1], X_point[1], nband)
    kGammaX = bk.vstack([_kx, _ky])
    _kx = bk.linspace(X_point[0], M_point[0], nband)
    _ky = bk.linspace(X_point[1], M_point[1], nband)
    kXM = bk.vstack([_kx, _ky])
    _kx = bk.linspace(M_point[0], Gamma_point[0], nband)
    _ky = bk.linspace(M_point[1], Gamma_point[1], nband)
    kMGamma = bk.vstack([_kx, _ky])
    ks = bk.vstack([kGammaX[:, :-1].T, kXM[:, :-1].T, kMGamma.T])
    return ks


def init_bands_plot(sym_points, nband):
    """
    Initialize the k points for a band structure plot.

    Parameters
    ----------
    sym_points : tuple of tuples
        A tuple of three tuples, each containing the x and y coordinates of
        a symmetry point in the Brillouin zone. The first tuple is the
        Gamma point, the second tuple is the X point, and the third tuple is
        the M point.
    nband : int
        The number of k points to use between each symmetry point.

    Returns
    -------
    ksplot : array
        A 1D array with shape (nk,), where each element is a k point in the
        Brillouin zone. The k points are ordered as follows: first, the k points
        from the Gamma point to the X point, then the k points from the X point
        to the M point, and finally the k points from the M point to the Gamma
        point.
    kdist : list of floats
        The distances between the symmetry points in the Brillouin zone.
    """
    Gamma_point, X_point, M_point = sym_points
    dMGamma = bk.linalg.norm(bk.array(Gamma_point) - bk.array(M_point))
    dGammaX = bk.linalg.norm(bk.array(X_point) - bk.array(Gamma_point))
    dXM = bk.linalg.norm(bk.array(M_point) - bk.array(X_point))
    _kx = bk.linspace(0, dGammaX, nband)[:-1]
    _kx1 = dGammaX + bk.linspace(0, dXM, nband)[:-1]
    _kx2 = dXM + dGammaX + bk.linspace(0, dMGamma, nband)
    ksplot = bk.hstack([_kx, _kx1, _kx2])
    kdist = [dGammaX, dXM, dMGamma]
    return ksplot, kdist
