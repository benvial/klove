#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


__all__ = ["GifMaker", "plot_bands"]


import glob
import os
import tempfile
from itertools import accumulate

import matplotlib.pyplot as plt
from PIL import Image

from . import backend as bk
from .bztools import init_bands_plot


def save_frame(i, folder=".frames"):
    """
    Save the current figure as a PNG in the given folder.

    Parameters
    ----------
    i : int
        The index of the frame
    folder : str, optional
        The folder to save the frame in. Defaults to ".frames".
    """
    _name = f"{i}".zfill(3)
    filename = f"{folder}/{_name}.png"
    plt.savefig(filename)


def make_gif(folder=".frames", fps=10, name="animation"):
    """
    Create a GIF from a folder of PNGs.

    Parameters
    ----------
    folder : str, optional
        The folder containing the PNGs. Defaults to ".frames".
    fps : int, optional
        The frames per second of the GIF. Defaults to 10.
    name : str, optional
        The name of the GIF. Defaults to "animation".

    Returns
    -------
    str
        The absolute path of the created GIF.
    """
    imgs = sorted(glob.glob(f"{folder}/*.png"))
    img, *imgs = [Image.open(f, mode="r").convert("RGB") for f in imgs]
    out = img.save(
        f"{name}.gif",
        format="GIF",
        append_images=imgs,
        save_all=True,
        duration=1000 / fps,
        loop=0,
    )
    return os.path.abspath(f"{name}.gif")


class GifMaker:
    """
    Initialize a GifMaker.

    Parameters
    ----------
    folder : str, optional
        The folder where the PNGs will be saved. Defaults to ".frames".
    """

    def __init__(self, folder=".frames"):
        self.folder = folder
        self.iteration = 0
        self.folder = tempfile.mkdtemp()

    def snap(self):
        """
        Save the current frame as a PNG in the folder specified in the constructor.

        The filename will be `frame_{:03d}.png` where the number is the frame number
        (starting from 0).

        """
        self.iteration += 1
        save_frame(self.iteration, self.folder)

    def save(self, *args, **kwargs):
        """
        Make a GIF from the PNGs saved in the folder specified in the constructor.

        Parameters
        ----------
        *args : tuple
            Arguments to pass to `make_gif`
        **kwargs : dict
            Keyword arguments to pass to `make_gif`

        Returns
        -------
        str
            The absolute path of the created GIF
        """

        return make_gif(self.folder, *args, **kwargs)


def plot_bands(
    sym_points,
    nband,
    eigenvalues,
    xtickslabels=[r"$\Gamma$", r"$X$", r"$M$", r"$\Gamma$"],
    color=None,
    **kwargs,
):
    # nband = int((len(eigenvalues)-2)/3)

    """
    Plot the band structure of a periodic structure.

    Parameters
    ----------
    sym_points : array
        The coordinates of the high symmetry points.
    nband : int
        The number of bands.
    eigenvalues : array
        The eigenvalues of the bands.
    xtickslabels : list of str, optional
        The labels of the high symmetry points.
    color : str, optional
        The color of the bands.
    **kwargs : dict
        Additional keyword arguments for matplotlib's plot function.

    Returns
    -------
    None

    Notes
    -----
    The x-axis is scaled so that the first and last high symmetry points are at the
    edges of the plot.

    """
    if color == None:
        color = "#4d63c5"
        if "color" in kwargs:
            kwargs.pop("colors")
        if "c" in kwargs:
            kwargs.pop("c")

    ksplot, kdist = init_bands_plot(sym_points, nband)
    plt.plot(ksplot, eigenvalues, color=color, **kwargs)
    # xticks = bk.cumsum(bk.array([0] + kdist))
    xticks = list(accumulate([0] + kdist))
    plt.xticks(xticks, xtickslabels)
    for x in xticks:
        plt.axvline(x, c="#8a8a8a")

    plt.xlim(xticks[0], xticks[-1])

    plt.ylabel(r"$\omega$")
