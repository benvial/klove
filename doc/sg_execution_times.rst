
:orphan:

.. _sphx_glr_sg_execution_times:


Computation times
=================
**00:03.490** total execution time for 9 files **from all galleries**:

.. container::

  .. raw:: html

    <style scoped>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    </style>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <script type="text/javascript" class="init">
    $(document).ready( function () {
        $('table.sg-datatable').DataTable({order: [[1, 'desc']]});
    } );
    </script>

  .. list-table::
   :header-rows: 1
   :class: table table-striped sg-datatable

   * - Example
     - Time
     - Mem (MB)
   * - :ref:`sphx_glr_examples_bands_plot_xiao.py` (``../examples/bands/plot_xiao.py``)
     - 00:03.490
     - 622.9
   * - :ref:`sphx_glr_examples_bands_plot_mass_pins.py` (``../examples/bands/plot_mass_pins.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_bands_plot_torrent.py` (``../examples/bands/plot_torrent.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_grating_plot_grating.py` (``../examples/grating/plot_grating.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_scattering_plot_chaplain.py` (``../examples/scattering/plot_chaplain.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_scattering_plot_cross.py` (``../examples/scattering/plot_cross.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_scattering_plot_marti_sabate.py` (``../examples/scattering/plot_marti_sabate.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_scattering_plot_porter.py` (``../examples/scattering/plot_porter.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_scattering_plot_rainbow_trapping.py` (``../examples/scattering/plot_rainbow_trapping.py``)
     - 00:00.000
     - 0.0
