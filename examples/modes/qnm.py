#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

"""
Finite cluster of resonators
============================

Finding modes.
"""

import sys
import time
from math import pi

import matplotlib.pyplot as plt
from tools import *

import klove as kl

plt.ion()
plt.close("all")
plt.clf()
# plt.gca().clear()

bk = kl.backend


#################################################################
# Parameters

a = 1

Nres = int(sys.argv[1])
alternative = True

study = "ps"
# study = "pw"
# study = "control"
# control = "killall"
# control = "kill"
# excase = "ps"
# excase = "pw"

if study != "control":
    excase = study
scattered_field_plt = True if excase == "pw" else False

omega0 = 0.01 - 1.0 * 1j
omega1 = 1.2 + 1e-2 * 1j

peak_ref = 4
N_guess_loc = 0

Rloc = 0.01
func_gives_der = False
refine = False
lambda_tol = 1e-9
max_iter = 100
# weight = "rayleigh symmetric"
weight = "max element"
init_vect = "random"
init_vect = "eig"
# init_vect = sys.argv[2]
# strategy = "grid"
strategy = "peaks"


guesses = None
# guesses = [0.4 - 0.1j]
# guesses = bk.linspace(0.21, 1.29, 5) - 0.1j

nfreq = 500
start = 0.18  # 0.001
# start = 0.01
stop = 0.31
# stop = 0.91

min_im = -0.03
#################################################################
# Plots

nptsx = 101
nptsy = nptsx
# nptsy = int(nptsx / Nres)
dbx = 3 * a
dbx = 6 * a
dby = 6 * a

recursive = True
plot_det = False
plot_solver = True
save_figures = True
Nplot = 9
Nbmax = 6


def savefig(name, folder="./figs", dpi=100):
    if save_figures:
        plt.savefig(f"{folder}/{name}.eps", dpi=dpi)
        plt.savefig(f"{folder}/{name}.png", dpi=dpi)


######################################################################
# Elastic plate

plate = kl.ElasticPlate(a * 1, 1, 1, 0.0)
p = 256 * plate.rho * plate.h * plate.D

omegap = plate.omega0(a)
m_plate = plate.rho * a**2 * plate.h

#################################################################
# Cluster
omega0 *= omegap
omega1 *= omegap


omegar0 = omegap * 1

m0 = m_plate
k0 = omegar0**2 * m0

#### line array
positionsx = bk.linspace(-Nres * a / 2, Nres * a / 2, Nres) * 1
positions = [(_x, 0) for _x in positionsx]

# import numpy as np
# positions = np.load("quasicrystal.npz")["vertices"]
# Nres = len(positions)
# positions = positions*Nres**0.5

# # #### ring
# r0 = a
# t = bk.linspace(0, 2 * pi, Nres + 1)[:-1]
# positions = bk.stack([r0 * bk.cos(t), r0 * bk.sin(t)]).T


stiffnesses = bk.ones(Nres) * k0
# masses = bk.ones(Nres) * m0
omegars = omegar0 * bk.linspace(1.0, 0.8, Nres)

masses = k0 / omegars**2  # graded
stiffnesses = bk.ones(Nres) * k0

# masses = bk.ones(Nres) * m0
# masses = bk.ones(Nres) *m0  # graded
# stiffnesses = m0 * omegars**2
# masses = bk.flipud(masses)
# masses = bk.ones(Nres) * m0
# stiffnesses = bk.linspace(1, 1.5, Nres) * k0  # graded

######################################################################
# Define the array of resonators

res_array = []
for i in range(Nres):
    res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))
# plt.plot(bk.array([r.omega_r for r in res_array]) / omegar0)
# plt.plot(omegars / omegar0, "o")

######################################################################
# Define the simulation object
simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)

#################################################################
# Grid for fields
x1 = bk.linspace(-dbx + positions[0][0], dbx + positions[-1][0], nptsx)
y1 = bk.linspace(-dby + positions[0][1], dby + positions[-1][1], nptsy)
x, y = bk.meshgrid(x1, y1, indexing="xy")

# plt.figure(figsize= (7,7))
if plot_det:
    #################################################################
    # Grid for complex frequency plane

    Nre, Nim = 200, 200

    # k0 = simu.wavenumber(omega0)
    # k1 = simu.wavenumber(omega1)
    # k0 = -3 - 3 * 1j
    # k1 = 3 + 3 * 1j

    # k_re = bk.linspace(k0.real, k1.real, Nre)
    # k_im = bk.linspace(k1.imag, k0.imag, Nim)
    # k_re_, k_im_ = bk.meshgrid(k_re, k_im, indexing="ij")
    # ks_complex = k_re_ + 1j * k_im_
    # omegas_re = simu.frequency(k_re)
    # omegas_im = -simu.frequency(k_im)
    # # omegas_complex = simu.frequency(ks_complex)

    omegas_re = bk.linspace(omega0.real, omega1.real, Nre)
    omegas_im = bk.linspace(omega1.imag, omega0.imag, Nim)
    omegas_re_, omegas_im_ = bk.meshgrid(omegas_re, omegas_im, indexing="ij")
    omegas_complex = omegas_re_ + 1j * omegas_im_

    #################################################################
    # Compute complex plane quantities

    Mc = simu.build_matrix(omegas_complex)
    # Mc = build_matrixk(simu,ks_complex)
    # Mc = bk.transpose(Mc, axes=(2, 3, 1, 0)).conj()
    Mc = bk.transpose(Mc, axes=(2, 3, 0, 1))
    det = bk.linalg.det(Mc)
    evsM = bk.linalg.eigvals(Mc)
    srt = bk.argsort(bk.abs(evsM), axis=-1)
    min_evs = bk.take_along_axis(evsM, srt, axis=-1)[:, :, 0]

    # min_evs = simu.plate.greens_function(omegas_complex,a)

    #################################################################
    # Plot complex plane quantities
    # plot_complex_plane_map(k_re, k_re_, bk.log10(bk.abs(det.T)))
    # plt.title(r"${\rm log}_{10} |{\rm det}\, M|$")
    # plot_complex_plane_map(k_re, k_re_, bk.log10(bk.abs(min_evs.T)))
    # plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")

    ax = plot_complex_plane_map(
        omegas_re, omegas_im, bk.log10(bk.abs(min_evs.T)), omegap=omegap
    )
    plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")
    plt.tight_layout()

    plt.pause(0.1)

# else:
#     ax = plt.gca()

# sys.exit(0)
#################################################################
# Eigenvalue problem

omegaT = simu.get_strength_poles()

# guesses = omegaT[omegaT.real > 0]  # /omegap

eigenvalues, eigenvectors = simu.eigensolve(
    omega0,
    omega1,
    recursive=recursive,
    lambda_tol=lambda_tol,
    max_iter=max_iter,
    guesses=guesses,
    N_guess_loc=N_guess_loc,
    func_gives_der=func_gives_der,
    init_vect=init_vect,
    weight=weight,
    Rloc=Rloc,
    peak_ref=peak_ref,
    verbose=True,
    strategy=strategy,
    plot_solver=plot_solver,
    scale=omegap,
)

singularity_Talpha = simu.get_strength_poles()
for res in simu.res_array:
    lres = plt.plot(res.omega_r.real / omegap, res.omega_r.imag / omegap, ".y", ms=3)
for s in singularity_Talpha:
    if s.real > 0:
        lsing = plt.plot(s.real / omegap, s.imag / omegap, "+b", ms=3)


######
# # plt.figure()
# # ax = plot_complex_plane_map(
# #     omegas_re, omegas_im, bk.log10(bk.abs(min_evs.T)), omegap=omegap, clim=(-2, 2)
# # )
# plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")
# plt.tight_layout()

# axin1 = ax.inset_axes([0.1, 0.1, 0.45, 0.45])
# for _ax in [ax, axin1]:
#     for res in simu.res_array:
#         lres = _ax.plot(
#             res.omega_r.real / omegap, res.omega_r.imag / omegap, ".y", ms=3
#         )
#     singularity_Talpha = simu.get_strength_poles()
#     for s in singularity_Talpha:
#         lsing = _ax.plot(s.real / omegap, s.imag / omegap, "+b", ms=3)

#     lev = _ax.plot(eigenvalues.real / omegap, eigenvalues.imag / omegap, "xr", ms=3)
#     labelcolor = "w" if plot_det else "k"
# plt.sca(ax)
# plt.legend(
#     [lev[0], lres[0], lsing[0]],
#     [
#         r"$\omega_n/\omega_p$",
#         r"$\omega_{R\alpha}/\omega_p$",
#         r"$\omega_{T\alpha}/\omega_p$",
#     ],
#     labelcolor=labelcolor,
# )
# plt.xlim(omega0.real / omegap, omega1.real / omegap)
# plt.ylim(omega0.imag / omegap, omega1.imag / omegap)
# plt.tight_layout()
# plt.pause(0.01)
# axin1.set_xlim(0.7, 1.1)
# axin1.set_ylim(-0.21, 0.03)
# axin1.set_xticklabels(axin1.get_xticklabels(), color="w", fontsize=10)
# axin1.set_yticklabels(axin1.get_yticklabels(), color="w", fontsize=10)

# savefig("det")

# keig = simu.wavenumber(eigenvalues)
# plt.figure()

# plt.plot(keig.real,keig.imag,"or")
# xsxs


# eigenvalues = eigenvalues[eigenvalues.real>0]
Nmodes = len(eigenvalues)
if Nmodes == 0:
    print("no modes found")
    sys.exit(0)

M = [simu.build_matrix(omega) for omega in eigenvalues]
dM = [simu.build_matrix_derivative(omega) for omega in eigenvalues]

# normalize modes
eigenvectors = simu.normalize_eigenvectors(eigenvalues, eigenvectors, dM)
multiplicities = simu.get_multiplicities(eigenvalues, M)
eigenvalues, eigenvectors = simu.update_eig(
    eigenvalues, eigenvectors, multiplicities, M, dM
)

# ######################################################
# # QNM expansion

# expo = float(sys.argv[3])
expo = -3


def arbitrary(omega):
    k = simu.wavenumber(omega)
    return k ** (expo)


omegas = bk.linspace(start, stop, nfreq)


if study == "ps":
    # point source
    incident = simu.point_source
    pos_source = (positions[0][0] - 0 * a / 1, 0)
    pos_probe = (x1[-1], 0)
    # pos_source = (0,0)
    # pos_probe = pos_source
    param_inc = pos_source

elif study == "pw":
    # plane wave
    incident = simu.plane_wave
    angle = 0
    param_inc = angle

else:
    n0 = 6  # mode to kill/select
    if control == "kill":
        # ---- kill mode n0
        psi0s = []
        for m0 in range(Nres):
            if m0 != n0:
                psi0 = (
                    eigenvectors[:, m0]
                    @ (M[m0] - M[n0])
                    / (eigenvalues[m0] - eigenvalues[n0])
                )
                rdn = 1
                psi0s.append(psi0 * rdn)
                # print(abs(psi0 @ eigenvectors[:, n0]))

        # sys.exit(0)
        psi0 = bk.sum(psi0s, axis=0)
    else:
        # ---- kill all modes appart from n0

        psi0s = []
        for m0 in range(Nres):
            if m0 != n0:
                psi0 = eigenvectors[:, m0]
                psi0s.append(psi0)

        psi0s = bk.array(psi0s)

        import scipy

        null = scipy.linalg.null_space(psi0s)
        psi0 = null[:, 0]

        rsources = (0.5 - bk.random.rand(Nres, 2)) * Nres * a
        rsourcesx = bk.ones(Nres) * -6
        rsourcesy = bk.linspace(-5, 5, Nres) + 1
        rsources = bk.stack([rsourcesx, rsourcesy]).T

    if excase == "ps":
        rsources = bk.array([res.position for res in simu.res_array])
        rsources[:, 0] = rsources[:, 0] + a / 2
        param_inc = rsources
    else:
        angles = bk.random.rand(Nres) * 2 * pi
        angles = bk.linspace(0, 1 / 1, Nres) * pi
        param_inc = angles

    Gmat = bk.zeros((nfreq, Nres, Nres), dtype=bk.complex128)
    for i in range(Nres):
        xi, yi = simu.res_array[i].position
        for j in range(Nres):
            if excase == "ps":
                xj, yj = rsources[j]
                r = kl.special.radial_coordinates(xi - xj, yi - yj)
                Gmat[:, i, j] = simu.plate.greens_function(omegas, r)
            else:
                Gmat[:, i, j] = simu.plane_wave(xi, yi, omegas, angles[j])

    p0 = bk.zeros((nfreq, Nres), dtype=bk.complex128)
    for i in range(nfreq):
        p0[i] = bk.linalg.solve(Gmat[i], psi0)

    p0_norma = bk.max(bk.abs(p0))
    p0 /= p0_norma

    def incident(x, y, omega, param_inc):
        Nres = len(param_inc)
        Gmat = bk.zeros((Nres, Nres), dtype=bk.complex128)
        for i in range(Nres):
            xi, yi = simu.res_array[i].position
            for j in range(Nres):
                if excase == "ps":
                    xj, yj = param_inc[j]
                    r = kl.special.radial_coordinates(xi - xj, yi - yj)
                    Gmat[i, j] = simu.plate.greens_function(omega, r)
                else:
                    Gmat[i, j] = simu.plane_wave(xi, yi, omega, param_inc[j])
        p0 = bk.linalg.solve(Gmat, psi0) / p0_norma
        out = 0
        for i in range(Nres):
            if excase == "ps":
                xi, yi = param_inc[i]
                r = kl.special.radial_coordinates(x - xi, y - yi)
                out += p0[i] * simu.plate.greens_function(omega, r)
            else:
                out += p0[i] * simu.plane_wave(x, y, omega, param_inc[i])
        return out

    psi_inc = bk.zeros((nfreq, Nres), dtype=bk.complex128)
    for i in range(nfreq):
        psi_inc[i] = simu.build_rhs(incident, omegas[i], param_inc)
    # print(psi0)
    # print(psi_inc)


## get expansion coeffs
t = -time.time()
bs = simu.get_expansion_coefficients(
    eigenvalues, eigenvectors, omegas, incident, param_inc, arbitrary
)
t += time.time()
print(t)

## compute Phi
direct = bk.zeros((nfreq, Nres), dtype=complex)
qmem = bk.zeros((nfreq, Nres), dtype=complex)
for i, omega in enumerate(omegas):
    direct[i] = simu.solve(incident, omega, param_inc)
    rec = 0
    for n in range(Nmodes):
        rec += bs[i, n] * eigenvectors[:, n]
    qmem[i] = rec
qmem = bk.array(qmem)


## check expansion coeffs direct
bs_direct = bk.zeros((nfreq, Nmodes), dtype=complex)
for i, omega in enumerate(omegas):
    M_omega = simu.build_matrix(omega)
    for n in range(Nmodes):
        omega_n = eigenvalues[n]
        bs_direct[i, n] = (
            direct[i]
            @ ((M[n] - M_omega) @ eigenvectors[:, n])
            / (omega_n - omega)
            * arbitrary(omega_n)
            / arbitrary(omega)
        )

# # ###########################################################
Nbmax = 8  # Nmodes
jplot = bk.argsort(bk.abs(eigenvalues.imag))[:Nplot]
jplot = jplot[bk.argsort(eigenvalues[jplot].real)]
jplot1 = jplot[start < eigenvalues[jplot].real]
jplot1 = jplot1[stop > eigenvalues[jplot1].real]


plot_expansion_coeffs(
    omegas,
    bs[:, jplot1[:Nbmax]],
    bs_direct[:, jplot1[:Nbmax]],
    omegap,
    "abs",
    modal=False,
)
plt.yscale("log")
plt.tight_layout()

savefig("plot_expansion_coeffs_abs")
plot_expansion_coeffs(
    omegas,
    bs[:, jplot1[:Nbmax]],
    bs_direct[:, jplot1[:Nbmax]],
    omegap,
    "phase",
    modal=False,
)
savefig("plot_expansion_coeffs_phase")

if study == "control":
    p0plot = p0
    plot_phis_opt(omegas, p0plot, omegap, "abs")
    savefig("plot_pbeta_abs")
    plot_phis_opt(omegas, p0plot, omegap, "phase")
    savefig("plot_pbeta_phase")


###########################################################


if (study == "pw") or (study == "control" and excase == "pw"):
    if study == "control" and excase == "pw":
        nrm = bk.abs(bk.sum(p0 * bk.cos(param_inc), axis=1)) ** 2
        # nrm = bk.sum(bk.abs(p0) ** 2, axis=1)
        # _y = bk.linspace(0,1,100)
        # nrm = bk.abs([incident(0,_y,omega,param_inc) for omega in omegas])**2
        # nrm = bk.trapz(nrm,_y)
    else:
        nrm = bk.ones(nfreq)
    angle = 0
    if study == "control" and excase == "pw":
        thetas = bk.linspace(0, 2 * pi, 199)

        mult_ff = 10 ** (5 / 2)

        ff = simu.get_far_field_radiation(direct, omegas, thetas) * mult_ff
        ff_modal = simu.get_far_field_radiation(qmem, omegas, thetas) * mult_ff
        scs = bk.trapz(abs(ff.T) ** 2, thetas) / 2 / pi
        scs_modal = bk.trapz(abs(ff_modal.T) ** 2, thetas) / 2 / pi

        fig, ax = plt.subplots(2, 1, figsize=(5, 5))
        plt.sca(ax[0])
        plt.pcolormesh(omegas / omegap, thetas, abs(ff) ** 2, cmap="inferno")

        for n in range(Nmodes):
            if start < eigenvalues[n].real < stop and eigenvalues[n].imag > min_im:
                plt.axvline(eigenvalues[n].real / omegap, c="w", ls=":", lw=0.8)
        ax[0].set_xticklabels([])
        ax[0].set_yticks([0, pi, 2 * pi], ["0", r"$\pi$", r"$2\pi$"])
        plt.colorbar(orientation="horizontal", location="bottom")
        plt.ylabel(r"$\theta$")
        plt.title(r"Far field $|f(\theta)|^2$")
        plt.xlim(0.7, 1.05)

        plt.sca(ax[1])
        l = plt.plot(omegas / omegap, scs, "-", label="scattering")
        plt.plot(omegas / omegap, scs_modal, "--", c="#d24949", label="modal")
        plt.xlabel(r"$\omega/\omega_p$")

        for n in range(Nmodes):
            if start < eigenvalues[n].real < stop and eigenvalues[n].imag > min_im:
                plt.axvline(eigenvalues[n].real / omegap, c="#8f8f8f", ls=":", lw=0.8)
        plt.legend()
        plt.ylabel(r"$\frac{1}{2\pi}\int_0^{2\pi} |f(\theta)|^2 {\rm d} \theta$")
        # plt.title("Scattering cross section")
        plt.xlim(0.7, 1.05)
        plt.tight_layout()

        savefig("scs")

    else:
        # nrm =simu.get_scattering_cross_section(p0, omegas)
        scs = simu.get_scattering_cross_section(direct, omegas) / nrm
        scs_modal = simu.get_scattering_cross_section(qmem, omegas) / nrm
        sce = simu.get_extinction_cross_section(direct, omegas, angle) / nrm
        sce_modal = simu.get_extinction_cross_section(qmem, omegas, angle) / nrm

        plt.figure()
        l = plt.plot(omegas / omegap, scs, "-", label="scattering")
        plt.plot(omegas / omegap, scs_modal, "--", c="#d24949", label="modal")
        plt.xlabel(r"$\omega/\omega_p$")
        plt.legend()
        plt.ylabel(r"$\sigma^{\rm s}$")
        plt.title("Scattering cross section")
        plt.tight_layout()

        plt.figure()
        l = plt.plot(omegas / omegap, sce, "-", label="scattering")
        plt.plot(omegas / omegap, sce_modal, "--", c="#d24949", label="modal")
        plt.xlabel(r"$\omega/\omega_p$")
        plt.legend()
        plt.ylabel(r"$\sigma^{\rm s}$")
        plt.title("Extinction cross section")
        plt.tight_layout()

        savefig("sce")


plot_phis(omegas, direct, qmem, omegap, "abs", modal=False)
savefig("plot_phis_abs")


# plot_phis(omegas, direct, qmem, omegap,"phase")
# savefig("plot_phis_phase")


err_mean_rec = bk.mean(bk.abs(direct - qmem) ** 2) ** 0.5
err_mean_rec /= bk.mean(bk.abs(direct) ** 2) ** 0.5
print("err_mean_rec = ", err_mean_rec)


# sys.exit(0)

###########################################################


# compute field along the array as a function of freq
direct_field = bk.zeros((nfreq, len(x1)), dtype=complex)
qmem_field = bk.zeros((nfreq, len(x1)), dtype=complex)
qmem_field_qnms = bk.zeros((nfreq, len(x1)), dtype=complex)

qnms_line = [
    simu.get_scattered_field(x1, 0 * x1, eigenvectors[:, n], eigenvalues[n])
    for n in range(Nmodes)
]

for i, omega in enumerate(omegas):
    if scattered_field_plt:
        direct_field[i] = simu.get_scattered_field(x1, 0 * x1, direct[i], omega)
        qmem_field[i] = simu.get_scattered_field(x1, 0 * x1, qmem[i], omega)
    else:
        k = simu.wavenumber(omega)
        if excase == "pw":
            _nrm = 1
        else:
            _nrm = 1j / (8 * k**2)
        direct_field[i] = (
            simu.get_field(x1, 0 * x1, incident, direct[i], omega, param_inc) / _nrm
        )
        qmem_field[i] = (
            simu.get_field(x1, 0 * x1, incident, qmem[i], omega, param_inc) / _nrm
        )

        qmem_field_qnms[i] = incident(x1, 0 * x1, omega, param_inc)
        for n in range(Nmodes):
            qmem_field_qnms[i] += bs[i, n] * qnms_line[n]

        qmem_field_qnms[i] /= _nrm

# ixprobe = 50

ixprobe = bk.argmin(bk.abs(positions[0][0] - x1))


plot_fields_omega(
    omegas,
    direct_field[:, ixprobe],
    qmem_field[:, ixprobe],
    scattered_field_plt,
    omegap,
)
# plt.plot(omegas / omegap, bk.abs(qmem_field_qnms[:, ixprobe]), ":k", label="QNMs")
plt.legend()
plt.title(rf"Field at $\boldsymbol{{R}}_p = ({x1[ixprobe]/a:.0f},0)a $")
title = (
    r"$|W^s(\boldsymbol{R}_p)|$"
    if scattered_field_plt
    else r"$|W(\boldsymbol{R}_p)/G(0)|$"
)
savefig("plot_fields_omega")


####### maps freq/x

fig, ax = plt.subplots()
plt.pcolormesh(x1 / a, omegas / omegap, abs(direct_field), rasterized=True)
for i in [0, -1]:
    plt.axvline(positions[i][0] / a, c="w", ls="--", lw=0.8)
for n in range(Nmodes):
    if start < eigenvalues[n].real < stop and eigenvalues[n].imag > min_im:
        plt.axhline(eigenvalues[n].real / omegap, c="w", ls=":", lw=0.8)
plt.xlabel(r"$x/a$")
plt.ylabel(r"$\omega/\omega_p$")

title1 = title + ", scattering"
plt.title(title1)
plt.colorbar()
savefig("plot_fields_omega_x_scatt")

fig, ax = plt.subplots()
plt.pcolormesh(x1 / a, omegas / omegap, abs(qmem_field), rasterized=True)
for i in [0, -1]:
    plt.axvline(positions[i][0] / a, c="w", ls="--", lw=0.8)
for n in range(Nmodes):
    if start < eigenvalues[n].real < stop and eigenvalues[n].imag > min_im:
        plt.axhline(eigenvalues[n].real / omegap, c="w", ls=":", lw=0.8)
plt.xlabel(r"$x/a$")
plt.ylabel(r"$\omega/\omega_p$")
title2 = title + ", modal"
plt.title(title2)
plt.colorbar()

savefig("plot_fields_omega_x_modal")

error = abs(direct_field - qmem_field)  # /bk.mean(abs(direct_field))

fig, ax = plt.subplots()
plt.pcolormesh(x1 / a, omegas / omegap, error, rasterized=True)
for i in [0, -1]:
    plt.axvline(positions[i][0] / a, c="w", ls="--", lw=0.8)
for n in range(Nmodes):
    if start < eigenvalues[n].real < stop and eigenvalues[n].imag > min_im:
        plt.axhline(eigenvalues[n].real / omegap, c="w", ls=":", lw=0.8)
plt.xlabel(r"$x/a$")
plt.ylabel(r"$\omega/\omega_p$")

title2 = r"error $|W- W^{\rm ref}|/|G(0)|$"

plt.title(title2)
plt.colorbar()

savefig("plot_fields_omega_x_error")

sys.exit(0)

###########################################################

imin_im = bk.argmin(abs(eigenvalues.imag))
omega = eigenvalues[imin_im]

omega = 0.78 * omegap
iomega = bk.argmin(bk.abs(omegas - omega))
# iomega = iomega0
# iomega=-1
omega = omegas[iomega]

# omega = eigenvalues[m0].real


qnms = [
    simu.get_scattered_field(x, y, eigenvectors[:, n], eigenvalues[n])
    for n in range(Nmodes)
]

if scattered_field_plt:
    direct_field_map = simu.get_scattered_field(x, y, direct[iomega], omega)
    qmem_field_map = simu.get_scattered_field(x, y, qmem[iomega], omega)
else:
    k = simu.wavenumber(omega)
    if excase == "pw":
        _nrm = 1
    else:
        _nrm = 1j / (8 * k**2)
    direct_field_map = (
        simu.get_field(x, y, incident, direct[iomega], omega, param_inc) / _nrm
    )
    qmem_field_map = (
        simu.get_field(x, y, incident, qmem[iomega], omega, param_inc) / _nrm
    )

    qmem_field_map_qnm = incident(x, y, omega, param_inc)
    for n in range(Nmodes):
        qmem_field_map_qnm += bs[iomega, n] * qnms[n]

    qmem_field_map_qnm /= _nrm


if excase == "ps":
    if study == "ps":
        pos_sources = [pos_source]
    else:
        pos_sources = rsources
else:
    pos_sources = []

# pos_sources = [pos_source]


# scatt_field_map = (
#         simu.get_scattered_field(x, y, direct[iomega], omega) / _nrm
#     )


# inc_field_map = direct_field_map - scatt_field_map


plot_field_maps(
    x1,
    y1,
    omega,
    direct_field_map,
    qmem_field_map,
    scattered_field_plt,
    res_array,
    pos_sources,
    omegap,
    a,
    "vertical",
)

title = (
    "Scattered field real part"
    if scattered_field_plt
    else "Normalized total field real part"
)
title = r"$\mathfrak{Re}\,W(\boldsymbol{R})/G(\omega,0)$"
title += rf", $\omega/\omega_p={omega/omegap:.3f}$"


errmap = abs(direct_field_map - qmem_field_map)  # /abs(direct_field_map)

fig, ax = plt.subplots(2, 1, figsize=(3.5, 4.5))
_ = ax[0].pcolormesh(
    x1 / a, y1 / a, bk.real(qmem_field_map), cmap="RdBu_r", rasterized=True
)
plt.colorbar(_)  # , orientation="horizontal", location="top")
_ = ax[1].pcolormesh(
    x1 / a,
    y1 / a,
    errmap,
    cmap="Reds",
    rasterized=True,
)
plt.colorbar(_)  # , orientation="horizontal", location="top")


# ax[0].annotate("modal",(0.05,0.85),xycoords='axes fraction',fontsize=20)


title = r"$\mathfrak{Re}\,(W/G(0))$"
Nres = len(res_array)

for i in range(Nres):
    xr, yr = res_array[i].position
    ax[0].plot(xr / a, yr / a, ".k", mew=0, ms=5)
    ax[1].plot(xr / a, yr / a, ".k", mew=0, ms=5)
ax[0].set_title(title)

title2 = r"error $|W- W^{\rm ref}|/|G(0)|$"

ax[1].set_title(title2)

for pos_source in pos_sources:
    ax[0].plot(pos_source[0] / a, pos_source[1] / a, "xk", mew=1, ms=5)
    ax[1].plot(pos_source[0] / a, pos_source[1] / a, "xk", mew=1, ms=5)
for _ax in ax:
    _ax.set_aspect(1)
ax[1].set_xlabel("$x/a$")
ax[0].set_ylabel("$y/a$")
ax[1].set_ylabel("$y/a$")

ax[0].set_xticklabels([])
plt.tight_layout()
plt.pause(0.01)


savefig("plot_field_maps_errors")


csdcsd


xsx

# inc_field_map = incident(x, y, omega,param_inc)

# plot_field_maps(
#     x1,
#     y1,
#     omega,
#     direct_field_map,
#     abs(inc_field_map),
#     scattered_field_plt,
#     res_array,
#     pos_sources,
#     omegap,
#     a,
# )


# plt.figure()
# plt.pcolormesh(x,y,bk.real(qmem_field_map_qnm),cmap=("RdBu_r"),rasterized=True)
# plt.colorbar()
# plt.axis("scaled")

err_mean_rec_field = bk.mean(bk.abs(direct_field_map - qmem_field_map) ** 2) ** 0.5
err_mean_rec_field /= bk.mean(bk.abs(direct_field) ** 2) ** 0.5
print("err_mean_rec_field = ", err_mean_rec_field)


###########################################################
plot_modes(
    simu, x, y, eigenvalues[jplot], eigenvectors[:, jplot], Ra=0.25, omegap=omegap, a=a
)

savefig("plot_modes")
sys.exit(0)


######################################################################
################################# QNMS ###############################
######################################################################


ytop = y1[-1]
ybottom = y1[0]
xright = x1[-1]
xleft = x1[0]


def get_mode(simu, x, y, phin, omegan):
    return simu._get_field(
        x, y, None, kl.special.greens_function_cartesian, phin, omegan
    )


def get_dmode_dk(simu, x, y, phin, omegan):
    return simu._get_field(
        x, y, None, kl.special.dgreens_function_cartesian_dk, phin, omegan
    )


def get_mode_prime(simu, x, y, phin, omegan):
    return get_dmode_dk(simu, x, y, phin, omegan) * simu.plate.dwavenumber_domega(
        omegan
    )


def get_grad_mode(simu, x, y, phin, omegan):
    def gx(k, x, y):
        return kl.special.grad_greens_function_cartesian(k, x, y)[0]

    def gy(k, x, y):
        return kl.special.grad_greens_function_cartesian(k, x, y)[1]

    out = lambda g: simu._get_field(x, y, None, g, phin, omegan)
    return bk.array([out(gx), out(gy)]).T


def get_dgrad_mode_dk(simu, x, y, phin, omegan):
    def gx(k, x, y):
        return kl.special.dgradg_dk(k, x, y)[0]

    def gy(k, x, y):
        return kl.special.dgradg_dk(k, x, y)[1]

    out = lambda g: simu._get_field(x, y, None, g, phin, omegan)
    return bk.array([out(gx), out(gy)]).T


def get_grad_mode_prime(simu, x, y, phin, omegan):
    return get_dgrad_mode_dk(simu, x, y, phin, omegan) * simu.plate.dwavenumber_domega(
        omegan
    )


def get_laplacian_mode(simu, x, y, phin, omegan):
    return simu._get_field(
        x, y, None, kl.special.laplacian_greens_function_cartesian, phin, omegan
    )


def get_dlaplacian_mode_dk(simu, x, y, phin, omegan):
    return simu._get_field(x, y, None, kl.special.dlapg_dk, phin, omegan)


def get_laplacian_mode_prime(simu, x, y, phin, omegan):
    return get_dlaplacian_mode_dk(
        simu, x, y, phin, omegan
    ) * simu.plate.dwavenumber_domega(omegan)


def get_grad_laplacian_mode(simu, x, y, phin, omegan):
    def gx(k, x, y):
        return kl.special.grad_laplacian_greens_function_cartesian(k, x, y)[0]

    def gy(k, x, y):
        return kl.special.grad_laplacian_greens_function_cartesian(k, x, y)[1]

    out = lambda g: simu._get_field(x, y, None, g, phin, omegan)
    return bk.array([out(gx), out(gy)]).T


def get_dgrad_laplacian_mode_dk(simu, x, y, phin, omegan):
    def gx(k, x, y):
        return kl.special.dgradlapg_dk(k, x, y)[0]

    def gy(k, x, y):
        return kl.special.dgradlapg_dk(k, x, y)[1]

    out = lambda g: simu._get_field(x, y, None, g, phin, omegan)
    return bk.array([out(gx), out(gy)]).T


def get_grad_laplacian_mode_prime(simu, x, y, phin, omegan):
    return get_dgrad_laplacian_mode_dk(
        simu, x, y, phin, omegan
    ) * simu.plate.dwavenumber_domega(omegan)


def boundary_term(W1, dW1, d2W1, d3W1, W2, dW2, d2W2, d3W2, n):
    bnd_term = d2W1 * (dW2 @ n) + W1 * (d3W2 @ n)
    bnd_term -= d2W2 * (dW1 @ n) + W2 * (d3W1 @ n)
    return bnd_term


def get_boundary_terms(simu, x, y, n, phi1, omega1, phi2, omega2):
    W1 = get_mode(simu, x, y, phi1, omega1)
    dW1 = get_grad_mode(simu, x, y, phi1, omega1)
    d2W1 = get_laplacian_mode(simu, x, y, phi1, omega1)
    d3W1 = get_grad_laplacian_mode(simu, x, y, phi1, omega1)
    W2 = get_mode(simu, x, y, phi2, omega2)
    dW2 = get_grad_mode(simu, x, y, phi2, omega2)
    d2W2 = get_laplacian_mode(simu, x, y, phi2, omega2)
    d3W2 = get_grad_laplacian_mode(simu, x, y, phi2, omega2)
    return boundary_term(W1, dW1, d2W1, d3W1, W2, dW2, d2W2, d3W2, n)


def boundary_term_prime(
    W1, W1_prime, d2W1, d2W1_prime, dW2, dW2_prime, d3W2, d3W2_prime, n
):
    bnd_term = d2W1_prime * (dW2 @ n) + d2W1 * (dW2_prime @ n)
    bnd_term += W1_prime * (d3W2 @ n) + W1 * (d3W2_prime @ n)
    return bnd_term * 1


def get_boundary_terms_prime(simu, x, y, n, phi1, omega1, phi2, omega2):
    W1 = get_mode(simu, x, y, phi1, omega1)
    W1_prime = get_mode_prime(simu, x, y, phi1, omega1)
    d2W1 = get_laplacian_mode(simu, x, y, phi1, omega1)
    d2W1_prime = get_laplacian_mode_prime(simu, x, y, phi1, omega1)
    dW2 = get_grad_mode(simu, x, y, phi2, omega2)
    dW2_prime = get_grad_mode_prime(simu, x, y, phi2, omega2)
    d3W2 = get_grad_laplacian_mode(simu, x, y, phi2, omega2)
    d3W2_prime = get_grad_laplacian_mode_prime(simu, x, y, phi2, omega2)
    return boundary_term_prime(
        W1, W1_prime, d2W1, d2W1_prime, dW2, dW2_prime, d3W2, d3W2_prime, n
    )


def get_resonators_terms(simu, phi1, omega1, phi2, omega2):
    res_term = 0
    for res in simu.res_array:
        xr, yr = res.position
        W1 = get_mode(simu, xr, yr, phi1, omega1)
        W2 = get_mode(simu, xr, yr, phi2, omega2)
        t1 = res.strength(omega1) / simu.plate.D
        t2 = res.strength(omega2) / simu.plate.D
        res_term += (t1 - t2) * W1 * W2
    return res_term


def get_resonators_terms_prime(simu, phi1, omega1, phi2, omega2):
    res_term = 0
    for res in simu.res_array:
        xr, yr = res.position
        W1 = get_mode(simu, xr, yr, phi1, omega1)
        W2 = get_mode(simu, xr, yr, phi2, omega2)
        t1prime = res.dstrength_domega(omega1) / simu.plate.D
        res_term += t1prime * W1 * W2
    return res_term


def get_resonators_term(simu, phi1, omega1, phi2, omega2):
    return_func = (
        get_resonators_terms_prime if omega1 == omega2 else get_resonators_terms
    )
    q = simu.plate.D / plate.rho / plate.h
    # coeff = q / (2 * omega1) if omega1 == omega2 else q / (omega1**2 - omega2**2)
    coeff = 1
    return return_func(simu, phi1, omega1, phi2, omega2) * coeff


def get_boundary_term(simu, phi1, omega1, phi2, omega2):
    top = dict(x=x1, y=ytop * bk.ones_like(x1), n=[0, 1], integ=x1)
    bottom = dict(x=x1, y=ybottom * bk.ones_like(x1), n=[0, -1], integ=x1)
    left = dict(x=xleft * bk.ones_like(y1), y=y1, n=[-1, 0], integ=y1)
    right = dict(x=xright * bk.ones_like(y1), y=y1, n=[1, 0], integ=y1)
    fbterm = get_boundary_terms_prime if omega1 == omega2 else get_boundary_terms
    q = simu.plate.D / plate.rho / plate.h
    # coeff = q / (2 * omega1) if omega1 == omega2 else q / (omega1**2 - omega2**2)
    coeff = 1
    contour = top, bottom, left, right
    bt = 0
    for pos in contour:
        integ = fbterm(simu, pos["x"], pos["y"], pos["n"], phi1, omega1, phi2, omega2)
        bt += bk.trapz(integ, pos["integ"])
    return bt * coeff


def get_surface_term(simu, phi1, omega1, phi2, omega2):
    W1 = get_mode(simu, x, y, phi1, omega1)
    W2 = get_mode(simu, x, y, phi2, omega2)
    q = simu.plate.D / plate.rho / plate.h
    coeff = q / (2 * omega1) if omega1 == omega2 else q / (omega1**2 - omega2**2)
    # coeff = 1
    return bk.trapz(bk.trapz(W1 * W2, x1), y1) / coeff


def get_scalar_product(simu, phi1, omega1, phi2, omega2):
    surf_term = get_surface_term(simu, phi1, omega1, phi2, omega2)
    line_term = get_boundary_term(simu, phi1, omega1, phi2, omega2)
    point_term = get_resonators_term(simu, phi1, omega1, phi2, omega2)
    # line_term = 0
    return surf_term + line_term + point_term


### check orthogonality

ortho = bk.zeros((Nmodes, Nmodes), dtype=bk.complex128)

for m in range(Nmodes):
    for n in range(Nmodes):
        phi1 = eigenvectors[:, m]
        phi2 = eigenvectors[:, n]
        omega1 = eigenvalues[m]
        omega2 = eigenvalues[n]
        sp = get_scalar_product(simu, phi1, omega1, phi2, omega2)
        ortho[m, n] = sp
        print(m, n, abs(sp))


ortho_norm = bk.zeros((Nmodes, Nmodes), dtype=bk.complex128)

for m in range(Nmodes):
    for n in range(Nmodes):
        ortho_norm[m, n] = ortho[m, n] / ortho[m, m] ** 0.5 / ortho[n, n] ** 0.5

        print(m, n, (ortho_norm[m, n]))

fig, ax = plt.subplots(1, 2, figsize=(9, 3.6))
_map = ax[0].imshow(bk.real(ortho_norm))
ax[0].set_title(r"Re $<W_m,W_n>$")
ax[0].set_xlabel("$m$")
ax[0].set_ylabel("$n$")
plt.colorbar(_map)
_map = ax[1].imshow(abs(bk.eye(Nmodes) - ortho_norm))
ax[1].set_title(r"$|I - <W_m,W_n>|$")
plt.colorbar(_map)
ax[1].set_xlabel("$m$")
ax[1].set_ylabel("$n$")
plt.tight_layout()
xsx

### QNM expansion (continuous)

normas_QNMs = bk.zeros(Nmodes, dtype=bk.complex128)
QNMs = []
for n in range(Nmodes):
    omegan = eigenvalues[n]
    phin = eigenvectors[:, n]
    Weig = get_mode(simu, x, y, phin, omegan)
    normas_QNMs[n] = get_scalar_product(simu, phin, omegan, phin, omegan)
    QNMs.append(Weig / normas_QNMs[n] ** 0.5)
QNMs = bk.array(QNMs)


# ###########################################################
# plot_modes(
#     simu,
#     x,
#     y,
#     eigenvalues[jplot],
#     eigenvectors[:, jplot],
#     Ra=0.25,
#     omegap=omegap,
#     a=a,
#     QNMs=QNMs,
# )

q = simu.plate.D / plate.rho / plate.h

rec_QNM = 0
inc = incident(x, y, omega, param_inc)
for n in range(Nmodes):
    omegan = eigenvalues[n]
    phin = eigenvectors[:, n]
    overlap = 0
    for res in simu.res_array:
        t_alpha = res.strength(omega) / simu.plate.D
        r_alpha = res.position
        Wn_alpha = get_mode(simu, *r_alpha, phin, omegan) / normas_QNMs[n] ** 0.5
        Winc_alpha = incident(*r_alpha, omega, param_inc)
        # Winc_alpha = bk.trapz(bk.trapz(inc, x1), y1)
        overlap += t_alpha * Winc_alpha * Wn_alpha
        # overlap += Winc_alpha*t_alpha * bk.trapz(bk.trapz(1*QNMs[n], x1), y1)
    # overlap = 1 * bk.trapz(bk.trapz(inc*QNMs[n], x1), y1)

    q = simu.plate.D / plate.rho / plate.h
    coeff = q / (2 * omega)
    coeff = 1

    rec_QNM += (
        overlap / (omega - omegan) * arbitrary(omegan) / arbitrary(omega) * QNMs[n]
    ) * coeff


sOmega = (x1[-1] - x1[0]) * (y1[-1] - y1[0])
# rec_QNM /= sOmega

#### total or scattered field?

if scattered_field_plt:
    _nrm = 1
else:
    inc_field = incident(x, y, omega, param_inc)
    rec_QNM += inc_field
    k = simu.wavenumber(omega)
    if excase == "pw":
        _nrm = 1
    else:
        _nrm = 1j / (8 * k**2)

rec_QNM /= _nrm


plt.close("all")

fig, ax = plt.subplots(1, 1)
_ = plt.pcolormesh(x, y, bk.abs(qmem_field_map), cmap="RdBu_r", rasterized=True)
plt.colorbar(_)  # , orientation="horizontal", location="top")
plt.title("QMEM discrete")
ax.set_aspect(1)

fig, ax = plt.subplots(1, 1)
_ = plt.pcolormesh(x, y, bk.abs(direct_field_map), cmap="RdBu_r", rasterized=True)
plt.colorbar(_)  # , orientation="horizontal", location="top")
plt.title("Scattering")
ax.set_aspect(1)


fig, ax = plt.subplots(1, 1)
_ = plt.pcolormesh(x, y, bk.abs(rec_QNM), cmap="RdBu_r", rasterized=True)
plt.colorbar(_)  # , orientation="horizontal", location="top")
plt.title("QMEM continuous")
ax.set_aspect(1)


# fig, ax = plt.subplots(1, 1)
# _ = plt.pcolormesh(x, y, bk.abs(rec_QNM)/bk.abs(direct_field_map), cmap="RdBu_r", rasterized=True)
# plt.colorbar(_)  # , orientation="horizontal", location="top")
# plt.title("ratio")
# ax.set_aspect(1)

# fig, ax = plt.subplots(1, 1)
# _ = plt.pcolormesh(x, y, bk.abs(qmem_field_map)/bk.abs(direct_field_map), cmap="RdBu_r", rasterized=True)
# plt.colorbar(_)  # , orientation="horizontal", location="top")
# plt.title("ratio")
# ax.set_aspect(1)
