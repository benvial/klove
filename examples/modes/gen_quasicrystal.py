#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove
import sys

import numpy as np
from penrose import BtileL, BtileS, PenroseP3

ngen = int(sys.argv[1])

scale = 1

tiling = PenroseP3(scale, ngen=ngen)

theta = np.pi / 5
alpha = np.cos(theta)
rot = np.cos(theta) + 1j * np.sin(theta)
A1 = scale + 0.0j
B = 0 + 0j
C1 = C2 = A1 * rot
A2 = A3 = C1 * rot
C3 = C4 = A3 * rot
A4 = A5 = C4 * rot
C5 = -A1
tiling.set_initial_tiles(
    [
        BtileS(A1, B, C1),
        BtileS(A2, B, C2),
        BtileS(A3, B, C3),
        BtileS(A4, B, C4),
        BtileS(A5, B, C5),
    ]
)
tiling.make_tiling()
# tiling.write_svg('example4.svg')
vertices = []
for e in tiling.elements:
    vertices.append(e.A)
    vertices.append(e.B)
    vertices.append(e.C)

vertices = np.unique(vertices)

vertices = np.stack([vertices.real, vertices.imag]).T
Nres = len(vertices)

np.savez(f"data/quasicrystal_{Nres}.npz", vertices=vertices)

# import matplotlib.pyplot as plt
# print(len(vertices))
# plt.ion()
# plt.close("all")
# for v in vertices:
#     plt.plot(*v, "or")
# plt.axis("equal")
