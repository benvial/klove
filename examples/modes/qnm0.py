#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

"""
Finite cluster of resonators
==============================================

Finding modes.
"""

import sys
import time
from math import pi

import matplotlib.pyplot as plt
import tetrachotomy as tt

import klove as kl

# import scienceplots

# plt.style.use('science')

plt.ion()
plt.close("all")

bk = kl.backend

colorcycle = plt.rcParams["axes.prop_cycle"].by_key()["color"]


#################################################################
# Parameters

Nres = int(sys.argv[1])
alternative = True

a = 1

omega0 = 0.01 - 0.1 * 1j
omega1 = 0.33 - 1e-12 * 1j
# omega0 = 0.4 - 0.3 * 1j
# omega1 = 2- 1e-14 * 1j
# omega0 = 0.15 - 0.06 * 1j
# omega1 = 0.25 - 1e-14 * 1j
#################################################################
# Plots

plot_det = True
plot_solver = False
plot_targets = False
anim = False

params = bk.linspace(0.001, 0.01, 50)

params = [211.7]
params = [100]
# params = bk.linspace(210,215,11)
# params = bk.logspace(1.5,3.5,50)
# params = bk.linspace(0.0022, 0.018, 155)
params = bk.logspace(-1, 2, 50)
# params = bk.linspace(1, 3, 250)

params = [1]

fig = plt.figure()
# plt.axis("scaled")
if anim:
    kl.viz.init_frame_folder()


for ipar, par in enumerate(params):
    print(par)
    print("-------")
    ######################################################################
    # Elastic plate
    plate = kl.ElasticPlate(a * 1, 1, 1, 0.0)
    p = 256 * plate.rho * plate.h * plate.D

    omega_plate0 = plate.omega0(a)
    m_plate = plate.rho * a**2 * plate.h

    #################################################################
    # Cluster
    omegar0 = omega_plate0 * 5
    omegar0 = 0.31

    m0 = m_plate * 5
    # k0 = 0.001
    # m0=k0/omegar0**2
    k0 = omegar0**2 * m0

    print(k0)
    # omegar0 = omega_plate0 * k0 * a / (16 * plate.D) /0.2
    # m0 = k0 / omegar0**2

    # plate = kl.ElasticPlate(a * par, 1, 1, 0.3)

    # m0 = 2.7e-2
    # k0 = 9.593e4

    #### line array
    positionsx = bk.linspace(-Nres * a / 2, Nres * a / 2, Nres) * 1
    positions = [(_x, 0) for _x in positionsx]

    # sys.exit(0)
    # m0 = plate.rho * a**2 * plate.h * 1
    # distrib =  bk.ones(Nres)
    grading_ratio = 0.5
    distrib = bk.linspace(0.0, 1.0, Nres) + 0.0
    distrib = 1 + 2 * distrib**0.5
    # distrib =  bk.linspace(0,1,Nres)
    # distrib =  bk.linspace(-3,3,Nres)
    # distrib = 2*bk.arctan(distrib) + 4

    omegaT_real = omegar0 * distrib
    masses = k0 / (omegaT_real**2 + k0**2 / p)
    # stiffnesses = k0 * distrib
    # stiffnesses = k0 * bk.ones(Nres)
    # distrib = bk.linspace(0.0, 1.0, Nres)
    # distrib = 1 + 1 * distrib
    # omegars = 1*omegar0 * distrib
    # masses = bk.ones(Nres) * m0
    distribQ = bk.linspace(1, 2, Nres)
    Q = 20 * distribQ**2

    omegars = omegaT_real * (1 + 4 * Q**2) ** 0.5 / (2 * Q)
    stiffnesses = omegars * (p / (1 + 4 * Q**2)) ** 0.5
    masses = stiffnesses / (omegaT_real**2 + stiffnesses**2 / p)

    masses = bk.ones(Nres) * m0
    stiffnesses = bk.ones(Nres) * k0
    stiffnesses = bk.linspace(0.9, 1.1, Nres) * k0

    # masses = 1/omegars * (p / (1 + 4 * Q**2))**0.5
    # stiffnesses = masses*omegars**2
    # stiffnesses = bk.flipud(stiffnesses)
    # # masses = masses * distrib

    # t = bk.linspace(0, 2 * pi, Nres + 1)[:-1]
    # Qc = 20
    # omega_c = 2*omegar0*(1-1j/2/Qc)
    # R = 0.01
    # omegan_tar = omega_c + R*bk.exp(1j*t)
    # omegaT_real = omegan_tar.real
    # Q = -omegan_tar.real/(2*omegan_tar.imag)

    # omegars = omegaT_real * (1 + 4 * Q**2)**0.5 / (2 * Q)
    # stiffnesses = omegars * (p / (1 + 4 * Q**2)) ** 0.5
    # masses = stiffnesses / (omegaT_real**2 + stiffnesses**2 / p)

    # # #### ring
    # r0 = a/2
    # t = bk.linspace(0, 2 * pi, Nres + 1)[:-1]
    # positions = bk.stack([r0 * bk.cos(t), r0 * bk.sin(t)]).T

    ######################################################################
    # Define the array of resonators

    res_array = []
    for i in range(Nres):
        res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))
    # res_array = bk.flipud(res_array)
    ######################################################################
    # Define the simulation object
    simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)

    #####################

    singularity_Talpha = simu.get_strength_poles()
    print(singularity_Talpha[0])
    #################################################################
    # Grid for fields

    nptsx = 301
    nptsy = 101
    # nptsy = int(nptsx / Nres)
    dbx = 2 * a
    dby = 5 * a

    x1 = bk.linspace(-dbx + positions[0][0], dbx + positions[-1][0], nptsx)
    y1 = bk.linspace(-dby + positions[0][1], dby + positions[-1][1], nptsy)

    # mini = min(bk.hstack([positions[:,0], positions[:,1]]))
    # maxi = max(bk.hstack([positions[:,0], positions[:,1]]))
    # lim = max(abs(maxi), abs(mini))
    # x1 = bk.linspace(-dbx - lim, dbx + lim, nptsx)
    # y1 = x1
    x, y = bk.meshgrid(x1, y1, indexing="xy")

    # omega = 0.6

    # incident = simu.point_source

    # pos_source = (x1[0], 0)
    # pos_probe = (x1[-1], 0)
    # # pos_source = (0,0)
    # # pos_probe = pos_source
    # param_inc = pos_source

    # sol = simu.solve(incident, omega, param_inc)

    # W = simu.get_field(x, y, incident, sol, omega, param_inc)
    # plt.figure()
    # plt.pcolormesh(x, y, bk.real(W), cmap="RdBu_r")
    # for res in res_array:
    #     plt.plot(*res.position, ".k")
    # plt.plot(*pos_source, "xk")
    # plt.colorbar(orientation="horizontal")
    # plt.axis("scaled")
    # plt.xlabel("$x$")
    # plt.ylabel("$y$")
    # plt.title(f"$\omega={omega:0.2f}$")
    # plt.show()

    # sys.exit(0)

    #################################################################
    # Grid for complex frequency plane

    Nre, Nim = 100, 100

    omegas_re = bk.linspace(omega0.real, omega1.real, Nre)
    omegas_im = bk.linspace(omega1.imag, omega0.imag, Nim)
    omegas_re_, omegas_im_ = bk.meshgrid(omegas_re, omegas_im, indexing="ij")

    #################################################################
    # Compute complex plane quantities

    omegas_complex = omegas_re_ + 1j * omegas_im_
    Mc = simu.build_matrix(omegas_complex)
    Mc = bk.transpose(Mc, axes=(2, 3, 0, 1))

    det = bk.linalg.det(Mc)
    evs = bk.linalg.eigvals(Mc)
    srt = bk.argsort(bk.abs(evs), axis=-1)
    min_evs = bk.take_along_axis(evs, srt, axis=-1)[:, :, 0]

    #################################################################
    # Plot complex plane quantities
    if plot_det:
        # plt.figure()
        # plt.pcolormesh(omegas_re, omegas_im, bk.log10(bk.abs(det.T)))
        # plt.colorbar()
        # plt.xlabel("Re $\omega$")
        # plt.ylabel("Im $\omega$")
        # plt.title(r"${\rm log}_{10} |{\rm det}\, M|$")
        # ax_det = plt.gca()

        plt.figure()
        plt.pcolormesh(
            omegas_re, omegas_im, bk.log10(bk.abs(min_evs.T)), cmap="inferno"
        )
        plt.colorbar()
        plt.xlabel("Re $\omega$")
        plt.ylabel("Im $\omega$")
        plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")
        ax_min_ev = plt.gca()
        plt.pause(0.1)

    def func(omega):
        M = simu.build_matrix(omega)
        det = bk.linalg.det(M)
        return det

    # def null(M):
    #     e, v = bk.linalg.eig(M)
    #     srt = bk.argsort(bk.abs(e))
    #     return v[:, srt[0]]  # .conj()

    # def get_residual(z):
    #     M = simu.build_matrix(z)
    #     phi = null(M)
    #     return bk.abs(phi @ M @ phi)

    #################################################################
    # Eigenvalue problem

    method = "tetrachotomy"
    # method = "polze"
    # method = "cxroots"
    method = "rayleigh"

    if method == "tetrachotomy":
        # Cauchy integrals

        options = tt.get_options()
        zeros, residues, ncut, neval = tt.find_zeros(
            func, omega0, omega1, order=1, options=options
        )

    elif method == "rayleigh":
        refine = True
        lambda_tol = 1e-12
        max_iter = 100
        peak_ref = 10
        evs0, eigenvectors0 = simu.eigensolve(
            omega0,
            omega1,
            lambda_tol=lambda_tol,
            max_iter=max_iter,
            # strategy="grid",
            N_guess_loc=0,
            # filter=False,
            peak_ref=peak_ref,
            verbose=True,
        )
        if refine:
            lambda_tol_ref = max(lambda_tol / 10, 1e-14)
            max_iter_ref = max_iter * 2
            evs, eigenvectors = simu.eigensolve(
                omega0,
                omega1,
                guesses=evs0,
                lambda_tol=lambda_tol_ref,
                max_iter=max_iter_ref,
                N_guess_loc=1,
                verbose=True,
            )
        else:
            evs, eigenvectors = evs0, eigenvectors0

    else:
        raise ValueError(f"Wrong method {method}")

    # print_results(evs0, res0, message=None)
    # print_results(evs, res, message="refined")

    # plt.figure()
    # ax=plt.gca()

    # evs = bk.sort(evs)

    # for ax in [ax_min_ev,ax]:
    # plt.sca(ax)

    lines = plt.gca().get_lines()
    for l in lines:
        l.set_alpha(0.2)

    for s in singularity_Talpha:
        plt.plot(s.real, s.imag, ".b", ms=5)
    plt.plot(evs.real, evs.imag, ".r", ms=4)

    plt.xlim(omega0.real, omega1.real)
    plt.ylim(omega0.imag, omega1.imag)

    plt.xlabel("Re $\omega$")
    plt.ylabel("Im $\omega$")
    plt.tight_layout()
    plt.pause(0.01)
    if anim:
        kl.viz.save_frame(ipar)

if anim:
    kl.viz.make_gif()
# sys.exit(0)
if plot_targets:
    plt.figure()
    res_freqs = bk.array([r.omega_r for r in simu.res_array])
    plt.plot(omegaT_real, "sk", label="target")
    plt.plot(singularity_Talpha.real, "-o", label="$\omega_T$")
    plt.plot(evs.real, "--+", label="$\omega_n$")
    # plt.plot(res_freqs.real, ":^", label="$\omega_r$")
    plt.xlabel("resonator number")
    plt.title("resonant frequency")
    plt.legend()
    plt.tight_layout()

    plt.figure()
    Qns = -evs.real / (2 * evs.imag)
    Qns_singularity = -singularity_Talpha.real / (2 * singularity_Talpha.imag)
    plt.plot(Q, "sk", label="target")
    plt.plot(Qns_singularity, "-o", label="$Q_T$")
    plt.plot(Qns, "--+", label="$Q_n$")
    plt.title("quality factor")
    plt.xlabel("resonator number")
    plt.legend()
    plt.tight_layout()

    evs_target = omegaT_real * (1 - 1j / (2 * Q))

    plt.figure()
    plt.plot(evs_target.real, evs_target.imag, "sk", label="target")
    plt.plot(singularity_Talpha.real, singularity_Talpha.imag, "o", label="$\omega_T$")
    plt.plot(evs.real, evs.imag, "+", label="$\omega_n$")
    plt.title("complex frequency")
    plt.legend()

    plt.xlabel("Re $\omega$")
    plt.ylabel("Im $\omega$")
    # plt.axis("scaled")
    plt.tight_layout()


# evs = evs[evs.real>0]
Nmodes = len(evs)
if Nmodes == 0:
    print("no modes found")
    sys.exit(0)
######################################################
# QNM expansion

# simu.alternative = False


incident = simu.point_source

pos_source = (x1[0], 0)
pos_probe = (x1[-1], 0)
# pos_source = (0,0)
# pos_probe = pos_source
param_inc = pos_source


# angle = 0
# param_inc = angle
# incident = simu.plane_wave


# ires = 2
# pos_source = res_array[ires].position
# pos_probe = pos_source

nfreq = 1000
start = max(omega0.real, 1e-12)
stop = omega1.real
start = 0.05
stop = 0.5
omegas = bk.linspace(start, stop, nfreq)


gammap = (plate.D / (plate.rho * plate.h)) ** 0.5

expo = float(sys.argv[2])


def arbitrary(omega):
    k = simu.wavenumber(omega)
    return k**expo
    # return 1
    # return 1/omega**(3/2)
    # return 1 / k**(Nres+1)
    # return 1/(8 * omega**3 / gammap / (1j * omega - 16 * gammap))
    # return 1#k**3
    # return kl.special.greens_function(k,0)/k
    # return simu.res_array[0].strength(omega)
    # return 1/simu.res_array[0].strength(omega)
    # return 1 / omega**1.5
    # return  kl.special.greens_function(k,0)/omega


gammas = bk.zeros((nfreq, Nmodes), dtype=complex)
M = [simu.build_matrix(omega) for omega in evs]
dM = [simu.build_matrix_derivative(omega) for omega in evs]

# normalize

eigenvectors = simu.normalize_eigenvectors(evs, eigenvectors)


# omegas = omegas + 0j

for i, omega in enumerate(omegas):
    gamma = bk.zeros(Nmodes, dtype=complex)
    phi0_vec = simu.build_rhs(incident, omega, param_inc)

    for n in range(Nmodes):
        omegan = evs[n]

        num = phi0_vec @ eigenvectors[:, n]
        gamma[n] = num / (omega - omegan) * arbitrary(omegan) / arbitrary(omega)
        # gamma[n] = num  *simu.build_matrix(omega)[0,0]/simu.build_matrix_derivative(omegan)[0,0]
        # M = simu.build_matrix(omega)
        # invM = bk.linalg.inv(M)

    gammas[i] = gamma

gammas = bk.array(gammas)

direct = bk.zeros((nfreq, Nres), dtype=complex)
qmem = bk.zeros((nfreq, Nres), dtype=complex)


t0 = -time.time()

for i, omega in enumerate(omegas):
    direct[i] = simu.solve(incident, omega, param_inc)
t0 += time.time()

t1 = -time.time()
for i, omega in enumerate(omegas):
    rec = 0
    for n in range(Nmodes):
        rec += gammas[i, n] * eigenvectors[:, n]

    qmem[i] = rec

qmem = bk.array(qmem)
t1 += time.time()
print(t0, t1)

# ortho = bk.zeros((Nmodes, Nmodes), dtype=complex)
# for m in range(Nmodes):
#     for n in range(Nmodes):
#         if m == n:
#             ortho[m, n] = eigenvectors[:, m] @ (dM[n] @ eigenvectors[:, n])
#         else:
#             ortho[m, n] = (
#                 eigenvectors[:, m]
#                 @ ((M[n] - M[m]) @ eigenvectors[:, n])
#                 / (evs[n] - evs[m])
#             )

# plt.figure()
# plt.imshow(abs(ortho))
# plt.colorbar()

gammas_direct = bk.zeros((nfreq, Nmodes), dtype=complex)

for i, omega in enumerate(omegas):
    M_omega = simu.build_matrix(omega)

    for n in range(Nmodes):
        omega_n = evs[n]
        gammas_direct[i, n] = (
            direct[i]
            @ ((M[n] - M_omega) @ eigenvectors[:, n])
            / (omega_n - omega)
            * arbitrary(omega_n)
            / arbitrary(omega)
        )

# # ###########################################################
# nplt = int(bk.ceil(Nmodes**0.5))

# fig, ax = plt.subplots(nplt, nplt, figsize=(10, 8))
# if nplt == 1:
#     ax = [ax]
# else:
#     ax = ax.ravel()
# maxiy = bk.max(abs(gammas_direct))
# for n in range(Nmodes):
#     l = ax[n].plot(
#         omegas, abs(gammas_direct[:, n]), "-", lw=3, c="red", label="scattering"
#     )
#     ax[n].plot(
#         omegas, abs(gammas[:, n]), "--", c="k", label="modal"
#     )  # ,c=l[0].get_color())
#     ax[n].set_xlabel("$\omega$")
#     ax[n].set_ylabel("$|b_n|$")
#     ax[n].set_ylim(0, maxiy)
#     ax[n].set_xlim(0, omegas[-1])
#     omega_n = evs[n]
#     ax[n].set_title(f"$\omega_{{{n+1}}}={omega_n:.3f}$")
# ax[0].legend()
# for ax_ in ax[n + 1 :]:
#     ax_.remove()
# plt.tight_layout()

# ###########################################################
fig, ax = plt.subplots(1, 1, figsize=(7, 7))
ax = [ax]

for n in range(Nmodes):
    l = ax[0].plot(omegas, abs(gammas_direct[:, n]), "-", label="scattering")
    ax[0].plot(
        omegas, abs(gammas[:, n]), "--", c=l[0].get_color(), label="modal"
    )  # ,c=l[0].get_color())
ax[0].set_xlabel("$\omega$")
ax[0].set_ylabel("$|b_n|$")
ax[0].set_xlim(0, omegas[-1])
ax[0].legend()
plt.tight_layout()

# fig, ax = plt.subplots(1, 1, figsize=(10, 8))
# ax=[ax]

# for n in range(Nmodes):
#     l = ax[0].plot(
#         omegas, abs(gammas_direct[:,n]-gammas[:,n]), "-", label="scattering"
#     )
# ax[0].set_xlabel("$\omega$")
# ax[0].set_ylabel("$|error b_n|$")
# ax[0].set_xlim(0, omegas[-1])
# ax[0].legend()
# plt.tight_layout()

###########################################################
ks = simu.wavenumber(omegas)

plt.figure()
for i in range(Nres):
    # res=simu.res_array[i]
    # T = res._strength(omega)
    l = plt.plot(omegas, bk.abs(direct[:, i]), "-", label=rf"scattering $\alpha$={i+1}")
    plt.plot(
        omegas,
        bk.abs(qmem[:, i]),
        "--",
        c=l[0].get_color(),
        label=rf"modal $\alpha$={i+1}",
    )
plt.xlabel("$\omega$")
plt.ylabel(r"$|\phi_\alpha|$")
plt.legend()
plt.tight_layout()

plt.figure()
for i in range(Nres):
    l = plt.plot(
        omegas,
        bk.unwrap(bk.angle(direct[:, i])),
        "-",
        label=rf"scattering $\alpha$={i+1}",
    )
    plt.plot(
        omegas,
        bk.unwrap(bk.angle(qmem[:, i])),
        "--",
        c=l[0].get_color(),
        label=rf"modal $\alpha$={i+1}",
    )
plt.xlabel("$\omega$")
plt.ylabel(r"${\rm arg}(\phi_\alpha)$")
plt.legend()
plt.tight_layout()


###########################################################
err_mean_rec = bk.mean(bk.abs(direct - qmem) ** 2) ** 0.5
err_mean_rec /= bk.mean(bk.abs(direct) ** 2) ** 0.5
print("err_mean_rec = ", err_mean_rec)

# sys.exit(0)

scattered_field_plt = False


direct_field = bk.zeros((nfreq), dtype=complex)
qmem_field = bk.zeros((nfreq), dtype=complex)

for i, omega in enumerate(omegas):
    if scattered_field_plt:
        direct_field[i] = simu.get_scattered_field(*pos_probe, direct[i], omega)
        qmem_field[i] = simu.get_scattered_field(*pos_probe, qmem[i], omega)
    else:
        k = simu.wavenumber(omega)
        if incident.__name__ == "plane_wave":
            _nrm = 1
        else:
            _nrm = 1j / (8 * k**2)
        # _nrm = 1/(4*k**3/bk.pi)
        direct_field[i] = (
            simu.get_field(*pos_probe, incident, direct[i], omega, param_inc) / _nrm
        )
        qmem_field[i] = (
            simu.get_field(*pos_probe, incident, qmem[i], omega, param_inc) / _nrm
        )

plt.figure()
# l = plt.plot(omegas, bk.abs(direct_field), "-", label="scattering")
# plt.plot(omegas, bk.abs(qmem_field), "--", c=l[0].get_color(), label="modal")
# plt.xlabel("$\omega$")
# ylabel = (
#     r"$|W^s(\boldsymbol{R}_p)|$"
#     if scattered_field_plt
#     else r"$|W(\boldsymbol{R}_p)/G_0(0)|$"
# )

l = plt.plot(omegas, bk.abs(direct_field), "-", label="scattering")
plt.plot(omegas, bk.abs(qmem_field), "--", c=l[0].get_color(), label="modal")
plt.xlabel("$\omega$")
ylabel = (
    r"$|W^s(\boldsymbol{R}_p)|$"
    if scattered_field_plt
    else r"$|W(\boldsymbol{R}_p)/G_0(0)|$"
)

title = (
    "Scattered field norm at probe position"
    if scattered_field_plt
    else "Normalized total field norm at probe position"
)
plt.title(title)
plt.ylabel(ylabel)
plt.legend()
plt.tight_layout()


# omega = evs[2].real
# omega = simu.res_array[ires].omega_r
omega = omegar0 * 1.1
omega = 0.22
iomega = bk.argmin(bk.abs(omegas - omega))
omega = omegas[iomega]

if scattered_field_plt:
    direct_field_map = simu.get_scattered_field(x, y, direct[iomega], omega)
    qmem_field_map = simu.get_scattered_field(x, y, qmem[iomega], omega)
else:
    k = simu.wavenumber(omega)
    if incident.__name__ == "plane_wave":
        _nrm = 1
    else:
        _nrm = 1j / (8 * k**2)
    direct_field_map = (
        simu.get_field(x, y, incident, direct[iomega], omega, param_inc) / _nrm
    )
    qmem_field_map = (
        simu.get_field(x, y, incident, qmem[iomega], omega, param_inc) / _nrm
    )

fig, ax = plt.subplots(2, 1)
_ = ax[0].pcolormesh(x1, y1, bk.real(direct_field_map), cmap="RdBu_r")
plt.colorbar(_)
_ = ax[1].pcolormesh(x1, y1, bk.real(qmem_field_map), cmap="RdBu_r")
plt.colorbar(_)

title = (
    "Scattered field real part"
    if scattered_field_plt
    else "Normalized total field real part"
)
title += rf", $\omega={omega:.3f}$"

for i in range(Nres):
    xr, yr = res_array[i].position
    ax[0].plot(xr / a, yr / a, ".k")
    ax[1].plot(xr / a, yr / a, ".k")
ax[0].set_title("direct")
ax[1].set_title("modal")
ax[0].set_aspect(1)
ax[1].set_aspect(1)
plt.suptitle(title)
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.tight_layout()
plt.pause(0.01)


err_mean_rec_field = bk.mean(bk.abs(direct_field_map - qmem_field_map) ** 2) ** 0.5
err_mean_rec_field /= bk.mean(bk.abs(direct_field) ** 2) ** 0.5
print("err_mean_rec_field = ", err_mean_rec_field)

sys.exit(0)


fig, ax = plt.subplots(nplt, nplt, figsize=(20, 8))
if nplt == 1:
    ax = [ax]
else:
    ax = ax.ravel()


# plt.close("all")
Ra = dbx * 0.5
for n in range(Nmodes):
    figx = 9
    ratio = (x1[-1] - x1[0]) / (y1[-1] - y1[0]) * 0.8
    # plt.figure(figsize=(figx, figx / ratio))
    omega_n = evs[n]

    phin = eigenvectors[:, n].copy()
    iphase = 0
    maxvect = bk.max(bk.abs(phin))
    scale = Ra / maxvect
    phin *= scale

    phin *= bk.exp(-1j * bk.angle(phin[iphase]))

    Weig = simu.get_scattered_field(x / a, y / a, phin, omega_n)
    ax[n].pcolormesh(x / a, y / a, Weig.real, cmap="RdBu_r")
    for i in range(Nres):
        xr, yr = res_array[i].position
        ax[n].plot(xr / a, yr / a, ".k")
        xa = phin[i].real
        ya = phin[i].imag
        # plt.plot([xr/a, (xr + xa)/a], [yr/a, (yr + ya/a)], "b-")
        ax[n].arrow(
            xr / a,
            yr / a,
            (xa) / a,
            (ya / a),
            head_width=0.033,
            head_length=0.05,
            fc="k",
            ec="k",
        )

    ax[n].set_title(f"$\omega = ${omega_n:.4f}")
    ax[n].axis("off")
    # plt.xlabel("$x/a$")
    # plt.ylabel("$y/a$")

    # plt.xlim((x1[0] - dbx - Ra) / a, (x1[-1] + dbx + Ra) / a)
    # plt.ylim((y1[0] - dby - Ra) / a, (y1[-1] + dby + Ra) / a)

    # plt.colorbar(orientation="vertical", fraction=0.046, pad=0.04)
    # plt.title(f"mode {n+1}, $\omega_{{{n+1}}}={omega_n:.3f}$")
    ax[n].set_aspect(1)
for ax_ in ax[n + 1 :]:
    ax_.remove()
plt.tight_layout()
# plt.savefig(f"/home/bench/pic/matplotlib/phononic_qnm_mode_{n+1}.png")
plt.savefig(f"/home/bench/pic/matplotlib/phononic_qnms.png")
# plt.pause(0.1)


#########


fig, ax = plt.subplots(nplt, nplt, figsize=(12, 8))
if nplt == 1:
    ax = [ax]
else:
    ax = ax.ravel()

for n in range(Nmodes):
    omega_n = evs[n]

    _ = ax[n].imshow(abs(M[n]))

    ax[n].set_title(f"$\omega = ${omega_n:.4f}")
    ax[n].axis("off")
    ax[n].set_aspect(1)
    plt.colorbar(_)
for ax_ in ax[n + 1 :]:
    ax_.remove()
plt.tight_layout()


sys.exit(0)
############## gradient
plt.close("all")
imode = 1
D = simu.plate.D
omegan = evs[imode]
phi = eigenvectors[:, imode]
domegadk = bk.zeros(simu.n_res, dtype=complex)
domegadm = bk.zeros(simu.n_res, dtype=complex)
for ires, res in enumerate(simu.res_array):
    k = res.stiffness
    m = res.mass
    omega_r = res.omega_r
    phidotphi = phi[ires] ** 2
    domegadk[ires] = -D / k**2 * phidotphi
    domegadm[ires] = D / (m**2 * omegan**2) * phidotphi

import copy

for ires, res in enumerate(simu.res_array):
    # dsimu = copy.copy(simu)
    for case in [0, 1]:
        dres_array = copy.deepcopy(simu.res_array)
        delta = 1e-4
        if case == 0:
            dm = masses[ires] * delta
            dres_array[ires].mass += dm
        else:
            dm = stiffnesses[ires] * delta
            dres_array[ires].stiffness += dm

        dsimu = kl.ScatteringSimulation(plate, dres_array, alternative=alternative)

        evspert0, respert0 = nonlinear_eigensolver(
            dsimu,
            omega0,
            omega1,
            lambda_tol=lambda_tol,
            max_iter=max_iter,
            N_guess_loc=3,
            verbose=True,
        )
        max_iter_ref = max_iter * 2
        evspert, respert = nonlinear_eigensolver(
            dsimu,
            omega0,
            omega1,
            guesses=evspert0,
            lambda_tol=lambda_tol_ref,
            max_iter=max_iter_ref,
            N_guess_loc=3,
            verbose=True,
        )
        fd = (evspert[imode] - evs[imode]) / dm

        print("exact")
        if case == 0:
            print(domegadm[ires])
        else:
            print(domegadk[ires])
        print("FD")
        print(fd)
        print("-------")
# print_results(evspert0, respert0, message=None)
# print_results(evspert, respert, message="refined")


# print(domegadm[ires])
# # print(domegadk[ires])
# print(fd)
# sys.exit(0)
