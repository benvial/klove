#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Finite cluster of resonators
==============================================

Finding modes.
"""

import sys
from math import pi

import matplotlib.pyplot as plt
import tetrachotomy as tt
from polze import PZ

import klove as kl

plt.ion()
plt.close("all")

bk = kl.backend

######################################################################
#


method = "tetrachotomy"
# method = "polze"
# method = "cxroots"
method = "rayleigh"

bk.random.seed(1654)
order = int(sys.argv[1])
Nres = 20
m = 10
k = 30
a = 0.5
positions = bk.random.rand(Nres, 2) * a - a / 2

nptsx = 201
nptsy = int(nptsx / Nres)

nptsx = 101
nptsy = 101


### pole search


omega0 = -3 - 6 * 1j
omega1 = 4 - 0 * 1j

# omega0 = 2 - 1 * 1j
# omega1 = 4 - 1e-16 * 1j


omega0 = -0.1 - 3 * 1j
omega1 = 4 - 0 * 1j

omega0 = 1.41 - 0.2 * 1j
omega1 = 2 - 0 * 1j

options = tt.get_options()
options["plot"]["rectangle"] = True
# options["plot"]["circle"] = True
options["plot"]["poles"] = True
options["plot"]["color"] = "#4d9aff"
options["plot"]["marker"]["value"] = "o"
options["plot"]["marker"]["size"] = 4

# q = (-1 + 5**0.5) / 2
q = 0.5
options["ratios"]["real"] = q
options["ratios"]["imag"] = 1 - q
options["ratios"]["circ"] = 0.5
options["refine"]["nref_max"] = 1
options["refine"]["method"] = "Nelder-Mead"
tolerances = options["tolerances"]
tol_moments = tolerances["moments"]
tol_moments["0"] = 1e-6
tol_moments["1"] = 1e-6
tol_moments["ratio"] = 1e-5

tolerances["pole"] = 1e-12
tolerances["residue"] = 1e-12
tolerances["integration"]["tol"] = 1e-6
tolerances["integration"]["divmax"] = 10


##### resonators


# n = 2
# Nres = n**2
# positions = bk.meshgrid(bk.linspace(-a / 2, a / 2, n), bk.linspace(-a / 2, a / 2, n))
# positions = bk.stack([positions[0].ravel(), positions[1].ravel()]).T

masses = bk.random.rand(Nres) * 0.03
stiff = bk.random.rand(Nres) * 4 * 0.03
stiff = bk.ones(Nres) * 1
masses = bk.ones(Nres) * 10
# masses = bk.arange(1, Nres + 1, 1) * 0.1
# stiff = bk.arange(1,Nres+1, 1) * 10
pos_source = bk.array([-a / 2, 0])
angle = 0
r0 = a
t = bk.linspace(0, 2 * pi, Nres + 1)[:-1]
# t = bk.random.rand(Nres) * 2 * pi
positions = bk.stack([r0 * bk.cos(t), r0 * bk.sin(t)]).T


######################################################################
# Define the elastic plate

plate = kl.ElasticPlate(0.03, 1, 1, 0)

omegar0 = 100 * plate.omega0(a)
m0 = plate.rho * a**2 * plate.h * 0.01
masses = bk.ones(Nres) * m0
# masses += 10*bk.random.rand(Nres) * m0

# masses = bk.linspace(0.2, 2, Nres) * m0
# # masses = bk.arange(1, Nres + 1, 1) * m0
positionsx = bk.linspace(0.0, Nres * a, Nres)
positionsx -= positionsx[-1] / 2
ks = m0 * omegar0**2 * bk.linspace(1, 2, Nres)
ks = m0 * omegar0**2 * bk.ones(Nres)
stiff = bk.flipud(ks) * 1

# stiff *= bk.linspace(1,2,Nres)
# stiff = m0 * omegar0**2 * bk.random.rand(Nres)


def null(M):
    e, v = bk.linalg.eig(M)
    srt = bk.argsort(bk.abs(e))
    return v[:, srt[0]]  # .conj()


# for p in positions:
#     plt.plot(*p,".r")
# plt.axis("scaled")

dbx = dby = 0.1


x1 = bk.linspace(-a * dbx - r0, a * dbx + r0, nptsx)
y1 = bk.linspace(-a * dby - r0, a * dby + r0, nptsy)


# positions = [(_x, 0) for _x in positionsx]
# x1 = bk.linspace(-a * dbx + positions[0][0], a * dbx + positions[-1][0], nptsx)
# y1 = bk.linspace(-a * dby + positions[0][1], a * dby + positions[-1][1], nptsy)


y1 = x1


x, y = bk.meshgrid(x1, y1, indexing="xy")


######################################################################
# Define the array of resonators

res_array = []
for i in range(Nres):
    res_array.append(kl.Resonator(masses[i], stiff[i], positions[i]))

######################################################################
# Define the simulation object
simu = kl.ScatteringSimulation(plate, res_array, alternative=True)

######################################################################
# Find zeros


# singularity_Talpha


singularity_Talpha = []
for res in res_array:
    q = (
        1 / 16 * (res.mass * res.stiffness / (plate.rho * plate.h * plate.D)) ** 0.5
        + 0j
    )
    # pole = res.omega_r/2 * (-1j*q - (4-q**2)**0.5)
    # singularity_Talpha.append(pole)
    pole = res.omega_r * (-1j * q + (1 - q**2) ** 0.5)
    singularity_Talpha.append(pole)

# print(singularity_Talpha)


Nre, Nim = 100, 100


omegas_re = bk.linspace(omega0.real, omega1.real, Nre)
omegas_im = bk.linspace(omega1.imag, omega0.imag, Nim)
omegas_re_, omegas_im_ = bk.meshgrid(omegas_re, omegas_im, indexing="ij")


omegas_complex = omegas_re_ + 1j * omegas_im_
Mc = simu.build_matrix(omegas_complex)
Mc = bk.transpose(Mc, axes=(2, 3, 0, 1))
det = bk.linalg.det(Mc)
evs = bk.linalg.eigvals(Mc)
srt = bk.argsort(bk.abs(evs), axis=-1)
min_evs = bk.take_along_axis(evs, srt, axis=-1)[:, :, 0]


# det = bk.zeros((Nre, Nim), dtype=bk.complex128)
# min_evs = bk.zeros((Nre, Nim), dtype=bk.complex128)
# for i, omega_re in enumerate(omegas_re):
#     for j, omega_im in enumerate(omegas_im):
#         print(i,j)
#         omega = omega_re + 1j * omega_im
#         omega1 = omegas_complex[i,j]
#         assert bk.allclose(omega,omega1)
#         M = simu.build_matrix(omega)
#         M = Mc[i,j]
#         # assert bk.allclose(M,M1)
#         det[i, j] = bk.linalg.det(M)
#         evs = bk.linalg.eigvals(M)
#         srt = bk.argsort(bk.abs(evs))
#         # srt = bk.argsort(bk.real(evs))
#         min_evs[i, j] = evs[srt[0]]

plt.close("all")


# plt.figure()
# plt.pcolormesh(omegas_re/omegar0, omegas_im/omegar0, (bk.angle(det.T)),cmap="hsv")
# plt.colorbar()
# plt.xlabel("Re $\omega$")
# plt.ylabel("Im $\omega$")
# # plt.title(r"${\rm log}_{10} |{\rm det}\, M|$")


plt.figure()
plt.pcolormesh(omegas_re, omegas_im, bk.log10(bk.abs(det.T)))
plt.colorbar()
plt.xlabel("Re $\omega$")
plt.ylabel("Im $\omega$")
plt.title(r"${\rm log}_{10} |{\rm det}\, M|$")
ax_det = plt.gca()


plt.figure()
plt.pcolormesh(omegas_re, omegas_im, bk.log10(bk.abs(min_evs.T)))
plt.colorbar()
plt.xlabel("Re $\omega$")
plt.ylabel("Im $\omega$")
plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")
ax_min_ev = plt.gca()


plt.axis("scaled")

plt.tight_layout()
plt.pause(0.01)


def func(omega):
    M = simu.build_matrix(omega)
    det = bk.linalg.det(M)
    return det


def func1(omega):
    M = simu.build_matrix(omega)
    evs = bk.linalg.eigvals(M)
    srt = bk.argsort(bk.abs(evs))
    return evs[srt[0]]


if method == "tetrachotomy":
    zeros, residues, ncut, neval = tt.find_zeros(
        func, omega0, omega1, order=order, options=options
    )


elif method == "rayleigh":
    from skimage.feature import peak_local_max

    from klove.eig import eig_newton

    func_gives_der = True
    im = -bk.log10(bk.abs(min_evs))
    coordinates = peak_local_max(im, min_distance=1)
    guess_peak = bk.array([omegas_complex[*c] for c in coordinates])
    plt.plot(guess_peak.real, guess_peak.imag, ".k")

    if func_gives_der:

        def func_eig(omega):
            return simu.build_matrix(omega), simu.build_matrix_derivative(omega)

    else:

        def func_eig(omega):
            return simu.build_matrix(omega)

    bk.random.seed(112)
    vect_init = bk.random.rand(simu.n_res)
    # vect_init = bk.ones(simu.n_res)
    Nguess_re = 20
    Nguess_im = 20
    weight = "rayleigh symmetric"
    weight = "max element"
    guesses_re = bk.linspace(omega0.real, omega1.real, Nguess_re)
    guesses_im = bk.linspace(omega0.imag, omega1.imag, Nguess_im)
    evs = []
    # for guess_re in guesses_re:
    #     for guess_im in guesses_im:
    #         guess = guess_re + 1j * guess_im
    for guess1 in guess_peak:
        # print(guess)
        # guesses = bk.linspace(guess1*0.9,guess1*1.1,3)
        N_guess_loc = 4
        Rloc = 0.1
        _t = bk.linspace(0, 2 * bk.pi, N_guess_loc + 1)[:-1]
        guesses = (
            guess1.real + Rloc * bk.cos(_t) + 1j * (guess1.imag + Rloc * bk.sin(_t))
        )
        guesses = bk.hstack([guesses, guess1])
        plt.plot(guesses.real, guesses.imag, "-w", lw=0.1)

        def get_residual(z):
            M = simu.build_matrix(z)
            phi = null(M)
            return bk.abs(phi @ M @ phi)

        # guesses = [guess1]
        for guess in guesses:
            lambda_tol = 1e-12
            max_iter = 100
            try:
                vect_init = null(simu.build_matrix(guess))
                res = eig_newton(
                    func_eig,
                    guess,
                    vect_init,
                    lambda_tol=lambda_tol,
                    max_iter=max_iter,
                    func_gives_der=func_gives_der,
                    weight=weight,
                )
                # print(res["eigval"])

                ev = res["eigval"]
                print(res["iter_count"], res["delta_lambda"])
                if (
                    ev.real > omega0.real
                    and ev.real < omega1.real
                    and ev.imag < omega1.imag
                    and ev.imag > omega0.imag
                ):
                    evs.append(ev)
                    plt.plot(ev.real, ev.imag, ".r")
                    plt.xlim(omega0.real, omega1.real)
                    plt.ylim(omega0.imag, omega1.imag)
                    plt.pause(0.001)

            except:
                print("not converged")
                # sys.exit(0)
                pass
    evs = bk.array(evs)
    precision = lambda_tol * 100
    evs_filter = bk.unique(
        (bk.floor(evs.real / precision) + 1j * bk.floor(evs.imag / precision))
        * precision
    )
    zeros0 = evs_filter
    print(len(zeros0))

    val_zeros0 = [bk.abs(func(z)) for z in zeros0]
    res0 = [get_residual(z) for z in zeros0]

    print("######   COARSE   #######")
    for z0, v0, r0 in zip(zeros0, val_zeros0, res0):
        print(f"{z0}    {v0}    {r0}")

    refine = False
    if refine:
        evsref = []
        for guess in zeros0:
            # print(guess)

            vect_init = null(simu.build_matrix(guess))
            lambda_tol1 = max(lambda_tol / 10, 1e-14)
            try:
                res = eig_newton(
                    func_eig,
                    guess,
                    vect_init,
                    lambda_tol=lambda_tol1,
                    max_iter=max_iter * 1,
                    func_gives_der=func_gives_der,
                    weight=weight,
                )
                # print(res["eigval"])
                evsref.append(res["eigval"])
            except:
                pass

        evs = bk.array(evsref)
        evs = bk.array(evs)
        evs = evs[evs.real > omega0.real]
        evs = evs[evs.real < omega1.real]
        evs = evs[evs.imag < omega1.imag]
        evs = evs[evs.imag > omega0.imag]
        precision = lambda_tol1 * 100
        evs_filter = bk.unique(
            (bk.floor(evs.real / precision) + 1j * bk.floor(evs.imag / precision))
            * precision
        )
        zeros = evs_filter
        val_zeros = [bk.abs(func(z)) for z in zeros]
        print(len(zeros))

        res = [get_residual(z) for z in zeros]
        print("######   REFINED   #######")
        for z0, v0, r0 in zip(zeros, val_zeros, res):
            print(f"{z0}    {v0}    {r0}")

    else:
        zeros = zeros0

elif method == "cxroots":
    from cxroots import Rectangle

    R = Rectangle((omega0.real, omega1.real), (omega0.imag, omega1.imag))
    sol = R.roots(func, verbose=True)
    # sol.show()

    zeros = sol.roots
else:
    # xs

    # Definition of the function
    # f = lambda z: np.tan(z)
    # df = lambda z: np.tan(z)**2 + 1 # [df is optional]
    df = None
    # Initialization of the solver
    Rpath = 0.5

    # _Npz_limit = 5;       """Number of Pole and Zeros max befor splitting path"""
    # _tol = 1e-5;          """General tolerance"""
    # _clean_tol = 1e-5;    """Spurious roots multiplicity tolerance"""
    # _NR_tol = 1e-12;      """Newton Raphson tolerance"""
    # _NiterMax  = 15;       """Max number of Newton Raphson iterations"""
    # _vectorized = False;  """Vectorized function evaluation"""
    # _RiShift = 1.02;      """Scaling factor of the radius when s0 is not an integer"""
    # _zeros_only = False;  """Use 0-moment for Npz estimate"""
    # _Niter_for_int = 5;   """Max Number of iterations to estimate the moments"""
    # _parallel = False;    """Split evaluation loop in a process pool executor"""
    # _max_workers = 1;     """Number of workers in the process pool executor"""

    pz = PZ(
        (func, df),
        Rmax=0.1,
        Npz=2,
        Ni=1024 * 4,
        # R0=(0.5 - 0.5 * 1j) * 1,
        R0=0.2158049 - 1.60411197j,
        options={
            "_Npz_limit": 2,
            "_RiShift": 1.01,
            "_tol": 1e-6,
            "_NiterMax": 50,
            "_Niter_for_int": 50,
        },
    )
    # Solve with moment method
    pz.solve()
    # Get the poles p and the zeros z
    (poles, mult_poles), (zeros, mult_zeros) = pz.dispatch(
        refine=True, multiplicities=True
    )

    # sss
    # plot the roots
    # pz.plot()
    # Performed an iterative refinement with Newton-Raphson method
    # res, mult, info = pz.iterative_ref()
    # zeros = res[len(p):]
    # poles = res[:len(p)]


# val_zeros = [bk.abs(func(z)) for z in zeros]
# print(val_zeros)
# zeros = zeros[zeros.real > 1e-19]
# zeros = zeros[zeros.imag <= 0]

singularity_Talpha = bk.unique(singularity_Talpha)
for ax in [ax_min_ev, ax_det]:
    plt.sca(ax)
    plt.plot(zeros.real, zeros.imag, ".g")
    for s in singularity_Talpha:
        plt.plot(s.real, s.imag, "xb")


plt.pause(0.01)
sys.exit(0)

# simu.alternative = True


# def approx_det(z):
#     out = 0
#     for zer, r in zip(zeros, residues):
#         out += r / (z - zer)
#     return 1 / out


# _re, _im = bk.meshgrid(omegas_re, omegas_im)
# Z = _re + 1j * _im

# approx = approx_det(Z)

# plt.figure()
# plt.pcolormesh(omegas_re, omegas_im, bk.log10(bk.abs(approx)))
# plt.colorbar()
# plt.xlabel("Re $\omega$")
# plt.ylabel("Im $\omega$")

################# plots ##############


def null(M, eps=1e-10):
    u, s, vh = bk.linalg.svd(M)
    null_mask = s <= eps

    null_space = bk.compress(null_mask, vh, axis=0)
    return null_space.ravel().conj()


def null(M):
    _, _, vh = bk.linalg.svd(M)
    return vh[-1].conj()


def null(M):
    e, v = bk.linalg.eig(M)
    srt = bk.argsort(bk.abs(e))
    return v[:, srt[0]]  # .conj()


def get_external_field_mode(simu, omega):
    M = simu.build_matrix(omega)
    phi_eig = null(M)
    return simu.get_external_field(None, phi_eig, omega)


Nmodes = len(zeros)


eigenvectors = [null(simu.build_matrix(zeros[n])) for n in range(Nmodes)]


plot_modes = False

if plot_modes:
    imode = 0
    for imode in range(Nmodes):
        omega_eig = zeros[imode]
        phi_eig = get_external_field_mode(simu, omega_eig)
        Weig = simu.get_scattered_field(x, y, phi_eig, omega_eig)
        plt.figure()
        plt.pcolormesh(x, y, Weig.real, cmap="RdBu_r")
        for res in simu.res_array:
            plt.plot(*res.position, ".k")
        plt.plot(*pos_source, "xk")
        plt.colorbar(orientation="horizontal")
        plt.axis("scaled")
        plt.xlabel("$x$")
        plt.ylabel("$y$")
        plt.title(f"mode {imode+1}, $\omega_{{{imode+1}}}={omega_eig:.3f}$")
        plt.tight_layout()
        plt.savefig(f"/home/bench/pic/matplotlib/phononic_qnm_mode_{imode+1}.png")
        plt.pause(0.1)


# plt.figure()
# plt.pcolormesh(x, y, bk.abs(Weig), cmap="inferno")
# for res in res_array:
#     plt.plot(*res.position, ".k")
# plt.plot(*pos_source, "xk")
# plt.colorbar()
# plt.axis("scaled")
# plt.xlabel("$x$")
# plt.ylabel("$y$")
# plt.title(f"mode {imode+1}, $\omega_{{{imode+1}}}={omega_eig:.3f}$")
# plt.show()
# plt.pause(0.01)

# ######################################################################
# # Get the field

# # omega_eig = 1.7728667508642741
# omega = omega_eig.real
# phi0 = simu.plane_wave
# phi0 = simu.point_source

# phie_alpha = simu.solve(phi0, omega, pos_source)

# # x1 = bk.linspace(-2 * a, 2 * a, 300)
# # y1 = bk.linspace(-2 * a, 2 * a, 300)
# x, y = bk.meshgrid(x1, y1, indexing="xy")
# # W = simu.get_scattered_field(x, y, phie_alpha, omega)
# W = simu.get_field(x, y, phi0, phie_alpha, omega, pos_source)

# ######################################################################
# # Plot the field map

# plt.figure()
# plt.pcolormesh(x, y, bk.real(W), cmap="RdBu_r")
# for res in res_array:
#     plt.plot(*res.position, ".k")
# plt.plot(*pos_source, "xk")
# plt.colorbar()
# plt.axis("scaled")
# plt.xlabel("$x$")
# plt.ylabel("$y$")
# plt.title(f"$\omega={omega:.3f}$")
# plt.show()
# plt.pause(0.01)


# plt.figure()
# plt.pcolormesh(x, y, bk.abs(W), cmap="inferno")
# for res in res_array:
#     plt.plot(*res.position, ".k")
# plt.plot(*pos_source, "xk")
# plt.colorbar()
# plt.axis("scaled")
# plt.xlabel("$x$")
# plt.ylabel("$y$")
# plt.title(f"$\omega={omega:.3f}$")
# plt.show()
# plt.pause(0.01)


# ###### Normalization ??????


ytop = y1[-1]
ybottom = y1[0]
xright = x1[-1]
xleft = x1[0]


domain_area = (x1[-1] - x1[0]) * (y1[-1] - y1[0])


def get_mode(simu, x, y, phin, omegan):
    return simu._get_field(
        x, y, None, kl.special.greens_function_cartesian, phin, omegan
    )


def get_grad_mode(simu, x, y, phin, omegan):
    def gx(k, x, y):
        return kl.special.grad_greens_function_cartesian(k, x, y)[0]

    def gy(k, x, y):
        return kl.special.grad_greens_function_cartesian(k, x, y)[1]

    out = lambda g: simu._get_field(x, y, None, g, phin, omegan)
    return out(gx), out(gy)


def get_laplacian_mode(simu, x, y, phin, omegan):
    return simu._get_field(
        x, y, None, kl.special.laplacian_greens_function_cartesian, phin, omegan
    )


def get_grad_laplacian_mode(simu, x, y, phin, omegan):
    def gx(k, x, y):
        return kl.special.grad_laplacian_greens_function_cartesian(k, x, y)[0]

    def gy(k, x, y):
        return kl.special.grad_laplacian_greens_function_cartesian(k, x, y)[1]

    out = lambda g: simu._get_field(x, y, None, g, phin, omegan)
    return out(gx), out(gy)


def boundary_term(W1, dW1, d2W1, d3W1, W2, dW2, d2W2, d3W2, n):
    bnd_term = d2W1 * (dW2[0] * n[0] + dW2[1] * n[1]) - d2W2 * (
        dW1[0] * n[0] + dW1[1] * n[1]
    )
    bnd_term += W1 * (d3W2[0] * n[0] + d3W2[1] * n[1]) - W2 * (
        d3W1[0] * n[0] + d3W1[1] * n[1]
    )
    return bnd_term


def get_boundary_terms(simu, x, y, n, phi1, omega1, phi2, omega2):
    W1 = get_mode(simu, x, y, phi1, omega1)
    dW1 = get_grad_mode(simu, x, y, phi1, omega1)
    d2W1 = get_laplacian_mode(simu, x, y, phi1, omega1)
    d3W1 = get_grad_laplacian_mode(simu, x, y, phi1, omega1)

    W2 = get_mode(simu, x, y, phi2, omega2)
    dW2 = get_grad_mode(simu, x, y, phi2, omega2)
    d2W2 = get_laplacian_mode(simu, x, y, phi2, omega2)
    d3W2 = get_grad_laplacian_mode(simu, x, y, phi2, omega2)

    return boundary_term(W1, dW1, d2W1, d3W1, W2, dW2, d2W2, d3W2, n)


def get_boundary_term(simu, phi1, omega1, phi2, omega2):
    top = dict(x=x1, y=ytop * bk.ones_like(x1), n=[0, 1], integ=x1)
    bottom = dict(x=x1, y=ybottom * bk.ones_like(x1), n=[0, -1], integ=x1)
    left = dict(x=xleft * bk.ones_like(y1), y=y1, n=[-1, 0], integ=y1)
    right = dict(x=xright * bk.ones_like(y1), y=y1, n=[1, 0], integ=y1)

    contour = top, bottom, left, right
    bt = 0
    for pos in contour:
        integ = get_boundary_terms(
            simu, pos["x"], pos["y"], pos["n"], phi1, omega1, phi2, omega2
        )
        bt += bk.trapz(integ, pos["integ"])
    return bt


def get_resonators_term(simu, phi1, omega1, phi2, omega2):
    D = simu.plate.bending_stiffness
    res_term = 0
    for res in simu.res_array:
        xr, yr = res.position
        W1 = get_mode(simu, xr, yr, phi1, omega1)
        W2 = get_mode(simu, xr, yr, phi2, omega2)
        t1 = res.strength(omega1) / D
        t2 = res.strength(omega2) / D
        res_term += (t1 - t2) * W1 * W2
    return res_term


mode_norms = bk.zeros(Nmodes, dtype=complex)
phins = [get_external_field_mode(simu, zeros[n]) for n in range(Nmodes)]


######################################################
################## QNM expansion #####################
######################################################


angle = 0
pos_source = (x1[0], 0)
pos_probe = (0, 0)
pos_source = (-a, 0)
pos_probe = pos_source

nfreq = 1000
start = max(omega0.real, 1e-12)
stop = omega1.real
omegas = bk.linspace(start, stop, nfreq)

incident = simu.point_source


def arbitrary(omega):
    return 1 / omega**2


gammas = bk.zeros((nfreq, Nmodes), dtype=complex)
dM = [simu.build_matrix_derivative(omega) for omega in zeros]

denom = [eigenvectors[n] @ (dM[n] @ eigenvectors[n]) for n in range(Nmodes)]

omegas = omegas + 0j

for i, omega in enumerate(omegas):
    print("----------------")
    print(omega)
    gamma = bk.zeros(Nmodes, dtype=complex)

    phi0_vec = simu.build_vector(incident, omega, pos_source)
    print(phi0_vec)

    for n in range(Nmodes):
        omegan = zeros[n]
        num = phi0_vec @ eigenvectors[n]
        gamma[n] = (
            num / denom[n] / (omega - omegan) * arbitrary(omegan) / arbitrary(omega)
        )

    gammas[i] = gamma

# print(gammas)

plt.figure()
plt.plot(omegas, abs(gammas))


direct = bk.zeros((nfreq, Nres), dtype=complex)
qmem = bk.zeros((nfreq, Nres), dtype=complex)


for i, omega in enumerate(omegas):
    print("----------------")
    print(omega)

    direct[i] = simu.solve(incident, omega, pos_source)
    rec = 0
    for n in range(Nmodes):
        Bn = eigenvectors[n]
        rec += gammas[i, n] * Bn

    qmem[i] = rec

ks = simu.wavenumber(omegas)

plt.figure()
for i in range(Nres):
    # res=simu.res_array[i]
    # T = res._strength(omega)
    l = plt.plot(omegas.real, bk.abs(direct[:, i]), "-", label="direct")
    plt.plot(omegas.real, bk.abs(qmem[:, i]), "--", c=l[0].get_color(), label="QMEM")
plt.xlabel("$\omega$")
plt.ylabel("$|a_n|$")
plt.legend()
plt.tight_layout()


plt.figure()
for i in range(Nres):
    l = plt.plot(omegas.real, bk.unwrap(bk.angle(direct[:, i])), "-", label="direct")
    plt.plot(
        omegas.real,
        bk.unwrap(bk.angle(qmem[:, i])),
        "--",
        c=l[0].get_color(),
        label="QMEM",
    )
plt.xlabel("$\omega$")
plt.ylabel(r"${\rm arg}(a_n)$")
plt.legend()
plt.tight_layout()


i = int(nfreq / 2)
omega = omegas[i]
rec = 0
for n in range(Nmodes):
    Wn = simu.get_scattered_field(x, y, eigenvectors[n], omega)
    rec += gammas[i, n] * Wn


plt_type = "abs"

cmap = "RdBu_r" if plt_type == "real" else "inferno"

fig, ax = plt.subplots(1, 2, figsize=(10, 4))
plt.sca(ax[0])
_rec = bk.real(rec) if plt_type == "real" else bk.abs(rec)
plt.pcolormesh(x, y, _rec, cmap=cmap)
for res in simu.res_array:
    plt.plot(*res.position, ".k")
plt.plot(*pos_source, "xk")
plt.colorbar(orientation="vertical")
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title("QMEM")
incident = simu.point_source
sol = simu.solve(incident, omega, pos_source)
# W = simu.get_field(x, y, incident, sol, omega, pos_source)
W = simu.get_scattered_field(x, y, sol, omega)

_W = bk.real(W) if plt_type == "real" else bk.abs(W)
plt.sca(ax[1])
plt.pcolormesh(x, y, _W, cmap=cmap)
for res in simu.res_array:
    plt.plot(*res.position, ".k")
plt.plot(*pos_source, "xk")
plt.colorbar(orientation="vertical")
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title("direct")
plt.tight_layout()

direct = bk.zeros(nfreq, dtype=complex)
qmem = bk.zeros(nfreq, dtype=complex)


for i, omega in enumerate(omegas):
    print("----------------")
    print(omega)
    sol = simu.solve(incident, omega, pos_source)
    W = simu.get_scattered_field(*pos_probe, sol, omega)

    direct[i] = W

    rec = 0
    for n in range(Nmodes):
        Wn = simu.get_scattered_field(*pos_probe, eigenvectors[n], omega)
        rec += gammas[i, n] * Wn

    qmem[i] = rec

ks = simu.wavenumber(omegas)
plt.figure()
plt.plot(omegas.real, abs(direct), "--", c="#268bd4", label="direct")
plt.plot(omegas.real, abs(qmem), c="#d42626", label="QMEM")
plt.xlabel("$\omega$")
plt.ylabel("$|W^s(r_s)|$")
plt.legend()
plt.tight_layout()


sys.exit(0)


# for n in range(Nmodes):
#     Wn = get_mode(simu, x, y, phins[n], zeros[n])
#     mode_norms[n] = bk.trapz(bk.trapz(Wn * Wn, x1), y1) ** 0.5


# phins = [phins[n] / mode_norms[n] for n in range(Nmodes)]

# check_ortho = bk.zeros((Nmodes, Nmodes), dtype=complex)

# for n in range(Nmodes):
#     omegan = zeros[n]
#     kn = simu.wavenumber(omegan)
#     phin = phins[n]
#     Wn = get_mode(simu, x, y, phin, omegan)
#     for m in range(Nmodes):
#         print("----------------")
#         print(n, m)
#         omegam = zeros[m]
#         km = simu.wavenumber(omegam)
#         phim = phins[m]
#         Wm = get_mode(simu, x, y, phim, omegam)
#         surf_int = bk.trapz(bk.trapz(Wn * Wm, x1), y1)
#         scalar_prod = surf_int
#         if n != m:
#             sigma = get_boundary_term(simu, phin, omegan, phim, omegam)
#             tau = get_resonators_term(simu, phin, omegan, phim, omegam)
#             scalar_prod += (sigma + tau) / (kn**4 - km**4)
#         check_ortho[n, m] = scalar_prod


# print(abs(check_ortho))

# testerr = abs(check_ortho) - bk.eye(Nmodes)
# print(testerr)
# plt.figure()
# plt.imshow(testerr)
# plt.colorbar()
######################################################
################## QNM expansion #####################
######################################################


def mu(simu, phi, omega):
    D = simu.plate.bending_stiffness
    res_term = 0
    for res in simu.res_array:
        W = get_mode(simu, *res.position, phi, omega)
        t = res.strength(omega) / D
        res_term += t * W * W
    return res_term


def mu_prime(simu, phi, omega):
    D = simu.plate.bending_stiffness
    res_term = 0
    for res in simu.res_array:
        W = get_mode(simu, *res.position, phi, omega)
        t = 2 * res.mass / D * res.omega_r**4 * omega / (res.omega_r**2 - omega**2) ** 2
        res_term += t * W * W
    return res_term


angle = 0
pos_source = (x1[0], 0)
pos_probe = (0, 0)

omega = 0.2
nfreq = 200
omegas = bk.linspace(0.02, 1.01, nfreq)

omegas = bk.linspace(omega0.real, omega1.real, nfreq)
# omegas = [bk.real(zeros[4])]
# omegas = bk.linspace(omega0.real, omega1.real, nfreq)
gammas = bk.zeros((nfreq, Nmodes), dtype=complex)

Wns = [get_mode(simu, x, y, phins[i], zeros[i]) for i in range(Nmodes)]

I0 = [bk.trapz(bk.trapz(Wn**2, x1), y1) for Wn in Wns]


def arbitrary(omega):
    return 1  # omega**2


for i, omega in enumerate(omegas):
    print("----------------")
    print(omega)
    gamma = bk.zeros(Nmodes, dtype=complex)

    k = simu.wavenumber(omega)

    for n in range(Nmodes):
        omegan = zeros[n]
        phin = phins[n]
        denom = I0[n]
        denom *= -plate.rho * plate.h / plate.D * omegan * 2
        # denom += mu(simu, phin, omegan)
        denom -= mu_prime(simu, phin, omegan)  # * domain_area

        # source = simu.plane_wave(x, y, omega, angle)

        Wnsource = get_mode(simu, *pos_source, phin, omegan)
        num = Wnsource

        # D = simu.plate.bending_stiffness
        # res_term = 0
        # k = simu.wavenumber(omega)
        # G0 = kl.special.greens_function_cartesian(k, *pos_source)
        # for res in simu.res_array:
        #     W = get_mode(simu, *res.position, phin, omega)
        #     t = res.strength(omega) / D
        #     res_term += t * W * G0
        # num += res_term
        gamma[n] = num / denom / (omega - omegan) * arbitrary(omegan) / arbitrary(omega)

    gammas[i] = gamma

print(gammas)

plt.figure()
plt.plot(omegas, abs(gammas))


i = int(nfreq / 2)
omega = omegas[i]
rec = 0
for n in range(Nmodes):
    Wn = get_mode(simu, x, y, phins[n], zeros[n])
    rec += gammas[i, n] * Wn


plt_type = "abs"

cmap = "RdBu_r" if plt_type == "real" else "inferno"

fig, ax = plt.subplots(1, 2, figsize=(10, 4))
plt.sca(ax[0])
_rec = bk.real(rec) if plt_type == "real" else bk.abs(rec)
plt.pcolormesh(x, y, _rec, cmap=cmap)
for res in simu.res_array:
    plt.plot(*res.position, ".k")
plt.plot(*pos_source, "xk")
plt.colorbar(orientation="vertical")
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title("QMEM")
incident = simu.point_source
sol = simu.solve(incident, omega, pos_source)
W = simu.get_field(x, y, incident, sol, omega, pos_source)
W = simu.get_field(x, y, None, sol, omega, pos_source)

_W = bk.real(W) if plt_type == "real" else bk.abs(W)
plt.sca(ax[1])
plt.pcolormesh(x, y, _W, cmap=cmap)
for res in simu.res_array:
    plt.plot(*res.position, ".k")
plt.plot(*pos_source, "xk")
plt.colorbar(orientation="vertical")
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title("direct")
plt.tight_layout()

direct = bk.zeros(nfreq, dtype=complex)
qmem = bk.zeros(nfreq, dtype=complex)


for i, omega in enumerate(omegas):
    print("----------------")
    print(omega)
    sol = simu.solve(incident, omega, pos_source)
    W = simu.get_field(*pos_probe, incident, sol, omega, pos_source)

    direct[i] = W
    rec = 0
    for n in range(Nmodes):
        Wn = get_mode(simu, *pos_probe, phins[n], zeros[n])
        rec += gammas[i, n] * Wn

    qmem[i] = rec

ks = simu.wavenumber(omegas)
plt.figure()
plt.plot(omegas, abs(direct) * ks**2 * 8, "--", c="#d42626", label="direct")
plt.plot(omegas, abs(qmem) * ks**2 * 8, c="#268bd4", label="QMEM")
plt.xlabel("$\omega$")
plt.ylabel("$|W(r_s)|$")
plt.legend()

# plt.figure()
# plt.plot(omegas, abs(qmem/direct), c="#268bd4", label="QMEM")

sys.exit(0)


######################

angle = 0
pos_source = (-1, 0)

omega = 0.2
nfreq = 1
omegas = bk.linspace(1e-6, 1.5, nfreq)
# omegas = bk.linspace(omega0.real, omega1.real, nfreq)
gammas = bk.zeros((nfreq, Nmodes), dtype=complex)
sigmas = bk.zeros((Nmodes, Nmodes), dtype=complex)
taus = bk.zeros((Nmodes, Nmodes), dtype=complex)
for i, omega in enumerate(omegas):
    print("----------------")
    print(omega)
    L = bk.zeros((Nmodes, Nmodes), dtype=complex)

    k = simu.wavenumber(omega)

    for n in range(Nmodes):
        omegan = zeros[n]
        kn = simu.wavenumber(omegan)
        phin = phins[n]
        Wn = get_mode(simu, x, y, phin, omegan)
        for m in range(Nmodes):
            omegam = zeros[m]
            km = simu.wavenumber(omegam)
            phim = phins[m]
            Wm = get_mode(simu, x, y, phim, omegam)

            # if n != m:
            #     if i == 0:
            #         sigmas[n, m] = get_boundary_term(simu, phin, omegan, phim, omegam)
            #         taus[n, m] = get_resonators_term(simu, phin, omegan, phim, omegam)
            #     tmp = -(sigmas[n, m] + taus[n, m]) / (kn**4 - km**4)
            #     tmp = bk.trapz(bk.trapz(Wn * Wm, x1), y1)
            # else:
            #     tmp = mode_norms[n]
            tmp = bk.trapz(bk.trapz(Wn * Wm, x1), y1)
            tmp *= kn**4 - k**4
            tmp += get_resonators_term(simu, phin, omega, phim, omegan)
            L[n, m] = tmp

    source = simu.plane_wave(x, y, omega, angle)

    s = bk.zeros(Nmodes, dtype=complex)
    for n in range(Nmodes):
        omegan = zeros[n]
        phin = phins[n]
        # Wn = get_mode(simu, x, y, phin, omegan)
        # s[n] = bk.trapz(bk.trapz(source * Wn, x1), y1)
        Wn = get_mode(simu, *pos_source, phin, omegan)
        s[n] = Wn

    gamma = bk.linalg.solve(L, s)
    gammas[i] = gamma

print(gammas)

# plt.figure()
# plt.plot(omegas, abs(gammas))


i = 0
omega = omegas[i]
rec = 0
for n in range(Nmodes):
    Wn = get_mode(simu, x, y, phins[n], zeros[n])
    rec += gammas[i, n] * Wn


fig, ax = plt.subplots(1, 2, figsize=(10, 4))
plt.sca(ax[0])
plt.pcolormesh(x, y, abs(rec), cmap="RdBu_r")
for res in simu.res_array:
    plt.plot(*res.position, ".k")
# plt.plot(*pos_source, "xk")
plt.colorbar(orientation="vertical")
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title("QMEM")
# plt.title(f"mode {imode+1}, $\omega_{{{imode+1}}}={omega_eig:.3f}$")
# plt.tight_layout()
# plt.savefig(f"/home/bench/pic/matplotlib/phononic_qnm_mode_{imode+1}.png")
# plt.pause(0.1)

# incident = simu.plane_wave
# sol = simu.solve(incident, omega, angle)
incident = simu.point_source
# sol = simu.solve(incident, omega, angle)
sol = simu.solve(incident, omega, pos_source)

# W = simu.get_field(x, y, None, sol, omega)
# W = simu.get_field(x, y, incident, sol, omega,angle)
W = simu.get_field(x, y, incident, sol, omega, pos_source)
W = simu.get_field(x, y, None, sol, omega, pos_source)

plt.sca(ax[1])
plt.pcolormesh(x, y, abs(W), cmap="RdBu_r")
for res in simu.res_array:
    plt.plot(*res.position, ".k")
# plt.plot(*pos_source, "xk")
plt.colorbar(orientation="vertical")
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title("direct")
plt.tight_layout()


L = bk.zeros((Nmodes, Nmodes), dtype=complex)
s = bk.zeros(Nmodes, dtype=complex)
for n in range(Nmodes):
    Wn = get_mode(simu, x, y, phins[n], zeros[n])
    for m in range(Nmodes):
        print(n, m)
        Wm = get_mode(simu, x, y, phins[m], zeros[m])
        L[n, m] = bk.trapz(bk.trapz(Wn * Wm, x1), y1)

    s[n] = bk.trapz(bk.trapz(W * Wn, x1), y1)

gamma1 = bk.linalg.solve(L, s)

sys.exit(0)


i = 0
omega = omegas[i]
rec = 0
for n in range(Nmodes):
    Wn = get_mode(simu, x1, 0, phins[n], zeros[n])
    rec += gammas[i, n] * Wn

W = simu.get_field(x1, 0, incident, sol, omega, pos_source)
W = simu.get_field(x1, 0, None, sol, omega, pos_source)

plt.figure()
plt.plot(x1, rec.real, label="QMEM")
plt.plot(x1, W.real, label="direct")
plt.legend()


plt.figure()
plt.plot(x1, abs(rec), label="QMEM")
plt.plot(x1, abs(W), label="direct")
plt.legend()
