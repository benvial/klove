#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Quasi-periodic line array of resonators
==============================================

Finding modes.
"""

import sys
import time
from math import pi

import matplotlib.pyplot as plt
import numpy as np
from tools import *

import klove as kl

plt.ion()
plt.close("all")

bk = kl.backend
pi = bk.pi

#################################################################
# Parameters


def xpos(alpha, a, theta, rhom):
    return a * alpha + rhom * bk.sin(alpha * theta)


TH = bk.linspace(0, 1, 51)
for ith, _th in enumerate(TH):
    periodic = True
    periodic = bool(float(sys.argv[1]))

    a = 1
    Nres = 16

    theta0 = _th * 2 * pi
    # theta0 = 0.9 * 2 * pi
    rhom0 = 0.5 * a

    if periodic:
        theta = 0.0 * 2 * pi
        rhom = 0.0 * a
        delta = xpos(1, a, theta0, rhom0) - 1
    else:
        theta = theta0
        rhom = rhom0
        delta = 0

    ######################################################################
    # Elastic plate

    plate = kl.ElasticPlate(a * 1, 1, 1, 0.0)
    p = 256 * plate.rho * plate.h * plate.D

    omegap = plate.omega0(a)
    m_plate = plate.rho * a**2 * plate.h

    alternative = True
    omega0 = (0.92 - 0.2 * 1j) * omegap
    omega1 = (0.9999 + 1e-12 * 1j) * omegap

    recursive = True
    peak_ref = 3
    N_guess_loc = 0
    Rloc = 0.01
    refine = True
    lambda_tol = 1e-12
    max_iter = 100
    plot_solver = False

    nfreq = 400
    start = 0.18
    stop = 0.31

    nptsx = 101
    nptsy = nptsx
    # nptsy = int(nptsx / Nres)
    dbx = 2 * a
    dby = 4 * a

    #################################################################
    # Cluster
    omegar0 = omegap * 1

    m0 = m_plate * 1
    k0 = omegar0**2 * m0

    #### line array
    positionsx0 = bk.array(
        [xpos(alpha, a, theta, rhom) for alpha in range(1, Nres + 1)]
    )

    positionsx0 += delta

    positionsx_flip = -bk.flipud(positionsx0)
    positionsx = bk.hstack([positionsx_flip, positionsx0])

    positions = bk.array([(_x, 0) for _x in positionsx])

    Nres *= 2

    Nrows = 0

    positions_ = []
    for i in range(-Nrows, Nrows + 1):
        p = positions.copy()
        p[:, 1] = i * a
        positions_.append(p)

    positions = bk.vstack(positions_)

    # ls = ".r" if periodic else "xk"
    # for pos in positions:
    #     plt.plot(*pos, ls)

    Nres = len(positions)

    stiffnesses = bk.ones(Nres) * k0
    masses = bk.ones(Nres) * m0
    res_array = []
    for i in range(Nres):
        res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))
        # res_array.append(kl.Mass(masses[i], positions[i]))
        # res_array.append(kl.Pin(positions[i]))

    ######################################################################
    # Define the simulation object
    simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)

    if ith == 0:
        guess = (0.9858 - 2e-04j) * omegap
    else:
        guess = eigenvalues[0]
    # guess = (0.999-2e-02j)*omegap

    guesses = [guess]

    guesses = None

    eigenvalues, eigenvectors = simu.eigensolve(
        omega0,
        omega1,
        guesses=guesses,
        recursive=recursive,
        lambda_tol=lambda_tol,
        max_iter=max_iter,
        N_guess_loc=N_guess_loc,
        Rloc=Rloc,
        peak_ref=peak_ref,
        verbose=False,
        # strategy="grid",
        plot_solver=plot_solver,
        scale=omegap,
    )

    if len(eigenvalues) == 0:
        print("No modes found")
        sys.exit(0)

    print(eigenvalues / omegap)

    # eigenvectors = simu.normalize_eigenvectors(eigenvalues, eigenvectors)

    # M = [simu.build_matrix(omega) for omega in eigenvalues]

    # dM = [simu.build_matrix_derivative(omega) for omega in eigenvalues]
    # eigenvectors = simu.normalize_eigenvectors(eigenvalues, eigenvectors, dM)

    # multiplicities = simu.get_multiplicities(eigenvalues, M, tol=lambda_tol**0.5)
    # eigenvalues, eigenvectors = simu.update_eig(
    #     eigenvalues, eigenvectors, multiplicities, M, dM
    # )

    Nmodes = len(eigenvalues)

    ###############################

    nptsx = 200
    nptsy = nptsx
    nptsy = int(nptsx / Nres)
    nptsy = 50
    dbx = 2 * a
    dby = 8 * a
    x1 = bk.linspace(-dbx + positions[0][0], dbx + positions[-1][0], nptsx)
    y1 = bk.linspace(-dby + positions[0][1], dby + positions[-1][1], nptsy)

    x, y = bk.meshgrid(x1, y1, indexing="xy")

    # imode = 0
    plt.close("all")
    plot_modes(simu, x, y, eigenvalues, eigenvectors, omegap=omegap, a=a)
    plt.pause(0.1)
    # plt.title(eigenvalues[-1] / omegap)

    ires = 0
    imode = 0
    DEV = bk.zeros((Nres, Nmodes))
    for imode in range(Nmodes):
        omega_n = eigenvalues[imode]
        phi_n = eigenvectors[:, imode]
        for ires in range(Nres):
            devx = simu.sensitivity_position(omega_n, phi_n, ires, 0)
            devy = simu.sensitivity_position(omega_n, phi_n, ires, 1)
            dev = (abs(devx) ** 2 + abs(devy) ** 2) ** 0.5
            DEV[ires, imode] = dev
        devnorm = bk.linalg.norm(DEV[:, imode])
        print(devnorm)


# npzname = (
#     f"data/saved_{Nres}_quasiline_periodic.npz"
#     if periodic
#     else f"data/saved_{Nres}_quasiline_quasiperiodic.npz.npz"
# )
# np.savez(
#     npzname,
#     eigenvalues=eigenvalues,
#     eigenvectors=eigenvectors,
#     DEV=DEV,
# )
