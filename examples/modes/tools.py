#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

import matplotlib.pyplot as plt

import klove as kl

# import scienceplots
# plt.style.use(["science"])
# plt.rcParams["font.size"] = 12
# plt.rcParams["figure.figsize"] = (6, 6 / 1.618)

plt.ion()
plt.close("all")
bk = kl.backend


def _pre_plot(typeplot, u):
    return bk.abs(u) if typeplot == "abs" else bk.unwrap(bk.angle(u))


def plot_phis(
    omegas, direct, qmem, omegap=1, typeplot=None, modal=True, ncol=2, ax=None
):
    if ax is not None:
        plt.sca(ax)
    Nres = direct.shape[1]

    def _add_legend(ls):
        lab_alpha = [rf"$\alpha$={i+1}" for i in range(Nres)]
        if modal:
            l1 = plt.gca().plot(0, 0, "-k")
            l2 = plt.gca().plot(0, 0, "--k")
            legend2 = plt.legend([l1[0], l2[0]], ["scattering", "modal"], loc=1)
            loc = 2
            plt.gca().add_artist(legend2)
        else:
            loc = 0
        legend1 = plt.legend(ls, lab_alpha, loc=loc, ncol=ncol)
        plt.gca().add_artist(legend1)
        plt.xlim(omegas[0] / omegap, omegas[-1] / omegap)

    if typeplot == None:
        typeplots = ["abs", "phase"]
    else:
        typeplots = [typeplot]
    for typeplot in typeplots:
        plt.figure()
        ls = []
        for i in range(Nres):
            dir_ = _pre_plot(typeplot, direct[:, i])
            l = plt.plot(
                omegas / omegap,
                dir_,
                "-",
            )
            ls.append(l[0])

            if modal:
                mod_ = _pre_plot(typeplot, qmem[:, i])
                plt.plot(
                    omegas / omegap,
                    mod_,
                    "--",
                    c=l[0].get_color(),
                )
        plt.xlabel(r"$\omega/\omega_p$")

        ylabel = (
            r"$|\phi_\alpha|$" if typeplot == "abs" else r"${\rm arg}(\phi_\alpha)$"
        )
        plt.ylabel(ylabel)
        _add_legend(ls)
        plt.tight_layout()


def plot_fields_omega(omegas, direct_field, qmem_field, scattered_field_plt, omegap=1):
    plt.figure()
    l = plt.plot(omegas / omegap, bk.abs(direct_field), "-", label="scattering")
    plt.plot(omegas / omegap, bk.abs(qmem_field), "--", c="#d24949", label="modal")
    plt.xlabel(r"$\omega/\omega_p$")
    ylabel = (
        r"$|W^s(\boldsymbol{R}_p)|$"
        if scattered_field_plt
        else r"$|W(\boldsymbol{R}_p)/G(\omega,0)|$"
    )
    title = (
        "Scattered field norm at probe position"
        if scattered_field_plt
        else "Normalized total field norm at probe position"
    )
    plt.title(title)
    plt.ylabel(ylabel)
    plt.legend()
    plt.tight_layout()


def plot_field_maps(
    x1,
    y1,
    omega,
    direct_field_map,
    qmem_field_map,
    scattered_field_plt,
    res_array,
    pos_sources,
    omegap=1,
    a=1,
    orientation="horizontal",
):
    if orientation == "horizontal":
        fig, ax = plt.subplots(1, 2)
    else:
        fig, ax = plt.subplots(2, 1)
    _ = ax[0].pcolormesh(
        x1 / a, y1 / a, bk.real(direct_field_map), cmap="RdBu_r", rasterized=True
    )
    plt.colorbar(_)  # , orientation="horizontal", location="top")
    _ = ax[1].pcolormesh(
        x1 / a, y1 / a, bk.real(qmem_field_map), cmap="RdBu_r", rasterized=True
    )
    plt.colorbar(_)  # , orientation="horizontal", location="top")

    title = (
        "Scattered field real part"
        if scattered_field_plt
        else "Normalized total field real part"
    )
    title += rf", $\omega/\omega_p={omega/omegap:.3f}$"
    Nres = len(res_array)

    for i in range(Nres):
        xr, yr = res_array[i].position
        ax[0].plot(xr / a, yr / a, ".k", mew=0, ms=5)
        ax[1].plot(xr / a, yr / a, ".k", mew=0, ms=5)
    ax[0].set_title("direct")
    ax[1].set_title("modal")
    plt.suptitle(title)
    for _ax in ax:
        for pos_source in pos_sources:
            _ax.plot(pos_source[0] / a, pos_source[1] / a, "xk", mew=1, ms=5)
        _ax.set_aspect(1)
        _ax.set_xlabel(r"$x/a$")
        _ax.set_ylabel(r"$y/a$")
    plt.tight_layout()
    plt.pause(0.01)


def plot_modes(
    simu,
    x,
    y,
    evs,
    eigenvectors,
    Ra=0.25,
    arrows=False,
    omegap=1,
    a=1,
    figx=6,
    QNMs=None,
    kx=0,
    nper=1,
):
    Nmodes = len(evs)

    nplt = int(bk.ceil(Nmodes**0.5))
    fig, ax = plt.subplots(nplt, nplt)
    if nplt == 1:
        ax = [ax]
    else:
        ax = ax.ravel()

    x1 = x[0]
    y1 = y[:, 0]

    for n in range(Nmodes):
        ratio = (x1[-1] - x1[0]) / (y1[-1] - y1[0]) * 0.8
        omega_n = evs[n]
        phin = eigenvectors[:, n].copy()
        iphase = 0
        maxvect = bk.max(bk.abs(phin))
        scale = Ra / maxvect
        phin *= scale
        phin *= bk.exp(-1j * bk.angle(phin[iphase]))
        if QNMs is None:
            if isinstance(simu, kl.DiffractionSimulation):
                k0 = simu.wavenumber(omega_n)
                angle = bk.arcsin(kx / k0) + bk.pi / 2
                Weig = simu.get_scattered_field(x, y, phin, omega_n, angle=angle)
            else:
                Weig = simu.get_scattered_field(x, y, phin, omega_n)
        else:
            Weig = QNMs[n]
        ax[n].pcolormesh(x, y, bk.real(Weig), cmap="RdBu_r", rasterized=True)
        for ip in range(nper):
            for i in range(simu.n_res):
                xr, yr = simu.res_array[i].position
                xr += ip * a
                ax[n].plot(xr, yr, ".k", mew=0, ms=5)
                if arrows:
                    xa = phin[i].real
                    ya = phin[i].imag
                    ax[n].arrow(
                        xr / a,
                        yr / a,
                        (xa) / a,
                        (ya) / a,
                        head_width=0.033,
                        head_length=0.05,
                        fc="k",
                        ec="k",
                    )
        sign = "-" if omega_n.imag < 0 else "+"
        ax[n].set_title(
            rf"$\omega_{{{n+1}}}/\omega_p = ${omega_n.real/omegap:.3f}{sign}{omega_n.imag/omegap:.3f}i"
        )
        # ax[n].axis("off")
        ax[n].set_aspect(1)
        if n >= nplt**2 - nplt:
            ax[n].set_xlabel(r"$x/a$")
        else:
            ax[n].set_xticklabels([])
        if n % nplt == 0:
            ax[n].set_ylabel(r"$y/a$")
        else:
            ax[n].set_yticklabels([])
    for ax_ in ax[n + 1 :]:
        ax_.remove()
    plt.tight_layout()


def plot_complex_plane_map(omegas_re, omegas_im, Z, omegap=1, **kwargs):
    plt.pcolormesh(
        omegas_re / omegap,
        omegas_im / omegap,
        Z,
        cmap="inferno",
        rasterized=True,
        **kwargs,
    )
    plt.colorbar()
    plt.xlabel(r"Re $\omega/\omega_p$")
    plt.ylabel(r"Im $\omega/\omega_p$")
    return plt.gca()


def plot_expansion_coeffs(
    omegas, bs, bs_direct, omegap=1, typeplot=None, modal=True, ncol=2, ax=None
):
    Nmodes = bs.shape[1]
    if typeplot == None:
        typeplots = ["abs", "phase"]
    else:
        typeplots = [typeplot]
    for iplt, typeplot in enumerate(typeplots):
        if ax is None:
            fig, ax = plt.subplots(1, 1)
            ax_ = [ax]
        else:
            ax_ = ax
        ls = []
        for n in range(Nmodes):
            dir_ = _pre_plot(typeplot, bs_direct[:, n])
            l = ax_[iplt].plot(omegas / omegap, dir_, "-")
            if modal:
                mod_ = _pre_plot(typeplot, bs[:, n])
                ax_[iplt].plot(
                    omegas / omegap, mod_, "--", c=l[0].get_color(), label=rf"${n}$"
                )  # ,c=l[0].get_color())
            ls.append(l[0])
        ax_[iplt].set_xlabel(r"$\omega/\omega_p$")
        ylabel = r"$|b_n|$" if typeplot == "abs" else r"${\rm arg}(b_n)$"
        ax_[iplt].set_ylabel(ylabel)
        labels = [rf"${n+1}$" for n in range(Nmodes)]
        if modal:
            l1 = ax_[iplt].plot(0, 0, "-k")
            l2 = ax_[iplt].plot(0, 0, "--k")
            #     # ax_[0].legend(loc=1)
            legend2 = ax_[iplt].legend([l1[0], l2[0]], ["scattering", "modal"], loc=2)
            ax_[iplt].add_artist(legend2)
            loc = 1
        else:
            loc = 1
        ax_[iplt].legend(ls, labels, loc=loc, ncol=ncol)
        # ax_[iplt].add_artist(legend1)
        plt.xlim(omegas[0] / omegap, omegas[-1] / omegap)
        plt.tight_layout()


def plot_phis_opt(omegas, direct, omegap=1, typeplot=None, cmap="turbo"):
    Nres = direct.shape[1]
    from matplotlib import colormaps

    colors = colormaps.get(cmap)(bk.linspace(0, 1, Nres))

    def _add_legend(ls):
        lab_alpha = [rf"$\alpha$={i+1}" for i in range(Nres)]
        legend1 = plt.legend(ls, lab_alpha, ncol=2)
        plt.gca().add_artist(legend1)
        plt.xlim(omegas[0] / omegap, omegas[-1] / omegap)

    if typeplot == None:
        typeplots = ["abs", "phase"]
    else:
        typeplots = [typeplot]
    for typeplot in typeplots:
        plt.figure()
        ls = []
        for i in range(Nres):
            dir_ = _pre_plot(typeplot, direct[:, i])
            l = plt.plot(
                omegas / omegap,
                dir_,
                "-",
                c=colors[i],
            )
            ls.append(l[0])
        plt.xlabel(r"$\omega/\omega_p$")

        ylabel = r"$|p_\alpha|$" if typeplot == "abs" else r"${\rm arg}(\phi_\alpha)$"
        plt.ylabel(ylabel)
        _add_legend(ls)
        plt.tight_layout()
