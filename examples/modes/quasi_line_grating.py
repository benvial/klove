#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Quasi-periodic line array of resonators
==============================================

Finding modes.
"""

import sys
import time
from math import pi

import matplotlib.pyplot as plt
import numpy as np
from tools import *

import klove as kl

plt.ion()
plt.close("all")

bk = kl.backend
pi = bk.pi


def plot_modes(
    simu,
    x,
    y,
    evs,
    eigenvectors,
    omegap=1,
    a=1,
    figx=6,
    QNMs=None,
    Nper=1,
    cmap="RdBu_r",
    ls=".k",
):
    Nmodes = len(evs)

    nplt = int(bk.ceil(Nmodes**0.5))
    fig, ax = plt.subplots(nplt, nplt)
    if nplt == 1:
        ax = [ax]
    else:
        ax = ax.ravel()

    x1 = x[0]
    y1 = y[:, 0]

    for n in range(Nmodes):
        ratio = (x1[-1] - x1[0]) / (y1[-1] - y1[0]) * 0.8
        omega_n = evs[n]
        if QNMs is None:
            phin = eigenvectors[:, n].copy()
            Weig = simu.get_scattered_field(x, y, phin, omega_n)
        else:
            Weig = QNMs[n]
        _ = ax[n].pcolormesh(x, y, bk.real(Weig), cmap=cmap, rasterized=True)
        plt.colorbar(_)
        for i in range(simu.n_res):
            xr, yr = simu.res_array[i].position
            for iper in range(Nper):
                ax[n].plot(iper * simu.period + xr, yr, ls, mew=0, ms=5)
        sign = "-" if omega_n.imag < 0 else "+"
        ax[n].set_title(
            rf"$\omega_{{{n+1}}}/\omega_p = ${omega_n.real/omegap:.3f}{sign}{omega_n.imag/omegap:.3f}i"
        )
        # ax[n].axis("off")
        ax[n].set_aspect(1)
        if n >= nplt**2 - nplt:
            ax[n].set_xlabel("$x/a$")
        else:
            ax[n].set_xticklabels([])
        if n % nplt == 0:
            ax[n].set_ylabel("$y/a$")
        else:
            ax[n].set_yticklabels([])
    for ax_ in ax[n + 1 :]:
        ax_.remove()
    plt.tight_layout()


#################################################################
# Parameters


def get_xpos(alpha, a, theta, rhom):
    return a * alpha + rhom * bk.sin(alpha * theta)


periodic = True
periodic = bool(float(sys.argv[1]))

a = 1
Nres = 8

period = a / 6

kx = 0 * pi / period
angle = pi / 2
kx = 0
angle = pi / 2

nh = 2


theta0 = 0.4525 * 2 * pi
rhom0 = 0.5 * a

if periodic:
    theta = 0.0 * 2 * pi
    rhom = 0.0 * a
    delta = get_xpos(1, a, theta0, rhom0) - 1
else:
    theta = theta0
    rhom = rhom0
    delta = 0


######################################################################
# Elastic plate

plate = kl.ElasticPlate(a * 1, 1, 1, 0.0)
p = 256 * plate.rho * plate.h * plate.D

omegap = plate.omega0(a)
m_plate = plate.rho * a**2 * plate.h


alternative = True
omega0 = (0.8 - 0.0003 * 1j) * omegap
omega1 = (1.1 + 1e-4 * 1j) * omegap

recursive = True
peak_ref = 10
N_guess_loc = 0
Rloc = 0.01
refine = True
lambda_tol = 1e-12
max_iter = 100
plot_solver = True


nfreq = 400
start = 0.18
stop = 0.31


#################################################################
# Cluster
omegar0 = omegap * 1

m0 = m_plate * 1
k0 = omegar0**2 * m0


#### circle
t = bk.linspace(0, 2 * bk.pi, Nres + 1)[:-1]
R0 = period / 2 * 0.9
xpos = period / 2 + R0 * bk.cos(t)
ypos = R0 * bk.sin(t)
positions = bk.vstack([xpos, ypos]).T


# #### line array
# positionsx0 = bk.array([get_xpos(alpha, a, theta, rhom) for alpha in range(1, Nres + 1)])

# positionsx0 += delta

# positionsx_flip = -bk.flipud(positionsx0)
# positionsx = bk.hstack([positionsx_flip, positionsx0])

# positions = bk.array([(_x, 0) for _x in positionsx])


# positions = bk.array([(period / 2, _x) for _x in positionsx])

# Nres *= 2


# # ls = ".r" if periodic else "xk"
# # for pos in positions:
# #     plt.plot(*pos, ls)


stiffnesses = bk.ones(Nres) * k0
masses = bk.ones(Nres) * m0
res_array = []
for i in range(Nres):
    res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))

######################################################################
# Define the simulation object
# simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)
simu = kl.DiffractionSimulation(plate, res_array, period=period, nh=nh)

eigenvalues, eigenvectors = simu.eigensolve(
    omega0,
    omega1,
    kx=kx,
    recursive=recursive,
    lambda_tol=lambda_tol,
    max_iter=max_iter,
    N_guess_loc=N_guess_loc,
    Rloc=Rloc,
    peak_ref=peak_ref,
    verbose=False,
    func_gives_der=False,
    # strategy="grid",
    plot_solver=plot_solver,
    scale=omegap,
)

if len(eigenvalues) == 0:
    print("No modes found")
    sys.exit(0)


# eigenvectors = simu.normalize_eigenvectors(eigenvalues, eigenvectors)

# M = [simu.build_matrix(omega) for omega in eigenvalues]

# dM = [simu.build_matrix_derivative(omega) for omega in eigenvalues]
# eigenvectors = simu.normalize_eigenvectors(eigenvalues, eigenvectors, dM)

# multiplicities = simu.get_multiplicities(eigenvalues, M, tol=lambda_tol**0.5)
# eigenvalues, eigenvectors = simu.update_eig(
#     eigenvalues, eigenvectors, multiplicities, M, dM
# )

Nmodes = len(eigenvalues)


###############################

nptsx = 201
nptsy = nptsx
# nptsy = int(nptsx / Nres)
# nptsy = 50
dbx = 2 * a
dby = 1 * a
# x1 = bk.linspace(-dbx + positions[0][0], dbx + positions[-1][0], nptsx)
# y1 = bk.linspace(-dby + positions[0][1], dby + positions[-1][1], nptsy)


Nper = 5

x1 = bk.linspace(0, Nper * period, nptsx)

Npy = 3
y1 = bk.linspace(-period / 2 * Npy, period / 2 * Npy, nptsy)


x, y = bk.meshgrid(x1, y1, indexing="xy")


QNMs = [
    bk.abs(simu.get_scattered_field(x, y, phin, omega_n, angle=angle))
    for phin, omega_n in zip(eigenvectors.T, eigenvalues)
]

# imode = 0
plot_modes(
    simu,
    x,
    y,
    eigenvalues,
    eigenvectors,
    omegap=omegap,
    a=a,
    QNMs=QNMs,
    Nper=Nper,
    ls=".w",
    cmap="inferno",
    # ls=".k",
    # cmap="RdBu_r",
)
# plt.title(eigenvalues[-1] / omegap)
xsx

ires = 0
imode = 0
DEV = bk.zeros((Nres, Nmodes))
for imode in range(Nmodes):
    omega_n = eigenvalues[imode]
    phi_n = eigenvectors[:, imode]
    for ires in range(Nres):
        devx = simu.sensitivity_position(omega_n, phi_n, ires, 0)
        devy = simu.sensitivity_position(omega_n, phi_n, ires, 1)
        dev = (abs(devx) ** 2 + abs(devy) ** 2) ** 0.5
        DEV[ires, imode] = dev
    print(bk.linalg.norm(DEV[:, imode]))


npzname = (
    f"data/saved_{Nres}_quasiline_periodic.npz"
    if periodic
    else f"data/saved_{Nres}_quasiline_quasiperiodic.npz.npz"
)
np.savez(
    npzname,
    eigenvalues=eigenvalues,
    eigenvectors=eigenvectors,
    DEV=DEV,
)
