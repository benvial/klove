#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

"""
LDOS in finite cluster of resonators
==============================================

LDOS.
"""

import sys
import time
from math import pi

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from tools import *

import klove as kl

plt.ion()
plt.close("all")


def pause(i):
    # return
    # return plt.pause(i)
    plt.gcf().canvas.draw_idle()
    plt.gcf().canvas.start_event_loop(i)


bk = kl.backend
bk.random.seed(3)

save_figures = True
compute_ldos_map = True
compute_ldos_freq = True


def savefig(name, folder="./figs_tmp", dpi=100):
    if save_figures:
        plt.savefig(f"{folder}/{name}.eps", dpi=dpi)
        plt.savefig(f"{folder}/{name}.png", dpi=dpi)


#################################################################
# Parameters


a = 1
plate = kl.ElasticPlate(a / 1, 1, 1, 0.0)  # ,incoming=True)

omegap = plate.omega0(a)

Nres = int(sys.argv[1])
alternative = True

omega0 = (-0.3 - 2.4 * 1j) * omegap
omega1 = (1.6 + 0.1 * 1j) * omegap

# omega0 = (-0.02 -1.14 * 1j) * omegap
# omega1 = (2. + 0.1* 1j) * omegap

omega0 = (0.6 - 1.2 * 1j) * omegap
omega1 = (1.6 + 0.1 * 1j) * omegap

omega0 = (1.15 - 0.09 * 1j) * omegap
omega1 = (1.35 + 0.0001 * 1j) * omegap
# omega0 = (0.8 - 1.0 * 1j) * omegap
# omega1 = (1.6 + 0.01 * 1j) * omegap


recursive = True
strategy = "peaks"
# strategy = "grid"
# strategy = "random"
func_gives_der = False
refine = False
N_guess_loc = 0
lambda_tol = 1e-12
max_iter = 100
peak_ref = 6
Rloc = 0.005
N_grid = 10, 10


guesses = bk.array([1.8 - 0.0j]) * omegap
guesses = (bk.linspace(1.7, 1.9, 3) - 0.05j) * omegap
guesses = bk.array([0.983 - 0.0j]) * omegap
guesses = (bk.linspace(1.15, 1.35, 7) - 0.08j) * omegap
# guesses = (bk.linspace(0.976,0.984, 1) - 0.0001j) * omegap
guesses = None
# guesses = bk.array([1.20475 - 0.0j]) * omegap

#################################################################
# Plots

plot_det = False
plot_solver = True

m_plate = plate.rho * a**2 * plate.h

#################################################################
# Cluster

honeycomb = bool(int(sys.argv[2]))

omegar0 = omegap * 1
m0 = m_plate * 1
k0 = omegar0**2 * m0

nfreq = 600
start = 0.6 * omegap
stop = 1.6 * omegap
start = 1.2 * omegap
stop = 1.28 * omegap

# #### line array
# positionsx = bk.linspace(-Nres * a / 2, Nres * a / 2, Nres)
# positionsy = bk.linspace(-Nres * a / 2, Nres * a / 2, Nres)


# positionsx, positionsy = bk.meshgrid(positionsx, positionsy)
# positions = bk.stack([positionsx.flatten(), positionsy.flatten()]).T

# positions = (0.5 - bk.random.rand(2 * Nres)) * Nres*a / 2
# positions = bk.reshape(positions, (Nres, 2))

# Penrose lattice

psi = (5**0.5 - 1) / 2
psi2 = 1 - psi

positions = np.load(f"data/quasicrystal_{Nres}.npz")["vertices"]
# Nres = len(positions)
# positions = positions  # * Nres**0.5

delta = []
for i in range(Nres):
    for j in range(Nres):
        if i != j:
            delta.append(bk.linalg.norm(positions[i] - positions[j]))

mind = bk.min(delta)
# mind = bk.mean(delta)
# print(mind)

meand = bk.mean(delta)
positions /= mind
# positions /= meand


# honeycomb lattice

q = 3**0.5 / 2
d = 1

ux = bk.array([d, 0])
uy = bk.array([d / 2, q])
positions_hc = []
Nhc = 20
Rmax = 11.8
for i in range(-Nhc, Nhc + 1):
    for j in range(-Nhc, Nhc + 1):
        pos = i * ux + j * uy
        pos *= 2 * q * 0.92
        if pos[0] ** 2 + pos[1] ** 2 < Rmax**2:
            positions_hc.append(pos)


positions_hc = bk.array(positions_hc)

Nres_hc = len(positions_hc)


delta = []
for i in range(Nres_hc):
    for j in range(Nres_hc):
        if i != j:
            delta.append(bk.linalg.norm(positions_hc[i] - positions_hc[j]))

mind_hc = bk.min(delta)

meand_hc = bk.mean(delta)
# positions_hc *= mind_hc
# positions_hc /= meand_hc


plt.plot(*positions.T, ".")
plt.plot(*positions_hc.T, ".")
plt.axis("equal")

POS = {}
POS[True] = positions_hc
POS[False] = positions

# sys.exit(0)

if honeycomb:
    Nres = Nres_hc
    positions = positions_hc.copy()


# #### ring
# r0 = a
# t = bk.linspace(0, 2 * pi, Nres + 1)[:-1]
# t = bk.random.rand(Nres) * 2*bk.pi
# positions = bk.stack([r0 * bk.cos(t), r0 * bk.sin(t)]).T

# # positions = [(_x, 0) for _x in positionsx]

# # stiffnesses = 1+(0.9*(0.5 - bk.random.rand(Nres))) * k0
# # masses = 1+(0.9*(0.5 - bk.random.rand(Nres))) * m0

masses = bk.ones(Nres) * m0
stiffnesses = bk.ones(Nres) * k0
######################################################################
# Define the array of resonators

res_array = []
for i in range(Nres):
    res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))

######################################################################
# Define the simulation object
simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)


#################################################################
# Grid for fields

nptsx = nptsy = 101
# nptsy = int(nptsx / Nres)
dbx = dby = 2 * a

x1 = bk.linspace(-dbx + positions[0][0], dbx + positions[-1][0], nptsx)
y1 = bk.linspace(-dby + positions[0][1], dby + positions[-1][1], nptsy)

mini = min(bk.hstack([positions[:, 0], positions[:, 1]]))
maxi = max(bk.hstack([positions[:, 0], positions[:, 1]]))
lim = max(abs(maxi), abs(mini))
x1 = bk.linspace(-dbx - lim, dbx + lim, nptsx)
y1 = x1
# y1 = bk.linspace(-dby + positions[1].min(), dby + positions[1].max(), nptsy)

x, y = bk.meshgrid(x1, y1, indexing="xy")


#################################################################
# Plot complex plane quantities
if plot_det:
    #################################################################
    # Grid for complex frequency plane

    Nre, Nim = 80, 80
    omegas_re = bk.linspace(omega0.real, omega1.real, Nre)
    omegas_im = bk.linspace(omega1.imag, omega0.imag, Nim)
    omegas_re_, omegas_im_ = bk.meshgrid(omegas_re, omegas_im, indexing="ij")

    #################################################################
    # Compute complex plane quantities

    omegas_complex = omegas_re_ + 1j * omegas_im_
    Mc = simu.build_matrix(omegas_complex)
    Mc = bk.transpose(Mc, axes=(2, 3, 0, 1))

    det = bk.linalg.det(Mc)
    eigenvalues = bk.linalg.eigvals(Mc)
    srt = bk.argsort(bk.abs(eigenvalues), axis=-1)
    min_evs = bk.take_along_axis(eigenvalues, srt, axis=-1)[:, :, 0]
    plt.figure()
    plt.pcolormesh(omegas_re / omegap, omegas_im / omegap, bk.log10(bk.abs(det.T)))
    plt.colorbar()
    plt.xlabel("Re $\omega/\omega_p$")
    plt.ylabel("Im $\omega/\omega_p$")
    plt.title(r"${\rm log}_{10} |{\rm det}\, M|$")
    ax_det = plt.gca()
    plt.figure()
    plt.pcolormesh(
        omegas_re / omegap, omegas_im / omegap, bk.angle(det.T), cmap="twilight"
    )
    plt.colorbar()
    plt.xlabel("Re $\omega/\omega_p$")
    plt.ylabel("Im $\omega/\omega_p$")
    plt.title(r"${\rm log}_{10} |{\rm det}\, M|$")
    ax_det = plt.gca()

    plt.figure()
    plt.pcolormesh(
        omegas_re / omegap,
        omegas_im / omegap,
        bk.log10(bk.abs(min_evs.T)),
        cmap="inferno",
    )
    plt.colorbar()
    plt.xlabel("Re $\omega/\omega_p$")
    plt.ylabel("Im $\omega/\omega_p$")
    plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")
    ax_min_ev = plt.gca()
    pause(0.1)

    plt.figure()
    plt.pcolormesh(
        omegas_re / omegap, omegas_im / omegap, (bk.angle(det.T)), cmap="twilight"
    )
    plt.colorbar()
    plt.xlabel("Re $\omega/\omega_p$")
    plt.ylabel("Im $\omega/\omega_p$")
    plt.title(r"phase of ${\rm det}\, M$")
    ax_min_ev = plt.gca()
    pause(0.1)

# xsaxs
#################################################################
# Eigenvalue problem

print("Computing modes")

fig = plt.figure()
# guesses=None

t_eig = -time.time()

npzname = (
    f"data/eigenpairs_{Nres}_hc.npz" if honeycomb else f"data/eigenpairs_{Nres}.npz"
)
compute_modes = False
if compute_modes:
    eigenvalues, eigenvectors = simu.eigensolve(
        omega0,
        omega1,
        guesses=guesses,
        recursive=recursive,
        lambda_tol=lambda_tol,
        max_iter=max_iter,
        N_guess_loc=N_guess_loc,
        Rloc=Rloc,
        peak_ref=peak_ref,
        verbose=True,
        N_grid=N_grid,
        strategy=strategy,
        plot_solver=plot_solver,
        scale=omegap,
    )

    np.savez(
        npzname,
        eigenvalues=eigenvalues,
        eigenvectors=eigenvectors,
    )
else:
    arch = np.load(npzname)
    eigenvalues, eigenvectors = arch["eigenvalues"], arch["eigenvectors"]

t_eig += time.time()

# eigenvalues = eigenvalues[eigenvalues.real>0]
Nmodes = len(eigenvalues)
if Nmodes == 0:
    print("no modes found")
    sys.exit(0)
else:
    print(f"Found {Nmodes} modes")


# eigenvalues,indices = np.unique(eigenvalues, return_index=True)
# eigenvectors = eigenvectors[:,indices]

M = [simu.build_matrix(omega) for omega in eigenvalues]

dM = [simu.build_matrix_derivative(omega) for omega in eigenvalues]
eigenvectors = simu.normalize_eigenvectors(eigenvalues, eigenvectors, dM)

multiplicities = simu.get_multiplicities(eigenvalues, M, tol=lambda_tol**0.5)
print(multiplicities)
eigenvalues, eigenvectors = simu.update_eig(
    eigenvalues, eigenvectors, multiplicities, M, dM
)

Nmodes = len(eigenvalues)

# plt.pause(0.1)

# ires = 0
# imode = 0
# DEV = bk.zeros((Nres,Nmodes))
# for imode in range(Nmodes):
#     omega_n = eigenvalues[imode]
#     phi_n = eigenvectors[:,imode]
#     for ires in range(Nres):
#         devx = simu.sensitivity_position(omega_n, phi_n, ires, 0)
#         devy = simu.sensitivity_position(omega_n, phi_n, ires, 1)
#         dev = (abs(devx)**2+abs(devy)**2)**0.5
#         DEV[ires,imode] = dev
#     print(bk.linalg.norm(DEV[:,imode]))

# ##### compare honeycom to quasiperiodic in terms of spectrum sensitivity
# comp = {}
# comp_imode = {}

# for honeycomb in [False, True]:
#     print("honeycomb = ", honeycomb)
#     Nres = 199 if honeycomb else 191

#     npzname = f"data/saved_{Nres}_hc.npz" if honeycomb else f"data/saved_{Nres}.npz"
#     # np.savez(
#     #     npzname,
#     #     eigenvalues=eigenvalues,
#     #     eigenvectors=eigenvectors,
#     #     DEV=DEV,
#     # )

#     # sys.exit(0)

#     arch = np.load(npzname)
#     eigenvalues, eigenvectors = arch["eigenvalues"], arch["eigenvectors"]
#     DEV = arch["DEV"]


#     srt = bk.argsort(bk.abs(eigenvalues), axis=-1)
#     eigenvalues = eigenvalues[srt]
#     eigenvectors = eigenvectors[:,srt]
#     # print(eigenvalues/omegap)

#     DEV=DEV[:,srt]

#     Nres, Nmodes = DEV.shape

#     sens_spectrum = bk.linalg.norm(DEV, axis=0) / Nres
#     sens_spectrum_norm = bk.linalg.norm(sens_spectrum, axis=0) / Nmodes
#     sens_spectrum_mean = bk.mean(sens_spectrum)

#     # print(sens_spectrum)
#     # print(sens_spectrum_norm)
#     # print(sens_spectrum_mean)

#     comp[honeycomb] = sens_spectrum_mean

#     masses = bk.ones(Nres) * m0
#     stiffnesses = bk.ones(Nres) * k0
#     ######################################################################
#     # Define the array of resonators
#     positions = POS[honeycomb]

#     res_array = []
#     for i in range(Nres):
#         res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))

#     ######################################################################
#     # Define the simulation object
#     simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)

#     imode = 18
#     omega_n = eigenvalues[imode]
#     phi_n = eigenvectors[:, imode]
#     mode = simu.get_mode(x, y, phi_n, omega_n)
#     plt.figure()
#     # plt.pcolormesh(x,y,mode.real,cmap="RdBu_r")
#     plt.pcolormesh(x, y, bk.abs(mode), cmap="magma")

#     print(f"sens. spectrum mode {imode} = ", sens_spectrum[imode])
#     comp_imode[honeycomb] = sens_spectrum[imode]

#     plt.plot(*POS[honeycomb].T, ".",c="#42d495",ms=2)
#     plt.title(omega_n / omegap)
#     plt.axis("equal")
#     plt.pause(0.1)

# ratio = comp[True] / comp[False]
# print(ratio)

# ratio = comp_imode[True] / comp_imode[False]
# print(ratio)

# sys.exit(0)


######################################################
# QNM expansion


def arbitrary(omega):
    k = simu.wavenumber(omega)
    return 1 / k**3


omegas = bk.linspace(start, stop, nfreq)
incident = simu.point_source


def compute_ldos(x1, y1, omegas):
    nptsx = len(x1)
    nptsy = len(y1)
    nfreq = len(omegas)

    times_qmem = []
    times_direct = []

    direct_field = bk.zeros((nfreq, nptsx, nptsy), dtype=complex)
    qmem_field = bk.zeros((nfreq, nptsx, nptsy), dtype=complex)

    Ms = bk.zeros((nfreq, Nres, Nres), dtype=complex)
    times_M = []
    for i, omega in enumerate(omegas):
        t0 = -time.time()
        print(f"build M @ ω: {i+1}/{len(omegas)}")
        Ms[i] = simu.build_matrix(omega)
        t0 += time.time()
        times_M.append(t0)
    for jx, xs in enumerate(x1):
        for jy, ys in enumerate(y1):
            pos_source = xs, ys
            pos_probe = pos_source

            print(f"position source rs: {pos_source}")
            ####### direct
            t0 = -time.time()
            direct = bk.zeros((nfreq, Nres), dtype=complex)
            for i, omega in enumerate(omegas):
                print(f"direct ω: {i+1}/{len(omegas)}")
                f = simu.build_rhs(incident, omega, pos_source)
                direct[i] = bk.linalg.solve(Ms[i], f)
                # direct[i] = simu.solve(incident, omega, pos_source)
            t0 += time.time()
            if jx == 0 and jy == 0:
                t0 += sum(times_M)

            ####### modal
            t1 = -time.time()
            gammas = bk.zeros((nfreq, Nmodes), dtype=complex)
            for i, omega in enumerate(omegas):
                print(f"modal ω: {i+1}/{len(omegas)}")
                gamma = bk.zeros(Nmodes, dtype=complex)
                phi0_vec = simu.build_rhs(incident, omega, pos_source)
                for n in range(Nmodes):
                    omegan = eigenvalues[n]
                    num = phi0_vec @ eigenvectors[:, n]
                    gamma[n] = (
                        num / (omega - omegan) * arbitrary(omegan) / arbitrary(omega)
                    )
                gammas[i] = gamma

            qmem = bk.zeros((nfreq, Nres), dtype=complex)
            for i, omega in enumerate(omegas):
                print(f" modal (extra) ω: {i+1}/{len(omegas)}")
                rec = 0
                for n in range(Nmodes):
                    rec += gammas[i, n] * eigenvectors[:, n]
                qmem[i] = rec
            t1 += time.time()
            speedup = t0 / t1
            # print(t0, t1, speedup)
            times_direct.append(t0)
            times_qmem.append(t1)

            for i, omega in enumerate(omegas):
                # print(f"ω: {i+1}/{len(omegas)}")
                k = simu.wavenumber(omega)
                LDOS_empty = k / (2 * bk.pi)
                coeff = 4 * k**3 / bk.pi
                gf = simu.get_field(*pos_probe, incident, direct[i], omega, pos_source)
                LDOS = coeff * gf
                direct_field[i, jx, jy] = LDOS / LDOS_empty
                gf = simu.get_field(*pos_probe, incident, qmem[i], omega, pos_source)
                LDOS = coeff * gf
                qmem_field[i, jx, jy] = LDOS / LDOS_empty

    times_qmem = bk.array(times_qmem)
    times_direct = bk.array(times_direct)
    return direct_field, qmem_field, times_direct, times_qmem


pos_probe = 0, 0
# pos_probe = positions[3]
re_eig_sorted_imag = bk.real(eigenvalues[bk.argsort(-eigenvalues.imag)])

re_eig_sorted_imag = np.unique(re_eig_sorted_imag)
re_eig_sorted_imag = re_eig_sorted_imag[re_eig_sorted_imag > 0]

omegas = bk.linspace(start, stop, nfreq)
omegas = bk.hstack((omegas, re_eig_sorted_imag))
omegas = bk.sort(omegas)
nfreq = len(omegas)


npzname = f"data/ldosfreq.npz"

if compute_ldos_freq:
    direct_field, qmem_field, times_direct, times_qmem = compute_ldos(
        [pos_probe[0]], [pos_probe[1]], omegas
    )

    np.savez(npzname, direct_field=direct_field, qmem_field=qmem_field)

    time_modal = bk.sum(times_qmem)
    time_direct = bk.sum(times_direct)

    print(
        f"t modal: mean = {bk.mean(times_qmem):.2e}s, std = {bk.std(times_qmem):.2e}s"
    )
    print(
        f"t direct: mean = {bk.mean(times_direct):.2e}s, std = {bk.std(times_direct):.2e}s"
    )
    print(f"t eig: {t_eig:.2e}s")
    print(f"t modal= {time_modal:.2f}")
    print(f"t direct= {time_direct:.2f}")
    print(f"speedup (without eig) = {time_direct/time_modal:.2f}")
    print(f"speedup = {time_direct/(time_modal + t_eig):.2f}")

else:
    data = np.load(npzname)
    direct_field = data["direct_field"]
    qmem_field = data["qmem_field"]


ldos_direct = bk.imag(direct_field[:, 0, 0]).copy()
ldos_modal = bk.imag(qmem_field[:, 0, 0]).copy()

# ldos_modal -= 1

log_lines = False

for log_lines in (True, False):
    fig, ax = plt.subplots(2, 1, sharex=True)
    plt.sca(ax[0])
    plt.plot(
        eigenvalues.real / omegap, eigenvalues.imag / omegap, "o", c="#d24949", ms=4
    )
    plt.ylabel("Im $\omega/\omega_p$")
    plt.sca(ax[1])
    plt.plot(omegas / omegap, ldos_direct, "-", label="scattering")
    plt.plot(omegas / omegap, ldos_modal, "--", label="modal", c="#d24949")
    if log_lines:
        plt.yscale("log")
    plt.xlabel("Re $\omega/\omega_p$")
    plt.ylabel("$2\pi\mathcal{L}(0,0)/k$")
    pause(0.1)
    eigenvalues_u = bk.unique(eigenvalues) / omegap
    for _ in ax:
        _.set_xlim(start / omegap, stop / omegap)
        _.set_xlim(1.2, 1.25)
    # ax[0].set_ylim(-0.4)
    for e in eigenvalues_u:
        for _ in ax:
            _.axvline(e.real, ls=":", lw=0.7, c="#9a9a9a")

    # ax[0].set_xticklabels([])
    ax[1].set_ylim(0.1)
    plt.legend()
    plt.tight_layout()

    savefig(f"LDOS_center_log_{log_lines}")


sys.exit(0)


# plt.figure()
# plt.plot(omegas / omegap, ldos_direct - ldos_modal, "--", label="error")
# # plt.yscale("log")
# plt.legend()
# plt.xlabel("$\omega/\omega_p$")
# # plt.ylabel("$2\pi\mathcal{L}(0,0)/k$")
# pause(0.1)
# # sys.exit(0)


ieig = -1
omega_map = re_eig_sorted_imag[ieig]

# omega_map = 1.21162276 * omegap

re_eig_sorted_imag = [re_eig_sorted_imag[0], re_eig_sorted_imag[2]]


for jmap, omega_map in enumerate(re_eig_sorted_imag):
    npzname = f"data/ldosmap_{jmap}.npz"

    if compute_ldos_map:
        (
            direct_field_map,
            qmem_field_map,
            times_direct_map,
            times_qmem_map,
        ) = compute_ldos(x1, y1, [omega_map])

        time_modal_map = bk.sum(times_qmem_map)
        time_direct_map = bk.sum(times_direct_map)

        print(
            f"t modal: mean = {bk.mean(times_qmem_map):.2e}s, std = {bk.std(times_qmem_map):.2e}s"
        )
        print(
            f"t direct: mean = {bk.mean(times_direct_map):.2e}s, std = {bk.std(times_direct_map):.2e}s"
        )
        print(f"t eig: {t_eig:.2e}s")
        print(f"t modal= {time_modal_map:.3f}")
        print(f"t direct= {time_direct_map:.3f}")
        print(f"speedup (without eig) = {time_direct_map/time_modal_map:.3f}")
        print(f"speedup = {time_direct_map/(time_modal_map + t_eig):.3}")
        np.savez(
            npzname, direct_field_map=direct_field_map, qmem_field_map=qmem_field_map
        )

    else:
        data = np.load(npzname)
        direct_field_map = data["direct_field_map"]
        qmem_field_map = data["qmem_field_map"]

    ifreq = 0
    err = bk.abs(direct_field_map - qmem_field_map)
    err_mean = bk.mean(err) / bk.mean(bk.abs(direct_field_map))
    err = bk.abs(bk.imag(direct_field_map - qmem_field_map))
    err_mean = bk.mean(err) / bk.mean(bk.abs(bk.imag(direct_field_map)))
    err = 1 - bk.imag(qmem_field_map) / bk.imag(direct_field_map)
    err_mean = bk.mean(bk.abs(err))
    print(err_mean)

    err_abs = bk.abs(direct_field_map[ifreq] - qmem_field_map[ifreq])

    plt.figure()
    plt.pcolormesh(x1 / a, y1 / a, err_abs.T, cmap="inferno")
    # plt.pcolormesh(x1 / a, y1 / a, (100 * err[ifreq]).T, cmap="RdBu_r")
    # plt.title(rf"Relative error = {100*err_mean:.3f}\%")
    plt.colorbar()
    plt.axis("scaled")
    savefig(rf"error_{omega_map}")

    field_dir = bk.imag(direct_field_map[ifreq]).T
    # field_dir = bk.log10(field_dir)

    field_mod = bk.imag(qmem_field_map[ifreq]).T
    # field_mod = bk.log10(field_mod)

    logmap = False
    norm = mpl.colors.LogNorm() if logmap else None
    fig, ax = plt.subplots(1, 2, figsize=(9, 4))
    pm = ax[0].pcolormesh(x1 / a, y1 / a, field_mod, cmap="magma", norm=norm)
    plt.colorbar(pm)
    ax[0].set_title("normalized LDOS")
    pm = ax[1].pcolormesh(x1 / a, y1 / a, err_abs, cmap="magma", norm=norm)
    plt.colorbar(pm)
    ax[1].set_title("error")
    for _ax in ax:
        plt.sca(_ax)
        for i in range(Nres):
            xr, yr = res_array[i].position
            _ax.plot(xr / a, yr / a, ".", c="#4dbf93", ms=4)
        plt.xlabel("$x/a$")
        plt.ylabel("$y/a$")
        _ax.set_aspect(1)
    title = rf"$\omega/\omega_p ={omega_map/omegap:.3f}$"
    plt.suptitle(title)
    plt.tight_layout()
    pause(0.1)
    savefig(f"LDOS_{omega_map}")

sys.exit(0)

field_dir = bk.imag(direct_field_map)
field_mod = bk.imag(qmem_field_map)
peudo_DOS_dir = bk.trapz(bk.trapz(field_dir, x1), y1) / bk.trapz(
    bk.trapz(1 + 0 * field_dir, x1), y1
)
peudo_DOS_mod = bk.trapz(bk.trapz(field_mod, x1), y1) / bk.trapz(
    bk.trapz(1 + 0 * field_mod, x1), y1
)
plt.figure()
plt.plot(omegas, peudo_DOS_dir, label="scattering")
plt.plot(omegas, peudo_DOS_mod, label="modal")

# n = bk.argsort(-eigenvalues.imag)[0]

# fig, ax = plt.subplots()
# # plt.close("all")
# Ra = dbx * 0.5

# figx = 9
# ratio = (x1[-1] - x1[0]) / (y1[-1] - y1[0]) * 0.8
# # plt.figure(figsize=(figx, figx / ratio))
# omega_n = eigenvalues[n]

# phin = eigenvectors[:, n].copy()
# iphase = 0
# maxvect = bk.max(bk.abs(phin))
# scale = Ra / maxvect
# phin *= scale

# phin *= bk.exp(-1j * bk.angle(phin[iphase]))

# Weig = simu.get_scattered_field(x / a, y / a, phin, omega_n)
# cm = ax.pcolormesh(x / a, y / a, Weig.real, cmap="RdBu_r")

# plt.colorbar(cm, orientation="vertical", fraction=0.046, pad=0.04)
# for i in range(Nres):
#     xr, yr = res_array[i].position
#     ax.plot(xr / a, yr / a, ".k")
#     xa = phin[i].real
#     ya = phin[i].imag
#     # plt.plot([xr/a, (xr + xa)/a], [yr/a, (yr + ya/a)], "b-")
#     ax.arrow(
#         xr / a,
#         yr / a,
#         (xa) / a,
#         (ya / a),
#         head_width=0.033,
#         head_length=0.05,
#         fc="k",
#         ec="k",
#     )

# ax.set_title(f"$\omega = ${omega_n:.4f}")
# plt.xlabel("$x/a$")
# plt.ylabel("$y/a$")

# # plt.xlim((x1[0] - dbx - Ra) / a, (x1[-1] + dbx + Ra) / a)
# # plt.ylim((y1[0] - dby - Ra) / a, (y1[-1] + dby + Ra) / a)

# plt.axis("scaled")

# plt.title(f"mode {n+1}, $\omega_{{{n+1}}}={omega_n:.3f}$")
# ax.set_aspect(1)
# plt.tight_layout()
