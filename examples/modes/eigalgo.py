#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: GPLv3

import matplotlib.pyplot as plt
import scienceplots

import klove as kl

plt.style.use(["science"])
plt.rcParams["font.size"] = 12
plt.rcParams["figure.figsize"] = (6, 6 / 1.618)

plt.ion()
plt.close("all")

import numpy as np

ev_random = np.load(f"/home/bench/doc/data/tmp_random.npz")["lambda_history"]
ev = np.load(f"/home/bench/doc/data/tmp_eig.npz")["lambda_history"]

conv_random = abs(ev_random[-1] - np.array(ev_random)[:-1])
conv = abs(ev[-1] - np.array(ev)[:-1])


plt.plot(range(1, 1 + len(conv_random)), conv_random, "-o", label="random")
plt.plot(range(1, 1 + len(conv)), conv, "s-", label="svd")
plt.yscale("log")
plt.xlabel("iteration number")
plt.ylabel("error")
plt.legend()
plt.tight_layout()
