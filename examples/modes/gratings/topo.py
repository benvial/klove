#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: GPLv3


#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: GPLv3

"""
Topological edge state
======================

Finding modes.
"""

import sys
import time
from math import pi

import matplotlib.pyplot as plt

import klove as kl

sys.path.append("../")
from tools import *

plot_array = False


plt.ion()
plt.close("all")
bk = kl.backend

shiftAngle = pi / 75

a = 1

elle = a
hache = 3**0.5 * elle / 6


# nptsx = 101
# nptsy = nptsx
# nptsy = int(nptsx * Nlines / 2)

# x1 = bk.linspace(0, period, nptsx)
# y1 = bk.linspace(-Nlines * hache, 6, nptsy)

# x, y = bk.meshgrid(x1, y1, indexing="xy")


plate = kl.ElasticPlate(1, 0.1, 1, 0.0)
k = (4 * pi) ** 2 * plate.omega0(a) * 20
m = 0.8
omega_res = (k / m) ** 0.5
omegap = plate.omega0(a)

positions0 = [
    (-1 / 2 * elle, -hache),
    (0, -hache),
    (1 / 2 * elle, -hache),
    (1 / 4 * elle, -hache + elle * 3**0.5 / 4),
    (0, -hache + elle * 3**0.5 / 2),
    (-1 / 4 * elle, -hache + elle * 3**0.5 / 4),
]


lattice_vectors = (
    (bk.sqrt(2) * a * bk.cos(pi / 6), bk.sqrt(2) * a * bk.sin(pi / 6)),
    (-bk.sqrt(2) * a * bk.cos(pi / 6), bk.sqrt(2) * a * bk.sin(pi / 6)),
)


def rotate(positions, shiftAngle):
    return bk.array(
        [
            (
                positions0[i][0] * bk.cos(shiftAngle)
                + positions0[i][1] * bk.sin(shiftAngle),
                -positions0[i][0] * bk.sin(shiftAngle)
                + positions0[i][1] * bk.cos(shiftAngle),
            )
            for i in range(len(positions))
        ]
    )


shiftAngle = 0 * pi / 75

positions = rotate(positions0, shiftAngle)

res_array = []
masses = m
stiffnesses = k

for pos in positions:
    res_array.append(kl.Resonator(masses, stiffnesses, pos))


simu = kl.BandsSimulation(plate, res_array, lattice_vectors)

plt.plot(*positions.T, "or")
xs

######################################################################
# Special points

b0 = bk.pi / a
Gamma_point = 0, 0
M_point = b0, -b0 / 3**0.5
K_point = 4 * b0 / 3, 0
sym_points = [Gamma_point, M_point, K_point]


######################################################################
# Build wavector path

nband = 51
ks = kl.init_bands(sym_points, nband)

eigenvalues = bk.array([simu.eigensolve(k, return_modes=False, Npw=3) for k in ks]).real
ksplot, kdist = kl.init_bands_plot(sym_points, nband)


######################################################################
# Plot bands

kl.plot_bands(sym_points, nband, eigenvalues)

plt.ylim(0, 100)
plt.tight_layout()
