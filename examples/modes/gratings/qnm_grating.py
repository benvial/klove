#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

"""
Finite cluster of resonators
============================

Finding modes.
"""
import importlib
import sys
import time
from math import pi

import matplotlib.pyplot as plt

import klove as kl

importlib.reload(kl.core)
importlib.reload(kl)
import tools
from tools import *

import klove as kl

importlib.reload(tools)
import tools
from tools import *

plt.ion()
plt.close("all")
bk = kl.backend


#################################################################
# Parameters

Nres = int(sys.argv[1])
incoming = bool(int(sys.argv[2]))
nh = int(sys.argv[3])

kx = float(sys.argv[4])
a = 1
period = a * 1.0


omega0 = -5.2 - 1.01 * 1j
omega1 = 2.1 + 1 * 1j

# omega0 = -2.001 - 1.01 * 1j
# omega1 = 2.1 + 0 * 1j


peak_ref = 30
N_guess_loc = 0

Rloc = 0.01
func_gives_der = False
refine = False
lambda_tol = 1e-8
recursive = False


nptsx = 101
nptsy = nptsx
# nptsy = int(nptsx / Nres)
dbx = 3 * a
dby = 6 * a

#################################################################
# Plots

plot_det = True
plot_solver = False
save_figures = False
Nplot = 9
Nbmax = 6


def savefig(name, folder="./figs/grating", dpi=100):
    if save_figures:
        # plt.savefig(f"{folder}/{name}.eps", dpi=dpi)
        plt.savefig(f"{folder}/{name}.png", dpi=dpi)


######################################################################
# Elastic plate

plate = kl.ElasticPlate(a * 1, 1, 1, 0.0, incoming=False)
p = 256 * plate.rho * plate.h * plate.D

omegap = plate.omega0(a)
m_plate = plate.rho * a**2 * plate.h

#################################################################
# Cluster
omega0 *= omegap
omega1 *= omegap


omegar0 = omegap * (1.5 - 0 * 1j)

m0 = m_plate
k0 = omegar0**2 * m0
damping = 0.0

#### line array
positionsy = bk.linspace(-Nres * a / 2, Nres * a / 2, Nres) * period * 1.5
positionsx = bk.linspace(0.1, 0.9, Nres) * period
# positionsx = bk.ones(Nres) * period / 2
positions = [(_x, _y) for _x, _y in zip(positionsx, positionsy)]
# positions = [(period / 2, _y) for _y in positionsy]

omegars = omegar0 * bk.linspace(1.0, 0.8, Nres)
masses = k0 / omegars**2  # graded
stiffnesses = bk.ones(Nres) * k0

######################################################################
# Define the array of resonators

res_array = []
for i in range(Nres):
    res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i], damping))

# plt.plot(bk.array([r.omega_r for r in res_array]) / omegar0)
# plt.plot(omegars / omegar0, "o")


######################################################################
# Define the simulation object
delta = 1e-4


def get_fd(f, omega, *args, **kargs):
    dsfd = f(omega + delta, *args, **kargs)
    dsfd -= f(omega, *args, **kargs)
    dsfd /= delta
    return dsfd


simu = kl.DiffractionSimulation(plate, res_array, period, nh=nh, force_propa=False)

omega = 0.7 - 0.1j

ds = simu.build_matrix_derivative(omega, kx)
dsfd = get_fd(simu.build_matrix, omega, kx=kx)
assert bk.allclose(ds, dsfd, atol=1e-3)

t = -time.time()
eigenvalues, eigenvectors_right, eigenvectors_left = simu.eigensolve(
    omega0,
    omega1,
    kx=kx,
    recursive=recursive,
    lambda_tol=lambda_tol,
    func_gives_der=func_gives_der,
    refine=refine,
    peak_ref=peak_ref,
    verbose=True,
    plot_solver=plot_solver,
    scale=omegap,
)
t += time.time()

c1 = bk.logical_and(eigenvalues.real > 0, eigenvalues.imag < 0)
c2 = bk.logical_and(eigenvalues.real < 0, eigenvalues.imag < 0)
c3 = bk.logical_and(eigenvalues.real > 0, eigenvalues.imag > 0)
c4 = bk.logical_and(eigenvalues.real < 0, eigenvalues.imag > 0)
# c3 = c4 = c1
cc = bk.logical_or(bk.logical_or(c1, c2), bk.logical_or(c3, c4))

idx = bk.where(cc)[0]
eigenvalues = eigenvalues[idx]
eigenvectors_right = eigenvectors_right[:, idx]
eigenvectors_left = eigenvectors_left[:, idx]


Nmodes = len(eigenvalues)
print(f"Found {Nmodes} eigenvalues in {t:.2f}s")


# idx = bk.where(eigenvalues.imag < 0)
# eigenvalues[idx] = eigenvalues[idx].conj()
# eigenvectors_right[:, idx] = eigenvectors_right[:, idx].conj()
# eigenvectors_left[:, idx] = eigenvectors_left[:, idx].conj()


tolcheck = lambda_tol * 10

M = [simu.build_matrix(omega, kx) for omega in eigenvalues]
dM = [simu.build_matrix_derivative(omega, kx) for omega in eigenvalues]

# normalize modes
eigenvectors_right, eigenvectors_left = simu.normalize_eigenvectors(
    kx,
    eigenvalues,
    eigenvectors_right,
    eigenvectors_left,
    dM,
)


multiplicities = simu.get_multiplicities(kx, eigenvalues, M)


eigenvalues, eigenvectors_right, eigenvectors_left = simu.update_eig(
    kx, eigenvalues, eigenvectors_right, eigenvectors_left, multiplicities, M, dM
)


for i in range(Nmodes):
    ev = eigenvalues[i]
    vr = eigenvectors_right[:, i]
    vl = eigenvectors_left[:, i]
    M0 = simu.build_matrix(ev, kx)
    dM0 = simu.build_matrix_derivative(ev, kx)
    dMfd0 = get_fd(simu.build_matrix, ev, kx)
    assert bk.allclose(dM0, dMfd0, 10 * delta)
    nrm = bk.linalg.norm(M0 @ vr)
    assert nrm < tolcheck
    nrm = bk.linalg.norm(vl.conj() @ M0)
    assert nrm < tolcheck

check_ortho = bk.zeros((Nmodes, Nmodes), dtype=bk.complex64)

for i in range(Nmodes):
    ev = eigenvalues[i]
    dM = simu.build_matrix_derivative(ev, kx)
    vr = eigenvectors_right[:, i]
    for j in range(Nmodes):
        vl = eigenvectors_left[:, j]
        dotp = vl.conj() @ (dM @ vr)
        check_ortho[i, j] = dotp

assert bk.allclose(bk.diagonal(check_ortho), 1)


check_ortho = bk.zeros((Nmodes, Nmodes), dtype=bk.complex64)

for m in range(Nmodes):
    evm = eigenvalues[m]
    vrm = eigenvectors_right[:, m]

    for n in range(Nmodes):
        evn = eigenvalues[n]
        vln = eigenvectors_left[:, n]
        if m == n:
            dM = simu.build_matrix_derivative(evm, kx)
        else:
            Mm = simu.build_matrix(evm, kx)
            Mn = simu.build_matrix(evn, kx)
            dM = (Mm - Mn) / (evm - evn)
        dotp = vln.conj() @ (dM @ vrm)
        check_ortho[m, n] = dotp


error = bk.abs(check_ortho - bk.eye(Nmodes))


# plt.clf()
# plt.imshow(bk.real(check_ortho))


# plt.figure()
# plt.imshow(error)
# plt.colorbar()

assert bk.all(error < 1e-7)

#################################################################
# QNM expansion

expo = float(sys.argv[-1])


def arbitrary(omega):
    k = simu.wavenumber(omega)
    return k ** (expo)


incident = simu.plane_wave

nfreq = 500
start = 0.001 * omegap
stop = 5 * omegap
omegas = bk.linspace(start, stop, nfreq)

bs = simu.get_expansion_coefficients(
    eigenvalues, eigenvectors_left, kx, omegas, arbitrary
)


## compute Phi
direct = bk.zeros((nfreq, Nres), dtype=complex)
qmem = bk.zeros((nfreq, Nres), dtype=complex)
for i, omega in enumerate(omegas):
    k0 = simu.plate.wavenumber(omega)
    angle = bk.arccos(kx / k0)
    direct[i] = simu.solve(omega, angle)
    rec = 0
    for n in range(Nmodes):
        rec += bs[i, n] * eigenvectors_right[:, n]
    qmem[i] = rec
qmem = bk.array(qmem)


## check expansion coeffs direct
bs_direct = bk.zeros((nfreq, Nmodes), dtype=complex)
for i, omega in enumerate(omegas):
    M_omega = simu.build_matrix(omega, kx)
    for n in range(Nmodes):
        omega_n = eigenvalues[n]
        bs_direct[i, n] = (
            (eigenvectors_left[:, n].conj() @ (M[n] - M_omega))
            @ direct[i]
            / (omega_n - omega)
            * arbitrary(omega_n)
            / arbitrary(omega)
        )


# expo=2.5
# plt.plot(omegas, direct/qmem)
# plt.plot(omegas, abs(omegas)**expo/bk.abs(eigenvalues[0])**expo)
# xssx
fig, ax = plt.subplots(1, 2)
plot_expansion_coeffs(omegas, bs, bs_direct, omegap, ax=ax)

# plt.figure()

# plt.plot(omegas / omegap, direct.imag)
# plt.plot(omegas / omegap, qmem.imag,"--")
# plt.xlabel(r"$\omega/\omega_p$")

# ylabel = r"$Re \phi_\alpha$"
# plt.ylabel(ylabel)


# plot_expansion_coeffs(omegas, bs, bs_direct, omegap,"abs")

plot_phis(omegas, direct, qmem, omegap, "abs")
# plot_phis(omegas, direct, qmem, omegap, "phase")
# savefig("plot_phis_abs")


print(bk.mean(bk.abs(direct - qmem)))


sys.exit(0)


#################################################################


# simu.harmonics = [11]
# simu.force_propa=True


# sys.exit(0)
#################################################################
# Grid for fields


x1 = bk.linspace(-dbx + positions[0][0], dbx + positions[-1][0], nptsx)
y1 = bk.linspace(-dby + positions[0][1], dby + positions[-1][1], nptsy)

x, y = bk.meshgrid(x1, y1, indexing="xy")


# plt.figure(figsize= (7,7))
if plot_det:
    simu.plate.incoming = incoming
    #################################################################
    # Grid for complex frequency plane

    Nre, Nim = 200, 200

    # k0 = simu.wavenumber(omega0)
    # k1 = simu.wavenumber(omega1)
    # k0 = -3 - 3 * 1j
    # k1 = 3 + 3 * 1j

    # k_re = bk.linspace(k0.real, k1.real, Nre)
    # k_im = bk.linspace(k1.imag, k0.imag, Nim)
    # k_re_, k_im_ = bk.meshgrid(k_re, k_im, indexing="ij")
    # ks_complex = k_re_ + 1j * k_im_
    # omegas_re = simu.frequency(k_re)
    # omegas_im = -simu.frequency(k_im)
    # # omegas_complex = simu.frequency(ks_complex)

    omegas_re = bk.linspace(omega0.real, omega1.real, Nre)
    omegas_im = bk.linspace(omega1.imag, omega0.imag, Nim)
    omegas_re_, omegas_im_ = bk.meshgrid(omegas_re, omegas_im, indexing="ij")
    omegas_complex = omegas_re_ + 1j * omegas_im_

    #################################################################
    # Compute complex plane quantities

    Mc = simu.build_matrix(omegas_complex, kx)
    # Mc = build_matrixk(simu,ks_complex)
    # Mc = bk.transpose(Mc, axes=(2, 3, 1, 0)).conj()
    Mc = bk.transpose(Mc, axes=(2, 3, 0, 1))
    det = bk.linalg.det(Mc)
    evsM = bk.linalg.eigvals(Mc)
    srt = bk.argsort(bk.abs(evsM), axis=-1)
    min_evs = bk.take_along_axis(evsM, srt, axis=-1)[:, :, 0]

    # min_evs = simu.plate.greens_function(omegas_complex,a)

    #################################################################
    # # Plot complex plane quantities
    # ax = plot_complex_plane_map(
    #     omegas_re, omegas_im, bk.log10(bk.abs(det.T)), omegap=omegap
    # )
    # plt.title(r"${\rm log}_{10} |{\rm det}\, M|$")
    # plt.tight_layout()

    # plt.pause(0.1)

    ax = plot_complex_plane_map(
        omegas_re, omegas_im, bk.log10(bk.abs(min_evs.T)), omegap=omegap
    )
    plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")
    plt.tight_layout()

    plt.pause(0.1)

    savefig(f"detmap_incoming_{incoming}")

    simu.plate.incoming = False

# else:
#     ax = plt.gca()

# sys.exit(0)

# kx = k0 * cos(angle)

# angle = pi / 2
omegas = bk.linspace(omega0.real, omega1.real, 1000)
Rs = []
Ts = []
Rtot = []
Ttot = []
for omega in omegas:
    k0 = simu.plate.wavenumber(omega)
    angle = bk.arccos(kx / k0)
    sol = simu.solve(omega, angle)
    R, T = simu.get_efficiencies(sol, omega, angle)
    Rs.append(R["energy"])
    Ts.append(T["energy"])
    Rtot.append(R["total"])
    Ttot.append(T["total"])


Nre = 100
Nim = 100

omegas_re = bk.linspace(omega0.real, omega1.real, Nre)
omegas_im = bk.linspace(omega1.imag, omega0.imag, Nim)
omegas_re_, omegas_im_ = bk.meshgrid(omegas_re, omegas_im, indexing="ij")
omegas_complex = omegas_re_ + 1j * omegas_im_
Rmap = bk.zeros_like(omegas_complex)
Tmap = bk.zeros_like(omegas_complex)
for i, omega_re in enumerate(omegas_re):
    for j, omega_im in enumerate(omegas_im):
        k0 = simu.plate.wavenumber(omega)
        angle = bk.arccos(kx / k0)
        omega = omega_re + 1j * omega_im
        sol = simu.solve(omega, angle)
        R, T = simu.get_efficiencies(sol, omega, angle)
        Rmap[i, j] = R["total"]
        Tmap[i, j] = T["total"]


plt.figure()
axRmap = plot_complex_plane_map(
    omegas_re, omegas_im, bk.log10(bk.abs(Rmap.T)), omegap=omegap
)
plt.title(r"${\rm log}_{10} |{R}|$")
plt.tight_layout()
savefig(f"Rmap_incoming_{incoming}")

plt.figure()
axTmap = plot_complex_plane_map(
    omegas_re, omegas_im, bk.log10(bk.abs(Tmap.T)), omegap=omegap
)
plt.title(r"${\rm log}_{10} |{T}|$")
plt.tight_layout()

savefig(f"Tmap_incoming_{incoming}")
# nhplt = 1  # simu.get_min_harmonics(omega1.real, angle)
# plt.figure()
# for i, harm in enumerate(range(-nhplt, nhplt + 1)):
#     Rplot = [_[str(harm)] for _ in Rs]
#     line = plt.plot(omegas / omegap, Rplot, label="R " + str(harm))
#     color = line[0].get_color()

#     Tplot = [_[str(harm)] for _ in Ts]
#     plt.plot(omegas / omegap, Tplot, "--", c=color, label="T " + str(harm))
#     plt.xlabel(r"$\omega$")
#     plt.legend()
# plt.show()

######################################################################
# Plot the total reflection, transmission and energy balance
Rtot = bk.array(Rtot)
Ttot = bk.array(Ttot)
# for b in Balance:
#     assert abs(b - 1) < 1e-9

Atot = 1 - Rtot - Ttot
Balance = Rtot + Ttot + Atot


plt.figure()
plt.plot(omegas / omegap, Rtot, "-", c="#4d8cdf", label="R")
plt.plot(omegas / omegap, Ttot, "-", c="#df4d59", label="T")
plt.plot(omegas / omegap, Atot, "-", c="#43b395", label="A")
# plt.plot(omegas / omegap, Balance, "-k", label="B")
plt.legend()
plt.xlabel(r"$\omega$")
plt.show()
axeff = plt.gca()

if plot_solver:
    plt.figure()


#################################################################
# Eigenvalue problem


# guesses = omegaT[omegaT.real > 0]  # /omegap


simu.plate.incoming = incoming


eigenvalues, eigenvectors_right, eigenvectors_left = simu.eigensolve(
    omega0,
    omega1,
    kx=kx,
    recursive=recursive,
    lambda_tol=lambda_tol,
    max_iter=max_iter,
    guesses=guesses,
    N_guess_loc=N_guess_loc,
    func_gives_der=func_gives_der,
    init_vect=init_vect,
    weight=weight,
    Rloc=Rloc,
    peak_ref=peak_ref,
    verbose=False,
    strategy=strategy,
    plot_solver=plot_solver,
    scale=omegap,
    return_left=True,
)

# for res in simu.res_array:
#     lres = plt.plot(res.omega_r.real / omegap, res.omega_r.imag / omegap, ".y", ms=3)
eigenvalues = bk.array(eigenvalues)
print(eigenvalues / omegap)


savefig(f"eigmap_incoming_{incoming}")


plt.sca(axeff)
for e in eigenvalues:
    plt.axvline(e.real / omegap, ls="--", color="#939393")


evn = eigenvalues / omegap
axRmap.plot(evn.real, evn.imag, ".g")
axTmap.plot(evn.real, evn.imag, ".g")
sys.exit(0)
