#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: GPLv3

"""
Topological edge state
======================

Finding modes.
"""

import sys
import time
from math import pi

import matplotlib.pyplot as plt

import klove as kl

sys.path.append("../")
from tools import *

plot_array = False


plt.ion()
plt.close("all")
bk = kl.backend

shiftAngle = pi / 75

a = 1

elle = a
hache = 3**0.5 * elle / 6


period = 1 * bk.sqrt(2) * a

Nlines = 3 * 20
nh = 2

kx = 0.1 * pi / period


nptsx = 101
nptsy = nptsx
nptsy = int(nptsx * Nlines / 2)

x1 = bk.linspace(0, period, nptsx)
y1 = bk.linspace(-Nlines * hache, 6, nptsy)

x, y = bk.meshgrid(x1, y1, indexing="xy")

shiftAngle = pi / 75


plate = kl.ElasticPlate(1, 0.1, 1, 0.0)
k = (4 * pi) ** 2 * plate.omega0(a) * 20
m = 0.8
omega_res = (k / m) ** 0.5

omegap = plate.omega0(a)

# omegar0 = omegap * (1.5 - 0 * 1j)

# m_plate = plate.rho * a**2 * plate.h
# m = m_plate
# k = omegar0**2 * m


# print(omega_res / omegap)


omega0 = (30 - 10.1 * 1j) * omegap
omega1 = (40 + 10.01 * 1j) * omegap
peak_ref = 2
N_guess_loc = 0

Rloc = 0.01
func_gives_der = False
refine = False
lambda_tol = 1e-3
recursive = False
max_iter = 30

plot_det = False

positions0 = [
    (-1 / 2 * elle, -hache),
    (0, -hache),
    (1 / 2 * elle, -hache),
    (1 / 4 * elle, -hache + elle * 3**0.5 / 4),
    (0, -hache + elle * 3**0.5 / 2),
    (-1 / 4 * elle, -hache + elle * 3**0.5 / 4),
]


positions1 = [
    (
        positions0[0][0] * bk.cos(shiftAngle) + positions0[0][1] * bk.sin(shiftAngle),
        -positions0[0][0] * bk.sin(shiftAngle) + positions0[0][1] * bk.cos(shiftAngle),
    ),
    (
        positions0[1][0] * bk.cos(shiftAngle) + positions0[1][1] * bk.sin(shiftAngle),
        -positions0[1][0] * bk.sin(shiftAngle) + positions0[1][1] * bk.cos(shiftAngle),
    ),
    (
        positions0[2][0] * bk.cos(shiftAngle) + positions0[2][1] * bk.sin(shiftAngle),
        -positions0[2][0] * bk.sin(shiftAngle) + positions0[2][1] * bk.cos(shiftAngle),
    ),
    (
        positions0[3][0] * bk.cos(shiftAngle) + positions0[3][1] * bk.sin(shiftAngle),
        -positions0[3][0] * bk.sin(shiftAngle) + positions0[3][1] * bk.cos(shiftAngle),
    ),
    (
        positions0[4][0] * bk.cos(shiftAngle) + positions0[4][1] * bk.sin(shiftAngle),
        -positions0[4][0] * bk.sin(shiftAngle) + positions0[4][1] * bk.cos(shiftAngle),
    ),
    (
        positions0[5][0] * bk.cos(shiftAngle) + positions0[5][1] * bk.sin(shiftAngle),
        -positions0[5][0] * bk.sin(shiftAngle) + positions0[5][1] * bk.cos(shiftAngle),
    ),
]

shiftAngle = -shiftAngle

positions2 = [
    (
        positions0[0][0] * bk.cos(shiftAngle) + positions0[0][1] * bk.sin(shiftAngle),
        -positions0[0][0] * bk.sin(shiftAngle) + positions0[0][1] * bk.cos(shiftAngle),
    ),
    (
        positions0[1][0] * bk.cos(shiftAngle) + positions0[1][1] * bk.sin(shiftAngle),
        -positions0[1][0] * bk.sin(shiftAngle) + positions0[1][1] * bk.cos(shiftAngle),
    ),
    (
        positions0[2][0] * bk.cos(shiftAngle) + positions0[2][1] * bk.sin(shiftAngle),
        -positions0[2][0] * bk.sin(shiftAngle) + positions0[2][1] * bk.cos(shiftAngle),
    ),
    (
        positions0[3][0] * bk.cos(shiftAngle) + positions0[3][1] * bk.sin(shiftAngle),
        -positions0[3][0] * bk.sin(shiftAngle) + positions0[3][1] * bk.cos(shiftAngle),
    ),
    (
        positions0[4][0] * bk.cos(shiftAngle) + positions0[4][1] * bk.sin(shiftAngle),
        -positions0[4][0] * bk.sin(shiftAngle) + positions0[4][1] * bk.cos(shiftAngle),
    ),
    (
        positions0[5][0] * bk.cos(shiftAngle) + positions0[5][1] * bk.sin(shiftAngle),
        -positions0[5][0] * bk.sin(shiftAngle) + positions0[5][1] * bk.cos(shiftAngle),
    ),
]


lattice_vectors = (
    (bk.sqrt(2) * a * bk.cos(pi / 6), bk.sqrt(2) * a * bk.sin(pi / 6)),
    (-bk.sqrt(2) * a * bk.cos(pi / 6), bk.sqrt(2) * a * bk.sin(pi / 6)),
)


res_array = []
masses = m
stiffnesses = k
NcellHor = 100
NcellVer = 100
rotAngle = -pi / 6
figureCluster = plt.figure()
Radd = (
    bk.sqrt(
        (lattice_vectors[0][0] + lattice_vectors[1][0]) ** 2
        + (lattice_vectors[0][1] + lattice_vectors[1][1]) ** 2
    )
    * bk.sqrt(3)
    / 3
)
for i in range(NcellHor):
    for j in range(NcellVer):
        # ((a*bk.cos(pi/6),a*bk.sin(pi/6)), (-a*bk.cos(pi/6),a*bk.sin(pi/6)))
        cellLatticeVector = (
            (j - NcellHor / 2) * bk.sqrt(2) * a * bk.cos(pi / 6)
            - (i - NcellHor / 2) * bk.sqrt(2) * a * bk.cos(pi / 6),
            (i - NcellHor / 2) * bk.sqrt(2) * a * bk.sin(pi / 6)
            + (j - NcellHor / 2) * bk.sqrt(2) * a * bk.sin(pi / 6),
        )

        if j <= (NcellVer / 2 - 1):
            P1 = (
                cellLatticeVector[0] + Radd * bk.cos(0),
                cellLatticeVector[1] + Radd * bk.sin(0),
            )
            P2 = (
                cellLatticeVector[0] + Radd * bk.cos(pi / 3),
                cellLatticeVector[1] + Radd * bk.sin(pi / 3),
            )
            P3 = (
                cellLatticeVector[0] + Radd * bk.cos(2 * pi / 3),
                cellLatticeVector[1] + Radd * bk.sin(2 * pi / 3),
            )
            P4 = (
                cellLatticeVector[0] + Radd * bk.cos(pi),
                cellLatticeVector[1] + Radd * bk.sin(pi),
            )
            P5 = (
                cellLatticeVector[0] + Radd * bk.cos(4 * pi / 3),
                cellLatticeVector[1] + Radd * bk.sin(4 * pi / 3),
            )
            P6 = (
                cellLatticeVector[0] + Radd * bk.cos(5 * pi / 3),
                cellLatticeVector[1] + Radd * bk.sin(5 * pi / 3),
            )

            P1 = (
                P1[0] * bk.cos(rotAngle) + P1[1] * bk.sin(rotAngle),
                -P1[0] * bk.sin(rotAngle) + P1[1] * bk.cos(rotAngle),
            )
            P2 = (
                P2[0] * bk.cos(rotAngle) + P2[1] * bk.sin(rotAngle),
                -P2[0] * bk.sin(rotAngle) + P2[1] * bk.cos(rotAngle),
            )
            P3 = (
                P3[0] * bk.cos(rotAngle) + P3[1] * bk.sin(rotAngle),
                -P3[0] * bk.sin(rotAngle) + P3[1] * bk.cos(rotAngle),
            )
            P4 = (
                P4[0] * bk.cos(rotAngle) + P4[1] * bk.sin(rotAngle),
                -P4[0] * bk.sin(rotAngle) + P4[1] * bk.cos(rotAngle),
            )
            P5 = (
                P5[0] * bk.cos(rotAngle) + P5[1] * bk.sin(rotAngle),
                -P5[0] * bk.sin(rotAngle) + P5[1] * bk.cos(rotAngle),
            )
            P6 = (
                P6[0] * bk.cos(rotAngle) + P6[1] * bk.sin(rotAngle),
                -P6[0] * bk.sin(rotAngle) + P6[1] * bk.cos(rotAngle),
            )

            if plot_array:
                plt.plot([P1[0], P2[0]], [P1[1], P2[1]], "r")
                plt.plot([P2[0], P3[0]], [P2[1], P3[1]], "r")
                plt.plot([P3[0], P4[0]], [P3[1], P4[1]], "r")
                plt.plot([P4[0], P5[0]], [P4[1], P5[1]], "r")
                plt.plot([P5[0], P6[0]], [P5[1], P6[1]], "r")
                plt.plot([P6[0], P1[0]], [P6[1], P1[1]], "r")
            for kk in range(len(positions1)):
                newPos = (
                    (positions1[kk][0] + cellLatticeVector[0]) * bk.cos(rotAngle)
                    + (positions1[kk][1] + cellLatticeVector[1]) * bk.sin(rotAngle),
                    -(positions1[kk][0] + cellLatticeVector[0]) * bk.sin(rotAngle)
                    + (positions1[kk][1] + cellLatticeVector[1]) * bk.cos(rotAngle),
                )
                res_array.append(kl.Resonator(masses, stiffnesses, newPos))

        else:
            P1 = (
                cellLatticeVector[0] + Radd * bk.cos(0),
                cellLatticeVector[1] + Radd * bk.sin(0),
            )
            P2 = (
                cellLatticeVector[0] + Radd * bk.cos(pi / 3),
                cellLatticeVector[1] + Radd * bk.sin(pi / 3),
            )
            P3 = (
                cellLatticeVector[0] + Radd * bk.cos(2 * pi / 3),
                cellLatticeVector[1] + Radd * bk.sin(2 * pi / 3),
            )
            P4 = (
                cellLatticeVector[0] + Radd * bk.cos(pi),
                cellLatticeVector[1] + Radd * bk.sin(pi),
            )
            P5 = (
                cellLatticeVector[0] + Radd * bk.cos(4 * pi / 3),
                cellLatticeVector[1] + Radd * bk.sin(4 * pi / 3),
            )
            P6 = (
                cellLatticeVector[0] + Radd * bk.cos(5 * pi / 3),
                cellLatticeVector[1] + Radd * bk.sin(5 * pi / 3),
            )

            P1 = (
                P1[0] * bk.cos(rotAngle) + P1[1] * bk.sin(rotAngle),
                -P1[0] * bk.sin(rotAngle) + P1[1] * bk.cos(rotAngle),
            )
            P2 = (
                P2[0] * bk.cos(rotAngle) + P2[1] * bk.sin(rotAngle),
                -P2[0] * bk.sin(rotAngle) + P2[1] * bk.cos(rotAngle),
            )
            P3 = (
                P3[0] * bk.cos(rotAngle) + P3[1] * bk.sin(rotAngle),
                -P3[0] * bk.sin(rotAngle) + P3[1] * bk.cos(rotAngle),
            )
            P4 = (
                P4[0] * bk.cos(rotAngle) + P4[1] * bk.sin(rotAngle),
                -P4[0] * bk.sin(rotAngle) + P4[1] * bk.cos(rotAngle),
            )
            P5 = (
                P5[0] * bk.cos(rotAngle) + P5[1] * bk.sin(rotAngle),
                -P5[0] * bk.sin(rotAngle) + P5[1] * bk.cos(rotAngle),
            )
            P6 = (
                P6[0] * bk.cos(rotAngle) + P6[1] * bk.sin(rotAngle),
                -P6[0] * bk.sin(rotAngle) + P6[1] * bk.cos(rotAngle),
            )

            if plot_array:
                plt.plot([P1[0], P2[0]], [P1[1], P2[1]], "b")
                plt.plot([P2[0], P3[0]], [P2[1], P3[1]], "b")
                plt.plot([P3[0], P4[0]], [P3[1], P4[1]], "b")
                plt.plot([P4[0], P5[0]], [P4[1], P5[1]], "b")
                plt.plot([P5[0], P6[0]], [P5[1], P6[1]], "b")
                plt.plot([P6[0], P1[0]], [P6[1], P1[1]], "b")

            for kk in range(len(positions2)):
                newPos = (
                    (positions2[kk][0] + cellLatticeVector[0]) * bk.cos(rotAngle)
                    + (positions2[kk][1] + cellLatticeVector[1]) * bk.sin(rotAngle),
                    -(positions2[kk][0] + cellLatticeVector[0]) * bk.sin(rotAngle)
                    + (positions2[kk][1] + cellLatticeVector[1]) * bk.cos(rotAngle),
                )
                res_array.append(kl.Resonator(masses, stiffnesses, newPos))


Nres = len(res_array)

lattice_vectors_rot = (
    (
        lattice_vectors[0][0] * bk.cos(rotAngle)
        + lattice_vectors[0][1] * bk.sin(rotAngle),
        -lattice_vectors[0][0] * bk.sin(rotAngle)
        + lattice_vectors[0][1] * bk.cos(rotAngle),
    ),
    (
        lattice_vectors[1][0] * bk.cos(rotAngle)
        + lattice_vectors[1][1] * bk.sin(rotAngle),
        -lattice_vectors[1][0] * bk.sin(rotAngle)
        + lattice_vectors[1][1] * bk.cos(rotAngle),
    ),
)

if plot_array:
    for i in range(len(res_array)):
        plt.plot(res_array[i].position[0], res_array[i].position[1], "k.", markersize=8)
    axisFigureCluster = figureCluster.gca()
    axisFigureCluster.set_aspect("equal")


###############################################################################
###############################################################################
# COMPUTE THE RIBBON AS A BAND STRUCTURE


res_array_ribbon = []
j = 0
for i in range(len(res_array)):
    if bk.logical_and(
        res_array[i].position[0] > 0,
        res_array[i].position[0] < period,
    ):
        if bk.logical_and(
            res_array[i].position[1]
            > (-(Nlines * bk.sqrt(2) * a - bk.sqrt(2) * a) / 2),
            res_array[i].position[1]
            < ((Nlines * bk.sqrt(2) * a - bk.sqrt(2) * 3 * a) / 2),
        ):
            res_array_ribbon.append(res_array[i])

            if plot_array:
                plt.plot(
                    res_array_ribbon[j].position[0],
                    res_array_ribbon[j].position[1],
                    "r.",
                    markersize=8,
                )
            j += 1

masses = m
stiffnesses = k
Nres = len(res_array_ribbon)


if plot_array:

    plt.plot(
        [0, 0],
        [
            -(Nlines * bk.sqrt(2) * a - bk.sqrt(2) * a) / 2,
            (Nlines * bk.sqrt(2) * a - 3 * bk.sqrt(2) * a) / 2,
        ],
        "g",
    )
    plt.plot(
        [period, period],
        [
            -(Nlines * bk.sqrt(2) * a - bk.sqrt(2) * a) / 2,
            (Nlines * bk.sqrt(2) * a - 3 * bk.sqrt(2) * a) / 2,
        ],
        "g",
    )
    plt.plot(
        [0, period],
        [
            -(Nlines * bk.sqrt(2) * a - bk.sqrt(2) * a) / 2,
            -(Nlines * bk.sqrt(2) * a - bk.sqrt(2) * a) / 2,
        ],
        "g",
    )
    plt.plot(
        [0, period],
        [
            (Nlines * bk.sqrt(2) * a - 3 * bk.sqrt(2) * a) / 2,
            (Nlines * bk.sqrt(2) * a - 3 * bk.sqrt(2) * a) / 2,
        ],
        "g",
    )

simu = kl.DiffractionSimulation(
    plate, res_array_ribbon, period, nh=nh, force_propa=True
)


# plt.figure()
# for iper in range(10):
#     for r in res_array_ribbon:
#         pos = bk.array(r.position)
#         pos[0] += + iper*period
#         plt.plot(*pos, ".b")
# plt.axis("scaled")


# sys.exit(0)

if plot_det:
    plt.figure(figsize=(7, 7))
    Nre, Nim = 100, 100
    omegas_re = bk.linspace(omega0.real, omega1.real, Nre)
    omegas_im = bk.linspace(omega1.imag, omega0.imag, Nim)
    omegas_re_, omegas_im_ = bk.meshgrid(omegas_re, omegas_im, indexing="ij")
    omegas_complex = omegas_re_ + 1j * omegas_im_

    Mc = simu.build_matrix(omegas_complex, kx)
    # Mc = build_matrixk(simu,ks_complex)
    # Mc = bk.transpose(Mc, axes=(2, 3, 1, 0)).conj()
    Mc = bk.transpose(Mc, axes=(2, 3, 0, 1))
    det = bk.linalg.det(Mc)
    evsM = bk.linalg.eigvals(Mc)
    srt = bk.argsort(bk.abs(evsM), axis=-1)
    min_evs = bk.take_along_axis(evsM, srt, axis=-1)[:, :, 0]
    ax = plot_complex_plane_map(
        omegas_re, omegas_im, bk.log10(bk.abs(min_evs.T)), omegap=omegap
    )
    plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")
    plt.tight_layout()

    plt.pause(0.1)

eigenvalues, eigenvectors_right, eigenvectors_left = simu.eigensolve(
    omega0,
    omega1,
    kx=kx,
    recursive=recursive,
    lambda_tol=lambda_tol,
    func_gives_der=func_gives_der,
    max_iter=max_iter,
    refine=refine,
    peak_ref=peak_ref,
    verbose=True,
    plot_solver=True,
    scale=omegap,
)

plt.plot(eigenvalues.real / omegap, eigenvalues.imag / omegap, "xw")

nper = 6
plot_modes(
    simu,
    x,
    y,
    eigenvalues,
    eigenvectors_right,
    omegap=omegap,
    kx=kx,
    nper=nper,
    a=period,
)
