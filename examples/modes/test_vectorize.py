#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

import sys
import time
from math import pi

import matplotlib.pyplot as plt

import klove as kl

plt.ion()
plt.close("all")


bk = kl.backend


#################################################################
# Parameters

a = 1

Nres = int(sys.argv[1])
alternative = True

plate = kl.ElasticPlate(a * 1, 1, 1, 0.0)
p = 256 * plate.rho * plate.h * plate.D

omegap = plate.omega0(a)
m_plate = plate.rho * a**2 * plate.h


omegar0 = omegap * 1

m0 = m_plate
k0 = omegar0**2 * m0

#### line array
positionsx = bk.linspace(-Nres * a / 2, Nres * a / 2, Nres) * 1
positions = [(_x, 0) for _x in positionsx]

stiffnesses = bk.ones(Nres) * k0
omegars = omegar0 * bk.linspace(1.0, 0.8, Nres)

masses = k0 / omegars**2  # graded
stiffnesses = bk.ones(Nres) * k0

res_array = []
for i in range(Nres):
    res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))
simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)


Nfreq = int(sys.argv[2])
omegas = bk.linspace(1.1, 3, Nfreq) * omegap

omegas = bk.random.rand(Nfreq, Nfreq) * omegap


def build_matrix(self, omega):
    """
    Build the matrix M.

    Parameters
    ----------
    omega : float
        Frequency

    Returns
    -------
    array
        The matrix M
    """
    omega = bk.array(omega)
    matrix = bk.array(
        bk.zeros((self.n_res, self.n_res, *omega.shape), dtype=bk.complex128)
    )
    for alpha, res_alpha in enumerate(self.res_array):
        for beta in range(alpha, self.n_res):
            res_beta = self.res_array[beta]
            delta = 1 if alpha == beta else 0
            dr = self._dr[alpha, beta]
            G0 = self.plate.greens_function(omega, dr)
            if self.alternative:
                t_beta = res_beta.strength(omega) / self.plate.D
                mat = delta / t_beta - G0
            else:
                T_beta = self._strength(omega, res_beta)
                mat = delta - (1 - delta) * T_beta * G0
            matrix[alpha, beta] = matrix[beta, alpha] = mat
    return matrix


t = -time.time()
M = build_matrix(simu, omegas)
t += time.time()
print(t)


def build_matrix_vec(self, omega):
    """
    Build the matrix M.

    Parameters
    ----------
    omega : float
        Frequency

    Returns
    -------
    array
        The matrix M
    """
    omega = bk.array(omega)
    matrix = bk.array(
        bk.zeros((self.n_res, self.n_res, *omega.shape), dtype=bk.complex128)
    )

    t_vec = []
    dr_tri = []
    for alpha, res_alpha in enumerate(self.res_array):
        for beta in range(alpha, self.n_res):
            dr_tri.append(self._dr[alpha, beta])
            t_vec.append(self.res_array[beta].strength(omegas))
    dr_tri = bk.array(dr_tri)
    omegas_vec = bk.broadcast_to(omegas, (*dr_tri.shape, *omegas.shape))
    dr_vec = bk.broadcast_to(dr_tri, (*omegas.shape, *dr_tri.shape)).T
    G0_vec = self.plate.greens_function(omegas_vec, dr_vec)
    idx = 0
    for alpha in range(self.n_res):
        for beta in range(alpha, self.n_res):
            delta = 1 if alpha == beta else 0
            G0 = G0_vec[idx]
            if self.alternative:
                t_beta = t_vec[idx] / self.plate.D
                mat = delta / t_beta - G0
            else:
                T_beta = t_vec[idx]
                mat = delta - (1 - delta) * T_beta * G0
            matrix[alpha, beta] = matrix[beta, alpha] = mat
            idx += 1
    return matrix


self = simu
# dr = self._dr
# omegas1 = bk.broadcast_to(omegas, (*dr.shape, *omegas.shape))
# dr1 = bk.broadcast_to(dr, (*omegas.shape, *dr.shape)).T
# G0 = self.plate.greens_function(omegas1, dr1)

# dr2 =[]
# for alpha, res_alpha in enumerate(self.res_array):
#     for beta in range(alpha, self.n_res):
#         dr2.append(self._dr[alpha, beta])
# dr2 = bk.array(dr2)
# omegas1 = bk.broadcast_to(omegas, (*dr2.shape, *omegas.shape))
# dr1 = bk.broadcast_to(dr2, (*omegas.shape, *dr2.shape)).T


t1 = -time.time()
M1 = build_matrix_vec(simu, omegas)
t1 += time.time()
print(t1)
print(t / t1)

assert bk.allclose(M1, M)
