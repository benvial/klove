#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

"""
Finite cluster of resonators
============================

Optimizing eigenvalues.
"""

try:
    import plothon as pl

    animator = pl.GIFAnimator()
except:
    pass
import sys
import time
from math import pi

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import minimize
from tools import *

import klove as kl

plt.ion()
plt.close("all")

bk = kl.backend


#################################################################
# Parameters

a = 1

# %run optmodes.py 40 3 bic
# %run optmodes.py 30 30 target


# optype = "target"
# optype = "bic"

optype = sys.argv[3]


Nres = int(sys.argv[1])
alternative = True

omega0 = 0.9 - 0.05 * 1j
omega1 = 1.2 - 1e-16 * 1j
if optype == "target":
    omega1 = 1.0 - 1e-16 * 1j

peak_ref = 3
N_guess_loc = 0

Rloc = 0.01
refine = False
lambda_tol = 1e-12
max_iter = 100


nfreq = 1000
start = 0.18  # 0.001
# start = 0.01
stop = 0.31


nptsx = 201
nptsy = nptsx
# nptsy = int(nptsx / Nres)
dbx = 3 * a
dby = 5 * a

#################################################################
# Plots

scale = float(sys.argv[2])
recursive = False
plot_mode_opt = False
plot_solver = False
save_figures = True
Nplot = 9
Nbmax = 6


def savefig(name, folder="./figs", dpi=100):
    if save_figures:
        plt.savefig(f"{folder}/{name}.eps", dpi=dpi)
        plt.savefig(f"{folder}/{name}.png", dpi=dpi)


######################################################################
# Elastic plate

plate = kl.ElasticPlate(a * 1, 1, 1, 0.0)
p = 256 * plate.rho * plate.h * plate.D

omegap = plate.omega0(a)
m_plate = plate.rho * a**2 * plate.h


omega0 *= omegap
omega1 *= omegap


omega_target = omegap * (1 - 0.0j)
omega_targets = np.array([0.95 - 0.02j, 1.1 - 0.01j]) * omegap
neigs = 4
re = np.random.rand(neigs)
re = (omega1.real - omega0.real) * re + omega0.real
im = np.random.rand(neigs)
im = (-omega0.imag) * im + omega0.imag
omega_targets = re + 1j * im

omegac = (omega1 + omega0) / 2

omegac = 0.95 * omegap + 1j * omegac.imag

# omegac = omegap-0.01j
rc = 0.003
teig = np.linspace(0, 2 * pi, neigs + 1)[:-1]  # + pi/4
omega_targets = omegac + rc * np.exp(1j * teig)


#################################################################
# Cluster
omegar0 = omegap * 1

m0 = m_plate * 1
k0 = omegar0**2 * m0

####  array


positions = Nres * a * (0.5 - np.random.rand(Nres, 2))


# _x = np.linspace(-0.5, 0.5, Nres)

# _y = 0.1 * np.sin(2 * pi * _x)
# positions = bk.stack([_x * Nres, _y * Nres]).T


# _x0 = np.linspace(-1, 1, Nres)

# _x = abs(_x0) ** 1.5 * np.sign(_x0)
# _y = 1 * 1 / pi * np.arctan(10 * (_x))
# positions = bk.stack([_x * 1, _y * 1]).T


_t = np.linspace(0, 2 * np.pi, Nres + 1)[:-1]
if optype == "target":
    _r = 1
else:
    nh = 5
    _r = 1
    for n in range(1, nh):
        # n *=2
        _r += abs(np.cos(n * _t))  # * 1*n#**0.5
        # _r += abs(np.sin(n * _t)) * 1*n#**0.5
        # _r += np.sin(n * _t) * np.random.rand(1)#/n#**0.5


_x = _r * np.cos(_t)
_y = _r * np.sin(_t)
# p = 10
# c, s = np.cos(_t), np.sin(_t)
# _x = np.abs(c)**(2/p) * np.sign(c)
# _y = np.abs(s)**(2/p) * np.sign(s)

positions = bk.stack([_x * 1, _y * 1]).T


def distribute_points_on_square(center_x, center_y, side_length, num_points):
    """
    Generates the coordinates of N points equally distributed on a square given the center coordinates,
    the length of its side, and the number of points.

    Args:
    - center_x (float): x-coordinate of the center of the square
    - center_y (float): y-coordinate of the center of the square
    - side_length (float): length of the side of the square
    - num_points (int): number of points to distribute on the square

    Returns:
    - NumPy array: Coordinates of N points equally distributed on the square
    """
    # Generate equally spaced points along the sides of the square
    side_points = np.linspace(-side_length / 2, side_length / 2, num_points // 4)

    # Generate points on each side of the square
    top_points = np.column_stack(
        (side_points, np.full(len(side_points), side_length / 2))
    )
    right_points = np.column_stack(
        (np.full(len(side_points), side_length / 2), -side_points)
    )
    bottom_points = np.column_stack(
        (-side_points, np.full(len(side_points), -side_length / 2))
    )
    left_points = np.column_stack(
        (np.full(len(side_points), -side_length / 2), side_points)
    )

    # Concatenate all points
    all_points = np.vstack((top_points, right_points, bottom_points, left_points))

    # Translate points to the center of the square
    translated_points = all_points + np.array([center_x, center_y])

    return translated_points


# positions = distribute_points_on_square(0,0,1,Nres)


positions *= scale

# n = Nres


# _x0 = np.linspace(-1, 1, n) * scale
# _y0 = scale * np.ones(n)
# positions1 = bk.stack([_x0, _y0]).T
# _x0 = np.linspace(-1, 1, n) * scale
# _y0 = -scale * np.ones(n)
# positions2 = bk.stack([_x0, _y0]).T

# _x0 = np.linspace(-1, 1, n + 1) * scale
# _x0 = np.flipud(_x0)[1:-1]
# _y0 = 1 * _x0
# positions3 = bk.stack([_x0, _y0]).T

# positions = bk.vstack([positions1, positions3, positions2])

# # positions1 = positions.copy()

# # positions1[:, 1] -= 1
# # positions1[:, 0] -= 3
# # # positions1[:, 1] -= 2
# # positions = bk.vstack([positions, positions1])


plt.plot(*positions.T, "or")
plt.axis("scaled")
# sys.exit(0)

# Nres = len(positions)

# opt_pos = True
opt_pos = True if optype == "target" else False

nvar = 4 * Nres if opt_pos else 2 * Nres


#################################################################
# Grid for fields

mini = min(bk.hstack([positions[:, 0], positions[:, 1]]))
maxi = max(bk.hstack([positions[:, 0], positions[:, 1]]))
lim = max(abs(maxi), abs(mini))
x1 = bk.linspace(-dbx - lim, dbx + lim, nptsx)
y1 = x1
x, y = bk.meshgrid(x1, y1, indexing="xy")

mini = np.min(positions, axis=0)
maxi = np.max(positions, axis=0)
x1 = bk.linspace(-dbx + mini[0], dbx + maxi[0], nptsx)
y1 = bk.linspace(-dbx + mini[1], dbx + maxi[1], nptsx)
x, y = bk.meshgrid(x1, y1, indexing="xy")


guess = None
# guess = [omega_target*(1-1e-12j)]
# guess = np.linspace(omega0.real, omega1.real, 10) * (1 - 1e-11j)

if plot_mode_opt:
    fig, axmode = plt.subplots()


fig, axcplx = plt.subplots()
# plt.tight_layout()


def plot_mode(ax, simu, eigenvalues, eigenvectors, target_index):
    ax.clear()
    omega_n = eigenvalues[target_index]
    modeplot = eigenvectors[:, target_index]
    Weig = simu.get_scattered_field(x, y, modeplot, omega_n)

    n = range(len(eigenvalues))[target_index]
    ax.pcolormesh(x, y, bk.abs(Weig), cmap="inferno", rasterized=True)
    for i in range(simu.n_res):
        xr, yr = simu.res_array[i].position
        ax.plot(xr, yr, ".w", mew=0, ms=5)
    sign = "-" if omega_n.imag < 0 else "+"
    ax.set_title(
        rf"$\omega_{{{n+1}}}/\omega_p = ${omega_n.real/omegap:.7f}{sign}{omega_n.imag/omegap:.7f}i"
    )
    # ax.axis("off")
    ax.set_aspect(1)
    ax.set_xlabel("$x/a$")
    ax.set_ylabel("$y/a$")
    plt.tight_layout()


positions0 = positions


def solve_modes(params):
    global guess

    masses = params[:Nres] * m0
    stiffnesses = params[Nres:] * k0

    if opt_pos:
        stiffnesses = params[Nres : 2 * Nres] * k0
        positions = (bk.array(params[2 * Nres :]) - 0.5) * scale
        positions = positions.reshape(Nres, 2)

        # plt.clf()

        # plt.plot(*positions.T, "or")
        # plt.axis("scaled")
    else:
        positions = positions0

        # xsxs

    res_array = []
    for i in range(Nres):
        res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))

    ######################################################################
    # Define the simulation object
    simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)

    #################################################################
    # Eigenvalue problem
    eigenvalues, eigenvectors = simu.eigensolve(
        omega0,
        omega1,
        recursive=recursive,
        lambda_tol=lambda_tol,
        max_iter=max_iter,
        N_guess_loc=N_guess_loc,
        Rloc=Rloc,
        peak_ref=peak_ref,
        verbose=False,
        # strategy="grid",
        plot_solver=plot_solver,
        scale=omegap,
        guesses=guess,
        refine=True,
        tol=1e-14,
    )
    ######
    Nmodes = len(eigenvalues)
    if Nmodes == 0:
        print("no modes found")
        sys.exit(0)

    M = [simu.build_matrix(omega) for omega in eigenvalues]
    dM = [simu.build_matrix_derivative(omega) for omega in eigenvalues]

    # normalize modes
    eigenvectors = simu.normalize_eigenvectors(eigenvalues, eigenvectors, dM)
    multiplicities = simu.get_multiplicities(eigenvalues, M)
    eigenvalues, eigenvectors = simu.update_eig(
        eigenvalues, eigenvectors, multiplicities, M, dM
    )
    return simu, eigenvalues, eigenvectors


def get_mode_index(eigenvalues, omega_target):
    # return bk.argsort(abs(eigenvalues - omega_target))[0]

    return bk.argsort(eigenvalues.imag)[-1]


alpha = 0

objectives = []


def optfun_bic(params, target_index=-1):
    global guess
    global ll
    global objectives
    try:
        ll[0].remove()
    except:
        pass
    simu, eigenvalues, eigenvectors = solve_modes(params)

    target_index = get_mode_index(eigenvalues, omega_target)

    if plot_mode_opt:
        plot_mode(axmode, simu, eigenvalues, eigenvectors, target_index)
        plt.pause(0.01)

    phi_n = eigenvectors[:, target_index]
    omega_n = eigenvalues[target_index]

    # ll = plt.plot(omega_n.real / omegap, omega_n.imag / omegap, "sr")
    # plt.pause(0.01)

    jac = []
    for ires in range(Nres):
        devdp = simu.sensitivity_mass(omega_n, phi_n, ires) * m0
        jac.append(devdp)
    for ires in range(Nres):
        devdp = simu.sensitivity_stiffness(omega_n, phi_n, ires) * k0
        jac.append(devdp)

    if opt_pos:
        for ires in range(Nres):
            for ipos in (0, 1):
                devdp = simu.sensitivity_position(omega_n, phi_n, ires, ipos) * scale
                jac.append(devdp)

    jac = bk.stack(jac)

    #### jacobian

    f1 = np.abs(omega_n - omega_target) ** 2
    df1 = 2 * np.real((omega_n - omega_target) * jac.conj())

    # df1 = -df1 / f1
    # f1 = -np.log(f1)

    f1 *= alpha
    df1 *= alpha

    # f += -np.log(-omega_n.imag) * (1 - alpha)
    # df += -jac.imag / omega_n.imag * (1 - alpha)

    f2 = -omega_n.imag
    df2 = -jac.imag

    # df2 = df2 / f2**2
    # f2 = -1/f2

    # df2 = df2 / f2
    # f2 = np.log(f2)

    f2 *= 1 - alpha
    df2 *= 1 - alpha

    f = f1 + f2
    df = df1 + df2

    print(f"val = {f}")
    print(f"ω/ωp = {omega_n/omegap}")
    print("------------------")

    objectives.append(f)

    # print(df)

    guess = bk.array([omega_n])
    print(guess)
    # guess = eigenvalues
    # guess = None

    # guess = [omega_target]
    # guess = [(0.95-0.001j)*omegap]
    # guess = [omega_n + jac@params ]

    return f, df


eig_current_hist = []
iopt = 0


def optfun_target(params, target_index=-1):
    global guess
    global ll
    global objectives
    global iopt
    try:
        ll[0].remove()
    except:
        pass
    simu, eigenvalues, eigenvectors = solve_modes(params)

    eigenvalues_all = eigenvalues.copy()

    max_eig = len(omega_targets)

    # srt = bk.argsort(abs(eigenvalues - omega_targets))[:max_eig]

    eigenvalues_tmp = eigenvalues.copy()
    vect = eigenvectors.T.tolist()

    evs = []
    modes = []

    srt = []
    for i in range(max_eig):
        eigenvalues_tmp = np.array(eigenvalues_tmp)
        _ = bk.argsort(abs(eigenvalues_tmp - omega_targets[i]))[0]

        # plt.plot(eigenvalues_tmp[_].real,eigenvalues_tmp[_].imag,"+g")

        evs.append(eigenvalues_tmp[_])
        modes.append(vect[_])
        eigenvalues_tmp = eigenvalues_tmp.tolist()
        eigenvalues_tmp.pop(_)
        vect.pop(_)
        srt.append(_)
    srt = np.array(srt)
    # eigenvalues, eigenvectors = eigenvalues[srt], eigenvectors[:,srt]
    eigenvalues, eigenvectors = np.array(evs), np.array(modes).T

    f = 0
    df = 0

    for target_index in range(max_eig):
        omega_target = omega_targets[target_index]

        phi_n = eigenvectors[:, target_index]
        omega_n = eigenvalues[target_index]

        jac = []
        for ires in range(Nres):
            devdp = simu.sensitivity_mass(omega_n, phi_n, ires) * m0
            jac.append(devdp)
        for ires in range(Nres):
            devdp = simu.sensitivity_stiffness(omega_n, phi_n, ires) * k0
            jac.append(devdp)
        if opt_pos:
            for ires in range(Nres):
                for ipos in (0, 1):
                    devdp = (
                        simu.sensitivity_position(omega_n, phi_n, ires, ipos) * scale
                    )
                    jac.append(devdp)

        jac = bk.stack(jac)

        #### jacobian

        f += np.abs(omega_n - omega_target) ** 2
        df += 2 * np.real((omega_n - omega_target) * jac.conj())

        if plot_mode_opt:
            plot_mode(axmode, simu, eigenvalues, eigenvectors, target_index)
            plt.pause(0.01)

    print(f"val = {f}")
    print(f"ω/ωp = {eigenvalues[:max_eig]/omegap}")
    print(f"ω/ωp = {omega_targets[:max_eig]/omegap}")
    print("------------------")

    # guess = None
    # _, eigenvalues_all, _ = solve_modes(params)

    axcplx.clear()
    plt.sca(axcplx)
    plt.plot(
        omega_targets[:max_eig].real / omegap,
        omega_targets[:max_eig].imag / omegap,
        "x",
        label="target",
    )
    plt.plot(eigenvalues_all.real / omegap, eigenvalues_all.imag / omegap, ".k")
    eig_current = eigenvalues[:max_eig]
    eig_current_hist.append(eig_current)
    plt.plot(
        eig_current.real / omegap, eig_current.imag / omegap, ".r", label="current"
    )
    plt.axis("scaled")
    plt.xlim(omega0.real / omegap, omega1.real / omegap)
    plt.ylim(omega0.imag / omegap, omega1.imag / omegap)
    plt.xlabel(r"Re $\omega/\omega_p$")
    plt.ylabel(r"Im $\omega/\omega_p$")
    plt.title(f"objective = {f:.3e}")
    plt.legend(loc=2)
    plt.pause(0.01)

    try:
        animator.snapshot(iopt)
    except:
        pass
    iopt += 1

    objectives.append(f)

    # print(df)

    guess = None
    guess = eigenvalues
    # guess = omega_targets
    guess = bk.hstack([eigenvalues, omega_targets])

    return f, df


optfun = optfun_target if optype == "target" else optfun_bic

# params0 = np.random.rand(nvar)
# params0 = np.ones(nvar) * 1

# simu, eigenvalues, eigenvectors = solve_modes(params0)
# plot_modes(
#     simu,
#     x,
#     y,
#     eigenvalues,
#     eigenvectors,
#     Ra=0.25,
#     arrows=False,
#     omegap=omegap,
#     a=1,
#     figx=6,
#     QNMs=None,
# )


# target_index = -1
# f, df = optfun(params0, target_index)

# deltap = 1e-6
# fd = []
# for i in range(nvar):
#     dparams0 = params0.copy()
#     dparams0[i]+=deltap
#     deltaf, _ = optfun(dparams0, target_index)
#     fd.append((deltaf-f)/deltap)
# fd=bk.array(fd)
# print("Finite differences")
# print(fd)
# print("Analytical")
# print(df)

# xsx

##################################################
#################### optimize ####################
##################################################

# nvar = 2 * Nres

# params0 = np.ones(nvar) * 1
# params0[2*Nres:] = np.random.rand(2*Nres)
if optype == "target":
    params0 = np.random.rand(nvar)
    params0 = np.ones(nvar) * 1
    # params0[2 * Nres :] = 1
    params0[2 * Nres :] = 0.5 * (positions / scale + 1).flatten()

    min_par = 1.0
    max_par = 1.0
else:
    params0 = np.ones(nvar) * 1
    # params0 = np.random.rand(nvar)
    min_par = 0.1
    max_par = 10

bounds = [(min_par, max_par) for i in range(2 * Nres)]
if opt_pos:
    bounds += [(0, scale / 10) for i in range(2 * Nres)]

run_opt = True
if run_opt:

    result = minimize(
        optfun,
        params0,
        method="L-BFGS-B",
        jac=True,
        bounds=bounds,
        tol=1e-6,
        options={
            "gtol": 1e-6,
            "maxls": 40,
            "ftol": 1e-12,
            "iprint": 0,
            # "maxiter": 23,
        },
    )

    np.savez(f"optmodes_{optype}.npz", result=result, optype=optype, opt_pos=opt_pos)
else:
    result = np.load(f"optmodes_{optype}.npz", allow_pickle=True)["result"].tolist()


params = result.x


if optype == "target":
    try:
        animator.build()
    except:
        pass

# sys.exit(0)


##################################################
#################### postpro ####################
##################################################

guess = [(1.039 - 0.00001j) * omegap]
simu, eigenvalues, eigenvectors = solve_modes(params)


# calculation of sensitivity of Q factor

target_index = get_mode_index(eigenvalues, omega_target)

phi_n = eigenvectors[:, target_index]
omega_n = eigenvalues[target_index]


def get_dQdp(omega, domega):
    re, im = omega.real, omega.imag
    dre, dim = domega.real, domega.imag
    return -0.5 * (dre * im - re * dim) / im**2


dQdp = {}
jac = []
for ires in range(Nres):
    devdp = simu.sensitivity_mass(omega_n, phi_n, ires) * m0
    _dQdp = get_dQdp(omega_n, devdp)
    jac.append(_dQdp)
dQdp["m"] = np.array(jac)
jac = []
for ires in range(Nres):
    devdp = simu.sensitivity_stiffness(omega_n, phi_n, ires) * k0
    _dQdp = get_dQdp(omega_n, devdp)
    jac.append(_dQdp)
dQdp["k"] = np.array(jac)

for ipos in (0, 1):
    jac = []
    for ires in range(Nres):
        devdp = simu.sensitivity_position(omega_n, phi_n, ires, ipos) * scale
        _dQdp = get_dQdp(omega_n, devdp)
        jac.append(_dQdp)
    key = "x" if ipos == 0 else "y"
    dQdp[key] = np.array(jac)


Q = -0.5 * omega_n.real / omega_n.imag


fig, axpar = plt.subplots(2, 2, figsize=(6, 7))
axpar = axpar.ravel()
cmap = mpl.colormaps["Spectral_r"]

masses = np.array([_.mass for _ in simu.res_array])

# ax.pcolormesh(x, y, bk.abs(u), cmap="inferno", rasterized=True)
for ip, ax in enumerate(axpar):
    for i in range(simu.n_res):
        res = simu.res_array[i]
        xr, yr = res.position
        if ip == 0:
            data = dQdp["m"]
            title = "$m$"
        elif ip == 1:
            data = dQdp["k"]
            title = "$k$"
        elif ip == 2:
            data = dQdp["x"]
            title = "$x$"
        else:
            data = dQdp["y"]
            title = "$y$"

        data = data / Q * 100
        # data = np.sign(data) * np.log10(np.abs(data))

        coeff = (max(data) - data[i]) / (max(data) - min(data))
        ax.plot(xr, yr, "o", mew=0, ms=5, c=cmap(coeff))

    norm = mpl.colors.Normalize(vmin=0, vmax=1)
    cb = fig.colorbar(
        mpl.cm.ScalarMappable(norm=norm, cmap=cmap),
        ax=ax,
        orientation="horizontal",
        label=title,
        location="top",
    )
    cb.set_ticks([0, 1], labels=[f"{min(data):.1f}", f"{max(data):.1f}"])
    dx = a
    ax.set_xlim(min(mini) - dx, max(maxi) + dx)
    ax.set_ylim(min(mini) - dx, max(maxi) + dx)
    ax.set_xlabel("$x/a$")
    ax.set_ylabel("$y/a$")
    ax.set_aspect(1)

axpar[1].set_ylabel(None)
axpar[2].set_ylabel(None)
axpar[1].set_yticklabels([])
axpar[2].set_yticklabels([])
plt.suptitle(r"$\frac{1}{Q}\frac{\partial Q}{\partial p}$ (\%)")
plt.tight_layout()
savefig(f"{optype}_dQdp")

#####


###########
for target_index in range(len(eigenvalues)):
    fig, axmode = plt.subplots()
    plot_mode(axmode, simu, eigenvalues, eigenvectors, target_index)
    plt.pause(0.01)
    savefig(f"{optype}_point_mode{target_index}")


omega = np.real(eigenvalues[target_index])


pos_src = simu.res_array[0].position


pos_src = 0, 0
incident = simu.point_source

sol = simu.solve(incident, omega, pos_src)

u = simu.get_field(x, y, incident, sol, omega, pos_src)


# u = incident(x,y,omega,pos_src)


# u = simu.get_scattered_field(x, y, sol, omega)


# fig, ax = plt.subplots()

# nphase = 11
# phase = bk.linspace(0, 2 * pi, nphase + 1)[:-1]
# tmp = []
# for i in range(nphase):

#     tmp.append(bk.real(u * np.exp(-1j * phase[i])))

# vmin = np.min(tmp)
# vmax = np.max(tmp)

# for i in range(nphase):

#     _ = ax.pcolormesh(
#         x,
#         y,
#         tmp[i],
#         cmap="RdBu_r",
#         rasterized=True,
#         vmin=vmin,
#         vmax=vmax,
#     )
#     # ax.pcolormesh(x, y, bk.abs(u), cmap="inferno", rasterized=True)
#     for i in range(simu.n_res):
#         xr, yr = simu.res_array[i].position
#         ax.plot(xr, yr, ".k", mew=0, ms=5)

#     ax.set_title(rf"$\omega/\omega_p = ${omega/omegap:.3f}")

#     ax.plot(*pos_src, "sr", ms=3)
#     # ax.axis("off")
#     ax.set_aspect(1)
#     ax.set_xlabel("$x/a$")
#     ax.set_ylabel("$y/a$")
#     # plt.colorbar(_)
#     plt.tight_layout()
#     plt.pause(0.01)

fig, ax = plt.subplots()

_ = ax.pcolormesh(
    x,
    y,
    bk.abs(u),
    cmap="inferno",
    rasterized=True,
)
# ax.pcolormesh(x, y, bk.abs(u), cmap="inferno", rasterized=True)
for i in range(simu.n_res):
    xr, yr = simu.res_array[i].position
    ax.plot(xr, yr, ".w", mew=0, ms=5)

ax.set_title(rf"$\omega/\omega_p = ${omega/omegap:.3f}")

ax.plot(*pos_src, "sg", ms=3)
# ax.axis("off")
ax.set_aspect(1)
ax.set_xlabel("$x/a$")
ax.set_ylabel("$y/a$")
# plt.colorbar(_)
plt.tight_layout()
plt.pause(0.01)
savefig(f"{optype}_point_source")


fig, axpar = plt.subplots(1, 3, figsize=(9, 3))
cmap = mpl.colormaps["Spectral_r"]

# Take colors at regular intervals spanning the colormap.

masses = [_.mass for _ in simu.res_array]
stiffs = [_.stiffness for _ in simu.res_array]
omega_res = [_.omega_r for _ in simu.res_array]

masses = bk.array(masses)
stiffs = bk.array(stiffs)
omega_res = bk.array(omega_res)

# ax.pcolormesh(x, y, bk.abs(u), cmap="inferno", rasterized=True)
for ip, ax in enumerate(axpar):
    for i in range(simu.n_res):
        res = simu.res_array[i]
        xr, yr = res.position
        if ip == 0:
            data = masses / m0
            title = "$m/m_0$"
        elif ip == 1:
            data = stiffs / k0
            title = "$k/k_0$"
        else:
            data = omega_res / omegar0
            title = r"$\omega_r/\omega_{r0}$"

        coeff = (max(data) - data[i]) / (max(data) - min(data))
        ax.plot(xr, yr, "o", mew=0, ms=5, c=cmap(coeff))

    norm = mpl.colors.Normalize(vmin=0, vmax=1)
    cb = fig.colorbar(
        mpl.cm.ScalarMappable(norm=norm, cmap=cmap),
        ax=ax,
        orientation="horizontal",
        label=title,
        location="top",
    )
    cb.set_ticks([0, 1], labels=[f"{min(data):.3f}", f"{max(data):.3f}"])

    # ax.set_xlim(mini[0],maxi[0])
    # ax.set_ylim(mini[1],maxi[1])
    dx = a

    ax.set_xlim(min(mini) - dx, max(maxi) + dx)
    ax.set_ylim(min(mini) - dx, max(maxi) + dx)
    ax.set_xlabel("$x/a$")
    ax.set_ylabel("$y/a$")
    ax.set_aspect(1)

axpar[1].set_ylabel(None)
axpar[2].set_ylabel(None)
axpar[1].set_yticklabels([])
axpar[2].set_yticklabels([])
plt.tight_layout()
savefig(f"{optype}_params1")

#####

objectives_array = bk.array(objectives)
plt.figure(figsize=(4, 4))
plt.plot(objectives_array / omegap)
plt.ylabel(r"$-\rm{Im}\,\omega_n/\omega_p$")
plt.ylabel(r"objective")
plt.xlabel("iteration number")
plt.yscale("log")
plt.tight_layout()
savefig(f"{optype}_history")


fig, axpar = plt.subplots(1, 3, figsize=(9, 3))

for ip, ax in enumerate(axpar):
    if ip == 0:
        data = masses / m0
        title = "$m/m_0$"
    elif ip == 1:
        data = stiffs / k0
        title = "$k/k_0$"
    else:
        data = omega_res / omegar0
        title = r"$\omega_r/\omega_{r0}$"

    ax.bar(range(1, Nres + 1), data - 1, bottom=1)
    ax.set_ylabel(title)
    # ax.set_ylim(min_par, max_par)
    ax.set_xlabel("resonator number")

plt.tight_layout()
savefig(f"{optype}_params2")

if optype == "target":
    plt.figure()

    eig_current_hist = bk.array(eig_current_hist)

    plt.plot(
        eig_current_hist[0].real / omegap,
        eig_current_hist[0].imag / omegap,
        "+k",
        label="initial",
    )
    plt.plot(
        omega_targets.real / omegap,
        omega_targets.imag / omegap,
        "s",
        color="none",
        mec="b",
        label="target",
    )

    plt.axis("equal")
    plt.xlim(omega0.real / omegap, omega1.real / omegap)
    plt.ylim(omega0.imag / omegap, omega1.imag / omegap)
    plt.xlabel(r"Re $\omega/\omega_p$")
    plt.ylabel(r"Im $\omega/\omega_p$")
    plt.title(f"objective = {objectives[-1]:.3e}")
    plt.pause(0.01)
    plt.plot(
        eig_current_hist.real / omegap,
        eig_current_hist.imag / omegap,
        "-s",
        ms=3,
        alpha=0.4,
    )
    plt.plot(
        eig_current_hist[-1].real / omegap,
        eig_current_hist[-1].imag / omegap,
        "or",
        ms=3,
        label="optimized",
    )
    plt.plot(
        eig_current_hist[0].real / omegap,
        eig_current_hist[0].imag / omegap,
        "+k",
    )
    plt.legend(loc=2)

    savefig(f"{optype}_trajectories")
