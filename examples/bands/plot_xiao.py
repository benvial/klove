#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Periodic array of resonators
================================

Band diagramm for an array on mass-spring resonators. 
Checking results from :cite:p:`xiao2012`
"""

from math import pi

import matplotlib.pyplot as plt

import klove as kl

bk = kl.backend


######################################################################
# Parameters.
# The lattice constant is `a=0.1 m`
a = 0.1

######################################################################
# We use `Npw=4` plane waves and `Nk=30` k points
Npw = 4
Nk = 30

######################################################################
# The symmetry points of the Brillouin zone are
# `Gamma=(0,0)`, `X=(pi/a,0)` and `M=(pi/a,pi/a)`
sym_points = (0, 0), (pi / a, 0), (pi / a, pi / a)

######################################################################
# We initialize the k points
ks = kl.init_bands(sym_points, Nk)
ksplot, kl.kdist = kl.init_bands_plot(sym_points, Nk)


def compute_bands(bs):
    """
    Compute the band diagramm for a given simulation object

    Parameters
    ----------
    bs : BandsSimulation
        The simulation object

    Returns
    -------
    bands : array
        The band diagramm
    """
    bands = []
    for K in ks:
        # Compute the eigenvalues and eigenvectors for the current k point
        omegans = bs.eigensolve(K, Npw, return_modes=False)
        # Append the eigenvalues to the band diagramm
        bands.append(omegans / (2 * pi))
    return bk.real(bands)


######################################################################
# We define the lattice vectors as `lattice_vectors=(a,0),(0,a)`
lattice_vectors = (a, 0), (0, a)

######################################################################
# We define the plate as a `ElasticPlate`
plate = kl.ElasticPlate(2e-3, 2700, 70e9, 0.3)

######################################################################
# We define resonator parameters
m = 2.7e-2
position = (0, 0)

######################################################################
# We create a simulation object for the bare plate
bs0 = kl.BandsSimulation(plate, [], lattice_vectors)


######################################################################
# Compute the bands
bands0 = compute_bands(bs0)


def run(k):
    """
    Run a simulation for a given resonator stiffness `k`
    """
    # We create a simulation object for the plate with a resonator
    res_array = [kl.Resonator(m, k, position)]
    bs = kl.BandsSimulation(plate, res_array, lattice_vectors)

    # We compute the band diagramm
    bands = compute_bands(bs)

    # We plot the band diagramm
    plt.figure()
    kl.plot_bands(sym_points, Nk, bands, color="k")
    line = plt.gca().get_lines()[0]
    plt.ylim(0, 800)
    fres = res_array[0].omega_r / (2 * pi)
    kl.plot_bands(sym_points, Nk, bands0, color="#3e68b0", ls="--")
    line0 = plt.gca().get_lines()[bands.shape[0]]
    plt.legend([line, line0], ["LR plate", "bare plate"])

    # We add a horizontal line at the resonator frequency
    plt.hlines(fres, 0, ksplot[-1], ls=":", colors="#8d8d8d")
    plt.ylabel("Frequency (Hz)")
    plt.title(rf"$f_R = {fres:.0f}$ Hz")


######################################################################
# Fig 2 (a)
run(k=9.593e4)


######################################################################
# Fig 2 (b)
run(k=1.925e5)
