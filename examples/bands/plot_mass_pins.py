#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Periodic array of pinned points and masses
==============================================

Band diagramms.
"""

from math import pi

import matplotlib.pyplot as plt

import klove as kl

bk = kl.backend


######################################################################
# Parameters.
#
# The lattice constant is `a=0.1 m`
a = 0.1

######################################################################
# We use `Npw=4` plane waves and `Nk=30` k points
Npw = 4
Nk = 30
sym_points = (0, 0), (pi / a, 0), (pi / a, pi / a)
ks = kl.init_bands(sym_points, Nk)
ksplot, kl.kdist = kl.init_bands_plot(sym_points, Nk)


def compute_bands(bs):
    """
    Compute the band diagramm of a simulation object `bs`.

    Parameters
    ----------
    bs : :class:`BandsSimulation`
        The simulation object

    Returns
    -------
    bands : array
        The band diagramm
    """
    bands = []
    for K in ks:
        _bds = []
        omegans = bs.eigensolve(K, Npw, return_modes=False)
        bands.append(omegans / (2 * pi))
    return bk.real(bands)


######################################################################
# The lattice vectors are `lattice_vectors=(a,0),(0,a)`
# The plate is an `ElasticPlate` with a thickness of `2e-3 m`,
# a density of `2700 kg/m^3`, a Young modulus of `70e9 Pa` and a
# Poisson ratio of `0.3`
lattice_vectors = (a, 0), (0, a)
plate = kl.ElasticPlate(2e-3, 2700, 70e9, 0.3)

######################################################################
# The resonator parameters are:
# - mass `m=2.7e-2 kg`
# - stiffness `k=1.925e5 N/m`
# - position `(0,0)`
m = 2.7e-2
k = 1.925e5
position = (0, 0)

######################################################################
# TDefine the simulation for the bare plate
bs0 = kl.BandsSimulation(plate, [], lattice_vectors)

######################################################################
# Compute the band diagramm of the bare plate
bands0 = compute_bands(bs0)


def run(case="pins"):
    """
    Run a band simulation for a given case.

    Parameters
    ----------
    case : str
        The case to run, either "pins", "masses" or "resonators"
    """
    if case == "pins":
        # Pinned points
        res_array = [kl.Pin(position)]
    elif case == "masses":
        # Masses
        res_array = [kl.Mass(m, position)]
    else:
        # Resonators
        res_array = [kl.Resonator(m, k, position)]
    bs = kl.BandsSimulation(plate, res_array, lattice_vectors)
    bands = compute_bands(bs)

    plt.figure()
    kl.plot_bands(sym_points, Nk, bands, color="k")
    line = plt.gca().get_lines()[0]
    plt.ylim(0, 1500)
    kl.plot_bands(sym_points, Nk, bands0, color="#3e68b0", ls="--")
    line0 = plt.gca().get_lines()[bands.shape[0]]
    plt.legend([line, line0], ["LR plate", "bare plate"])

    plt.ylabel("Frequency (Hz)")
    plt.title(case)


######################################################################
# Pins

run("pins")


######################################################################
# Masses

run("masses")


######################################################################
# Resonators

run("resonators")
