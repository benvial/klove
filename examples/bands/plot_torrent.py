#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

"""
Hexagonal array of resonators
================================

Band diagramm for an array on mass-spring resonators. 
Checking results from :cite:p:`torrent2013`.
"""


from math import pi

import matplotlib.pyplot as plt

import klove as kl

bk = kl.backend


######################################################################
# Parameters.
# The lattice constant is `a=1 m`
a = 1

######################################################################
# The coordinates of the resonators are
# `q * cos(pi/6)` and `-q * cos(pi/6)`
q = a / (2 * 3**0.5)
positions = [
    (q * bk.cos(pi / 6), q * bk.sin(pi / 6)),
    (-q * bk.cos(pi / 6), -q * bk.sin(pi / 6)),
]

######################################################################
# The lattice vectors are `(a,0)` and `(a*cos(pi/3),a*sin(pi/3))`
lattice_vectors = (a, 0), (a * bk.cos(pi / 3), a * bk.sin(pi / 3))

######################################################################
# The plate is a thin plate with a thickness of `0.1 m`
# a density of `1 kg/m^2`, a Young modulus of `1 Pa` and a Poisson ratio of `0.0`
plate = kl.ElasticPlate(1, 0.1, 1, 0.0)

######################################################################
# The resonators have a mass of `1 kg` and a stiffness of `k`
k = (4 * pi) ** 2 * plate.omega0(a)
m = 1
masses = [m, m]
stiff = [k, k]
res_array = []
for m, k, pos in zip(masses, stiff, positions):
    res_array.append(kl.Resonator(m, k, pos))

######################################################################
# The bands simulation is initialized with the plate and the array of resonators
bs = kl.BandsSimulation(plate, res_array, lattice_vectors)

######################################################################
# The number of plane waves is `Npw=4`
Npw = 4

######################################################################
# The number of k points is `Nk=21`
Nk = 21

######################################################################
# The k points are computed using the `init_bands` function
# The k points are computed in the first Brillouin zone
b0 = bk.pi / a
Gamma_point = 0, 0
K_point = 2 * b0 / 3, 2 * b0 / 3**0.5
M_point = 0, 2 * b0 / 3**0.5
sym_points = [Gamma_point, M_point, K_point]
ks = kl.init_bands(sym_points, Nk)
ksplot, kdist = kl.init_bands_plot(sym_points, Nk)


def compute_bands(bs):
    """
    Compute the band structure of the array of resonators.

    Parameters
    ----------
    bs : :class:`BandsSimulation`
        The bands simulation

    Returns
    -------
    bands : array
        The band structure
    """
    bands = []
    for K in ks:
        # Compute the eigenvalues and eigenvectors at the k point K
        omegans = bs.eigensolve(K, Npw, return_modes=False, hermitian=False)
        # Append the eigenvalues to the bands array
        bands.append(omegans)

    return bk.real(bands)


######################################################################
# Compute the band structure of the array of resonators
bands = compute_bands(bs)

######################################################################
# Compute the band structure of the plate
bsplate = kl.BandsSimulation(plate, [], lattice_vectors)
bands_plate = compute_bands(bsplate)

######################################################################
# Plot the band structure
plt.figure()
kl.plot_bands(
    sym_points, Nk, bands, xtickslabels=[r"$\Gamma$", r"$M$", r"$K$", r"$\Gamma$"]
)
plt.plot(ksplot, bands_plate, "k--")
plt.ylabel(r"$\Omega a$")
plt.ylim(0, 5)
plt.show()
