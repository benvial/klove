"""
Far field radiation and scattering cross section
==============================================

This example shows how to compute the far field radiation pattern and the
scattering cross section of a cluster of resonators, as well as a mode of this 
open resonator.
This checks results from :cite:p:`marti2023bound`.
"""

import matplotlib.pyplot as plt
import numpy as np

import klove as kl

######################################################################
# We define an elastic plate with a thickness of 1, a density of 1, a Young's
# modulus of 1 and a Poisson's ratio of 0.0.
plate = kl.ElasticPlate(h=1, rho=1, E=1, nu=0.0)


######################################################################
# We define a circle of 50 resonators with a radius of 1. The strength of the
# resonators is set to -69.81 times the bending stiffness of the plate.
R = 1
Nres = 50
t0 = -69.81 * plate.bending_stiffness


######################################################################
# We create an array of resonators with the positions of the resonators
# computed using the equation of a circle.
resarray = []
for alpha in range(Nres):
    xalpha = R * np.cos(2 * np.pi * alpha / Nres)
    yalpha = R * np.sin(2 * np.pi * alpha / Nres)
    resarray.append(kl.ConstantStrength(t0, (xalpha, yalpha)))

######################################################################
# We create a ScatteringSimulation object with the elastic plate and the array
# of resonators.
sim = kl.ScatteringSimulation(plate, resarray)

######################################################################
# We compute the far field radiation pattern for 1500 frequencies from 0.1 to
# 8 in the far field. We use 360 angles from 0 to 2 pi.
Nomegas = 2500
ks = np.linspace(0.1, 8, Nomegas)
omegas = sim.plate.frequency(ks)
Ntheta = 360
angles = np.linspace(0, 2 * np.pi, Ntheta)

######################################################################
# We solve the scattering problem for an incident plane wave at normal
# incidence.
incident_angle = 0
incident = sim.plane_wave
phi_ns = sim.solve(incident, omegas, angle=incident_angle)

######################################################################
# We compute the far field radiation pattern of the scattered field.
far_field = sim.get_far_field_radiation(phi_ns, omegas, angles)

######################################################################
# We compute the scattering cross section of the scattered field.
sigma_sc = sim.get_scattering_cross_section(phi_ns, omegas)

######################################################################
# Plot the results
# We plot the scattering cross section as a function of frequency, as well as
# the far field radiation pattern as a function of angle and frequency.
# We create a figure with two subplots, one for the scattering cross section
# and one for the far field radiation pattern.

fig, ax = plt.subplots(1, 2)
plt.sca(ax[1])
plt.plot(sigma_sc, ks * R, "r")
plt.xlabel(r"$\sigma_{sca}$")
plt.ylabel(r"$kR_0$")
plt.ylim([np.min(ks * R), np.max(ks * R)])
plt.sca(ax[0])
fmap = plt.pcolormesh(
    angles,
    ks * R,
    np.log10(np.abs(far_field.T)),
    cmap="viridis",
)
plt.xlabel(r"${\theta}$")
plt.ylabel(r"$kR_0$")
with plt.rc_context({"axes.edgecolor": "w", "xtick.color": "w", "ytick.color": "w"}):
    cbaxes = ax[0].inset_axes([0.25, 0.1, 0.5, 0.05])
    cbar = plt.colorbar(fmap, cax=cbaxes, orientation="horizontal")
plt.show()

######################################################################
# Compute the modes (quasi bound state in the continuum)
#
# We start from an initial guess for the eigenvalue. We then use the
# eigensolver to find the eigenvalues and eigenvectors of the scattering
# matrix. The eigensolver is a non-linear solver that searches for the
# eigenvalues and eigenvectors of the matrix. The eigensolver takes as input
# the range of frequencies where the eigenvalues are expected to be found,
# and the initial guess for the eigenvalue.
#
# In this case, we search for the eigenvalues in a rectangular region of the complex plane
# specified by th bottom left corner omega0 and the top right corner omega1.
# The initial guess for the eigenvalue is the
# frequency of the plate at kR = 5.12.

omega_guess = sim.plate.frequency(5.12 / R)
guesses = [omega_guess]
omega0 = omega_guess * 0.5 - 0.1j
omega1 = omega_guess * 1.5 + 0.1j
eigenvalues, eigenvectors = sim.eigensolve(
    omega0,
    omega1,
    guesses=guesses,
)
kRevs = sim.plate.wavenumber(eigenvalues) * R
print(kRevs)


######################################################################
# Compute the matrices M and dM at each eigenvalue
M = [sim.build_matrix(omega) for omega in eigenvalues]
dM = [sim.build_matrix_derivative(omega) for omega in eigenvalues]

######################################################################
# Normalize the modes such that they satisfy the orthonormality condition
# with respect to the inner product defined by the derivative of the
# matrix M.
eigenvectors = sim.normalize_eigenvectors(eigenvalues, eigenvectors, dM)

######################################################################
# Compute the multiplicity of each eigenvalue, i.e. the number of times
# each eigenvalue appears as an eigenvalue of the matrix M.
multiplicities = sim.get_multiplicities(eigenvalues, M)
print(multiplicities)

######################################################################
# Refine the eigenvalues and eigenvectors by taking into account the
# multiplicities of the eigenvalues.
eigenvalues, eigenvectors = sim.update_eig(
    eigenvalues, eigenvectors, multiplicities, M, dM
)


######################################################################
# We plot the modes using the get_mode method of the simulation object.
# We loop over the number of eigenvalues and for each eigenvalue, we compute
# the field at the positions x and y using the get_mode method. We then plot
# the real part of the field using pcolormesh. We also plot the positions of
# the resonators as black dots.
#

x = y = np.linspace(-1.5 * R, 1.5 * R, 151)
x, y = np.meshgrid(x, y)

for i in range(len(eigenvalues)):
    plt.figure()
    mode = sim.get_mode(x, y, eigenvectors[:, i], eigenvalues[i])
    plt.pcolormesh(x, y, mode.real, cmap="RdBu_r")
    for res in resarray:
        plt.plot(*res.position, ".k")
    plt.axis("scaled")
    plt.xlabel(r"$x$")
    plt.ylabel(r"$y$")
    kRev = sim.plate.wavenumber(eigenvalues[i]) * R
    plt.title(f"$kR = ${kRev:.3e}")
    plt.show()
