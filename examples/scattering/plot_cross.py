#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Finite array of beams resonators
====================================

Scattering simulation of a finite array of resonators, each resonator being a beam.
"""


from math import pi

import matplotlib.pyplot as plt

import klove as kl

bk = kl.backend


######################################################################
# Define the elastic plate

plate = kl.ElasticPlate(1, 1, 1, 0)


######################################################################
# Define the parameters of the resonators

L = 0.25 / plate.D**0.5
S = 10 * plate.D**0.5
rho = 1
E = 1


######################################################################
# Define the positions of the resonators

a = 1
Nx, Ny = 24, 24
xbeam = ybeam = bk.linspace(0, (Nx - 1) * a, Nx) - (Nx - 1) * a / 2

res_array = []
for x0 in xbeam:
    for y0 in ybeam:
        pos = x0, y0
        res_array.append(kl.Beam(L, S, rho, E, pos))


######################################################################
# Initialize the simulation

simu = kl.ScatteringSimulation(plate, res_array)


######################################################################
# Define the frequency

omega = 3.6 * plate.D**0.5


######################################################################
# Define the source position

pos_source = (0, 0)


######################################################################
# Solve the problem

phi0 = simu.point_source
phie_alpha = simu.solve(phi0, omega, pos_source)


######################################################################
# Get the field

lextra = 8 * a
x1 = bk.linspace(-lextra + xbeam[0], lextra + xbeam[-1], 300)
y1 = bk.linspace(-lextra + ybeam[0], lextra + ybeam[-1], 300)
x, y = bk.meshgrid(x1, y1, indexing="xy")
W = simu.get_field(x, y, phi0, phie_alpha, omega, pos_source)


######################################################################
# Plot the real part

plt.figure()
plt.pcolormesh(x, y, bk.real(W), cmap="RdBu_r")
for res in res_array:
    plt.plot(*res.position, ".k", ms=1)
plt.colorbar()
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.show()


######################################################################
# Plot the magnitude

plt.figure()
plt.pcolormesh(x, y, bk.abs(W), cmap="inferno")
for res in res_array:
    plt.plot(*res.position, ".w", ms=1)
plt.colorbar()
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.show()
