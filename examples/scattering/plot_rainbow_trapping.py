#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Rainbow trapping
==========================

Scattering simulation of a graded array of resonators 
"""


import matplotlib.animation as animation
import matplotlib.pyplot as plt

import klove as kl

bk = kl.backend

plt.ion()

plt.close("all")


######################################################################
# We first define the parameters
# The plate is characterized by its thickness, density, Young's modulus
# and Poisson's ratio.
a = 0.1
plate = kl.ElasticPlate(2e-3, 2700, 70e9, 0.3)
omega0 = 2e3

######################################################################
# The array is positioned from 0 to Nres * a with a spacing of a
# The strength of the resonators is set to -69.81 times the bending
# stiffness of the plate.
Nres = 30
positions = bk.linspace(0, Nres * a, Nres)
m0 = plate.rho * a**2 * plate.h
ms = bk.ones(Nres) * m0
alpha = 2
dist = bk.linspace(0.5, 1.5, Nres)
ks = m0 * omega0**2 * dist**2
ks = bk.flipud(ks)

######################################################################
# We create an array of resonators with the positions and stiffnesses
# computed above.
resarray = []
for m, k, pos in zip(ms, ks, positions):
    resarray.append(kl.Resonator(m, k, (pos, 0)))


######################################################################
# Run a scattering simulation and plot the field norm
# We create a ScatteringSimulation object with the plate and the array
# of resonators.
sim = kl.ScatteringSimulation(plate, resarray)

######################################################################
# We define the frequency range of the simulation
omegas = bk.linspace(omega0 - omega0 / alpha, omega0, 100) * 1.01

######################################################################
# We compute the solution at the first frequency
omega = omegas[0]
pos_source = -a, 0
incident = sim.point_source

######################################################################
# We solve the scattering problem and compute the field norm at the
# positions x and y.
sol = sim.solve(incident, omega, pos_source)

x1 = bk.linspace(-2 * a, a * Nres + 3 * a, 151)
y1 = bk.linspace(-3 * a, 3 * a, 151)
x, y = bk.meshgrid(x1, y1, indexing="xy")
W = sim.get_field(x, y, incident, sol, omega, pos_source)

######################################################################
# We plot the field norm
plt.pcolormesh(x, y, bk.abs(W), cmap="inferno")
for res in resarray:
    plt.plot(*res.position, ".w")
plt.plot(*pos_source, "xk")
plt.colorbar(orientation="horizontal")
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.show()


######################################################################
# Frequency sweep.
# We compute the solution at each frequency in the range
# and store the value of the field norm at the maximum
# position.
maxfield = []
maxloc = []
data = []

solutions = sim.solve(incident, omegas, pos_source)

for iomega, omega in enumerate(omegas):
    x1 = bk.linspace(0, a * Nres + 3 * a, 151)
    Wl = sim.get_field(x1, 0, incident, solutions[iomega], omega, pos_source)
    imax = bk.argmax(bk.abs(Wl))
    maxW = bk.abs(Wl)[imax]
    maxpos = x1[imax]
    maxloc.append(maxpos)
    maxfield.append(maxW)
    W = sim.get_field(x, y, incident, solutions[iomega], omega, pos_source)
    data.append(W)

data_norm = bk.abs(data)

######################################################################
# We create a figure with a pcolormesh of the field norm
fig, ax = plt.subplots()
fmap = plt.pcolormesh(x, y, bk.abs(W), cmap="inferno", vmax=1e-3)
for res in resarray:
    plt.plot(*res.position, ".w")
plt.plot(*pos_source, "xk")
plt.colorbar(orientation="horizontal")
plt.axis("scaled")
plt.xlabel(r"$x$")
plt.ylabel(r"$y$")


def _update_plot(num):
    ax.set_title(rf"$\omega = {omegas[num]:.1f}$ rad/s")
    fmap.set_array(data_norm[num])
    return (fmap,)


ani = animation.FuncAnimation(fig, _update_plot, len(omegas), interval=50, blit=False)


######################################################################
# Position of maximum


plt.figure()
plt.plot(omegas, maxloc, ".r")
plt.xlabel(r"$\omega$")
plt.ylabel(r"$x_{max}$")
for res in resarray:
    plt.axhline(res.position[0], c="#797979", ls=":", lw=0.4)
plt.tight_layout()
plt.show()

######################################################################
# Value of maximum

plt.figure()
plt.plot(omegas, maxfield, "-.")
plt.xlabel(r"$\omega$")
plt.ylabel(r"$|W|_{max}$")
plt.tight_layout()
plt.show()
