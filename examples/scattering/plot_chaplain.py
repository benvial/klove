#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Array of masses
==========================

Scattering simulation of an array of masses atop an elastic plate
excited with a point source. This reproduce results from 
:cite:p:`chaplain2019`.
"""


import matplotlib.pyplot as plt

import klove as kl

bk = kl.backend


######################################################################
# Define the parameters
# ----------------------
#
# We first define the parameters of the problem. The number of
# resonators `Nres`, the frequency `Omega`, the mass `m` and the
# lattice spacing `a`.

Nres = 41
Omega = 2.16
m = 10
a = 1

######################################################################
# We then define the positions of the resonators
positions = bk.linspace(0, a * Nres, Nres)

######################################################################
# And the position of the point source
pos_source = bk.array([0.0, 0.0])


######################################################################
# Define the elastic plate
# -------------------------
#
# We define the elastic plate with the following parameters:
# - Young modulus 1
# - Poisson ratio 1
# - Plate thickness 1
# - Plate density 1
# - incoming wave True

plate = kl.ElasticPlate(1, 1, 1, 0)

######################################################################
# We then define the frequency of the incoming wave
omega = Omega * (plate.D / plate.rho / plate.h) ** 0.5


######################################################################
# Define the array of masses
# ---------------------------
#
# We define an array of masses of length `Nres` with the same mass
# `m` at the positions defined above.

mass_array = []
for i in range(Nres):
    mass_array.append(kl.Mass(m, (positions[i], 0)))


######################################################################
# Define the simulation object
# -----------------------------
#
# We define the simulation object with the plate and the array of
# masses.

simu = kl.ScatteringSimulation(plate, mass_array)


######################################################################
# Define the point source and solve
# ---------------------------------
#
# We define the point source and solve the scattering problem.

phi0 = simu.point_source
phie_alpha = simu.solve(phi0, omega, pos_source)


######################################################################
# Get the field
# -------------
#
# We get the field at the positions `x` and `y` defined below.

x1 = bk.linspace(-6 * a, a * Nres + 6 * a, 300)
y1 = bk.linspace(-12 * a, 12 * a, 600)
x, y = bk.meshgrid(x1, y1, indexing="xy")
W = simu.get_field(x, y, phi0, phie_alpha, omega, pos_source)


######################################################################
# Plot the field map
# -------------------
#
# We plot the field map with the real part of the field as the color,
# the positions of the resonators as dots and the point source as a cross.

plt.figure()
plt.pcolormesh(x, y, bk.real(W), cmap="RdBu_r")
for res in mass_array:
    plt.plot(*res.position, ".k")
plt.plot(*pos_source, "xk")
plt.colorbar(orientation="horizontal")
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title(f"$\Omega={Omega:0.2f}$")
plt.show()
