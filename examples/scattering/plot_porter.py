#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Cluster of pinned points
==========================

Scattering simulation of points arranged at regular 
intervals on a circle.
"""
# sphinx_gallery_thumbnail_number = -1
from math import pi

import matplotlib.pyplot as plt

import klove as kl

bk = kl.backend

######################################################################
# Checking results from :cite:p:`evans2007`.
#
# See figure 4 of the paper for a comparison of the results.
#
# We first define the parameters of the plate.

a = 1
x1 = bk.linspace(-3 * a, 3 * a, 300)
y1 = bk.linspace(-3 * a, 3 * a, 300)
x, y = bk.meshgrid(x1, y1, indexing="xy")
plate = kl.ElasticPlate(1, 1, 1, 0)
Q = (plate.rho * plate.h / plate.bending_stiffness) ** 0.5


######################################################################
# We then define a function to build a simulation object with the plate
# and an array of resonators arranged at regular intervals on a circle.


def build_simu(M, Nres):
    """
    Build a simulation object with the plate and an array of resonators
    arranged at regular intervals on a circle.
    """
    m = M * 4
    res_array = []
    for i in range(Nres):
        R = 2**0.5 if Nres == 4 else 1
        t = 2 * (i) * bk.pi / Nres
        if Nres == 4:
            t += bk.pi / Nres
        res_array.append(kl.Mass(m, (R * bk.cos(t), R * bk.sin(t))))

    return kl.ScatteringSimulation(plate, res_array)


######################################################################
# We then define a function to run a simulation for a given number of
# resonators and a given range of incoming wavenumbers.


def run_spectra(Nres):
    kmax = 6 if Nres == 4 else 8
    kincs = bk.linspace(1e-5, kmax, 300)
    plt.figure()
    plt.gca().set_prop_cycle(
        "color", [plt.cm.Spectral(i) for i in bk.linspace(0, 1, 4)]
    )
    Ms = [0.1, 1, 10, 100] if Nres == 4 else [0.01, 0.1, 1, 10]
    for M in Ms:
        simu = build_simu(M, Nres)
        phi0 = simu.plane_wave
        unorm = []
        for kinc in kincs:
            omega = kinc**2 / Q
            phie_alpha = simu.solve(phi0, omega, angle=0)
            W = simu.get_field(0, 0, phi0, phie_alpha, omega, angle=0)
            unorm.append(bk.abs(W))

        plt.plot(kincs, unorm, label=r"$\tilde{\mu}=$" + str(M))
        plt.pause(0.1)
        plt.xlim(0, kincs[-1])
    plt.legend()
    plt.title(f"{Nres} resonators")
    plt.xlabel("$ka$")
    plt.ylabel("$|u|$")
    maxu = 8 if Nres == 4 else 10.7
    plt.ylim(0, maxu)
    plt.tight_layout()


######################################################################
# We then define a function to run a simulation for a given number of
# resonators and a given angle of incidence.


def run_fields(Nres):
    """
    Run a simulation for a given number of resonators and a given angle of
    incidence.
    """
    M = 100 if Nres == 4 else 10
    kinc = 1.792 if Nres == 4 else 5.5344
    angle = pi / 4 if Nres == 4 else 0
    simu = build_simu(M, Nres)
    omega = kinc**2 / Q
    phi0 = simu.plane_wave
    phie_alpha = simu.solve(phi0, omega, angle)
    W = simu.get_field(x, y, phi0, phie_alpha, omega, angle)
    plt.figure()
    plt.pcolormesh(x, y, bk.abs(W), cmap="inferno")
    for res in simu.res_array:
        plt.plot(*res.position, ".r")
    plt.colorbar()
    plt.axis("scaled")
    plt.xlabel("$x/a$")
    plt.ylabel("$y/a$")
    theta = "\pi / 4" if Nres == 4 else "0"
    _ = plt.title(rf"$|w|, ka={kinc:0.3f}, \theta={theta}$")


######################################################################
# Spectra for 4 resonators

run_spectra(4)


######################################################################
# Spectra for 8 resonators

run_spectra(8)

######################################################################
# Field map for 4 resonators

run_fields(4)

######################################################################
# Field map for 8 resonators

run_fields(8)
