#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Diffraction by a grating
==========================

Periodic array of rings of resonators
"""

import matplotlib.pyplot as plt

import klove as kl

bk = kl.backend


######################################################################
# Define the array of resonators

or0 = 0.55  # distance from the center of the plate to the resonators
m0 = 1e-1  # mass of each resonator
k0 = or0**2 * m0  # strength of each resonator
Nres = 10  # number of resonators
period = 1.0  # period of the grating
t = bk.linspace(0, 2 * bk.pi, Nres + 1)[:-1]  # angles of the resonators
R0 = period / 2 * 0.8  # radius of the circle of resonators
xpos = period / 2 + R0 * bk.cos(t)  # x positions of the resonators
ypos = R0 * bk.sin(t)  # y positions of the resonators
resarray = []  # list of resonators
for xp, yp in zip(xpos, ypos):
    res = kl.Resonator(m0, k0, (xp, yp))  # create a resonator
    resarray.append(res)  # add the resonator to the list


######################################################################
# Define the elastic plate

plate = kl.ElasticPlate(0.1, 10, 1, 0.3)  # create an elastic plate


######################################################################
# Define the simulation object

simu = kl.DiffractionSimulation(
    plate, resarray, period, nh=0
)  # create a simulation object


######################################################################
# Define the frequency and angle (:math:`pi/2`: normal incidence)

omega = 0.5  # frequency
angle = bk.pi / 2 - bk.pi / 6  # angle of incidence

######################################################################
# We can find the minimum number of propagating harmonics


nh = simu.get_min_harmonics(omega, angle)  # minimum number of propagating harmonics
print(nh)  # print the minimum number of propagating harmonics
simu.nh = nh * 5  # use 5 times the minimum number of propagating harmonics


######################################################################
# Solve

sol = simu.solve(omega, angle)  # solve the simulation


######################################################################
# Get the field

ny = 4  # number of periods in the y direction
npx = 100  # number of points in the x direction
npy = npx * ny  # number of points in the y direction
x1 = bk.linspace(0, period, npx)  # x positions of the field
y1 = bk.linspace(-ny * period, ny * period, npy)  # y positions of the field
x, y = bk.meshgrid(x1, y1, indexing="xy")  # create a grid of points
W = simu.get_field(x, y, sol, omega, angle)  # get the total field
Ws = simu.get_scattered_field(x, y, sol, omega, angle)  # get the scattered field


######################################################################
# Plot the field maps:
#
# Total field amplitude

fig, ax = plt.subplots(1)  # create a figure and axes
field = bk.abs(W)  # get the amplitude of the total field
plt.pcolormesh(x, y, field, cmap="inferno")  # plot the amplitude of the total field
plt.gca().set_aspect("equal")  # make the aspect ratio of the plot equal
for res in resarray:
    plt.plot(*res.position, ".w")  # plot the positions of the resonators
plt.colorbar()  # add a colorbar
plt.xlabel("$x$")  # label the x axis
plt.ylabel("$y$")  # label the y axis
plt.suptitle("total")  # add a title to the plot
plt.show()  # show the plot


######################################################################
# Scattered field amplitude

fig, ax = plt.subplots(1)  # create a figure and axes
field = bk.abs(Ws)  # get the amplitude of the scattered field
plt.pcolormesh(x, y, field, cmap="inferno")  # plot the amplitude of the scattered field
plt.gca().set_aspect("equal")  # make the aspect ratio of the plot equal
for res in resarray:
    plt.plot(*res.position, ".w")  # plot the positions of the resonators
plt.colorbar()  # add a colorbar
plt.xlabel("$x$")  # label the x axis
plt.ylabel("$y$")  # label the y axis
plt.suptitle("scattered")  # add a title to the plot
plt.show()  # show the plot


######################################################################
# Total field real and imaginary parts

fig, ax = plt.subplots(1, 2)  # create a figure and axes
for i in range(2):
    plt.sca(ax[i])  # select the axes
    field = (
        bk.real(W) if i == 0 else bk.imag(W)
    )  # get the real or imaginary part of the total field
    plt.pcolormesh(
        x, y, field, cmap="RdBu_r"
    )  # plot the real or imaginary part of the total field
    plt.gca().set_aspect("equal")  # make the aspect ratio of the plot equal
    for res in resarray:
        plt.plot(*res.position, ".k")  # plot the positions of the resonators
    plt.colorbar()  # add a colorbar
    plt.xlabel("$x$")  # label the x axis
    plt.ylabel("$y$")  # label the y axis
plt.suptitle("total")  # add a title to the plot
plt.show()  # show the plot
