#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


import matplotlib.pyplot as plt

import klove as kl

plt.ion()

plt.close("all")
bk = kl.backend


######################################################################
# Define the array of resonators

or0 = 2.5
m0 = 1e-1
k0 = or0**2 * m0
k0 = 1e-6
m0 = k0 / or0**2
Nres = 1
period = 1.0
t = bk.linspace(0, 2 * bk.pi, Nres + 1)[:-1]
R0 = period / 2 * 0.8
xpos = period / 2 + R0 * bk.cos(t)
ypos = R0 * bk.sin(t)
resarray = []
for xp, yp in zip(xpos, ypos):
    res = kl.Resonator(m0, k0, (xp, yp))
    res = kl.Pin((xp, yp))
    resarray.append(res)


######################################################################
# Define the elastic plate

plate = kl.ElasticPlate(0.1, 10, 1, 0.3)

######################################################################
# Define the simulation object


kx = 0

omega0 = 0.1 - 1.5 * 1j
omega1 = 4.5 + 1.5 * 1j
omega0 = 0.1 - 0.5 * 1j
omega1 = 13 + 0.5 * 1j


import sys

nh0 = int(sys.argv[1])


simu = kl.DiffractionSimulation(plate, resarray, period, nh=nh0)

# for nh in range(100):
#     simu.nh = nh
#     M = simu.build_matrix(omega1,kx)
#     det= bk.linalg.det(M)
#     print(det)

# xssx

simu.nh = nh0

Nre, Nim = 100, 100


omegas_re = bk.linspace(omega0.real, omega1.real, Nre)
omegas_im = bk.linspace(omega1.imag, omega0.imag, Nim)
omegas_re_, omegas_im_ = bk.meshgrid(omegas_re, omegas_im, indexing="ij")


omegas_complex = omegas_re_ + 1j * omegas_im_
# Mc = simu.build_matrix(omegas_complex,kx)
# Mc = bk.transpose(Mc, axes=(2, 3, 0, 1))
# det = bk.linalg.det(Mc)
# evs = bk.linalg.eigvals(Mc)
# srt = bk.argsort(bk.abs(evs), axis=-1)
# min_evs = bk.take_along_axis(evs, srt, axis=-1)[:, :, 0]


det = bk.zeros((Nre, Nim), dtype=bk.complex128)
min_evs = bk.zeros((Nre, Nim), dtype=bk.complex128)
for i, omega_re in enumerate(omegas_re):
    for j, omega_im in enumerate(omegas_im):
        print(i, j)
        omega = omega_re + 1j * omega_im
        omega1 = omegas_complex[i, j]
        assert bk.allclose(omega, omega1)
        M = simu.build_matrix(omega, kx)
        det[i, j] = bk.linalg.det(M)
        evs = bk.linalg.eigvals(M)
        srt = bk.argsort(bk.abs(evs))
        min_evs[i, j] = evs[srt[0]]


plt.figure()
plt.pcolormesh(omegas_re, omegas_im, bk.log10(bk.abs(det.T)))
plt.colorbar()
plt.xlabel("Re $\omega$")
plt.ylabel("Im $\omega$")
plt.title(r"${\rm log}_{10} |{\rm det}\, M|$")
ax_det = plt.gca()


plt.figure()
plt.pcolormesh(omegas_re, omegas_im, bk.log10(bk.abs(min_evs.T)))
plt.colorbar()
plt.xlabel("Re $\omega$")
plt.ylabel("Im $\omega$")
plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")
ax_min_ev = plt.gca()

plt.pause(0.01)
