#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Whispering galllery modes
==========================

Structured rings of point-masses placed
atop a thin elastic plate: modal analysis
"""

import matplotlib.pyplot as plt

import klove as kl

bk = kl.backend
plt.ion()
plt.close("all")

######################################################################
# Checking results from :cite:p:`chaplain2019`.
# We first define the parameters

Nres = 20
a = 1
R = 0.5
Omega = 10
theta = bk.linspace(0, 2 * bk.pi, Nres + 1)[:-1]
xres, yres = R * bk.cos(theta), R * bk.sin(theta)
positions = bk.vstack([xres, yres]).T

pos_source = 0, 0


######################################################################
# Define the elastic plate
rho = 1
h = 1
plate = kl.ElasticPlate(h, rho, 1, 0)


######################################################################
# Define the array of masses
M = 4 * plate.rho * plate.h * a**2

mass_array = []
for i in range(Nres):
    mass_array.append(kl.Mass(M, (positions[i])))


######################################################################
# Define the simulation object

simu = kl.ScatteringSimulation(plate, mass_array, alternative=False)

################


D = plate.bending_stiffness
k0 = 1e-6 - 1 * 1j
k1 = 5 - 0 * 1j


Nre, Nim = 100, 100


k_re = bk.linspace(k0.real, k1.real, Nre)
k_im = bk.linspace(k1.imag, k0.imag, Nim)
ks_re_, ks_im_ = bk.meshgrid(k_re, k_im, indexing="ij")


omegas_complex = ks_re_ + 1j * ks_im_

# omegas_complex = (D / rho / h) ** 0.5 * ks_complex**2
Mc = simu.build_matrix(omegas_complex)
Mc = bk.transpose(Mc, axes=(2, 3, 0, 1))
det = bk.linalg.det(Mc)
evs = bk.linalg.eigvals(Mc)
srt = bk.argsort(bk.abs(evs), axis=-1)
min_evs = bk.take_along_axis(evs, srt, axis=-1)[:, :, 0]


def func(omegas_complex):
    Mc = simu.build_matrix(omegas_complex)
    # Mc = bk.transpose(Mc, axes=(2, 3, 0, 1))
    det = bk.linalg.det(Mc)
    return det


xplot = k_re * a
yplot = k_im * a

xlab = "Re $k a$"
ylab = "Im $k a$"

# plt.figure()
# plt.pcolormesh(
#     xplot,
#     yplot,
#     bk.log10(bk.abs(det.T)),
#     cmap="inferno",
# )
# plt.colorbar()
# plt.xlabel(xlab)
# plt.ylabel(ylab)
# plt.title(r"${\rm log}_{10} |{\rm det}\, M|$")
# plt.tight_layout()
# ax_det = plt.gca()


plt.figure()
plt.pcolormesh(
    xplot,
    yplot,
    bk.log10(bk.abs(min_evs.T)),
    cmap="inferno",
)
plt.colorbar()
plt.xlabel(xlab)
plt.ylabel(ylab)
plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")
plt.tight_layout()
ax_min_ev = plt.gca()

import tetrachotomy as tt

tt.set_log_level("WARNING")
tt.set_log_level("DEBUG")
# tt.set_log_level("INFO")
options = tt.get_options()

# options = tt.get_options()
# options["ncuts_max"] = 40
# options["plot"]["rectangle"] = True
# options["plot"]["circle"] = True
# options["plot"]["poles"] = True
# res = tt.find_zeros(func, k0, k1, order=1, options=options)

# zeros = res.poles
# residues = res.residues


######################################################################
# Eigenproblem

import sys

from klove.eig import *

func_gives_der = False

if func_gives_der:

    def func_eig(omega):
        return simu.build_matrix(omega), simu.build_matrix_derivative(omega)

else:

    def func_eig(omega):
        return simu.build_matrix(omega)


bk.random.seed(1)
vect_init = bk.random.rand(simu.n_res)
# vect_init = bk.ones(simu.n_res)
guesses_re = bk.linspace(1e-4, 1, 10)
guesses_im = bk.linspace(-3, 0, 10)
evs = []
for guess_re in guesses_re:
    for guess_im in guesses_im:
        guess = guess_re + 1j * guess_im
        print(guess)
        try:
            res = eig_newton(
                func_eig,
                guess,
                vect_init,
                lambda_tol=1e-6,
                max_iter=20,
                func_gives_der=func_gives_der,
            )
            print(res["eigval"])
            evs.append(res["eigval"])
        except:
            pass
evs = bk.array(evs)
evs0 = evs.copy()
evs = evs[evs.real > 0]
evs = evs[evs.imag < 0]
evs = evs[evs.imag > -5]
evs = evs[evs.real < 10]
for e in evs:
    plt.plot(e.real, e.imag, "og")

sys.exit(0)

######################################################################
# Define the line source and solve

phi0 = simu.point_source
phie_alpha = simu.solve(phi0, omega, pos_source)

######################################################################
# Get the field

x1 = bk.linspace(-2 * R, 2 * R, 100)
y1 = bk.linspace(-2 * R, 2 * R, 100)
x, y = bk.meshgrid(x1, y1, indexing="xy")
W = simu.get_field(x, y, phi0, phie_alpha, omega, pos_source)


######################################################################
# Plot the field map

plt.figure()
plt.pcolormesh(x, y, bk.real(W), cmap="RdBu_r")
for res in mass_array:
    plt.plot(*res.position, ".k")
plt.plot(*pos_source, "xk")
plt.colorbar(orientation="horizontal")
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title(f"$\Omega={Omega:0.2f}$")
plt.show()
