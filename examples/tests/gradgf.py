#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

import os

os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"

from math import pi

import matplotlib.pyplot as plt

plt.ion()
plt.close("all")

import klove as kl

bk = kl.backend
from klove.special import *

x = bk.linspace(-1, 2, 200)
y = bk.linspace(-1, 1, 500)
# x = bk.linspace(0.1, 1, 400)
# y = bk.linspace(0.1, 1, 400)
dx = x[1] - x[0]
dy = y[1] - y[0]

k = 13

x, y = bk.meshgrid(x, y, indexing="ij")
r = (x**2 + y**2) ** 0.5
# kr = k * r
f = greens_function_cartesian(k, x, y)
df = bk.gradient(f)
df = df[0] / dx, df[1] / dy
dfana = grad_greens_function_cartesian(k, x, y)

err1 = bk.mean(bk.abs(dfana[0] - df[0])) / bk.mean(bk.abs(dfana[0]))
err2 = bk.mean(bk.abs(dfana[1] - df[1])) / bk.mean(bk.abs(dfana[1]))
# err1 = bk.mean(bk.abs(1 - dfana[0] / df[0]))
# err2 = bk.mean(bk.abs(1 - dfana[1] / df[1]))

print(err1)
print(err2)


for j in range(2):
    fig, ax = plt.subplots(2, 3)
    _ = ax[0, 0].pcolormesh(x, y, df[j].real, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[0, 0])
    ax[0, 0].set_aspect(1)
    ax[0, 0].set_title("Re num")
    _ = ax[0, 1].pcolormesh(x, y, dfana[j].real, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[0, 1])
    ax[0, 1].set_aspect(1)
    ax[0, 1].set_title("Re ana")
    _ = ax[1, 0].pcolormesh(x, y, df[j].imag, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[1, 0])
    ax[1, 0].set_aspect(1)
    ax[1, 0].set_title("Im num")
    _ = ax[1, 1].pcolormesh(x, y, dfana[j].imag, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[1, 1])
    ax[1, 1].set_aspect(1)
    ax[1, 1].set_title("Im ana")

    _ = ax[0, 2].pcolormesh(x, y, (df[j] - dfana[j]).real, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[0, 2])
    ax[0, 2].set_aspect(1)
    ax[0, 2].set_title("Err Re")
    _ = ax[1, 2].pcolormesh(x, y, (df[j] - dfana[j]).imag, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[1, 2])
    ax[1, 2].set_aspect(1)
    ax[1, 2].set_title("Err Im")


d2fx = bk.gradient(dfana[0])[0] / dx
d2fy = bk.gradient(dfana[1])[1] / dy
d2f = d2fx + d2fy
d2fana = laplacian_greens_function_cartesian(k, x, y)

r = kl.special.radial_coordinates(x, y)

# q = kl.special.bessel_j0(k * r)
# dfx = bk.gradient(q)[0] / dx
# dfy = bk.gradient(q)[1] / dy
# d2f = bk.gradient(dfx)[0] / dx + bk.gradient(dfy)[1] / dy

# d2fana = 2 * kl.special.bessel_y2(k * r) / r - k * kl.special.bessel_y3(k * r)

# # d2fana *= x / r

# d2fana = -2 * k / r * kl.special.bessel_j1(k * r) + k**2 * kl.special.bessel_j2(k * r)

err3 = bk.mean(bk.abs(d2fana - d2f)) / bk.mean(bk.abs(d2fana))
# err3 = bk.mean(bk.abs(1 - d2fana / d2f))

print(err3)
fig, ax = plt.subplots(2, 3)
_ = ax[0, 0].pcolormesh(x, y, d2f.real, cmap="RdBu_r")
plt.colorbar(_, ax=ax[0, 0])
ax[0, 0].set_aspect(1)
ax[0, 0].set_title("Re num")
_ = ax[0, 1].pcolormesh(x, y, d2fana.real, cmap="RdBu_r")
plt.colorbar(_, ax=ax[0, 1])
ax[0, 1].set_aspect(1)
ax[0, 1].set_title("Re ana")
_ = ax[1, 0].pcolormesh(x, y, d2f.imag, cmap="RdBu_r")
plt.colorbar(_, ax=ax[1, 0])
ax[1, 0].set_aspect(1)
ax[1, 0].set_title("Im num")
_ = ax[1, 1].pcolormesh(x, y, d2fana.imag, cmap="RdBu_r")
plt.colorbar(_, ax=ax[1, 1])
ax[1, 1].set_aspect(1)
ax[1, 1].set_title("Im ana")

_ = ax[0, 2].pcolormesh(x, y, (d2f - d2fana).real, cmap="RdBu_r")
plt.colorbar(_, ax=ax[0, 2])
ax[0, 2].set_aspect(1)
ax[0, 2].set_title("Err Re")
_ = ax[1, 2].pcolormesh(x, y, (d2f - d2fana).imag, cmap="RdBu_r")
plt.colorbar(_, ax=ax[1, 2])
ax[1, 2].set_aspect(1)
ax[1, 2].set_title("Err Im")
# plt.tight_layout()


offset = 1


df = bk.gradient(d2fana)
d3f = df[0] / dx, df[1] / dy
d3fana = grad_laplacian_greens_function_cartesian(k, x, y)

err1 = bk.mean(bk.abs(d3fana[0][offset:-offset] - d3f[0][offset:-offset])) / bk.mean(
    bk.abs(d3fana[0][offset:-offset])
)
err2 = bk.mean(bk.abs(d3fana[1][offset:-offset] - d3f[1][offset:-offset])) / bk.mean(
    bk.abs(d3fana[1][offset:-offset])
)
# err1 = bk.mean(bk.abs(1 - d3fana[0] / d3f[0]))
# err2 = bk.mean(bk.abs(1 - d3fana[1] / d3f[1]))
# err1 = bk.mean(bk.abs(bk.real(d3fana[0] - d3f[0]))) / bk.mean(bk.abs(d3fana[0]))
# err2 = bk.mean(bk.abs(bk.real(d3fana[1] - d3f[1]))) / bk.mean(bk.abs(d3fana[1]))
print(err1)
print(err2)
# err1 = bk.mean(bk.abs(bk.imag(d3fana[0] - d3f[0]))) / bk.mean(bk.abs(d3fana[0]))
# err2 = bk.mean(bk.abs(bk.imag(d3fana[1] - d3f[1]))) / bk.mean(bk.abs(d3fana[1]))
# print(err1)
# print(err2)


for j in range(2):
    fig, ax = plt.subplots(2, 3)
    _ = ax[0, 0].pcolormesh(x, y, d3f[j].real, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[0, 0])
    ax[0, 0].set_aspect(1)
    ax[0, 0].set_title("Re num")
    _ = ax[0, 1].pcolormesh(x, y, d3fana[j].real, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[0, 1])
    ax[0, 1].set_aspect(1)
    ax[0, 1].set_title("Re ana")
    _ = ax[1, 0].pcolormesh(x, y, d3f[j].imag, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[1, 0])
    ax[1, 0].set_aspect(1)
    ax[1, 0].set_title("Im num")
    _ = ax[1, 1].pcolormesh(x, y, d3fana[j].imag, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[1, 1])
    ax[1, 1].set_aspect(1)
    ax[1, 1].set_title("Im ana")

    _ = ax[0, 2].pcolormesh(x, y, (d3f[j] - d3fana[j]).real, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[0, 2])
    ax[0, 2].set_aspect(1)
    ax[0, 2].set_title("Err Re")
    _ = ax[1, 2].pcolormesh(x, y, (d3f[j] - d3fana[j]).imag, cmap="RdBu_r")
    plt.colorbar(_, ax=ax[1, 2])
    ax[1, 2].set_aspect(1)
    ax[1, 2].set_title("Err Im")
    # plt.tight_layout()


# import scipy.special as sp


# def j2(kr):
#     return sp.jn(2, kr)


# def y2(kr):
#     return sp.yv(2, kr)


# def k2(kr):
#     return sp.kv(2, kr)


# def j3(kr):
#     return sp.jn(3, kr)


# def y3(kr):
#     return sp.yv(3, kr)


# def k3(kr):
#     return sp.kv(3, kr)


# kr = bk.linspace(0, 1, 100)

# x = y = bk.linspace(0, 1, 101)
# y = bk.linspace(0, 2, 201)
# dx = x[1] - x[0]
# dy = y[1] - y[0]

# k = 30.1


# x, y = bk.meshgrid(x, y, indexing="ij")
# r = (x**2 + y**2) ** 0.5
# kr = k * r

# df = bk.gradient(j0(kr))
# df = df[0] / dx, df[1] / dy

# df_ana = -k * y / r * j1(kr)

# d2fx = bk.gradient(df[0])[0] / dx
# d2fy = bk.gradient(df[1])[1] / dy
# d2f = d2fx + d2fy
# d2f_ana = -k / r * j1(kr) + k**2 * j2(kr)

# # fig, ax = plt.subplots(1, 2)
# # _ = ax[0].pcolormesh(x, y, df[1], cmap="RdBu_r")
# # plt.colorbar(_)
# # ax[0].set_aspect(1)
# # _ = ax[1].pcolormesh(x, y, df_ana, cmap="RdBu_r")
# # plt.colorbar(_)
# # ax[1].set_aspect(1)

# fig, ax = plt.subplots(1, 2)
# _ = ax[0].pcolormesh(x, y, d2f, cmap="RdBu_r", vmin=-300, vmax=300)
# plt.colorbar(_)
# ax[0].set_aspect(1)
# _ = ax[1].pcolormesh(x, y, d2f_ana, cmap="RdBu_r", vmin=-300, vmax=300)
# plt.colorbar(_)
# ax[1].set_aspect(1)

# # plt.figure()
# # plt.pcolormesh(x, y, bk.log10(abs(d2f_ana-d2f)), cmap="RdBu_r")
# # plt.colorbar()
# # plt.axis("equal")


# d3fx = bk.gradient(d2f)[0] / dx
# d3fy = bk.gradient(d2f)[1] / dy
# d3fx_ana = (
#     k * x / r**3 * j1(kr) + k**2 * x / r**2 * j2(kr) - k**3 * x / r * j3(kr)
# )

# fig, ax = plt.subplots(1, 2)
# _ = ax[0].pcolormesh(x, y, d3fx, cmap="RdBu_r", vmin=-5000, vmax=5000)
# plt.colorbar(_)
# ax[0].set_aspect(1)
# _ = ax[1].pcolormesh(x, y, d3fx_ana, cmap="RdBu_r", vmin=-5000, vmax=5000)
# plt.colorbar(_)
# ax[1].set_aspect(1)
