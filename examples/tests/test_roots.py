#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


#### https://rparini.github.io/cxroots/

from numpy import cos, exp, sin

f = lambda z: (exp(2 * z) * cos(z) - 1 - sin(z) + z**5) * (z * (z + 2)) ** 2

from cxroots import Circle, Rectangle

C = Rectangle((0.01, 3), (-3, -0.01))
C = Circle(0, 3)
# roots = C.roots(f)
# roots.show()


import matplotlib.pyplot as plt

##### https://github.com/nennigb/polze
import numpy as np

plt.ion()
from polze import *

# Definition of the function
# f = lambda z: np.tan(z)
# df = lambda z: np.tan(z)**2 + 1 # [df is optional]
df = None
# Initialization of the solver
pz = PZ(
    (f, df),
    Rmax=3,
    Npz=10,
    Ni=1024,
    R0=0.0,
    options={"_Npz_limit": 110, "_RiShift": 1.1, "_tol": 1e-3},
)
# Solve with moment method
pz.solve()
# Get the poles p and the zeros z
p, z = pz.dispatch()
# plot the roots
pz.plot()
# Performed an iterative refinement with Newton-Raphson method
pr, mult, info = pz.iterative_ref()
