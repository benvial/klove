#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: GPLv3


from dataclasses import dataclass

from klove import backend as bk
from klove.core import Scatterer, ScatteringSimulation
from klove.special import (
    EPS,
    bessel_j0,
    bessel_j1,
    bessel_y0,
    bessel_y1,
    radial_coordinates,
)


def _gfreal_helm(kr):
    return bk.where(bk.abs(bk.array(kr)) < EPS, 1.0, bessel_j0(kr))


def _gfimag_helm(kr):
    return bk.where(
        bk.abs(bk.array(kr)) < EPS,
        -1 / EPS,
        bessel_y0(kr),
    )


def _dgfreal_helm(kr):
    return bk.where(bk.abs(bk.array(kr)) < EPS, 0.0, -bessel_j1(kr))


def _dgfimag_helm(kr):
    return bk.where(
        bk.abs(bk.array(kr)) < EPS,
        1 / EPS,
        -bessel_y1(kr),
    )


def greens_function_helm(k, r):
    kr = k * r
    return _gfreal_helm(kr) + 1j * _gfimag_helm(kr)


def dgreens_function_dk_helm(k, r):
    kr = k * r
    return k * (_dgfreal_helm(kr) + 1j * _dgfimag_helm(kr))


class HelmholtzMedium:
    def __init__(self, incoming=False):
        self.incoming = incoming

    def wavenumber(self, omega):
        return omega

    def dwavenumber_domega(self, omega):
        return bk.ones_like(omega)

    def frequency(self, k):
        return k

    def greens_function_cartesian(self, omega, x, y):
        if self.incoming:
            omega = omega.conj()
        r = radial_coordinates(x, y)
        return self.greens_function(omega, r)

    def greens_function(self, omega, r):
        if self.incoming:
            omega = omega.conj()
        return greens_function_helm(self.wavenumber(omega), r)

    def dgreens_function_domega(self, omega, r):
        if self.incoming:
            omega = omega.conj()
        return dgreens_function_dk_helm(
            self.wavenumber(omega), r
        ) * self.dwavenumber_domega(omega)


gamma = 0.5772156649015329
pi = bk.pi


@dataclass
class HelmholtzScatterer(Scatterer):
    """Class representing a scatterer (Helmholtz).

    Parameters
    ----------
    epsilon : float
        Value of the small radius.
    position : tuple
        The position of the scatterer.

    """

    epsilon: float
    position: tuple

    def strength(self, omega) -> float:
        """Strength function of frequency `omega`"""
        # return -pi * 1j / 2 / (bk.log(2 / omega / self.epsilon) - gamma + pi * 1j / 2)
        return -bessel_j0(omega * self.epsilon) / greens_function_helm(
            omega, self.epsilon
        )

    def dstrength_domega(self, omega) -> float:
        """Derivative of strength function w.r.t. `omega`"""
        return (
            -pi
            * 1j
            / 2
            / omega
            / (bk.log(2 / omega / self.epsilon) - gamma + pi * 1j / 2) ** 2
        )


class HelmholtzScatteringSimulation(ScatteringSimulation):
    """Class to run a scattering simulation.

    Parameters
    ----------
    medium : :class:`HelmholtzMedium`
        The medium
    res_array : array of :class:`HelmholtzScatterer`
        An array containing the scatterers

    """

    def __init__(self, medium, res_array):
        super().__init__(medium, res_array, True)
        self.medium = medium

    def build_matrix(self, omega):
        omega = bk.array(omega)
        matrix = bk.array(
            bk.zeros((self.n_res, self.n_res, *omega.shape), dtype=bk.complex128)
        )
        for alpha, res_alpha in enumerate(self.res_array):
            for beta in range(alpha, self.n_res):
                if alpha == beta:
                    mat = 1 / self.res_array[beta].strength(omega)
                else:
                    mat = -self.medium.greens_function(omega, self._dr[alpha, beta])
                matrix[alpha, beta] = matrix[beta, alpha] = mat
        return matrix

    def build_matrix_derivative(self, omega):
        omega = bk.array(omega)
        matrix = bk.array(
            bk.zeros((self.n_res, self.n_res, *omega.shape), dtype=bk.complex128)
        )
        for alpha, res_alpha in enumerate(self.res_array):
            for beta in range(alpha, self.n_res):
                res_beta = self.res_array[beta]
                delta = 1 if alpha == beta else 0
                dr = self._dr[alpha, beta]
                dG0 = (
                    self.medium.dgreens_function_domega(omega, dr)
                    if alpha != beta
                    else 0
                )
                t_beta = res_beta.strength(omega)
                dt_beta = res_beta.dstrength_domega(omega)
                mat = -dt_beta / t_beta**2 * delta - dG0
                matrix[alpha, beta] = matrix[beta, alpha] = mat
        return matrix
