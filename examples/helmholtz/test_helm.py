#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


import helmholtz as hm
import matplotlib.pyplot as plt

import klove as kl

plt.ion()
plt.close("all")

bk = kl.backend
pi = bk.pi


#################################################################
# Parameters

a = 1
epsilon = 0.05

q = 1 / 2**0.5
# q = 1


medium = hm.HelmholtzMedium()


Nx, Ny = 18, 18
res_array = []
for i in range(Nx):
    xs = bk.linspace(-(Nx - 1 - i), (Nx - 1 - i), (Nx - i)) * a * q
    for x0 in xs:
        pos = x0, i * a * q
        res_array.append(hm.HelmholtzScatterer(epsilon, pos))
        if i != 0:
            pos = x0, -i * a * q
            res_array.append(hm.HelmholtzScatterer(epsilon, pos))

# res_array = []
# for i in range(Nx):
#     for j in range(Ny):
#             pos = (i-Nx/2+0.5) * a * q, (j-Nx/2+0.5) * a * q
#             res_array.append(hm.HelmholtzScatterer(epsilon, pos))


######################################################################
# Define the simulation object
simu = hm.HelmholtzScatteringSimulation(medium, res_array)
# omega = 3-1j

omega = 1.82
# M = simu.build_matrix(omega)
# print(M)
# dM = simu.build_matrix_derivative(omega)
# print(dM)

pos_source = (0, 0)
phi0 = simu.point_source
phie_alpha = simu.solve(phi0, omega, pos_source)

lextra = 6 * a
x1 = bk.linspace(-lextra - Nx * a * q, lextra + Nx * a * q, 201)
y1 = x1
x, y = bk.meshgrid(x1, y1, indexing="xy")
W = simu.get_field(x, y, phi0, phie_alpha, omega, pos_source)
# W = simu.get_scattered_field(x, y,  phie_alpha, omega)


######################################################################
# Plot the real part
plt.figure()
plt.pcolormesh(x, y, bk.real(W), cmap="RdBu_r")
for res in res_array:
    plt.plot(*res.position, ".k", ms=1)
plt.colorbar()
plt.axis("scaled")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.show()


# plt.figure()
# for res in res_array:
#     plt.plot(*res.position, ".k", ms=1)
# # plt.colorbar()
# plt.axis("scaled")
# plt.xlabel("$x$")
# plt.ylabel("$y$")
# plt.pause(0.01)

# for t in bk.linspace(0,2*pi,33):
#     plt.pcolormesh(x, y, bk.real(W*bk.exp(1j*t)), cmap="RdBu_r")
#     plt.pause(0.01)


# import scipy.special as sp

# gamma = 0.5772156649015329
# t = -pi * 1j / 2 / (bk.log(2 / omega / epsilon) - gamma + pi * 1j / 2)


# n_res = len(res_array)

# omega = bk.array(omega)
# matrix = bk.array(bk.zeros((n_res, n_res, *omega.shape), dtype=bk.complex128))
# for alpha, res_alpha in enumerate(res_array):
#     for beta, res_beta in enumerate(res_array):
#         if alpha == beta:
#             mat = 1 / t
#         else:
#             pos_alpha = res_alpha.position
#             pos_beta = res_beta.position
#             dr = (
#                 (pos_alpha[0] - pos_beta[0]) ** 2 + (pos_alpha[1] - pos_beta[1]) ** 2
#             ) ** 0.5
#             mat = -sp.hankel1(0, omega * dr)
#         matrix[alpha, beta] = matrix[beta, alpha] = mat

# rhs = bk.array(bk.zeros((n_res, *omega.shape), dtype=bk.complex128))
# for alpha, res_alpha in enumerate(res_array):
#     pos_alpha = res_alpha.position
#     dr = (
#         (pos_alpha[0] - pos_source[0]) ** 2 + (pos_alpha[1] - pos_source[1]) ** 2
#     ) ** 0.5
#     rhs[alpha] = sp.hankel1(0, omega * dr)

# sol = bk.linalg.solve(matrix, rhs)


# dr = ((x - pos_source[0]) ** 2 + (y - pos_source[1]) ** 2) ** 0.5

# W = sp.hankel1(0, omega * dr)

# for alpha, res_alpha in enumerate(res_array):
#     pos_alpha = res_alpha.position
#     dr = ((pos_alpha[0] - x) ** 2 + (pos_alpha[1] - y) ** 2) ** 0.5
#     W += sp.hankel1(0, omega * dr) * sol[alpha]
# ######################################################################
# # Plot the real part
# plt.figure()
# plt.pcolormesh(x, y, bk.real(W), cmap="RdBu_r")
# for res in res_array:
#     plt.plot(*res.position, ".k", ms=1)
# plt.colorbar()
# plt.axis("scaled")
# plt.xlabel("$x$")
# plt.ylabel("$y$")
# plt.show()
