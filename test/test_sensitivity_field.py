#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


import numpy as npo

import klove as kl

bk = kl.backend
pi = bk.pi
a = 1
Nres = 4

plot = False
savefig = False
picfolder = "/home/bench/pic"

if plot:
    import matplotlib.pyplot as plt

    plt.ion()
    plt.close("all")


plate = kl.ElasticPlate(a * 1, 1, 1, 0.0)
p = 256 * plate.rho * plate.h * plate.D

omegap = plate.omega0(a)
m_plate = plate.rho * a**2 * plate.h

alternative = True
omega0 = (0.01 - 5.8 * 1j) * omegap
omega1 = (5 + 0.1 * 1j) * omegap

delta = 1e-7
# rtol = delta * 10

recursive = True
peak_ref = 5
lambda_tol = 1e-6

omegar0 = omegap * 1

m0 = m_plate * 1
k0 = omegar0**2 * m0

npo.random.seed(1984)

positions = bk.array(npo.random.rand(Nres, 2) * a)
stiffnesses = bk.array(npo.random.rand(Nres) * k0)
masses = bk.array(npo.random.rand(Nres) * m0)
res_array = []
for i in range(Nres):
    res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))


simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)


nfreq = 300
omegas = bk.linspace(0.1, 2, nfreq) * omegap
pos_source = (0, 0)
expo = -3

incident = simu.point_source
param_inc = pos_source

# incident = simu.plane_wave
# param_inc = 0

tol_rel_err = 1e-1


def arbitrary(omega):
    k = simu.wavenumber(omega)
    return k ** (expo)


# if True:
def check(ipos, ires, param, results=None):

    if results == None:

        eigenvalues, eigenvectors = simu.eigensolve(
            omega0,
            omega1,
            recursive=recursive,
            lambda_tol=lambda_tol,
            peak_ref=peak_ref,
        )

        M = [simu.build_matrix(omega) for omega in eigenvalues]
        dM = [simu.build_matrix_derivative(omega) for omega in eigenvalues]
        eigenvectors = simu.normalize_eigenvectors(eigenvalues, eigenvectors)
        multiplicities = simu.get_multiplicities(eigenvalues, M)
        eigenvalues, eigenvectors = simu.update_eig(
            eigenvalues, eigenvectors, multiplicities, M, dM
        )
        Nmodes = len(eigenvalues)
        print(f"Found {Nmodes} modes")
        print(eigenvalues)
        # compute qnm expansion coeeficients
        bs = simu.get_expansion_coefficients(
            eigenvalues, eigenvectors, omegas, incident, param_inc, arbitrary
        )
        ## compute Phi with direct problem and QNM expansion
        direct = bk.zeros((nfreq, Nres), dtype=bk.complex128)
        qmem = bk.zeros((nfreq, Nres), dtype=bk.complex128)
        for i, omega in enumerate(omegas):
            direct[i] = simu.solve(incident, omega, param_inc)
            rec = 0
            for n in range(Nmodes):
                rec += bs[i, n] * eigenvectors[:, n]
            qmem[i] = rec
        qmem = bk.array(qmem)
        error = bk.linalg.norm(qmem - direct)
        norm = bk.linalg.norm(direct)
        rel_err = error / norm
        print(f"relative error field QNM/direct:        {rel_err*100:.3f}%")
        assert rel_err < tol_rel_err
    else:
        eigenvalues, eigenvectors, qmem, direct = results
        Nmodes = len(eigenvalues)
        # print(f"Found {Nmodes} modes")
        # print(eigenvalues)

    if plot and results == None:

        fig, ax = plt.subplots(2, 1, figsize=(8, 8))
        ax[0].plot(omegas / omegap, direct.real, label="direct")
        ax[0].set_prop_cycle(None)
        ax[0].plot(omegas / omegap, qmem.real, ".", label="qnm")
        # ax[0].set_xlabel("$\omega/\omega_p$")
        ax[0].set_ylabel(r"Re $\Phi$")
        ax[1].plot(omegas / omegap, direct.imag, label="direct")
        ax[1].set_prop_cycle(None)
        ax[1].plot(omegas / omegap, qmem.imag, ".", label="qnm")
        ax[1].set_xlabel(r"$\omega/\omega_p$")
        ax[1].set_ylabel(r"Im $\Phi$")
        (line1,) = ax[1].plot([], "-k", label="direct")
        (line2,) = ax[1].plot([], ".k", label="qnm")
        ax[1].legend(handles=[line1, line2])
        plt.tight_layout()
        if savefig:
            plt.savefig(
                rf"{picfolder}/expansion_field_resonator_{ires}_param_{param}_pos_{ipos}.png"
            )

    ############################################################################
    #### perturbed simulation

    ## perturbed simulation
    dpositions = bk.zeros_like(positions)
    dpositions[:] = positions[:]
    dmasses = bk.zeros_like(masses)
    dmasses[:] = masses[:]
    dstiffnesses = bk.zeros_like(stiffnesses)
    dstiffnesses[:] = stiffnesses[:]

    if param == "mass":
        dp = m0 * delta
        dmasses[ires] += dp
    elif param == "stiffness":
        dp = k0 * delta
        dstiffnesses[ires] += dp
    else:
        dp = a * delta
        dpositions[ires, ipos] += dp

    dres_array = []
    for i in range(Nres):
        dres_array.append(kl.Resonator(dmasses[i], dstiffnesses[i], dpositions[i]))

    dsimu = kl.ScatteringSimulation(plate, dres_array, alternative=alternative)

    ## compute dPhi/dp with direct problem and QNM expansion
    phi = qmem
    # phi = direct
    cs = simu.get_expansion_coefficients_adjoint(
        eigenvalues,
        eigenvectors,
        omegas,
        incident,
        param_inc,
        phi,
        param,
        ires,
        ipos,
        arbitrary,
    )

    direct_adj = bk.zeros((nfreq, Nres), dtype=bk.complex128)
    qmem_adj = bk.zeros((nfreq, Nres), dtype=bk.complex128)
    for i, omega in enumerate(omegas):
        matrix = simu.build_matrix(omega)
        dM = simu.build_dmatrix_dparam(omega, ires, param, ipos)
        M = simu.build_matrix(omega)
        Mpert = dsimu.build_matrix(omega)
        dM_fd = (Mpert - M) / dp
        assert bk.allclose(dM, dM_fd, rtol=1e-4)

        angle_test = 0.9
        rhs = simu.build_rhs(simu.plane_wave, omega, angle_test)
        rhs_pert = dsimu.build_rhs(simu.plane_wave, omega, angle_test)
        drhs_fd = (rhs_pert - rhs) / dp

        drhs = simu.build_dincident_dposition_rhs(
            omega, simu.plane_wave, angle_test, ires, param, ipos
        )

        assert bk.allclose(drhs, drhs_fd, rtol=1e-4)

        rhs = simu.build_rhs(incident, omega, param_inc)
        rhs_pert = dsimu.build_rhs(incident, omega, param_inc)
        drhs_fd = (rhs_pert - rhs) / dp
        drhs = simu.build_dincident_dposition_rhs(
            omega, incident, param_inc, ires, param, ipos
        )
        assert bk.allclose(drhs, drhs_fd, rtol=1e-4)
        rhs = (
            simu.build_dincident_dposition_rhs(
                omega, incident, param_inc, ires, param, ipos
            )
            - dM @ phi[i]
        )
        direct_adj[i] = bk.linalg.solve(matrix, rhs)
        rec = 0
        for n in range(Nmodes):
            rec += cs[i, n] * eigenvectors[:, n]
        qmem_adj[i] = rec
    qmem_adj = bk.array(qmem_adj)
    error = bk.linalg.norm(qmem_adj - direct_adj)
    norm = bk.linalg.norm(direct_adj)

    rel_err = error / norm
    print(f"relative error gradient QNM/direct:    {rel_err*100:.3f}%")
    assert rel_err < tol_rel_err

    ##  finite differences
    ddirect = bk.zeros((nfreq, Nres), dtype=bk.complex128)
    for i, omega in enumerate(omegas):
        ddirect[i] = dsimu.solve(incident, omega, param_inc)

    dPhidp_fd = (ddirect - direct) / dp
    error = bk.linalg.norm(dPhidp_fd - direct_adj)
    norm = bk.linalg.norm(direct_adj)
    rel_err = error / norm
    print(f"relative error gradient FD/direct:     {rel_err*100:.3f}%")
    assert rel_err < tol_rel_err

    error = bk.linalg.norm(dPhidp_fd - qmem_adj)
    norm = bk.linalg.norm(qmem_adj)
    rel_err = error / norm
    print(f"relative error gradient FD/QNM:        {rel_err*100:.3f}%")
    assert rel_err < tol_rel_err

    if plot:
        fig, ax = plt.subplots(2, 1, figsize=(8, 8))
        ax[0].plot(omegas / omegap, bk.abs(direct_adj), label="direct", lw=3, alpha=0.6)
        ax[0].set_prop_cycle(None)
        ax[0].plot(omegas / omegap, bk.abs(qmem_adj), ".", label="qnm")
        ax[0].set_prop_cycle(None)
        ax[0].plot(omegas / omegap, bk.abs(dPhidp_fd), "--", label="fd")
        # ax[0].set_xlabel("$\omega/\omega_p$")
        ax[0].set_ylabel(r"$|d\Phi/dp|$")
        ax[0].set_yscale("log")
        ax[1].plot(
            omegas / omegap, bk.angle(direct_adj), label="direct", lw=3, alpha=0.6
        )
        ax[1].set_prop_cycle(None)
        ax[1].plot(omegas / omegap, bk.angle(qmem_adj), ".", label="qnm")
        ax[1].set_prop_cycle(None)
        ax[1].plot(omegas / omegap, bk.angle(dPhidp_fd), "--", label="fd")
        ax[1].set_xlabel(r"$\omega/\omega_p$")
        ax[1].set_ylabel(r"arg $d\Phi/dp$")
        (line1,) = ax[1].plot([], "-k", label="direct", lw=3, alpha=0.6)
        (line2,) = ax[1].plot([], ".k", label="qnm")
        (line3,) = ax[1].plot([], "--k", label="fd")
        ax[1].legend(handles=[line1, line2, line3])
        title = rf"resonator {ires}, param $p$: {param}"
        if param == "pos":
            title += f" {ipos}"
        plt.suptitle(title)
        plt.tight_layout()
        if savefig:
            plt.savefig(
                rf"{picfolder}/matplotlib/sens_field_resonator_{ires}_param_{param}_pos_{ipos}.png"
            )

    return eigenvalues, eigenvectors, qmem, direct


def test_sensitivities():
    results = None
    for ires in range(Nres):
        for param in ["mass", "stiffness", "pos"]:
            for ipos in [0, 1]:
                print("")
                print(
                    "#############################################################################################"
                )
                title = f"resonator {ires}, {param}"
                if param == "pos":
                    title += f" {ipos}"
                length = len(title)
                npad = 30 - length
                ipad = 1 if npad % 2 != 0 else 0
                npad = npad // 2
                title = " " * npad + f"{title}" + " " * (npad + ipad)
                print(
                    f"###############################{title}################################"
                )
                print(
                    "#############################################################################################"
                )
                print("")
                recompute = False
                results = check(ipos, ires, param, results)
                if recompute:
                    results = None
                if param != "pos":
                    break


if __name__ == "__main__":
    test_sensitivities()
