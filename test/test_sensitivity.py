#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


import numpy as npo

import klove as kl

bk = kl.backend
pi = bk.pi
a = 1
Nres = 4
######################################################################
# Elastic plate

plate = kl.ElasticPlate(a * 1, 1, 1, 0.0)
p = 256 * plate.rho * plate.h * plate.D

omegap = plate.omega0(a)
m_plate = plate.rho * a**2 * plate.h

alternative = True
omega0 = (0.01 - 5.8 * 1j) * omegap
omega1 = (5 + 0.5 * 1j) * omegap

recursive = True
peak_ref = 4
refine = True
lambda_tol = 1e-12


delta = 1e-6


######################################
# Cluster

omegar0 = omegap * 1

m0 = m_plate * 1
k0 = omegar0**2 * m0

npo.random.seed(1984)

positions = bk.array(npo.random.rand(Nres, 2) * a)
stiffnesses = bk.array(npo.random.rand(Nres) * k0)
masses = bk.array(npo.random.rand(Nres) * m0)
res_array = []
for i in range(Nres):
    res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))

######################################################################
# Define the simulation object
simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)


def check(ipos, ires, param, eig=None):

    if eig == None:
        evs, eigenvectors = simu.eigensolve(
            omega0,
            omega1,
            recursive=recursive,
            lambda_tol=lambda_tol,
            peak_ref=peak_ref,
            refine=refine,
        )
        Nmodes = len(evs)
        print(f"Found {Nmodes} modes")
        print(evs)

        eigenvectors = simu.normalize_eigenvectors(evs, eigenvectors)
    else:
        evs, eigenvectors = eig
        Nmodes = len(evs)

    ## perturbed simulation
    dpositions = bk.zeros_like(positions)
    dpositions[:] = positions[:]
    dmasses = bk.zeros_like(masses)
    dmasses[:] = masses[:]
    dstiffnesses = bk.zeros_like(stiffnesses)
    dstiffnesses[:] = stiffnesses[:]

    if param == "mass":
        dp = m0 * delta
        dmasses[ires] += dp
    elif param == "stiffness":
        dp = k0 * delta
        dstiffnesses[ires] += dp
    else:
        dp = a * delta
        dpositions[ires, ipos] += dp

    dres_array = []
    for i in range(Nres):
        dres_array.append(kl.Resonator(dmasses[i], dstiffnesses[i], dpositions[i]))

    dsimu = kl.ScatteringSimulation(plate, dres_array, alternative=alternative)

    devs, deigenvectors = dsimu.eigensolve(
        omega0,
        omega1,
        guesses=evs,
        recursive=False,
        lambda_tol=lambda_tol,
        peak_ref=peak_ref,
        refine=refine,
    )
    deigenvectors = dsimu.normalize_eigenvectors(devs, deigenvectors)
    Ndmodes = len(devs)
    print(f"Found {Ndmodes} modes")
    print(devs)

    ###############################
    for imode in range(Nmodes):
        print("")
        print(
            f"========================================    mode {imode}    ======================================="
        )
        print("")
        omega_n = evs[imode]
        devdpfd = (devs[imode] - evs[imode]) / dp
        phi_n = eigenvectors[:, imode]
        devectdpfd = (deigenvectors[:, imode] - eigenvectors[:, imode]) / dp
        if param == "mass":
            devdp = simu.sensitivity_mass(omega_n, phi_n, ires)
            # devectdp = simu.sensitivity_eigenvector_mass(omega_n, phi_n, ires)
        elif param == "stiffness":
            devdp = simu.sensitivity_stiffness(omega_n, phi_n, ires)
            # devectdp = 0 * devectdpfd
        else:
            M = simu.build_matrix(omega_n)
            dM = dsimu.build_matrix(omega_n)
            dMdpfd = (dM - M) / dp
            devdp = simu.sensitivity_position(omega_n, phi_n, ires, ipos)
            # devectdp = 0 * devectdpfd
        error = bk.abs(devdp - devdpfd)
        error_rel = error / bk.abs(devdpfd) * 100
        print(
            f"                       FD                          analytical               error   (rel.)"
        )
        print(
            "---------------------------------------------------------------------------------------------"
        )
        print(
            f"Eigenvalue   {devdpfd:.6e}   {devdp:.6e}     {error:.2e} ({error_rel:.4f}%)"
        )
        # print(f"Eigenvector")
        # for icomp in range(Nres):
        #     print(f"             {devectdpfd[icomp]:.6e}   {devectdp[icomp]:.6e} ")

        # print(simu.res_array[ires])
        # print(dsimu.res_array[ires])
        assert error_rel < 1e-1
    return evs, eigenvectors


def test_sensitivities():
    eig = None
    for ires in range(Nres):
        for param in ["mass", "stiffness", "pos"]:
            for ipos in [0, 1]:
                print("")
                print(
                    "#############################################################################################"
                )
                title = f"resonator {ires}, {param}"
                if param == "pos":
                    title += f" {ipos}"
                length = len(title)
                npad = 30 - length
                ipad = 1 if npad % 2 != 0 else 0
                npad = npad // 2
                title = " " * npad + f"{title}" + " " * (npad + ipad)
                print(
                    f"###############################{title}################################"
                )
                print(
                    "#############################################################################################"
                )
                print("")

                eig = check(ipos, ires, param, eig)

                if param != "pos":
                    break


if __name__ == "__main__":
    test_sensitivities()
