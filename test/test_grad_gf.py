#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: GPLv3

from klove.special import *


def test_grad_gf():
    k = bk.array(2.5 - 1.1j)

    r = 1.1

    x = 1.4
    y = 1.1

    dk = 1e-3
    rtol = 1e-3

    fd = (g0(k + dk, r) - g0(k, r)) / dk
    test = dg0_dk(k, r)

    print(fd)
    print(test)
    assert bk.allclose(fd, test, rtol=rtol)

    fd = (g1(k + dk, r) - g1(k, r)) / dk
    test = dg1_dk(k, r)
    print(fd)
    print(test)
    assert bk.allclose(fd, test, rtol=rtol)

    fd = (g2(k + dk, r) - g2(k, r)) / dk
    test = dg2_dk(k, r)
    print(fd)
    print(test)
    assert bk.allclose(fd, test, rtol=rtol)

    fd = (g3(k + dk, r) - g3(k, r)) / dk
    test = dg3_dk(k, r)
    print(fd)
    print(test)
    assert bk.allclose(fd, test, rtol=rtol)

    fd = (gradg(k + dk, x, y) - gradg(k, x, y)) / dk
    test = dgradg_dk(k, x, y)
    print(fd)
    print(test)
    assert bk.allclose(fd, test, rtol=rtol)

    fd = (lapg(k + dk, x, y) - lapg(k, x, y)) / dk
    test = dlapg_dk(k, x, y)
    print(fd)
    print(test)
    assert bk.allclose(fd, test, rtol=rtol)

    fd = (gradlapg(k + dk, x, y) - gradlapg(k, x, y)) / dk
    test = dgradlapg_dk(k, x, y)
    print(fd)
    print(test)
    assert bk.allclose(fd, test, rtol=rtol)
