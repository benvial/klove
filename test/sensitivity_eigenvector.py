#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


import numpy as npo

import klove as kl

kl.set_backend("torch")


bk = kl.backend
pi = bk.pi
a = 1
Nres = 4


ieig = 1


def build_mat(p):
    p = bk.array(p, dtype=bk.complex128)
    M = bk.stack([bk.stack([p + p**3 * 1j, p**2]), bk.stack([p**2, p**4])])
    return M


def build_dmat(p):
    p = bk.array(p, dtype=bk.complex128)
    dM = bk.stack(
        [bk.stack([p * 0 + 1 + 3 * p**2 * 1j, 2 * p]), bk.stack([2 * p, 4 * p**3])]
    )
    return dM


def fun_ev(p):
    M = build_mat(p)
    evs, vects = bk.linalg.eig(M)
    return evs[ieig]


def fun_evect(p):
    M = build_mat(p)
    evs, vects = bk.linalg.eig(M)
    return bk.abs(vects[:, ieig]) ** 2


p = 0.35
dp = 1e-5


A = fun_ev(p)
dA = fun_ev(p + dp)
fd_ev = (dA - A) / dp

A = fun_evect(p)
dA = fun_evect(p + dp)
fd_evect = (dA - A) / dp


def dev(p):
    dfun_ = []
    fun_scalar_re = lambda p: fun_ev(p).real
    fun_scalar_im = lambda p: fun_ev(p).imag
    return kl.grad(fun_scalar_re)(p) + 1j * kl.grad(fun_scalar_im)(p)


def devect(p):
    dfun_ = []
    for i in range(2):
        fun_scalar_re = lambda p: fun_evect(p)[i].real
        # fun_scalar_im = lambda p: fun_evect(p)[i].imag
        dfun_.append(kl.grad(fun_scalar_re)(p))  # + 1j * kl.grad(fun_scalar_im)(p))
    return bk.stack(dfun_)


# dev = kl.grad(fun_ev)

print(fd_ev)
print(dev(p))


def dev_ana(p):
    A = build_mat(p)
    dAdp = build_dmat(p)
    evs, vects = bk.linalg.eig(A)
    phi = vects[:, ieig]
    phi /= (-phi.T @ phi) ** 0.5
    out = -phi.T @ (dAdp @ phi)
    return out
    # M -= evs[ieig] * bk.eye(2)
    # dMdev = -bk.eye(2)


def devect_ana(p):
    A = build_mat(p)
    dAdp = build_dmat(p)
    evs, vects = bk.linalg.eig(A)
    sens = dev(p)
    phi_n = vects[:, ieig]
    phi_n /= (-phi_n.T @ phi_n) ** 0.5
    dMdomega = -bk.eye(2)
    dMdp = dAdp - sens * bk.eye(2)
    M = A - evs[ieig] * bk.eye(2)
    rhs = -(dMdp + sens * dMdomega) @ phi_n
    dphidp = bk.linalg.solve(M, rhs)
    out = dphidp
    out = 2 * bk.real(dphidp @ phi_n.conj())
    return out


print(dev_ana(p))


print(fd_evect)
print(devect(p))
print(devect_ana(p))
xsxs


def check(ipos, ires, param):
    ######################################################################
    # Elastic plate

    plate = kl.ElasticPlate(a * 1, 1, 1, 0.0)
    p = 256 * plate.rho * plate.h * plate.D

    omegap = plate.omega0(a)
    m_plate = plate.rho * a**2 * plate.h

    alternative = True
    omega0 = (0.1 - 0.8 * 1j) * omegap
    omega1 = (2 + 1e-3 * 1j) * omegap

    recursive = False
    peak_ref = 3
    N_guess_loc = 0
    Rloc = 0.01
    refine = True
    lambda_tol = 1e-12
    max_iter = 100
    plot_solver = False

    ######################################
    # Cluster

    omegar0 = omegap * 1

    m0 = m_plate * 1
    k0 = omegar0**2 * m0

    npo.random.seed(1984)

    #### line array
    positions = bk.array(npo.random.rand(Nres, 2) * a)
    stiffnesses = bk.array(npo.random.rand(Nres) * k0)
    masses = bk.array(npo.random.rand(Nres) * m0)
    res_array = []
    for i in range(Nres):
        res_array.append(kl.Resonator(masses[i], stiffnesses[i], positions[i]))

    ######################################################################
    # Define the simulation object
    simu = kl.ScatteringSimulation(plate, res_array, alternative=alternative)

    delta = 1e-5
    rtol = delta * 10

    dpositions = positions  # .copy()
    dmasses = masses  # .copy()
    dstiffnesses = stiffnesses  # .copy()

    if param == "mass":
        dp = m0 * delta
        dmasses[ires] += dp
    elif param == "stiffness":
        dp = k0 * delta
        dstiffnesses[ires] += dp
    else:
        dp = a * delta
        dpositions[ires][ipos] += dp

    dres_array = []
    for i in range(Nres):
        dres_array.append(kl.Resonator(dmasses[i], dstiffnesses[i], dpositions[i]))

    # dres_array[ires].position[0] = positions[ires][0]
    dsimu = kl.ScatteringSimulation(plate, dres_array, alternative=alternative)

    # assert bk.allclose(dres_array[ires].position[0] - res_array[ires].position[0], dp)

    evs, eigenvectors = simu.eigensolve(
        omega0,
        omega1,
        recursive=recursive,
        lambda_tol=lambda_tol,
        max_iter=max_iter,
        N_guess_loc=N_guess_loc,
        Rloc=Rloc,
        peak_ref=peak_ref,
        plot_solver=plot_solver,
        scale=omegap,
    )

    eigenvectors = simu.normalize_eigenvectors(evs, eigenvectors)

    devs, deigenvectors = dsimu.eigensolve(
        omega0,
        omega1,
        recursive=recursive,
        lambda_tol=lambda_tol,
        max_iter=max_iter,
        N_guess_loc=N_guess_loc,
        Rloc=Rloc,
        peak_ref=peak_ref,
        plot_solver=plot_solver,
        scale=omegap,
    )
    deigenvectors = dsimu.normalize_eigenvectors(devs, deigenvectors)

    ###############################
    for imode in range(len(evs)):
        print("")
        print(
            f"=============================    mode {imode}    ============================="
        )
        print("")
        omega_n = evs[imode]
        devdpfd = (devs[imode] - evs[imode]) / dp
        phi_n = eigenvectors[:, imode]
        devectdpfd = (deigenvectors[:, imode] - eigenvectors[:, imode]) / dp
        if param == "mass":
            devdp = simu.sensitivity_mass(omega_n, phi_n, ires)
            devectdp = simu.sensitivity_eigenvector_mass(omega_n, phi_n, ires)
        elif param == "stiffness":
            devdp = simu.sensitivity_stiffness(omega_n, phi_n, ires)
            devectdp = 0 * devectdpfd
        else:
            M = simu.build_matrix(omega_n)
            dM = dsimu.build_matrix(omega_n)
            dMdpfd = (dM - M) / dp
            devdp = simu.sensitivity_position(omega_n, phi_n, ires, ipos)
            devectdp = 0 * devectdpfd
        # print("Eigenvalues")
        # print("-----------")
        # print(f"FD:         {devdpfd}")
        # print(f"analytical: {devdp}")
        # print("Eigenvectors")
        # print("------------")
        # print(f"FD:         {devectdpfd}")
        # print(f"analytical: {devectdp}")
        # assert bk.allclose(devdp, devdpfd, rtol=rtol)

        print(f"                       FD                          analytical")
        print(
            "--------------------------------------------------------------------------"
        )
        print(f"Eigenvalue   {devdpfd:.6e}   {devdp:.6e} ")
        print(f"Eigenvector")
        for icomp in range(Nres):
            print(f"             {devectdpfd[icomp]:.6e}   {devectdp[icomp]:.6e} ")


check(0, 1, "mass")


def test_sensitivities():
    for ires in range(Nres):
        for param in ["mass", "stiffness", "pos"]:
            print("")
            print(
                "########################################################################"
            )
            print(
                f"########################    {param} resonator {ires}    #########################"
            )
            print(
                "########################################################################"
            )
            print("")
            check(0, ires, param)
            if param == "pos":
                check(1, ires, param)


# test_sensitivities()
