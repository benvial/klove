#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


import pytest


def test_far_field():
    from math import pi

    import klove as kl

    bk = kl.backend

    plate = kl.ElasticPlate(h=1, rho=1, E=1, nu=0.21)

    omega0 = 2e3
    a = 1
    R = 1
    Nres = 6

    m0 = plate.rho * a**2 * plate.h
    ms = bk.ones(Nres) * m0
    alpha = 2
    t0 = -69.81 * bk.ones(Nres) * plate.bending_stiffness
    resarray = []
    for alpha in range(Nres):
        q = bk.array(2 * pi * alpha / Nres)
        xalpha = R * bk.cos(q)
        yalpha = R * bk.sin(q)
        resarray.append(kl.ConstantStrength(t0[alpha], (xalpha, yalpha)))

    sim = kl.ScatteringSimulation(plate, resarray, alternative=True)

    Nomegas = 5
    Ntheta = 360
    omegas = bk.linspace(0.1, 8, Nomegas)
    angle = 0
    angles = bk.linspace(0, 2 * pi, Ntheta)
    phi_n = sim.solve(sim.plane_wave, omegas, angle=angle)
    far_field = sim.get_far_field_radiation(phi_n, omegas, angles)
    sigma_sc = sim.get_scattering_cross_section(phi_n, omegas)
    sigma_se = sim.get_extinction_cross_section(phi_n, omegas, angle)
    assert bk.allclose(sigma_sc, sigma_se)


@pytest.mark.parametrize("alternative", [True, False])
def test_optical_theorem(alternative):
    from math import pi

    import klove as kl

    bk = kl.backend

    plate = kl.ElasticPlate(h=1, rho=1, E=1, nu=0)

    a = 1
    R = 1
    Nres = 2

    m0 = plate.rho * a**2 * plate.h
    ms = bk.ones(Nres) * m0
    resarray = []
    for alpha in range(Nres):
        q = bk.array(2 * pi * alpha / Nres)
        xalpha = R * bk.cos(q)
        yalpha = R * bk.sin(q)
        resarray.append(kl.Resonator(1, 1, (xalpha, yalpha), damping=0.1))

    sim = kl.ScatteringSimulation(plate, resarray, alternative=alternative)

    Nomegas = 100
    omegas = bk.linspace(1, 8, Nomegas)
    Ntheta = 360
    angles = bk.linspace(0, 2 * pi, Ntheta)
    angle = 0
    phi_n = sim.solve(sim.plane_wave, omegas, angle=angle)
    far_field = sim.get_far_field_radiation(phi_n, omegas, angles)
    sigma_sc = sim.get_scattering_cross_section(phi_n, omegas)
    sigma_se = sim.get_extinction_cross_section(phi_n, omegas, angle)
    sigma_sa = sim.get_absorption_cross_section(phi_n, omegas) * plate.D
    assert bk.allclose(sigma_sc + sigma_sa, sigma_se)
