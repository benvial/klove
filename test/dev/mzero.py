#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


"""
Algorithm to find zeros and poles in the complex plane based on [Haotian2002]_

.. [Haotian2002] Haotian Chen, On locating the zeros and poles of a meromorphic function, 
    Journal of Computational and Applied Mathematics, Volume 402, 113796 (2022).
"""


import matplotlib.pyplot as plt
import numpy as np
import scipy.linalg as sla

plt.ion()
plt.close("all")
np.random.seed(22)

pi = np.pi

# pol0 = 0.4 + 0.2 * 1j
# zer0 = -0.02 - 0.06 * 1j

# # Definition of the function
# func = lambda z: (z - zer0) / (z - pol0)
# dfunc = lambda z: -pol0 * zer0 / (z - pol0) ** 2
Npoles = 1
Nzeros = 2
mpoles = np.random.randint(1, 2, Npoles)
mzeros = np.random.randint(1, 2, Nzeros)
zeros = np.random.rand(Nzeros) + 1j * np.random.rand(Nzeros)
poles = np.random.rand(Npoles) + 1j * np.random.rand(Npoles)
zeros = 2 * zeros - 1 - 1j
poles = 2 * poles - 1 - 1j
Ntot = Npoles + Nzeros
Ntot = sum(mpoles) + sum(mzeros)
Ntot = 5


def func(z):
    out = np.ones_like(z)
    for i in range(Nzeros):
        out *= (z - zeros[i]) ** mzeros[i]
    for i in range(Npoles):
        out /= (z - poles[i]) ** mpoles[i]
    return out


dfunc = None

if dfunc == None:
    dfunc = lambda z: np.gradient(func(z)) / np.gradient(z)


def probe(s, k, gamma=0, dgamma=1):
    return s ** (gamma + k * dgamma)


def transformation(z, epsilon0, z0, L, alpha):
    return np.exp(-1j / L * (2 * np.pi - epsilon0) * (z * np.exp(-1j * alpha) - z0))


def inverse_transformation(s, epsilon0, z0, L, alpha):
    return (z0 + 1j * L / (2 * np.pi - epsilon0) * np.log(s)) * np.exp(1j * alpha)


def parametrization_line(z0, z1, loc):
    x0, x1, y0, y1 = z0.real, z1.real, z0.imag, z1.imag
    if loc == 0:
        a, b = 0, pi / 2
        A, B = 2 / pi * (x1 - x0), z0
    elif loc == 1:
        a, b = pi / 2, pi
        A, B = 1j * 2 / pi * (y1 - y0), x1 + 1j * (2 * y0 - y1)
    elif loc == 2:
        a, b = pi, 3 * pi / 2
        A, B = -2 / pi * (x1 - x0), 3 * x1 - 2 * x0 + 1j * y1
    elif loc == 3:
        a, b = 3 * pi / 2, 2 * pi
        A, B = -1j * 2 / pi * (y1 - y0), x0 + 1j * (4 * y1 - 3 * y0)
    else:
        raise TypeError("loc must be an integer betwwen 0 and 3")
    return a, b, A, B


def poly_period(x, a, b):
    P = (
        (a - x) ** 3
        * (a**2 - 5 * a * b + 3 * a * x + 10 * b**2 - 15 * b * x + 6 * x**2)
    ) / (a - b) ** 5
    dP = -(30 * (a - x) ** 2 * (b - x) ** 2) / (a - b) ** 5
    return P, dP


def get_integral_params(a, b, A, B, x):
    P, dP = poly_period(x, a, b)
    Pt, dPt = (b - a) * P + a, (b - a) * dP
    zper = A * Pt + B

    def Fper(fper):
        return A * fper * dPt

    return zper, Fper


def integrate(f, z):
    return np.trapz(f, z)


z0 = -1 + -1 * 1j
z1 = 1 + 1 * 1j
Npts = 100 * 1
zpers = []
integs = []
for loc in range(4):
    a, b, A, B = parametrization_line(z0, z1, loc)
    x = np.linspace(a, b, Npts)
    zper, Fper = get_integral_params(a, b, A, B, x)
    fper = func(zper)
    dfper = dfunc(zper)
    integ = Fper(dfper / fper)
    zpers.append(zper)
    integs.append(integ)

integs = np.hstack(integs)
zpers = np.hstack(zpers)

import sys

N0 = Ntot
N0 = int(sys.argv[1])
N = N0


G = [integrate(integs * zpers**n, zpers) / (2 * 1j * pi) for n in range(0, 2 * N)]

# H0 = np.zeros((N, N), dtype=complex)
# for i in range(0, N):
#     for j in range(0, N):
#         H0[i, j] = G[i + j]
# H1 = np.zeros((N, N), dtype=complex)
# for i in range(0, N):
#     for j in range(0, N):
#         H1[i, j] = G[i + j + 1]


H0 = sla.hankel(G[0:N], G[N - 1 : 2 * N - 1])
H1 = sla.hankel(G[1 : N + 1], G[N : 2 * N])


rank = np.linalg.matrix_rank(H0, tol=None, hermitian=False)
tolG0 = 1e-3
if abs(G[0] - np.round(G[0])) > tolG0:
    print(f"The first moment is not an integer (G0={G[0]}, tol={tolG0}).")

print(G[0])
print(rank)

eigvals, modes = sla.eig(H1, H0)


# Compute the multiplicities by solving the Vandermonde system
vm = np.vander(eigvals, increasing=True)
mult = sla.solve(vm.T, G[:N])
print(mult)

ind = mult.real > 0
poles_found = eigvals[~ind]
zeros_found = eigvals[ind]


def error_estimate(eigvals):
    N = len(eigvals)
    theta = np.angle(eigvals)
    rplus = np.max(np.abs(eigvals))
    rminus = np.min(np.abs(eigvals))
    prod = 1
    for i in range(N):
        for j in range(N):
            if i != j:
                prod /= np.abs(np.exp(1j * theta[i]) - np.exp(1j * theta[j]))
    return N**2 * (rplus / rminus) ** 2 * (N - 1) * (1 + rplus) ** 2 * (N - 1) * prod**2


# print(G)

kappa = error_estimate(eigvals)
print(kappa)


Nre, Nim = 100, 100


omegas_re = np.linspace(z0.real, z1.real, Nre)
omegas_im = np.linspace(z0.imag, z1.imag, Nim)
omegas_re_, omegas_im_ = np.meshgrid(omegas_re, omegas_im, indexing="ij")


omegas_complex = omegas_re_ + 1j * omegas_im_
fplot = func(omegas_complex)

tol = 1e-4
pz = []
mu = []
for i, m in enumerate(mult):
    ind = np.abs(m) > tol
    # dist_mult_to_int = np.abs(m[ind] - np.round(m[ind].real))
    pz.append(eigvals[i][ind])
    mu.append(m[ind])

plt.close("all")


plt.figure()
plt.pcolormesh(omegas_re, omegas_im, np.log10(np.abs(fplot.T)))
plt.colorbar()
plt.xlabel("Re $\omega$")
plt.ylabel("Im $\omega$")
plt.title(r"${\rm log}_{10} |f|$")

plt.plot(eigvals.real, eigvals.imag, "+r")

# ax_det = plt.gca()

# # xssx

# # Nroots the number of zeros and poles

# Nroots = 3

# from tetrachotomy.tetrachotomy import romberg


# class Rectangle:
#     def __init__(self, z0, z1):
#         self.z0 = z0
#         self.z1 = z1
