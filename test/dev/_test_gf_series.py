#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


import matplotlib.pyplot as plt
import numpy as np

from klove.special import *

plt.ion()
plt.close("all")
pi = np.pi

x = np.linspace(0, 15, 100) + 1j

F = bessel_j0(x) + 1j * bessel_y0(x) + 1j * 2 / pi * bessel_k0(x)


plt.plot(x.real, F.real)

plt.plot(x.real, F.imag, "--")

# plt.plot(x.real, bessel_j0(x)-bessel_y0(x).imag-2 / pi * bessel_k0(x).imag, ":")
