#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove

import sys

sys.path.append("examples")


def test_porter():
    import plot_porter


def test_chaplain():
    import plot_chaplain


def test_xiao():
    import plot_xiao


def test_torrent():
    import plot_torrent
