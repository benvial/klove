#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: Benjamin Vial, Marc Martí-Sabaté
# This file is part of klove
# License: GPLv3
# See the documentation at benvial.gitlab.io/klove


import logging
import uuid

import numpy as np
import scipy.linalg as la
import scipy.special


class Identified(object):
    """An object which can be uniquely identified by an id number. It is
    assumed that any object which subclasses Identified is immutable, so that
    its id can be used for caching complex results which depend on this object.
    """

    def __init__(self):
        self.id = uuid.uuid4()

    def __hash__(self):
        return self.id.__hash__()

    def __eq__(self, other):
        return hasattr(other, "id") and (self.id == other.id)

    def __repr__(self):
        "Represent the object by its id, in addition to its memory address"
        return "<%s at 0x%08x with id %s>" % (
            str(self.__class__)[8:-2],
            id(self),
            str(self.id),
        )


class IntegrationRule(Identified):
    def __len__(self):
        return len(self.points)

    def __repr__(self):
        return "%s.%s(%d)" % (type(self).__module__, type(self).__name__, self.order)

    def __iter__(self):
        "Iterate over all integration points and weights"
        for point, w in zip(self.points, self.weights):
            yield point, w


class GaussLegendreRule(IntegrationRule):
    """1D Gauss Legendre Quadrature Rule

    Defined over the range (0, 1)
    """

    def __init__(self, order):
        "Weights and abscissae of Gauss-Legendre quadrature of order N"
        super(GaussLegendreRule, self).__init__()
        a = scipy.special.sh_legendre(order).weights

        self.weights = a[:, 1].real
        self.points = a[:, 0].real


class Contour(object):
    """A contour for line integration in the complex plane"""

    def points_inside(self, points):
        "Check each point to see whether it lies within the contour"
        vertices = np.array([x for x, w in self])
        vertices = np.hstack((vertices.real[:, None], vertices.imag[:, None]))
        inside = np.empty(np.product(points.shape), dtype=bool)

        for point_num, point in enumerate(points.flat):
            inside[point_num] = wn_PnPoly((point.real, point.imag), vertices)

        return inside


class RectangularContour(Contour):
    """A rectangular contour in the complex frequency plane"""

    def __init__(self, s_min, s_max, integration_rule=GaussLegendreRule(20)):
        """
        Parameters
        ----------
        s_min, s_max: complex
            The corners of the rectangle
        """
        min_real, max_real = sorted((s_min.real, s_max.real))
        min_imag, max_imag = sorted((s_min.imag, s_max.imag))

        self.integration_rule = integration_rule
        self.coordinates = (
            min_real + 1j * min_imag,
            max_real + 1j * min_imag,
            max_real + 1j * max_imag,
            min_real + 1j * max_imag,
        )

    def __iter__(self):
        """
        Returns
        -------
        gen: generator
            A generator which yields (s, w), where s is the complex frequency
            and w is the integration weight
        """
        # integrate over all 4 lines
        for line_count in range(4):
            s_start = self.coordinates[line_count]
            s_end = self.coordinates[(line_count + 1) % 4]

            ds = s_end - s_start
            for x, w in self.integration_rule:
                s = s_start + ds * x
                yield (s, w * ds)

    def __len__(self):
        return 4 * len(self.integration_rule)


class ConvergenceError(Exception):
    pass


# Copyright 2001, softSurfer (www.softsurfer.com)
# This code may be freely used and modified for any purpose
# providing that this copyright notice is included with it.
# SoftSurfer makes no warranty for this code, and cannot be held
# liable for any real or imagined damage resulting from its use.
# Users of this code must verify correctness for their application.

# translated to Python by Maciej Kalisiak <mac@dgp.toronto.edu>

# http://www.dgp.toronto.edu/~mac/e-stuff/

#   a Point is represented as a tuple: (x,y)

# ===================================================================

# is_left(): tests if a point is Left|On|Right of an infinite line.

#   Input: three points P0, P1, and P2
#   Return: >0 for P2 left of the line through P0 and P1
#           =0 for P2 on the line
#           <0 for P2 right of the line
#   See: the January 2001 Algorithm "Area of 2D and 3D Triangles and Polygons"


def is_left(P0, P1, P2):
    return (P1[0] - P0[0]) * (P2[1] - P0[1]) - (P2[0] - P0[0]) * (P1[1] - P0[1])


# ===================================================================

# cn_PnPoly(): crossing number test for a point in a polygon
#     Input:  P = a point,
#             V[] = vertex points of a polygon
#     Return: 0 = outside, 1 = inside
# This code is patterned after [Franklin, 2000]


def cn_PnPoly(P, V):
    cn = 0  # the crossing number counter

    # repeat the first vertex at end
    V = tuple(V[:]) + (V[0],)

    # loop through all edges of the polygon
    for i in range(len(V) - 1):  # edge from V[i] to V[i+1]
        if (V[i][1] <= P[1] and V[i + 1][1] > P[1]) or (  # an upward crossing
            V[i][1] > P[1] and V[i + 1][1] <= P[1]
        ):  # a downward crossing
            # compute the actual edge-ray intersect x-coordinate
            vt = (P[1] - V[i][1]) / float(V[i + 1][1] - V[i][1])
            if P[0] < V[i][0] + vt * (V[i + 1][0] - V[i][0]):  # P[0] < intersect
                cn += 1  # a valid crossing of y=P[1] right of P[0]

    return cn % 2  # 0 if even (out), and 1 if odd (in)


# ===================================================================

# wn_PnPoly(): winding number test for a point in a polygon
#     Input:  P = a point,
#             V[] = vertex points of a polygon
#     Return: wn = the winding number (=0 only if P is outside V[])


def wn_PnPoly(P, V):
    wn = 0  # the winding number counter

    # repeat the first vertex at end
    V = tuple(V[:]) + (V[0],)

    # loop through all edges of the polygon
    for i in range(len(V) - 1):  # edge from V[i] to V[i+1]
        if V[i][1] <= P[1]:  # start y <= P[1]
            if V[i + 1][1] > P[1]:  # an upward crossing
                if is_left(V[i], V[i + 1], P) > 0:  # P left of edge
                    wn += 1  # have a valid up intersect
        else:  # start y > P[1] (no test needed)
            if V[i + 1][1] <= P[1]:  # a downward crossing
                if is_left(V[i], V[i + 1], P) < 0:  # P right of edge
                    wn -= 1  # have a valid down intersect
    return wn


def eig_newton(
    func,
    lambda_0,
    x_0,
    lambda_tol=1e-8,
    max_iter=20,
    func_gives_der=False,
    G=None,
    args=[],
    weight="rayleigh symmetric",
    y_0=None,
):
    """Solve a nonlinear eigenvalue problem by Newton iteration

    Parameters
    ----------
    func : function
        The function with input `lambda` which returns the matrix
    lambda_0 : complex
        The starting guess for the eigenvalue
    x_0 : ndarray
        The starting guess for the eigenvector
    lambda_tol : float
        The relative tolerance in the eigenvalue for convergence
    max_iter : int
        The maximum number of iterations to perform
    func_gives_der : boolean, optional
        If `True`, then the function also returns the derivative as the second
        returned value. If `False` finite differences will be used instead,
        which will have reduced accuracy
    args : list, optional
        Any additional arguments to be supplied to `func`
    weight : string, optional
        How to perform the weighting of the eigenvector

        'max element' : The element with largest magnitude will be preserved

        'rayleigh' : Rayleigh iteration for Hermition matrices will be used

        'rayleigh symmetric' : Rayleigh iteration for complex symmetric
        (i.e. non-Hermitian) matrices will be used

        'rayleigh asymmetric' : Rayleigh iteration for general matrices

    y_0 : ndarray, optional
        For 'rayleigh asymmetric weighting', this is required as the initial
        guess for the left eigenvector

    Returns
    -------
    res : dictionary
        A dictionary containing the following members:

        `eigval` : the eigenvalue

        'eigvec' : the eigenvector

        'iter_count' : the number of iterations performed

        'delta_lambda' : the change in the eigenvalue on the final iteration


    See:
    1.  P. Lancaster, Lambda Matrices and Vibrating Systems.
        Oxford: Pergamon, 1966.

    2.  A. Ruhe, “Algorithms for the Nonlinear Eigenvalue Problem,”
        SIAM J. Numer. Anal., vol. 10, no. 4, pp. 674–689, Sep. 1973.

    """

    x_s = x_0
    lambda_s = lambda_0

    if weight.lower() == "rayleigh asymmetric":
        if y_0 is None:
            raise ValueError("Parameter y_0 must be supplied for asymmetric " "case")
        y_s = y_0

    logging.debug("Searching for zeros with eig_newton")
    logging.debug("Starting guess %+.4e %+.4ej" % (lambda_0.real, lambda_0.imag))

    converged = False

    if not func_gives_der:
        # evaluate at an arbitrary nearby starting point to allow finite
        # differences to be taken
        lambda_sm = lambda_0 * (1 + 10j * lambda_tol)
        T_sm = func(lambda_sm, *args)

    for iter_count in range(max_iter):
        if func_gives_der:
            T_s, T_ds = func(lambda_s, *args)
        else:
            T_s = func(lambda_s, *args)
            T_ds = (T_s - T_sm) / (lambda_s - lambda_sm)

        T_s_lu = la.lu_factor(T_s)
        u = la.lu_solve(T_s_lu, np.dot(T_ds, x_s))

        # if known_vects is supplied, we should take this into account when
        # finding v
        if weight.lower() == "max element":
            v_s = np.zeros_like(x_s)
            v_s[np.argmax(abs(x_s))] = 1.0
        elif weight.lower() == "rayleigh":
            v_s = np.dot(T_s.T, x_s.conj())
        elif weight.lower() == "rayleigh symmetric":
            v_s = np.dot(T_s.T, x_s)
        elif weight.lower() == "rayleigh asymmetric":
            y_s = la.lu_solve(T_s_lu, np.dot(T_ds.T, y_s), trans=1)
            y_s /= np.sqrt(np.sum(np.abs(y_s) ** 2))
            v_s = np.dot(T_s.T, y_s)
        else:
            raise ValueError("Unknown weighting method %s" % weight)

        delta_lambda_abs = np.dot(v_s, x_s) / (np.dot(v_s, u))

        delta_lambda = abs(delta_lambda_abs / lambda_s)
        converged = delta_lambda < lambda_tol
        if converged:
            break

        lambda_s1 = lambda_s - delta_lambda_abs
        x_s1 = u / np.sqrt(np.sum(np.abs(u) ** 2))

        # update variables for next iteration
        if not func_gives_der:
            lambda_sm = lambda_s
            T_sm = T_s

        lambda_s = lambda_s1
        x_s = x_s1
        logging.debug("%+.4e %+.4ej" % (lambda_s.real, lambda_s.imag))

    if not converged:
        raise ConvergenceError("maximum iterations reached, no convergence")

    res = {
        "eigval": lambda_s,
        "iter_count": iter_count + 1,
        "delta_lambda": delta_lambda,
    }

    if weight.lower() == "rayleigh asymmetric":
        # Scale both the left and right eigenvector identically first
        y_s /= np.sqrt(np.vdot(y_s, y_s) / np.vdot(x_s, x_s))

        # Then scale both to match the eigenvalue derivative
        dz_ds = np.dot(y_s, np.dot(T_ds, x_s))
        y_s /= np.sqrt(dz_ds)
        res["eigvec_left"] = y_s

        x_s /= np.sqrt(dz_ds)
        res["eigvec"] = x_s

    else:
        # scale the eigenvector so that the eigenvalue derivative is 1
        dz_ds = np.dot(x_s, np.dot(T_ds, x_s))
        x_s /= np.sqrt(dz_ds)
        res["eigvec"] = x_s
        res["eigvec_left"] = x_s

    return res


def poles_cauchy(
    Z_func, contour, svd_threshold=1e-10, previous_result=None, iter_wrap=lambda x: x
):
    """Estimate location and residue of the poles of a matrix function by
    Cauchy integration. Uses a technique described in:

    D. A. Bykov and L. L. Doskolovich, "Numerical Methods for Calculating
    Poles of the Scattering Matrix With Applications in Grating Theory,"
    Journal of Lightwave Technology, vol. 31, no. 5, pp. 793-801, Mar. 2013.

    Parameters
    ----------
    Z : Matrix function
        The impedance matrix as a function of frequency s
    contour: Contour
        The object describing the contour integration path and rule
    svd_threshold : float, optional
        The threshold on singular values to determine the rank of the matrix
    previous_result: dictionary, optional
        By passing a dictionary previously returned by poles_cauchy, it is
        possible to refine the svd_threshold without having to repeat the
        contour integration

    Returns
    -------
    result: dictionary
        Contains several elements. Relevant ones to the user are
        's': The complex frequencies
        'vl': The left eigenvectors
        'vr': The right eigenvectors
        's_out', 'vl_out', 'vr_out' : corresponding quantities for solutions
        outside the integration region, which may not be meaningful
        'C1_s' : The singular values of C1
    """

    if previous_result is not None:
        result = previous_result
    else:
        logging.info("Performing full cauchy integration")

        # integrate over the entire contour
        for s, w in iter_wrap(contour):
            Z_inv = la.inv(Z_func(s)[:], overwrite_a=True) * w
            # This trick avoids having to know the size of C1 and C2 in
            # advance
            try:
                C1 += Z_inv
                C2 += s * Z_inv
            except UnboundLocalError:
                C1 = Z_inv
                C2 = s * Z_inv

        C1_U, C1_S, C1_Vh = la.svd(C1)
        result = {"C2": C2, "C1_U": C1_U, "C1_S": C1_S, "C1_Vh": C1_Vh}

    # Determine the rank of the SVD matrix for the given threshold
    sv = result["C1_S"]

    C1_rank = np.sum(sv > svd_threshold * sv[0])
    logging.info(
        "Rank of integrated matrix %d with threshold %e" % (C1_rank, svd_threshold)
    )

    # construct a reduced rank approximation
    U_r = result["C1_U"][:, :C1_rank]
    Uh_r = U_r.T.conjugate()
    Vh_r = result["C1_Vh"][:C1_rank, :]
    V_r = Vh_r.T.conjugate()
    S_r = sv[:C1_rank]

    # solved the reduced eigenvalue problem
    mode_s, vl, vr = la.eig(Uh_r.dot(result["C2"].dot(V_r)), np.diag(S_r), left=True)

    in_region = contour.points_inside(mode_s)
    outside_region = np.logical_not(in_region)

    # sort modes inside the region by frequency
    in_region = np.where(in_region)[0]
    in_order = np.argsort(mode_s[in_region].imag)
    in_region = in_region[in_order]
    result["s"] = mode_s[in_region]
    result["s_out"] = mode_s[outside_region]

    # Return the left and right eigenvectors in the full problem space.
    full_r = U_r.dot(np.diag(S_r).dot(vr))
    full_l = (V_r.dot(np.diag(S_r).dot(vl))).conjugate()
    # conjugate comes from scipy's vs my definition of left eigenvectors

    result["vl"] = full_l[:, in_region].T
    result["vr"] = full_r[:, in_region]
    result["vl_out"] = full_l[:, outside_region].T
    result["vr_out"] = full_r[:, outside_region]

    return result


np.random.seed(2)
dim = 2

lambda_0 = -2
x_0 = np.random.rand(dim)

M1 = np.random.rand(dim, dim) + 1j * np.random.rand(dim, dim)
M2 = np.random.rand(dim, dim) + 1j * np.random.rand(dim, dim)


bk = np
import klove as kl

Nres = dim
a = 0.5
positions = bk.random.rand(Nres, 2) * a - a / 2
plate = kl.ElasticPlate(0.03, 1, 1, 0)

omegar0 = 100 * plate.omega0(a)
m0 = plate.rho * a**2 * plate.h * 0.01
masses = 1 * bk.random.rand(Nres) * m0
stiff = m0 * omegar0**2 * bk.random.rand(Nres)

positionsx = bk.linspace(0.0, Nres * a, Nres)
positionsx -= positionsx[-1] / 2

positions = [(_x, 0) for _x in positionsx]
res_array = []
for i in range(Nres):
    res_array.append(kl.Resonator(masses[i], stiff[i], positions[i]))
simu = kl.ScatteringSimulation(plate, res_array, alternative=True)


def func1(omega):
    M = simu.build_matrix(omega)
    # det = bk.linalg.det(M)
    return 1 / M


def func(lambda_):
    return lambda_**2 * M2 + lambda_ * M1 + np.eye(dim)


# res = eig_newton(func, lambda_0, x_0, max_iter=210)
# print(res)

omega0 = -2 - 2j
omega1 = 5 + 0j


# def func1(z):
#     return func(z)


svd_threshold = 1e-10
contour = RectangularContour(omega0, omega1)
out = poles_cauchy(func1, contour, svd_threshold=svd_threshold)
print("-------")
print(out["s"])


poles = out["s"]

Nre = 150
Nim = 150

omegas_re = bk.linspace(omega0.real, omega1.real, Nre)
omegas_im = bk.linspace(omega1.imag, omega0.imag, Nim)
omegas_re_, omegas_im_ = bk.meshgrid(omegas_re, omegas_im, indexing="ij")


omegas_complex = omegas_re_ + 1j * omegas_im_

Mc = simu.build_matrix(omegas_complex)
Mc = bk.transpose(Mc, axes=(2, 3, 0, 1))
det = bk.linalg.det(Mc)
evs = bk.linalg.eigvals(Mc)
srt = bk.argsort(bk.abs(evs), axis=-1)
min_evs = bk.take_along_axis(evs, srt, axis=-1)[:, :, 0]
# evs = bk.linalg.eigvals(Mc)
# srt = bk.argsort(bk.abs(evs), axis=-1)
# min_evs = bk.take_along_axis(evs, srt, axis=-1)[:, :, 0]

# det = bk.zeros((Nre, Nim), dtype=bk.complex128)
# for i, omega_re in enumerate(omegas_re):
#     for j, omega_im in enumerate(omegas_im):
#         # print(i, j)
#         omega = omegas_complex[i, j]
#         M = func1(omega)
#         det[i, j] = 1 / bk.linalg.det(M)


import matplotlib.pyplot as plt

plt.ion()
plt.close("all")

plt.figure()
plt.pcolormesh(omegas_re, omegas_im, bk.log10(bk.abs(det.T)))
plt.colorbar()
plt.xlabel("Re $\omega$")
plt.ylabel("Im $\omega$")
plt.title(r"${\rm log}_{10} |F_0|$")
ax_det = plt.gca()


plt.figure()
plt.pcolormesh(omegas_re, omegas_im, bk.log10(bk.abs(min_evs.T)))
plt.colorbar()
plt.xlabel("Re $\omega$")
plt.ylabel("Im $\omega$")
plt.title(r"${\rm log}_{10} |{\rm min}\, \omega_n|$")
ax_min_ev = plt.gca()


plt.plot(poles.real, poles.imag, "or")
xsxs
for i in range(5):
    svd_threshold /= 10
    out = poles_cauchy(func1, contour, svd_threshold=svd_threshold, previous_result=out)
    print("-------")
    print(out["s"])
    poles = out["s"]
    plt.plot(poles.real, poles.imag, "or")
    plt.pause(0.1)
